<?php
/**
 * eZExceed device operators
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

use \ezexceed\models\device\UserDevice;
use \eZUser;

/**
 * List devices
 */
class Device
{
    /**
     * Return an array with the template operatos(s).
     * @return array
     */
    public static function operatorList()
    {
        return array('devices');
    }

    /**
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue)
    {
        $http = \eZHTTPTool::instance();
        $userId = $http->sessionVariable('eZUserLoggedInID');
        switch ($operatorName)
        {
            case 'devices':
                $devices = UserDevice::fetchByUserId($userId);
                if ($devices) {
                    $devs = array();
                    foreach ($devices as $d) {
                        $devs[] = array(
                            'name' => $d->name,
                            'type' => $d->type,
                            'id' => $d->uniquekey,
                            'os' => $d->os,
                            'osVersion' => $d->os_version
                        );
                    }
                    $operatorValue = json_encode($devs);
                }
                break;
        }
    }
}
