<?php
/**
 * Retrieves the rootNode for the sitemap
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

use \ezexceed\classes\utilities\IniUtilities;
use ezexceed\models\nodestructure\NodeOperations;

/**
 * Retrieves the rootNode for the sitemap.
 */
class SiteMapRootNode
{
    const OPERATOR_NAME = 'ezexceedsitemaprootnode';

    /**
     *
     * Return an array with the template operator name.
     *
     * @return array
     *
     */
    public function operatorList()
    {
        return array(self::OPERATOR_NAME);
    }

    /**
     *
     * Return true to tell the template engine that the parameter list exists per operator type,
     * this is needed for operator classes that have multiple operators.
     *
     * @return bool
     *
     */
    public function namedParameterPerOperator()
    {
        return true;
    }

    /**
     *
     * Returns an array of named parameters, this allows for easier retrieval
     * of operator parameters. This also requires the function modify() has an extra
     * parameter called $namedParameters.
     *
     * @return array
     *
     */
    public function namedParameterList()
    {
        return array();
    }

    /**
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     * @param $namedParameters
     * @param array $namedParameters
     *
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, $namedParameters)
    {
        switch ($operatorName)
        {
            case self::OPERATOR_NAME:
                $operatorValue = self::checkGetRootNode($tpl);
            break;
        }
    }

    /**
     *
     * Retrieves the current root-node.
     *
     * @return bool|string
     *
     */
    protected function checkGetRootNode()
    {
        $ini = \eZINI::instance('ezexceed.ini');

        $fallback = function()
        {
            return NodeOperations::getContentRootNodeId();
        };

        return IniUtilities::checkGetSetting($ini, 'eZExceedSiteMap', 'RootNode', $fallback);
    }
}
