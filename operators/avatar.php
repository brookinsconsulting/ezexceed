<?php
/**
 * eZExceed HTTP header methods
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

use \eZUser;
use \eZContentObject;
use \ezexceed\models\content\Object;

/**
 * Find user avatars
 */
class Avatar
{
    const OPERATOR_NAME = 'avatar';

    /**
     * Return an array with the template operatos(s).
     * @return array
     */
    public static function operatorList()
    {
        return array(self::OPERATOR_NAME);
    }

    /**
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * Supported parameters are eZContentObject[], eZContentObjectTreeNode[], eZPageBlock
     * 'nodes' => array of nodeIds, 'objects' => array of objectIds.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue)
    {
        switch ($operatorName)
        {
            case self::OPERATOR_NAME:
                $object = $operatorValue;
                if ($object instanceof \eZUser) {
                    $object = $object->attribute('contentobject');
                }

                if ($object instanceof \eZContentObject) {
                    $images = Object::images($object, array(
                        'width' => 68,
                        'height' => 68
                    ));
                    $operatorValue = false;
                    if ($images && isset($images['thumb']))
                        $operatorValue = $images['thumb'];
                }
            break;
        }
    }
}
