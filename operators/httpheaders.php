<?php
/**
 * eZExceed HTTP header methods
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

use \eZUser;

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = array();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_')
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
        return $headers;
    }
}

/**
 * Read HTTP headers
 */
class HTTPHeaders
{
    const OPERATOR_NAME = 'http-header';

    protected static $headers = array();

    /**
     *
     * Return an array with the template operatos(s).
     *
     * @return array
     *
     */
    public static function operatorList()
    {
        return array(self::OPERATOR_NAME);
    }

    /**
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * Supported parameters are eZContentObject[], eZContentObjectTreeNode[], eZPageBlock
     * 'nodes' => array of nodeIds, 'objects' => array of objectIds.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue)
    {
        switch ($operatorName)
        {
            case self::OPERATOR_NAME:
                if (count(self::$headers) === 0) {
                    self::$headers = getallheaders();
                }
                if (is_string($operatorValue) && $operatorValue !== '') {
                    $key = $operatorValue;
                }
                elseif ($operatorParameters) {
                    $param = array_shift($operatorParameters);
                    $param = array_shift($param);
                    if (isset($param[1]))
                        $key = $param[1];
                }

                if ($key && isset(self::$headers[$key])) {
                    $operatorValue = self::$headers[$key];
                }
                else {
                    $operatorValue = '';
                }
            break;
        }
    }
}
