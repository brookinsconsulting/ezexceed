<?php
/**
 * eZExceed movetotrash template operator
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

use \ezexceed\classes\utilities\Objects;

/**
 *
 * Generates a pencil.
 *
 */
class TrashEnabled
{
    const OPERATOR_TRASHENABLED = 'trashenabled';

    /**
     *
     * Return an array with the template operator(s).
     *
     * @return array
     *
     */
    public static function operatorList()
    {
        return array(self::OPERATOR_TRASHENABLED);
    }

    /**
     *
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     *
     * @return bool
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue)
    {
        $operatorValue = false;

        switch ($operatorName)
        {
            case self::OPERATOR_TRASHENABLED:
                $operatorValue = Objects::moveToTrash();
                break;
        }
    }
}