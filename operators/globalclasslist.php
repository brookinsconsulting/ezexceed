<?php
/**
 * Retrieves a global can-create-class-list for the current user
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

use \ezexceed\models\ezsugar\Access;

/**
 *
 * Description
 *
 * Retrieves a global can-create-class-list for the current user.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 30.04.2012
 *
 */
class GlobalClassList
{
    const OPERATOR_NAME = 'globalclasslist';

    /**
     * Return an array with the template operator name.
     *
     * @return array
     */
    public function operatorList()
    {
        return array(self::OPERATOR_NAME);
    }

    /**
     * Return true to tell the template engine that the parameter list exists per operator type,
     * this is needed for operator classes that have multiple operators.
     *
     * @return bool
     */
    public function namedParameterPerOperator()
    {
        return true;
    }

    /**
     * Returns an array of named parameters, this allows for easier retrieval
     * of operator parameters. This also requires the function modify() has an extra
     * parameter called $namedParameters.
     *
     * @return array
     */
    public function namedParameterList()
    {
        return array();
    }

    /**
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     * @param $namedParameters
     * @param array $namedParameters
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, $namedParameters)
    {
        switch ($operatorName)
        {
            case self::OPERATOR_NAME:
                $user = \eZUser::currentUser();
                $operatorValue = Access::getGlobalClassList($user, 'content', 'create');
            break;
        }
    }
}
