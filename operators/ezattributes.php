<?php
/**
 * Groups object-attributes by CategoryList.
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

/**
 * Description
 *
 * Retrieves a global can-create-class-list for the current user.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 30.04.2012
 */
class Attributes
{
    const ISDEFAULTCATEGORY_OPERATOR_NAME = 'isdefaultcategory';
    const ISVISIBLEGROUP_OPERATOR_NAME = 'contentgroupisvisible';

    const SETTINGS_PREFIX = 'eZExceed_category_group_';

    /** @var array */
    protected $groupedAttributes;
    /** @var string */
    protected $defaultCategory;
    /** @var \eZINI */
    protected $ini;

    /**
     * Initiates operator.
     */
    public function __construct()
    {
        $this->ini = \eZINI::instance('content.ini');
        $this->groupedAttributes = array();
    }

    /**
     * Return an array with the template operator name.
     *
     * @return array
     */
    public static function operatorList()
    {
        return array(self::ISDEFAULTCATEGORY_OPERATOR_NAME, self::ISVISIBLEGROUP_OPERATOR_NAME);
    }

    /**
     * Return true to tell the template engine that the parameter list exists per operator type,
     * this is needed for operator classes that have multiple operators.
     *
     * @return bool
     */
    public function namedParameterPerOperator()
    {
        return true;
    }

    /**
     * Returns an array of named parameters, this allows for easier retrieval
     * of operator parameters. This also requires the function modify() has an extra
     * parameter called $namedParameters.
     *
     * @return array
     */
    public function namedParameterList()
    {
        return array(
            self::ISVISIBLEGROUP_OPERATOR_NAME => array(
                'classIdentifier' => array(
                    'type' => 'string',
                    'required' => true,
                    'default' => null
                ),
                'groupName' => array(
                    'type' => 'string',
                    'required' => true,
                    'default' => null
                )
            )
        );
    }

    /**
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     * @param $namedParameters
     * @param array $namedParameters
     *
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue, $namedParameters)
    {
        switch ($operatorName) {
            case self::ISDEFAULTCATEGORY_OPERATOR_NAME:
                $operatorValue = $this->getDefaultCategory() === $operatorValue;
            break;

            case self::ISVISIBLEGROUP_OPERATOR_NAME:
                $operatorValue = $this->groupIsVisible($namedParameters['classIdentifier'], $namedParameters['groupName']);
            break;
        }
    }

    /**
     * Determines if a content-group is visible for a specific user and class.
     *
     * @param $classIdentifier
     * @param $groupName
     *
     * @return bool
     */
    protected function groupIsVisible($classIdentifier, $groupName)
    {
        $settingsKey = self::SETTINGS_PREFIX . $classIdentifier;
        $currentSettings = json_decode(\eZPreferences::value($settingsKey));

        if (is_object($currentSettings) && isset($currentSettings->$groupName))
            return $currentSettings->$groupName;
        else
            return $this->getDefaultCategory() === $groupName;
    }

    protected function getDefaultCategory()
    {
        if (!$this->defaultCategory)
            $this->defaultCategory = strtolower($this->ini->variable('ClassAttributeSettings', 'DefaultCategory'));
        return $this->defaultCategory;
    }
}
