<?php
/**
 * eZExceed objectname template operator
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\operators;

use \ezexceed\models\content\Object;
use \eZUser;

/**
 *
 * Generates a pencil.
 *
 */
class Objectname
{
    const OPERATOR_OBJECTNAME = 'objectname';

    /**
     *
     * Return an array with the template operator(s).
     *
     * @return array
     *
     */
    public static function operatorList()
    {
        return array(self::OPERATOR_OBJECTNAME);
    }

    /**
     *
     * Executes the PHP function for the operator cleanup and modifies $operatorValue.
     *
     * Supported parameters are eZContentObject[], eZContentObjectTreeNode[], eZPageBlock
     * 'nodes' => array of nodeIds, 'objects' => array of objectIds.
     *
     * @param \eZTemplate $tpl
     * @param string $operatorName
     * @param array $operatorParameters
     * @param string $rootNamespace
     * @param string $currentNamespace
     * @param mixed $operatorValue
     *
     */
    public function modify($tpl, $operatorName, $operatorParameters, $rootNamespace, $currentNamespace, &$operatorValue)
    {
        switch ($operatorName)
        {
            case self::OPERATOR_OBJECTNAME:
                if ($operatorValue instanceof \eZContentObject)
                {
                    $operatorValue = Object::name($operatorValue);
                }
            break;
        }
    }
}
