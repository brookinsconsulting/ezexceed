<?php

/**
 *
 * Description
 *
 * Global dataprovider for testing multiple browsers.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 01.11.2011
 *
 */
class JSTestCase extends PHPUnit_Framework_TestCase
{
    /** @var WebDriver */
    protected $webdriver;

    /**
     * Automatic login.
     */
    protected function login()
    {
        $this->webdriver->get(JSTestSettings::loginUrl);
        usleep(250000);

        $usernameInput = $this->webdriver->findElementBy(LocatorStrategy::id, 'id1');
        $this->assertNotNull($usernameInput, 'Could not find input-field for username.');
        $usernameInput->sendKeys(array(JSTestSettings::username));
        //usleep(500000);

        $passwordInput = $this->webdriver->findElementBy(LocatorStrategy::id, 'id2');
        $this->assertNotNull($usernameInput, 'Could not find input-field for password.');
        $passwordInput->sendKeys(array(JSTestSettings::password));
        //usleep(2000000);

        $loginButton = $this->webdriver->findElementBy(LocatorStrategy::name, 'LoginButton');
        $this->assertNotNull($loginButton, 'Could not find login-button.');
        $loginButton->click();
    }

    /**
     *
     * Opens eZ and navigates to the url specified in JSTestSettings.
     *
     * @param $browser
     */
    protected function eZOpen($browser)
    {
        $this->webdriver->connect($browser);
        $this->login();
        $this->webdriver->get(JSTestSettings::url);
    }

    /**
     *
     * Provides browser-parameters for the different testcases.
     *
     * @return array
     */
    public static function browserProvider()
    {
        return array
        (
            //array('opera'),
            array('firefox')
            //array('internet explorer')
        );
    }

    /**
     * Initializes the webdriver.
     */
    protected function setUp()
    {
        $this->webdriver = new WebDriver(JSTestSettings::server, JSTestSettings::port);
    }

    /**
     *
     * Closes the webdriver after use.
     *
     */
    protected function tearDown()
    {
        if ($this->webdriver instanceof WebDriver)
        {
            $this->webdriver->close();
        }
    }
}