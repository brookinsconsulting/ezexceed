<?php

/**
 *
 * Description
 *
 * Testing of the finder-component.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 02.11.2011
 *
 */
class JSFinderTest extends JSTestCase
{
    /**
     *
     * Tests if it is possible to navigate.
     *
     * @dataProvider browserProvider
     *
     * @param $browser
     *
     */
    public function testSitemapNavigation($browser)
    {
        $this->eZOpen($browser);

        $sitemapHref = $this->webdriver->findElementBy(LocatorStrategy::cssSelector, '.kp-toolbar-sitemap');
        $this->assertNotNull($sitemapHref, 'Unable open the sitemap.');
        $sitemapHref->click();
        usleep(250000);

        $actionHomeShow = $this->webdriver->findElementBy(LocatorStrategy::cssSelector, '.show');
        $this->assertNotNull($actionHomeShow, 'Show action-element not found.');
        $firstHref = $actionHomeShow->getAttribute('href');
        $this->assertNotEmpty($firstHref, 'First href is empty!');

        $firstNodeLi = $this->webdriver->findElementBy(LocatorStrategy::cssSelector, '.nodes .node');
        $this->assertNotNull($firstNodeLi, 'No sitemap-nodes found.');
        $firstNodeLi->click();
        usleep(250000);

        $secondActionShow = $this->webdriver->findElementBy(LocatorStrategy::cssSelector, '.show');
        $this->assertNotNull($secondActionShow, 'The second action-element not found.');
        $secondHref = $secondActionShow->getAttribute('href');
        $this->assertNotEmpty($secondHref, 'Second href is empty!');

        $this->assertNotEquals($firstHref, $secondHref, 'Click did not trigger navigation.');
    }
}