<?php

/**
 *
 * Test for search-gui.
 *
 */
class JSSearchTest extends JSTestCase
{
    /**
     *
     * Tests the searchfield.
     *
     * @dataProvider browserProvider
     *
     * @param string $browser
     *
     */
    public function testSearchField($browser)
    {
        $this->eZOpen($browser);

        $element = $this->webdriver->findElementBy(LocatorStrategy::id, 'kp-search-entry');
        $this->assertNotNull($element, 'Could not find input for search-entry.');
        $element->sendKeys(array('ez'));
        usleep(2000000);

        $drawerCloseButton = $this->webdriver->findElementBy(LocatorStrategy::cssSelector, '.close');
        $this->assertNotNull($drawerCloseButton, 'Could not find button for closing the drawer.');
        $drawerCloseButton->click();

        $this->webdriver->close();
    }
}
