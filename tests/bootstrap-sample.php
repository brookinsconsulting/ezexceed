<?php

/**
 *
 * This file needs to include the phpwebdriver for Selenium.
 * @link http://code.google.com/p/php-webdriver-bindings/downloads/list
 *
 * Default location is tests/phpwebdriver/WebDriver.php which is already defined in .gitignore.
 *
 */

/*

require_once('abstractTestCase.php');
require_once('phpwebdriver/WebDriver.php');

class JSTestSettings
{
    const server = 'localhost';
    const port = '4444';
    const url = 'http://e.no/eng';
    const browser = 'firefox';
    const loginUrl = 'http://e.no/eng/user/login';
    const username = 'admin';
    const password = 'password';
}
*/