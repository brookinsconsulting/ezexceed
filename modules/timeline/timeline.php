<?php
/**
 * Renders timeline-template
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\timeline;

use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;

/**
 *
 * Description
 *
 * Renders timeline-template
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 13.12.2011
 *
 */
class Timeline extends Controller
{
    /**
     *
     * Renders the template
     *
     * @param $nodeId
     * @return Response
     */
    public static function get($nodeId)
    {
        $tpl = \eZTemplate::factory();

        $node = \eZContentObjectTreeNode::fetch($nodeId);
        $tpl->setVariable('node', $node);

        $html = $tpl->fetch('design:extra/timeline.tpl');

        return self::response(compact('html'), array('type' => 'json'));
    }
}
