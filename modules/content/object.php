<?php
/**
 * GET/POST ContentObject module
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\content;

use \ezexceed\models\content\Object as Model;
use \ezexceed\models\content\object\Restore;
use \ezexceed\classes\ote\Controller;
use \ezexceed\classes\ote\Response;
use \ezexceed\models\nodestructure\NodeOperations;

/**
 * Interact with data structures (content objects)
 *
 * @author Raymond Julin <raymond@keyteq.no>
 * Created 20.08.2012
 */
class Object extends Controller
{
    /** @var array Statuses. */
    public static $statuses = array
    (
        \eZContentObject::STATUS_ARCHIVED => 'archived',
        \eZContentObject::STATUS_PUBLISHED => 'published',
        \eZContentObject::STATUS_DRAFT => 'draft',
    );

    /**
     * Saves an attribute.
     *
     * @param bool $id
     * @param bool $version
     * @param bool $languageId
     *
     * @return Response
     */
    public static function save($id = false, $version = false, $languageId = false)
    {
        /** Must manipulate POST-data to meet ez's requirements. */
        $attributes = (array) self::$http->variable('attributes', array());
        $publish = (bool) self::$http->variable('publishChanges', false);

        /**
         * File uploaded with plupload can't have multidimensional POST.
         * Are posted with name 'file' in $_FILES. inputFormName' are the name that ez expects.
         */
        if (self::$http->hasVariable('inputFormName')) {
            /** For file upload. */
            $_FILES[self::$http->variable('inputFormName')] = $_FILES['file'];
        }

        $saveOptions = array(
            'version' => $version,
            'languageId' => $languageId,
            'publish' => $publish,
            'returnTemplates' => (bool) self::$http->hasVariable('returnTemplates')
        );
        $response = Model::save($id, $attributes, $saveOptions);

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Retrieves a single object.
     */
    public static function get($objectId)
    {
        $response = array('ok' => false);

        if ($object = \eZContentObject::fetch($objectId)) {
            $response['ok'] = true;
            $response['object'] = Model::formatter(true)->format($object);
        }

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Retrieves objects for grid-view.
     *
     * @return Response
     */
    public static function find()
    {
        $start = microtime(true);
        $sort = self::$http->variable('sort', 'date');
        $length = (int) self::$http->variable('length', 25);
        $offset = (int) self::$http->variable('offset', 0);
        $limits = compact('length', 'offset');

        if (isset($limits['length'])) $limits['length'] += 1;

        // Criteria input
        $q = self::$http->variable('q', false);
        $type = self::$http->variable('type', false);
        $author = self::$http->variable('author', false);
        $date = self::$http->variable('date', false);

        $criteria = array(
            'mine' => self::$http->variable('mine', false),
            'trash' => self::$http->variable('trash', false)
        );

        if ($q && strlen($q) > 1) {
            $criteria['name'] = $q;
        }
        if ($type) {
            $criteria['type'] = $type;
        }
        if ($author) {
            $criteria['owner_id'] = (int) $author;
        }
        if ($date) {
            $criteria['modified'] = strtotime($date);
        }

        switch ($sort) {
            case 'author':
                $sort = array('vo.owner_id' => 'desc');
                break;
            case 'type':
                $sort = array('vo.contentclass_id' => 'desc');
                break;
            case 'date':
            default:
                $field = $criteria['mine'] ? 'v.modified' : 'vo.modified';
                $sort = array($field => 'desc');
                break;
        }

        $formatter = Model::formatter(true);

        $objects = Model::find(
            $criteria, $limits, $sort,
            array($formatter, 'format'),
            array($formatter,'preProcess')
        );
        $time = array(
            'find' => microtime(true) - $start
        );

        $total = count($objects);
        if ($total === $limits['length']) {
            $total = $limits['length'] + $limits['offset'];
            array_pop($objects);
        }

        return self::response(compact('total', 'time', 'objects'), array('type' => 'json'));
    }

    /**
     * Find authors in the system
     *
     * @return Response
     */
    public static function authors()
    {
        $authors = array();

        foreach (Model::owners() as $author) {
            $authors[] = array(
                'id' => (int) $author['id'],
                'name' => $author['name']
            );
        }

        return self::Response(compact('authors'), array('type' => 'json'));
    }

    public static function statistics()
    {
        return self::response(Model::statistics(), array('type' => 'json'));
    }

    /**
     * Batch remove
     *
     * @param int $id ContentObject id
     * @param bool $permanently
     * @return Response
     */
    public static function remove($id, $permanently = false)
    {
        $ok = Model::remove(array($id), $permanently);
        return self::response(compact('ok', 'id'), array('type'=>'json'));
    }

    /**
     * Remove unused drafts for object
     *
     * @param int $id ContentObject id
     * @return Response
     */
    public static function removeUnused($id)
    {
        $ok = Model::removeUnused($id);
        return self::response(compact('ok', 'id'), array('type' => 'json'));
    }

    /**
     * Remove internal drafts for object
     *
     * @param int $id ContentObject id
     * @return Response
     */
    public static function removeInternalDrafts($id)
    {
        $versions = self::$http->postVariable('versions');
        $ok = Model::removeDrafts($id, (array) $versions, \eZContentObjectVersion::STATUS_INTERNAL_DRAFT);
        return self::response(compact('ok', 'id'), array('type' => 'json'));
    }

    /**
     * Remove all drafts for object
     *
     * @param int $id ContentObject id
     * @return Response
     */
    public static function removeDrafts($id)
    {
        $versions = self::$http->postVariable('versions');
        $ok = Model::removeDrafts($id, (array)$versions);
        return self::response(compact('ok', 'id'), array('type' => 'json'));
    }

    /**
     * Publishes an object.
     *
     * @param bool|int $id
     * @return Response
     */
    public static function publish($id = false)
    {
        if (!$id) {
            $response = array(
                'ok' => false,
                'error' => \ezpI18n::tr('publish', 'Lacking object id')
            );

            return self::response($response);
        }

        $object = \eZContentObject::fetch($id);

        if ($object) {
            $userId = self::$http->postVariable('userId');
            $versions = self::$http->postVariable('versions', false);
            $languageId = $object->attribute('initial_language_id');

            if (!$versions || !is_array($versions)) {
                $draft = Model::fetchLatestUserDraft($id, $userId, null);
                $versions = array($draft->attribute('version'));
            }

            if (!Model::publishInProgress($object->ID)) {
                $response = Model::publish($object, $versions);
            }
            else {
                $response['ok'] = true;
            }

            if ($response['ok'])
                $response['object'] = Model::getContentObject($id, $languageId);

            return self::response($response, array('type' => 'json'));
        }
        else {
            $response = array (
                'ok' => false,
                'error' => \ezpI18n::tr('publish', 'Object not found')
            );

            return self::response($response);
        }
    }


    /**
     * Create a new content object
     *
     * @return Response
     */
    public static function create()
    {
        $response = array();
        $hideNode = false;

        $name = self::$http->postVariable('name');
        $typeId = self::$http->postVariable('typeId');
        $nodeId = self::$http->postVariable('nodeId');

        /** Set to root. */
        if ($nodeId === null) {
            $nodeId = NodeOperations::getAssignableNodeId();
            $hideNode = true;
        }

        if ($name !== null && $typeId !== null && !$nodeId !== null)
            $response = Model::create($nodeId, $typeId, $hideNode, $name);

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Find keywords that starts with $q
     *
     * @param string $q
     * @return \ezexceed\classes\ote\Response
     */
    public static function findKeywords($q)
    {
        $keywords = Model::keywords($q);
        return self::response($keywords, array('type' => 'json'));
    }

    /**
     * Set preferences
     *
     * @return Response
     */
    public static function preferences()
    {
        $type = self::$http->variable('type', false);

        if (!$type)
            return self::response(array('ok' => false, 'error' => 'Type not specified'), array('type' => 'json'));

        $data = self::$http->variable('data', array());
        \eZPreferences::setValue($type, json_encode($data));

        return self::response(array('ok' => true), array('type' => 'json'));
    }

    /**
     * Restore a content object
     *
     * @param int $id
     * @return \ezexceed\classes\ote\Response
     */
    public static function restore($id)
    {
        $returnOptions = array('type' => 'json');
        try {
            $response = Restore::contentObject($id);
        }
        catch (\Exception $e) {
            $response = array(
                'ok' => false,
                'error' => $e->getMessage()
            );
            return self::response($response, $returnOptions);
        }
        return self::response($response, $returnOptions);
    }
}
