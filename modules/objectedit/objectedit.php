<?php
/**
 * Replaces the REST-API
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\objectedit;

use \ezexceed\models\content\Object;
use \ezexceed\models\nodestructure\NodeTree;
use \ezexceed\models\nodestructure\NodeOperations;
use \ezexceed\models\objectrenderer\ObjectRenderer;
use \ezexceed\models\objectrenderer\ObjectAttributeRenderer;
use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;

use \ezpI18n;

/**
 * Description
 *
 * Replaces the REST-API
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 28.02.2012
 */
class ObjectEdit extends Controller
{
    /** @var \eZContentObject */
    protected static $currentObject;

    /**
     * Retrieves all data necessary for rendering object-edit-gui.
     *
     * @param $objectId
     * @param bool $language
     *
     * @return Response
     */
    public static function get($objectId, $language = false)
    {
        try {
            self::$currentObject = Object::safeFetch($objectId);
        }
        catch (\Exception $e) {
            return self::response(array('renderedOutput' => '<p>' . $e->getMessage() . '</p>'), array('type' => 'json'));
        }

        if (self::$currentObject && self::$currentObject->attribute('can_edit')) {
            $renderer = new ObjectRenderer(self::$currentObject, $language);

            $response = Object::formatter()->format(self::$currentObject);

            $mainNode = self::$currentObject->attribute('main_node');
            if ($mainNode) {
                $response['subtree'] = NodeTree::getSubTree($mainNode->attribute('node_id'), array(
                    'limit' => 25,
                    'sort' => \eZContentObjectTreeNode::sortFieldName($mainNode->attribute('sort_field')),
                    'order' => $mainNode->attribute('sort_order')
                ));
            }

            $response['versions'] = self::getVersions($renderer);
            $response['renderedOutput'] = $renderer->render();
        }
        else
            $response = array('renderedOutput' => '<p>Access denied</p>');

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Render a single-attribute edit GUI
     *
     * @param int $objectId
     * @param int $attribute
     * @param bool $language
     *
     * @return Response
     */
    public static function getAttribute($objectId, $attribute, $language = false)
    {
        $response = array('renderedOutput' => '<p>Access denied</p>');

        try {
            self::$currentObject = Object::safeFetch($objectId);
        }
        catch (\Exception $e) {
            return self::response(array('renderedOutput' => '<p>' . $e->getMessage() . '</p>'), array('type' => 'json'));
        }

        if (self::$currentObject && self::$currentObject->attribute('can_edit'))
        {
            $http = \eZHTTPTool::instance();
            $fromVersion = $http->getVariable('version');
            $renderer = new ObjectAttributeRenderer(self::$currentObject, $attribute, $language, $fromVersion);

            $response = Object::formatter()->format(self::$currentObject);
            $response['versions'] = self::getVersions($renderer);
            $response['renderedOutput'] = trim($renderer->render());
        }

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Retrieves a subtree for a node.
     *
     * @param $nodeId
     * @param int $offset
     * @param int $limit
     *
     * @return Response
     *
     */
    public static function getSubtree($nodeId, $offset = 0, $limit = 25)
    {
        $node = \eZContentObjectTreeNode::fetch($nodeId);
        if (!$node)
            return self::response(array('ok' => false, 'error' => 'Node not found'), array('type' => 'json'));
        if (!$node->attribute('can_edit'))
            return self::response(array('ok' => false, 'error' => 'Access denied'), array('type' => 'json'));

        $params = array(
            'limit' => (int)$limit,
            'offset' => (int)$offset,
            'sort' => \eZContentObjectTreeNode::sortFieldName($node->attribute('sort_field')),
            'order' => $node->attribute('sort_order'),
        );

        $searchString = self::$http->variable('searchString', false);

        if ($searchString) {
            $params['objectNameFilter'] = '%' . $searchString;
        }

        $response['subtree'] = NodeTree::getSubTree($nodeId, $params);
        return self::response($response, array('type' => 'json'));
    }

    /**
     * Return json of tinymce translations
     *
     * @param string $lang
     * @return Response
     */
    public static function tinyMceLanguagePack($lang = '-en')
    {
        $locale = \eZLocale::instance();
        $translations = array(
            'common' => array(
                'edit_confirm' => ezpI18n::tr( 'design/standard/ezoe', "Do you want to use the WYSIWYG mode for this textarea?"),
                'apply' => ezpI18n::tr( 'design/standard/ezoe', "Apply"),
                'insert' => ezpI18n::tr( 'design/standard/ezoe', "Insert"),
                'update' => ezpI18n::tr( 'design/standard/ezoe', "Update"),
                'cancel' => ezpI18n::tr( 'design/standard/ezoe', "Cancel"),
                'close' => ezpI18n::tr( 'design/standard/ezoe', "Close"),
                'browse' => ezpI18n::tr( 'design/standard/ezoe', "Browse"),
                'class_name' => ezpI18n::tr( 'design/standard/ezoe', "Class"),
                'not_set' => ezpI18n::tr( 'design/standard/ezoe', "-- Not set --"),
                'clipboard_msg' => ezpI18n::tr( 'design/standard/ezoe', "Copy/Cut/Paste is not available in Mozilla and Firefox.\nDo you want more information about this issue?"),
                'clipboard_no_support' => ezpI18n::tr( 'design/standard/ezoe', "Currently not supported by your browser, use keyboard shortcuts instead."),
                'popup_blocked' => ezpI18n::tr( 'design/standard/ezoe', "Sorry, but we have noticed that your popup-blocker has disabled a window that provides application functionality. You will need to disable popup blocking on this site in order to fully utilize this tool."),
                'invalid_data' => ezpI18n::tr( 'design/standard/ezoe', "Error: Invalid values entered, these are marked in red."),
                'more_colors' => ezpI18n::tr( 'design/standard/ezoe', "More colors")
            ),
            'validator_dlg' => array(
                'required' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; is required and must have a value', null, array( '%label' => '<label>' )),
                'number' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be a valid number', null, array( '%label' => '<label>' )),
                'int' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be a valid integer number', null, array( '%label' => '<label>' )),
                'url' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be a valid absolute url address', null, array( '%label' => '<label>' )),
                'email' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be a valid email address', null, array( '%label' => '<label>' )),
                'size' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be a valid css size/unit value', null, array( '%label' => '<label>' )),
                'html_id' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be a valid html element id', null, array( '%label' => '<label>' )),
                'min' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be higher then %min', null, array( '%label' => '<label>', '%min' => '<min>' )),
                'max' => ezpI18n::tr( 'design/standard/ezoe/validator', '&quot;%label&quot; must be lower then %max', null, array( '%label' => '<label>', '%max' => '<max>' )),
            ),
            'contextmenu' => array(
                'align' => ezpI18n::tr( 'design/standard/ezoe', "Alignment"),
                'left' => ezpI18n::tr( 'design/standard/ezoe', "Left"),
                'center' => ezpI18n::tr( 'design/standard/ezoe', "Center"),
                'right' => ezpI18n::tr( 'design/standard/ezoe', "Right"),
                'full' => ezpI18n::tr( 'design/standard/ezoe', "Full")
            ),
            'insertdatetime' => array(
                'date_fmt' => ezpI18n::tr( 'design/standard/ezoe', "%Y-%m-%d"),
                'time_fmt' => ezpI18n::tr( 'design/standard/ezoe', "%H:%M:%S"),
                'insertdate_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert date"),
                'inserttime_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert time"),
                'months_long' => implode(',', $locale->LongMonthNames ),
                'months_short' => implode(',', $locale->ShortMonthNames ),
                'day_long' => implode( ',', $locale->LongDayNames ) . ',' . $locale->longDayName(0),
                'day_short' => implode( ',', $locale->ShortDayNames ) . ',' . $locale->shortDayName(0)
            ),
            'print' => array(
                'print_desc' => ezpI18n::tr( 'design/standard/ezoe', "Print")
            ),
            'preview' => array(
                'preview_desc' => ezpI18n::tr( 'design/standard/ezoe', "Preview")
            ),
            'directionality' => array(
                'ltr_desc' => ezpI18n::tr( 'design/standard/ezoe', "Direction left to right"),
                'rtl_desc' => ezpI18n::tr( 'design/standard/ezoe', "Direction right to left")
            ),
            'save' => array(
                'save_desc' => ezpI18n::tr( 'design/standard/ezoe', "Save"),
                'cancel_desc' => ezpI18n::tr( 'design/standard/ezoe', "Cancel all changes")
            ),
            'nonbreaking' => array(
                'nonbreaking_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert non-breaking space character")
            ),
            'iespell' => array(
                'iespell_desc' => ezpI18n::tr( 'design/standard/ezoe', "Run spell checking"),
                'download' => ezpI18n::tr( 'design/standard/ezoe', "ieSpell not detected. Do you want to install it now?")
            ),
            'advhr' => array(
                'advhr_desc' => ezpI18n::tr( 'design/standard/ezoe', "Horizontale rule")
            ),
            'emotions' => array(
                'emotions_desc' => ezpI18n::tr( 'design/standard/ezoe', "Emotions")
            ),
            'emotions_dlg' => array(
                'title' => ezpI18n::tr( 'design/standard/ezoe', "Insert emotion"),
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Emotions"),
                'cool' => ezpI18n::tr( 'design/standard/ezoe', "Cool"),
                'cry' => ezpI18n::tr( 'design/standard/ezoe', "Cry"),
                'embarassed' => ezpI18n::tr( 'design/standard/ezoe', "Embarassed"),
                'foot_in_mouth' => ezpI18n::tr( 'design/standard/ezoe', "Foot in mouth"),
                'frown' => ezpI18n::tr( 'design/standard/ezoe', "Frown"),
                'innocent' => ezpI18n::tr( 'design/standard/ezoe', "Innocent"),
                'kiss' => ezpI18n::tr( 'design/standard/ezoe', "Kiss"),
                'laughing' => ezpI18n::tr( 'design/standard/ezoe', "Laughing"),
                'money_mouth' => ezpI18n::tr( 'design/standard/ezoe', "Money mouth"),
                'sealed' => ezpI18n::tr( 'design/standard/ezoe', "Sealed"),
                'smile' => ezpI18n::tr( 'design/standard/ezoe', "Smile"),
                'surprised' => ezpI18n::tr( 'design/standard/ezoe', "Surprised"),
                'tongue_out' => ezpI18n::tr( 'design/standard/ezoe', "Tongue out"),
                'undecided' => ezpI18n::tr( 'design/standard/ezoe', "Undecided"),
                'wink' => ezpI18n::tr( 'design/standard/ezoe', "Wink"),
                'usage' => ezpI18n::tr( 'design/standard/ezoe', "Use left and right arrows to navigate."),
                'yell' => ezpI18n::tr( 'design/standard/ezoe', "Yell"),
            ),
            'searchreplace' => array(
                'search_desc' => ezpI18n::tr( 'design/standard/ezoe', "Find"),
                'replace_desc' => ezpI18n::tr( 'design/standard/ezoe', "Find/Replace")
            ),
            'paste' => array(
                'paste_text_desc' => ezpI18n::tr( 'design/standard/ezoe', "Paste as Plain Text"),
                'paste_word_desc' => ezpI18n::tr( 'design/standard/ezoe', "Paste from Word"),
                'selectall_desc' => ezpI18n::tr( 'design/standard/ezoe', "Select All"),
                'plaintext_mode_sticky' => ezpI18n::tr( 'design/standard/ezoe', "Paste is now in plain text mode. Click again to toggle back to regular paste mode. After you paste something you will be returned to regular paste mode."),
                'plaintext_mode' => ezpI18n::tr( 'design/standard/ezoe', "Paste is now in plain text mode. Click again to toggle back to regular paste mode."),
            ),
            'paste_dlg' => array(
                'text_title' => ezpI18n::tr( 'design/standard/ezoe', "Use CTRL+V on your keyboard to paste the text into the window."),
                'text_linebreaks' => ezpI18n::tr( 'design/standard/ezoe', "Keep linebreaks"),
                'word_title' => ezpI18n::tr( 'design/standard/ezoe', "Use CTRL+V on your keyboard to paste the text into the window.")
            ),
            'table' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Inserts a new table"),
                'row_before_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert row before"),
                'row_after_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert row after"),
                'delete_row_desc' => ezpI18n::tr( 'design/standard/ezoe', "Delete row"),
                'col_before_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert column before"),
                'col_after_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert column after"),
                'delete_col_desc' => ezpI18n::tr( 'design/standard/ezoe', "Remove column"),
                'split_cells_desc' => ezpI18n::tr( 'design/standard/ezoe', "Split merged table cells"),
                'merge_cells_desc' => ezpI18n::tr( 'design/standard/ezoe', "Merge table cells"),
                'row_desc' => ezpI18n::tr( 'design/standard/ezoe', "Table row properties"),
                'cell_desc' => ezpI18n::tr( 'design/standard/ezoe', "Table cell properties"),
                'props_desc' => ezpI18n::tr( 'design/standard/ezoe', "Table properties"),
                'paste_row_before_desc' => ezpI18n::tr( 'design/standard/ezoe', "Paste table row before"),
                'paste_row_after_desc' => ezpI18n::tr( 'design/standard/ezoe', "Paste table row after"),
                'cut_row_desc' => ezpI18n::tr( 'design/standard/ezoe', "Cut table row"),
                'copy_row_desc' => ezpI18n::tr( 'design/standard/ezoe', "Copy table row"),
                'del' => ezpI18n::tr( 'design/standard/ezoe', "Delete table"),
                'row' => ezpI18n::tr( 'design/standard/ezoe', "Row"),
                'col' => ezpI18n::tr( 'design/standard/ezoe', "Column"),
                'rows' => ezpI18n::tr( 'design/standard/ezoe', "Rows"),
                'cols' => ezpI18n::tr( 'design/standard/ezoe', "Columns"),
                'cell' => ezpI18n::tr( 'design/standard/ezoe', "Cell")
            ),
            'autosave' => array(
                'unload_msg' => ezpI18n::tr( 'design/standard/ezoe', "The changes you made will be lost if you navigate away from this page.")
            ),
            'fullscreen' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Toggle fullscreen mode")
            ),
            'media' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert / edit embedded media"),
                'edit' => ezpI18n::tr( 'design/standard/ezoe', "Edit embedded media")
            ),
            'fullpage' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Document properties")
            ),
            'template' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert predefined template content")
            ),
            'visualchars' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Visual control characters on/off.")
            ),
            'spellchecker' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Toggle spellchecker"),
                'menu' => ezpI18n::tr( 'design/standard/ezoe', "Spellchecker settings"),
                'ignore_word' => ezpI18n::tr( 'design/standard/ezoe', "Ignore word"),
                'ignore_words' => ezpI18n::tr( 'design/standard/ezoe', "Ignore all"),
                'langs' => ezpI18n::tr( 'design/standard/ezoe', "Languages"),
                'wait' => ezpI18n::tr( 'design/standard/ezoe', "Please wait..."),
                'sug' => ezpI18n::tr( 'design/standard/ezoe', "Suggestions"),
                'no_sug' => ezpI18n::tr( 'design/standard/ezoe', "No suggestions"),
                'no_mpell' => ezpI18n::tr( 'design/standard/ezoe', "No misspellings found.")
            ),
            'pagebreak' => array(
                'desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert page break.")
            ),
            'advanced' => array(
                'style_select' => ezpI18n::tr( 'design/standard/ezoe', "Styles"),
                'block' => ezpI18n::tr( 'design/standard/ezoe', "Format"),
                'paragraph' => ezpI18n::tr( 'design/standard/ezoe', "Paragraph"),
                'div' => ezpI18n::tr( 'design/standard/ezoe', "Div"),
                'pre' => ezpI18n::tr( 'design/standard/ezoe', "Literal"),
                'h1' => ezpI18n::tr( 'design/standard/ezoe', "Heading 1"),
                'h2' => ezpI18n::tr( 'design/standard/ezoe', "Heading 2"),
                'h3' => ezpI18n::tr( 'design/standard/ezoe', "Heading 3"),
                'h4' => ezpI18n::tr( 'design/standard/ezoe', "Heading 4"),
                'h5' => ezpI18n::tr( 'design/standard/ezoe', "Heading 5"),
                'h6' => ezpI18n::tr( 'design/standard/ezoe', "Heading 6"),
                'code' => ezpI18n::tr( 'design/standard/ezoe', "Code"),
                'samp' => ezpI18n::tr( 'design/standard/ezoe', "Code sample"),
                'dt' => ezpI18n::tr( 'design/standard/ezoe', "Definition term"),
                'dd' => ezpI18n::tr( 'design/standard/ezoe', "Definition description"),
                'bold_desc' => ezpI18n::tr( 'design/standard/ezoe', "Bold (Ctrl+B)"),
                'italic_desc' => ezpI18n::tr( 'design/standard/ezoe', "Italic (Ctrl+I)"),
                'underline_desc' => ezpI18n::tr( 'design/standard/ezoe', "Underline (Ctrl+U)"),
                'striketrough_desc' => ezpI18n::tr( 'design/standard/ezoe', "Strikethrough"),
                'justifyleft_desc' => ezpI18n::tr( 'design/standard/ezoe', "Align left"),
                'justifycenter_desc' => ezpI18n::tr( 'design/standard/ezoe', "Align center"),
                'justifyright_desc' => ezpI18n::tr( 'design/standard/ezoe', "Align right"),
                'justifyfull_desc' => ezpI18n::tr( 'design/standard/ezoe', "Align full"),
                'bullist_desc' => ezpI18n::tr( 'design/standard/ezoe', "Unordered list"),
                'numlist_desc' => ezpI18n::tr( 'design/standard/ezoe', "Ordered list"),
                'outdent_desc' => ezpI18n::tr( 'design/standard/ezoe', "Outdent"),
                'indent_desc' => ezpI18n::tr( 'design/standard/ezoe', "Indent"),
                'undo_desc' => ezpI18n::tr( 'design/standard/ezoe', "Undo (Ctrl+Z)"),
                'redo_desc' => ezpI18n::tr( 'design/standard/ezoe', "Redo (Ctrl+Y)"),
                'link_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert/edit link"),
                'unlink_desc' => ezpI18n::tr( 'design/standard/ezoe', "Unlink"),
                'image_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert/edit image"),

                'object_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert/edit object"),
                'file_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert/edit file"),
                'custom_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert custom tag"),
                'literal_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert literal text"),
                'pagebreak_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert pagebreak"),
                'disable_desc' => ezpI18n::tr( 'design/standard/content/datatype', "Disable editor"),
                'store_desc' => ezpI18n::tr( 'design/standard/content/edit', "Store draft"),
                'publish_desc' => ezpI18n::tr( 'design/standard/content/edit', "Send for publishing"),
                'discard_desc' => ezpI18n::tr( 'design/standard/content/edit', "Discard"),

                'cleanup_desc' => ezpI18n::tr( 'design/standard/ezoe', "Cleanup messy code"),
                'code_desc' => ezpI18n::tr( 'design/standard/ezoe', "Edit HTML Source"),
                'sub_desc' => ezpI18n::tr( 'design/standard/ezoe', "Subscript"),
                'sup_desc' => ezpI18n::tr( 'design/standard/ezoe', "Superscript"),
                'hr_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert horizontal ruler"),
                'removeformat_desc' => ezpI18n::tr( 'design/standard/ezoe', "Remove formatting"),
                'custom1_desc' => ezpI18n::tr( 'design/standard/ezoe', "Your custom description here"),
                'charmap_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert special character"),
                'visualaid_desc' => ezpI18n::tr( 'design/standard/ezoe', "Toggle guidelines/invisible elements"),
                'anchor_desc' => ezpI18n::tr( 'design/standard/ezoe', "Insert/edit anchor"),
                'cut_desc' => ezpI18n::tr( 'design/standard/ezoe', "Cut"),
                'copy_desc' => ezpI18n::tr( 'design/standard/ezoe', "Copy"),
                'paste_desc' => ezpI18n::tr( 'design/standard/ezoe', "Paste"),
                'image_props_desc' => ezpI18n::tr( 'design/standard/ezoe', "Image properties"),
                'newdocument_desc' => ezpI18n::tr( 'design/standard/ezoe', "New document"),
                'help_desc' => ezpI18n::tr( 'design/standard/ezoe', "Help"),
                'clipboard_msg' => ezpI18n::tr( 'design/standard/ezoe', "Copy/Cut/Paste is not available in Mozilla and Firefox.\nDo you want more information about this issue?"),
                'path' => ezpI18n::tr( 'design/standard/ezoe', "Path"),
                'newdocument' => ezpI18n::tr( 'design/standard/ezoe', "Are you sure you want clear all contents?"),
                'toolbar_focus' => ezpI18n::tr( 'design/standard/ezoe', "Jump to tool buttons - Alt+Q, Jump to editor - Alt-Z, Jump to element path - Alt-X"),
                'next' => ezpI18n::tr( 'design/standard/ezoe', "Next"),
                'previous' => ezpI18n::tr( 'design/standard/ezoe', "Previous"),
                'select' => ezpI18n::tr( 'design/standard/ezoe', "Select"),
                'type' => ezpI18n::tr( 'design/standard/ezoe', "Type")
            ),
            'advanced_dlg' => array(
                'about_general' => ezpI18n::tr( 'design/standard/ezoe', "About"),
                'about_help' => ezpI18n::tr( 'design/standard/ezoe', "Help"),
                'about_license' => ezpI18n::tr( 'design/standard/ezoe', "License"),
                'about_plugins' => ezpI18n::tr( 'design/standard/ezoe', "Plugins"),
                'about_plugin' => ezpI18n::tr( 'design/standard/ezoe', "Plugin"),
                'about_author' => ezpI18n::tr( 'design/standard/ezoe', "Author"),
                'about_version' => ezpI18n::tr( 'design/standard/ezoe', "Version"),
                'about_loaded' => ezpI18n::tr( 'design/standard/ezoe', "Loaded plugins"),
                'code_title' => ezpI18n::tr( 'design/standard/ezoe', "HTML Source Editor"),
                'code_wordwrap' => ezpI18n::tr( 'design/standard/ezoe', "Word wrap"),
                'colorpicker_title' => ezpI18n::tr( 'design/standard/ezoe', "Select a color"),
                'colorpicker_picker_tab' => ezpI18n::tr( 'design/standard/ezoe', "Picker"),
                'colorpicker_picker_title' => ezpI18n::tr( 'design/standard/ezoe', "Color picker"),
                'colorpicker_palette_tab' => ezpI18n::tr( 'design/standard/ezoe', "Palette"),
                'colorpicker_palette_title' => ezpI18n::tr( 'design/standard/ezoe', "Palette colors"),
                'colorpicker_named_tab' => ezpI18n::tr( 'design/standard/ezoe', "Named"),
                'colorpicker_named_title' => ezpI18n::tr( 'design/standard/ezoe', "Named colors"),
                'colorpicker_color' => ezpI18n::tr( 'design/standard/ezoe', "Color"),
                'colorpicker_name' => ezpI18n::tr( 'design/standard/ezoe', "Name"),
                'charmap_usage' => ezpI18n::tr( 'design/standard/ezoe', "Use left and right arrows to navigate."),
                'charmap_title' => ezpI18n::tr( 'design/standard/ezoe', "Select special character"),
                'link_titlefield' => ezpI18n::tr( 'design/standard/ezoe', "Title"),
                'link_is_email' => ezpI18n::tr( 'design/standard/ezoe', "The URL you entered seems to be an email address, do you want to add the required mailto: prefix?"),
                'link_is_external' => ezpI18n::tr( 'design/standard/ezoe', "The URL you entered seems to external link, do you want to add the required http:// prefix?"),
                'link_list' => ezpI18n::tr( 'design/standard/ezoe', "Link list")
            ),
            'ez' => array(
                'root_node_name' => ezpI18n::tr( 'kernel/content', 'Top Level Nodes'),
                'empty_search_result' => ezpI18n::tr( 'design/standard/content/search', 'No results were found when searching for &quot;%1&quot;', null, array( '%1' => '<search_string>' )),
                'empty_bookmarks_result' => ezpI18n::tr( 'design/standard/content/view', 'You have no bookmarks')
            ),
            'searchreplace_dlg' => array(
                'searchnext_desc' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Find again"),
                'notfound' => ezpI18n::tr('design/standard/ezoe/searchreplace', "The search has been completed. The search string could not be found."),
                'search_title' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Find"),
                'replace_title' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Find/Replace"),
                'allreplaced' => ezpI18n::tr('design/standard/ezoe/searchreplace', "All occurrences of the search string were replaced."),
                'findwhat' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Find what"),
                'replacewith' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Replace with"),
                'direction' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Direction"),
                'up' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Up"),
                'down' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Down"),
                'mcase' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Match case"),
                'findnext' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Find next"),
                'replace' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Replace"),
                'replaceall' => ezpI18n::tr('design/standard/ezoe/searchreplace', "Replace all")
            ),
        );
        $response = array($lang => $translations);
        return self::response($response, array('type' => 'json'));
    }

    /**
     *
     * Retrieves all locations for an object.
     *
     * @return array
     *
     */
    protected static function getLocations()
    {
        $locations = array();

        /** @var $node \eZContentObjectTreeNode */
        foreach (self::$currentObject->attribute('assigned_nodes') as $node)
        {
            $tree = new NodeTree();
            $tree->init($node->attribute('node_id'), $node);

            $location = $tree->buildTreeArray(false);

            if (isset($location['node']))
                $locations[] = $location['node'];
        }

        return $locations;
    }

    /**
     * Retrieves versions.
     *
     * @param ObjectRenderer $renderer
     * @param bool $max
     *
     * @return array
     */
    protected static function getVersions($renderer, $max = false)
    {
        /** @var $version \eZContentObjectVersion */
        $versions = array();
        foreach ($renderer->versions() as $id => $version) {
            /** @var $language \eZContentLanguage */
            $language = \eZContentLanguage::fetch($version->attribute('initial_language_id'));
            $versions[] = array(
                'id' => (int) $id,
                'language' => $language->attribute('locale'),
                'created' => (int) $version->attribute('created'),
                'modified' => (int) $version->attribute('modified'),
                'status' => (int) $version->attribute('status'),
                'version' => (int) $version->attribute('version'),
            );

            if (is_numeric($max) && count($versions) === $max)
                return $versions;
        }

        return $versions;
    }
}
