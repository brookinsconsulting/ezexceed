<?php
/**
 * Filter for viewing latest user draft in view mode
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\view;

use \ezexceed\models\device\Preview;
use \ezexceed\models\device\UserDevice;
use \ezexceed\models\ezsugar\Access;
use \eZContentObjectTreeNode;

/**
 * @author Bjørnar Helland/ bh@keyteq.no
 * @since 15.03.2012
 */
class View
{
    /**
     * Retrieves the latest user-draft if support is enabled.
     *
     * @param $nodeId
     * @return mixed
     */
    public static function filter($nodeId)
    {
        $userLoggedIn = \eZUser::currentUser()->isLoggedIn();
        $node = eZContentObjectTreeNode::fetch($nodeId);
        $published = false;
        if ($node) {
            $published = (int) $node->ContentObject->Published;
            $published = $published !== 0 && $published <= time();
        }

        if (!$userLoggedIn) {
            /**
             * If the object is not published, an anonymous user should not be able to see it
             */
            if (!$published) {
                return -1;
            }
            return $nodeId;
        }

        if (!Access::userCanAccess(\eZUser::currentUser(), 'websitetoolbar', 'use')) {
            if (!$published) {
                return -1;
            }
            return $nodeId;
        }

        $contentINI = \eZINI::instance('content.ini');
        $displayMode = $contentINI->variable('VersionManagement', 'DisplayMode');
        if (!$displayMode || $displayMode !== 'latest')
            return $nodeId;

        // enable drafts for current user
        if (!is_callable('eZContentObjectTreeNode::setUseCurrentUserDraft'))
            return $nodeId;
        eZContentObjectTreeNode::setUseCurrentUserDraft(true);

        $siteINI = \eZINI::instance('site.ini');

        // enable view cache pr user
        $viewCacheTweaks = array();
        if ($siteINI->hasVariable('ContentSettings', 'ViewCacheTweaks'))
            $viewCacheTweaks = $siteINI->variable('ContentSettings', 'ViewCacheTweaks');

        $key = 'global';
        if (isset($viewCacheTweaks[$nodeId]))
            $key = $nodeId;

        if (!isset($viewCacheTweaks[$key]))
            $viewCacheTweaks[$key] = '';

        $viewCacheTweaks[$key] .= ';pr_user';
        $siteINI->setVariable('ContentSettings', 'ViewCacheTweaks', $viewCacheTweaks);

        // Trigger follow mode after checking that current session has followers
        $followers = Preview::followers();
        if ($followers) {
            $url = \eZSys::requestURI();
            $userId = \eZSession::get('eZUserLoggedInID');
            foreach ($followers as $uniqueKey => $following) {
                if ($following) {
                    $device = UserDevice::fetchByUniqueKey($uniqueKey, $userId);
                    if ($device) {
                        Preview::previewUrlOnDevice($device->attribute('device_id'), $uniqueKey, $device->attribute('os'), $url);
                    }
                }
            }
        }

        return $nodeId;
    }
}

?>
