<?php
/**
 * Retrieves html for adding content to a node
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\add_content;

use ezexceed\models\add_content\Content;
use ezexceed\classes\ote\Controller;
use ezexceed\classes\ote\Response;

/**
 *
 * Description
 *
 * Retrieves html for adding content to a node.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 12.12.2011
 *
 */
class AddContent extends Controller
{
    /**
     *
     * Retrieves template for adding content.
     *
     * @param int $nodeId
     *
     * @return Response
     *
     */
    public static function get($nodeId)
    {
        $classList = Content::getClassListForNodeId($nodeId);

        return self::response($classList, array('type' => 'json'));
    }
}
