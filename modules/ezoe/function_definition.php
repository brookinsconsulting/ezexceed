<?php
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

$FunctionList = array();

$FunctionList['get'] = array
(
    'name' => 'get',
    'call_method' => array
    (
        'class' => 'ezexceed\models\ezoe\EzOE',
        'method' => 'getSettings'
    ),
    'parameters' => array()
);
