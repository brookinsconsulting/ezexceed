<?php
/**
 * Connect handheld devices to user accounts
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\device;

use \eZHTTPTool;
use \eZUser;
use \eZINI;
use \eZSys;
use \eZSession;
use \ezexceed\classes\ote\Router;
use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;

use \ezexceed\models\device\UserDevice;
use \ezexceed\models\device\Preview;


/**
 * Connect handheld devices to user accounts
 *
 * @author Raymond Julin <raymond@keyteq.no>
 * @since 04.11.2011
 *
 */
class Device extends Controller
{
    public static $DEVICENAME_SESSION_KEY = 'EZEXCEED_CURRENT_DEVICENAME';

    /**
     * Register a new device for a given user
     *
     * @return Response
     */
    public static function register()
    {
        $http = eZHTTPTool::instance();
        $domain = eZSys::serverUrl();

        $fields = array(
            'deviceId' => 'device_id',
            'type' => 'type',
            'name' => 'name',
            'os' => 'os',
            'osVersion' => 'os_version'
        );

        if (!$userId = $http->sessionVariable('eZUserLoggedInID')) {
            return self::response(array('ok' => false, 'errors' => array('Not logged in')), array(
                'type' => 'json'
            ));
        }

        $ok = true;
        foreach ($fields as $remote => $local) {
            if ($ok && !$http->hasPostVariable($remote))
                $ok = false;
        }

        if (!$ok) {
            $data = array('ok' => $ok, 'errors' => array('Missing input'));
            return self::response($data, array(
                'type' => 'json'
            ));
        }

        $deviceId = $http->postVariable('deviceId');
        /** @var $device UserDevice */
        $device = UserDevice::fetchOne($userId, $deviceId, $domain);

        if (empty($device)) {
            $uniqueKey = self::generateUniqueDeviceKey();

            $device = UserDevice::create(array(
                'user_id' => $userId,
                'device_id' => $http->postVariable('deviceId'),
                'type' => $http->postVariable('type'),
                'domain' => $domain,
                'name' => $http->postVariable('name'),
                'os' => $http->postVariable('os'),
                'os_version' => $http->postVariable('osVersion'),
                'uniquekey' => $uniqueKey
            ));
        } else {
            $device->type = $http->postVariable('type');
            $device->name = $http->postVariable('name');
            $device->os = $http->postVariable('os');
            $device->os_version = $http->postVariable('os_version');

            if (empty($device->uniquekey)) {
                $uniqueKey = self::generateUniqueDeviceKey();
                $device->uniquekey = $uniqueKey;
            }
        }

        $device->store();

        $data = array('ok' => true, 'uniquekey' => $device->uniquekey, 'message' => array(
            'Device saved'
        ));
        return self::response($data, array(
            'type' => 'json'
        ));
    }

    public static function imageUpload()
    {
        $http = eZHTTPTool::instance();
        $userId = $http->sessionVariable('eZUserLoggedInID');
        if (!$userId) {
            $data = array('ok' => false, 'message' => 'Not logged in');

            return self::response($data, array('type' => 'json'));
        }

        $location = $http->postVariable('parentNodeId', 'auto');

        $upload = new \eZContentUpload();
        if (isset($_FILES['video'])) {
            $upload->handleUpload($result, 'video', $location, false, 'Uploaded mobile video');
        } else {
            $imageName = isset($_FILES['image']['name']) ? '' : 'Uploaded mobile image';

            $upload->handleUpload($result, 'image', $location, false, $imageName);
        }

        if (is_object($result['contentobject']) && $result['contentobject']->ID > 0) {
            $data = array('ok' => true, 'object_id' => $result['contentobject']->ID);
        } else {
            $data = array('ok' => false, 'message' => 'Object creation failed', 'result' => $result);
        }

        return self::response($data, array(
            'type' => 'json'
        ));
    }

    public static function unregister($userId)
    {
        $http = eZHTTPTool::instance();
        $uniqueKey = $http->postVariable('uniqueKey');

        $resp = array('ok' => false);
        if ($userId && $uniqueKey) {
            $device = UserDevice::fetchByUniqueKey($uniqueKey, $userId);
            if ($device) {
                $device->remove();
                $resp = array('ok' => true);
            }
        }
        return self::response($resp, array(
            'type' => 'json'
        ));
    }

    public static function notifyLike($userId, $likedByUser, $activityId, $objectId)
    {
        $text = $likedByUser->Name . ' liked your post';

        $customFields = array();
        $customFields['a'] = $activityId;
        if (!empty($objectId))
            $customFields['o'] = $objectId;

        \ezexceed\models\device\PushQueue::enqueueForUserDevices($userId, $text, $customFields);
    }

    public static function notifyReply($userId, $repliedByUser, $activityId, $objectId)
    {
        $text = $repliedByUser->Name . ' replied to your post';

        $customFields = array();
        $customFields['a'] = $activityId;
        if (!empty($objectId))
            $customFields['o'] = $objectId;

        \ezexceed\models\device\PushQueue::enqueueForUserDevices($userId, $text, $customFields);
    }

    public static function notifyMention($userId, $mentionedByUser, $activityId, $objectId, $formattedActivityString)
    {
        $text = 'Mentioned by @' . $mentionedByUser->Name . ': ' . $formattedActivityString;
        if (mb_strlen($text) > 150)
            $text = mb_substr($text, 0, 147) . '...';

        $customFields = array();
        $customFields['a'] = $activityId;
        if (!empty($objectId))
            $customFields['o'] = $objectId;

        \ezexceed\models\device\PushQueue::enqueueForUserDevices($userId, $text, $customFields);
    }

    protected static function generateUniqueDeviceKey()
    {
        // use base64 rather than hex, to keep entropy while producing a shorter string (critical in Apple's 255 bytes max length push messages)
        return substr(base64_encode(sha1(mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000) . mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000), true)), 0, 6);
    }

    protected static function generateEncryptionKey()
    {
        // use a random md5 as encryption key (AES 128 bit), encode the 128 bit binary md5 to base64
        return base64_encode(md5(mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000) . '|' . mt_rand(0, 100000000), true));
    }

    /**
     * Check if site is ready for device registration
     *
     * @return Response
     */
    public static function ready()
    {
        $data = array(
            'ok' => true,
            'domains' => array(
                'expect domains here'
            )
        );
        return self::response($data, array(
            'type' => 'json'
        ));
    }

    public static function isLoggedIn()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID')) {
            $data = array('loggedin' => false);
        } else {
            $data = array('loggedin' => true);
        }

        return self::response($data, array(
            'type' => 'json'
        ));
    }

    /**
     * List users devices for site
     *
     * @return Response
     */
    public static function find()
    {
        $domain = \eZSys::serverUrl();
        $userId = \eZUser::currentUserID();

        $data = UserDevice::fetchByUserIdDomain($userId, $domain);
        $followers = Preview::followers();
        $devices = array();

        // get current device's name, if any. set in login(), posted by device app
        $currentName = \eZSession::get(self::$DEVICENAME_SESSION_KEY);

        foreach ($data as $d) {
            $current = !empty($currentName) && $d->name == $currentName;

            if (!$current) {
                $devices[] = array(
                    'id' => $d->uniquekey,
                    'name' => $d->name,
                    'type' => $d->type,
                    'os' => $d->os,
                    'current' => $current,
                    'osVersion' => $d->os_version,
                    'following' => (bool)$followers[$d->uniquekey]
                );
            }
        }
        $ok = true;

        $data = compact('ok', 'devices');
        return self::response($data, array('type' => 'json'));
    }

    public static function preview()
    {
        $http = eZHTTPTool::instance();
        $resp = array(
            'ok' => false
        );

        if ($http->hasPostVariable('url') && $http->hasPostVariable('uniqueKey')) {
            $uniqueKey = $http->postVariable('uniqueKey');
            $url = $http->postVariable('url');
            $userId = $http->sessionVariable('eZUserLoggedInID');

            $device = UserDevice::fetchByUniqueKey($uniqueKey, $userId);
            if ($device) {
                $deviceId = $device->attribute('device_id');
                $os = $device->attribute('os');
                $resp['ok'] = Preview::previewUrlOnDevice($deviceId, $uniqueKey, $os, $url);
            }
            else {
                $resp['error'] = 'No device found for key';
            }
        }
        else {
            $resp += array(
                'error' => 'Missing url or uniqueKey'
            );
        }
        return self::response($resp, array(
            'type' => 'json'
        ));
    }

    public static function follow()
    {
        $http = eZHTTPTool::instance();
        $followers = Preview::followers();
        $resp = array(
            'ok' => false
        );
        if ($http->hasPostVariable('follow') && $http->hasPostVariable('uniqueKey')) {
            $uniqueKey = $http->postVariable('uniqueKey');
            $follow = $http->postVariable('follow');

            $followers[$uniqueKey] = (bool)$follow;
            Preview::followers($followers);
        }

        $resp['followers'] = $followers;
        return self::response($resp, array(
            'type' => 'json'
        ));
    }

    public static function logout()
    {
        $user = eZUser::instance();
        $user->logoutCurrent();
    }

    /**
     * Login user from device
     *
     * @return Response
     */
    public static function login()
    {
        $http = eZHTTPTool::instance();
        if ($http->hasPostVariable('username') && $http->hasPostVariable('password')) {
            $username = $http->postVariable('username');
            $password = $http->postVariable('password');

            $ini = eZINI::instance();
            if ($ini->hasVariable('UserSettings', 'LoginHandler'))
                $handlers = $ini->variable('UserSettings', 'LoginHandler');
            else
                $handlers = array('standard');

            $hasAccessToSite = true;
            foreach (array_keys($handlers) as $key) {
                $handler = $handlers[$key];
                $userClass = \eZUserLoginHandler::instance($handler);
                if (!is_object($userClass)) continue;

                /** @var $user \ezuser */
                $user = $userClass->loginUser($username, $password);
                if ($user instanceof eZUser) {
                    $hasAccessToSite = $user->canLoginToSiteAccess($GLOBALS['eZCurrentAccess']);
                    if (!$hasAccessToSite) {
                        $user->logoutCurrent();
                        $user = null;
                    }
                    break;
                }
            }
            if ($user instanceof eZUser && $hasAccessToSite) {
                $userID = $user ? $user->id() : false;
                $http->removeSessionVariable('eZUserLoggedInID');
                $http->setSessionVariable('eZUserLoggedInID', $userID);

                if ($ini->hasVariable('SiteSettings', 'SiteName'))
                    $sitename = $ini->variable('SiteSettings', 'SiteName');
                else
                    $sitename = "";

                $data = array(
                    'ok' => true,
                    'id' => $userID,
                    'sitename' => $sitename
                );

                $activeExtensions = \eZExtension::activeExtensions();

                if (in_array('ezformtoken', $activeExtensions)) {
                    $data['formtoken'] = \ezxFormToken::getToken();
                }

                // remember the device's name, to avoid showing it when populating devices
                \eZSession::set(self::$DEVICENAME_SESSION_KEY, $http->postVariable('devicename'));
            } else {
                $data = array(
                    'ok' => false,
                    'errors' => array(
                        'Failed login'
                    )
                );
            }
        } else
            $data = array('ok' => false, 'errors' => array('Missing username/password'));

        return self::response($data, array(
            'type' => 'json'
        ));
    }
}
