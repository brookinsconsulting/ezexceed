<?php
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

if (isset($Params) && isset($Params['FunctionName']))
    $Result = \ezexceed\classes\ote\Router::handleModule($Params);
else
    list($Module, $FunctionList, $ViewList) = \ezexceed\modules\activity\Activity::getDefinition();
