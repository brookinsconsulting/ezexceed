<?php
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

$FunctionList = array();

$FunctionList['globalCount'] = array
(
    'name' => 'globalCount',
    'call_method' => array
    (
        'class' => 'ezexceed\modules\activity\Activity',
        'method' => 'getGlobalCount'
    ),
    'parameters' => array()
);

$FunctionList['objectCount'] = array
(
    'name' => 'objectCount',
    'call_method' => array
    (
        'class' => 'ezexceed\modules\activity\Activity',
        'method' => 'getObjectCount'
    ),
    'parameters' => array
    (
        array
        (
            'name' => 'objectId',
            'type' => 'string',
            'required' => false
        )
    )
);

$FunctionList['unreadCount'] = array
(
    'name' => 'unreadCount',
    'call_method' => array
    (
        'class' => 'ezexceed\modules\activity\Activity',
        'method' => 'getUnreadCountNumber'
    ),
    'parameters' => array
    (

    )
);
