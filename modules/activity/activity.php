<?php

/**
 * Activity
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\activity;

use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;
use \ezexceed\models\activity\Activity as ActivityModel;
use \ezexceed\models\activity\Like;
use \ezexceed\models\activity\Mention;
use \ezexceed\models\activity\Tag;
use \ezexceed\models\activity\Sticky;
use \ezexceed\models\content\Object;

/**
 * Activity stream
 *
 * @author Jon Helge Stensrud <jon@keyteq.no>
 * @since 19.01.2012
 *
 */
class Activity extends Controller
{
    const PREFERENCE_LASTACTIVITY_READ = 'ActivityLastRead';

    /**
     *
     * Counts all the global entries.
     *
     * @return int
     *
     */
    public static function getGlobalCount()
    {
//        $result = ActivityModel::fetchGlobalCount();
        $result = 0;

        return array('result' => $result);
    }

    /**
     *
     * Retrieves or stores activity-entries.
     *
     * @return Response
     */
    public static function activityEndpoint()
    {
        if (self::$http->hasVariable('activitystring')) {
            return self::submitNewActivity();
        }

        $http = \eZHTTPTool::instance();
        $tag = $http->variable('tag');
        $objectid = $http->variable('object_id');

        if ($tag)
            return self::getActivityStreamByHashtag();

        if ($objectid)
            return self::getActivityStreamForObject();

        return self::getActivityStream();
    }

    public static function getStickies()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID')) {
            return self::handleError('User not logged in');
        }

        /** @var $stickyList ActivityModel[] */
        $stickyList = ActivityModel::fetchBySticky($sessionUserId);
        $data = self::generateActivitiesData($stickyList, $sessionUserId);

        return self::handleOk('Stickies fetched', $data, array());
    }

    /**
     *
     * Retrieves mentions.
     *
     * @return Response
     */
    public static function mentions()
    {
        $mentionuserid = self::$http->variable('user_id');
        if ($mentionuserid) {
            return self::getMentions();
        }
        return self::getMentionsForLoggedInUser();
    }

    /**
     *
     * Get activity stream for content object
     * Required POST vars: object_id
     * Optional GET vars: limit, offset
     *
     * @return Response
     *
     */
    protected static function getActivityStreamForObject()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID')) {
            return self::handleError('User not logged in');
        }

        $object_id = $http->variable('object_id');

        if (empty($object_id)) {
            return self::handleError('Object id not set');
        }

        $limit = $http->variable('limit');
        if (empty($limit))
            $limit = 10;

        $offset = $http->variable('offset');
        if (empty($offset))
            $offset = 0;

        $data = self::generateActivitiesData(ActivityModel::fetchForContentObject($object_id, $limit, $offset), $sessionUserId);
        return self::handleOk('Activities fetched', $data, array());
    }

    protected static function getStickyIdx($userId)
    {
        $stickiesIdx = array();

        $stickyList = ActivityModel::fetchBySticky($userId);
        foreach ($stickyList as $stickyActivity) {
            $stickiesIdx[$stickyActivity->id] = true;
        }
        return $stickiesIdx;
    }

    public static function setLastRead()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID')) {
            return self::handleError('User not logged in');
        }

        $id = $http->variable('activity_id');

        if (empty($id)) {
            return self::handleError('activity_id id not set');
        }

        $lastActivityRead = \eZPreferences::value(static::PREFERENCE_LASTACTIVITY_READ);
        if ($id>$lastActivityRead) {
            \eZPreferences::setValue(static::PREFERENCE_LASTACTIVITY_READ, $id);
            return self::handleOk('Last read updated');
        }

        return self::handleOk('Last read not updated, already read newer items');
    }

    public static function getUnreadCount()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID')) {
            return self::handleError('User not logged in');
        }

        $lastActivityRead = \eZPreferences::value(static::PREFERENCE_LASTACTIVITY_READ);

        if (empty($lastActivityRead)) {
            $lastActivityRead = 0;
        }
        else {
            $lastActivityRead = (int)$lastActivityRead;
        }

        $count = ActivityModel::unreadCount($lastActivityRead, $sessionUserId);
        return self::handleOk('Unread count fetched', array('count' => $count, 'lastActivityRead' => $lastActivityRead));
    }

    public static function getUnreadCountNumber()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID')) {
            return self::handleError('User not logged in');
        }

        $lastActivityRead = \eZPreferences::value(static::PREFERENCE_LASTACTIVITY_READ);

        if (empty($lastActivityRead))
        {
            $lastActivityRead = 0;
        }
        else
        {
            $lastActivityRead = (int)$lastActivityRead;
        }

        $count = ActivityModel::unreadCount($lastActivityRead, $sessionUserId);

        return $count;
    }

    /**
     *
     * Search for user, support function for adding mentions in JS
     * Required GET vars: name
     * Optional GET vars: limit
     *
     * @return Response
     *
     */
    public static function lookUpUser()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
            return self::handleError('User not logged in');

        return self::handleOk('Users fetched', Object::owners());
    }

    /**
     *
     * Get global activity stream
     * Optional GET vars: limit, offset
     *
     * @return Response
     *
     */
    protected static function getActivityStream()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        $limit = $http->variable('limit');
        if (empty($limit))
            $limit = 10;

        $offset = $http->variable('offset');
        if (empty($offset))
            $offset = 0;

        /** @var $objects \eZPersistentObject[] */
        $data = self::generateActivitiesData(ActivityModel::fetchGlobal($sessionUserId, $limit, $offset), $sessionUserId);
        return self::handleOk('Activities fetched', $data, array());
    }

    /**
     * Get activity stream for hashtag
     * Required GET vars: tag
     * Optional GET vars: limit, offset
     *
     * @return Response
     */
    protected static function getActivityStreamByHashtag()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        $tag = $http->variable('tag');

        if(empty($tag))
        {
            return self::handleError('Tag not set');
        }

        // strip # char, if exists
        if (substr($tag,0,1)=='#')
            $tag = substr($tag,1);

        $limit = $http->variable('limit');
        if (empty($limit))
            $limit = 10;

        $offset = $http->variable('offset');
        if (empty($offset))
            $offset = 0;

        $data = self::generateActivitiesData(ActivityModel::fetchByHashtag($tag, $limit, $offset), $sessionUserId);
        return self::handleOk('Activities fetched', $data);
    }

    /**
     *
     * Get activity stream for hashtag
     * Required GET vars: user_id
     * Optional GET vars: limit, offset
     *
     * @return Response
     *
     */
    protected static function getMentions()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        $user_id = $http->variable('user_id');

        if(empty($user_id))
        {
            return self::handleError('User id not set');
        }

        $limit = $http->variable('limit');
        if (empty($limit))
            $limit = 10;

        $offset = $http->variable('offset');
        if (empty($offset))
            $offset = 0;

        $data = self::generateActivitiesData(ActivityModel::fetchMentions($user_id, $limit, $offset), $sessionUserId);

        return self::handleOk('Activities fetched', $data);
    }

    protected static function generateActivitiesData($objects, $sessionUserId)
    {

        $stickiesIdx = self::getStickyIdx($sessionUserId);
        $data = array();

        $i = 0;

        /** @var $object ActivityModel */
        foreach ($objects as $object)
        {
            if (is_array($object)) {
                $data[$i] = $object;
                $objectId = $object['id'];
            }
            else {
                $data[$i] = $object->getData();
                $objectId = $object->id;
            }
            $data[$i]['sticky'] = false;
            if (isset($stickiesIdx[$objectId]) && $stickiesIdx[$objectId])
            {
                $data[$i]['sticky'] = true;
            }
            $data[$i]['user_color'] = substr(md5($data[$i]['user_fullname']), 0, 6);

            $contentObjectId = $data[$i]['contentobject_id'];
            if ($contentObjectId > 0 && $data[$i]['replyto_id'] == 0)
            {
                $contentObject = \eZContentObject::fetch($contentObjectId);

                $url = "";
                if ($contentObject) {
                    $data[$i]['contentobject_name'] = $contentObject->name();
                    $mainNode = $contentObject->mainNode();
                    if ($mainNode) {
                        $url = $mainNode->urlAlias();
                        \eZURI::transformURI($url);
                    }
                }
                $data[$i]['contentobject_view_url'] = $url;
            }

            $currentUser = \eZUser::currentUser();
            $data[$i]['currentuser_id'] = $currentUser->id();
            $data[$i]['avatar'] = self::getThumb(\eZContentObject::fetch($data[$i]['user_id']));

            if (isset($data[$i]['replies']) && count($data[$i]['replies']) > 0)
            {
                $data[$i]['replies'] = self::generateActivitiesData($data[$i]['replies'], $sessionUserId);
            }
            else
                $data[$i]['replies'] = array();

            $i++;
        }

        return $data;
    }

    /**
     * Get thumb for user object, if any
     *
     * @param object $object
     * @return string
     */
    protected static function getThumb($object)
    {
        /** User might not exist anymore. */
        if (!$object) {
            return null;
        }

        $images = Object::images($object, array(
            'width' => 68,
            'height' => 68
        ));

        return isset($images['thumb']) ? $images['thumb'] : null;
    }

    /**
     *
     * Get activity stream for hashtag
     * Required GET vars: user_id
     * Optional GET vars: limit, offset
     *
     * @return Response
     *
     */
    protected static function getMentionsForLoggedInUser()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        $limit = $http->variable('limit');
        if (empty($limit))
            $limit = 10;

        $offset = $http->variable('offset');
        if (empty($offset))
            $offset = 0;

        $queryResult = ActivityModel::fetchMentions($sessionUserId, $limit, $offset);

        /** @var $result ActivityModel */
        foreach($queryResult as $key => $result) {
            $replyToId = (int)$result->attribute('replyto_id');

            if ($replyToId > 0)
                $queryResult[$key] = ActivityModel::fetch($replyToId);
        }

        $data = self::generateActivitiesData($queryResult, $sessionUserId);
        return self::handleOk('Activities fetched', $data);
    }

    // temporary method, until cronjob or other mechanism is in place
    public static function handlePushQueue()
    {
         \ezexceed\models\device\PushQueue::handleQueue();
    }

    /**
     *
     * Submit new activity to the stream for logged in user
     * Required POST vars: activitystring
     * Optional POST vars: replyto_id, contentobject_id
     *
     * @return Response
     *
     */
    public static function submitNewActivity()
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        $activityString = $http->postVariable('activitystring');
        $replyToId = $http->postVariable('replyto_id');

        if (empty($replyToId))
            $replyToId = 0;

        $contentObjectId = $http->postVariable('contentobject_id');

        if (empty($contentObjectId))
            $contentObjectId = 0;

        $activity = ActivityModel::create(array('activitystring' => $activityString, 'replyto_id' => $replyToId, 'user_id' => $sessionUserId, 'contentobject_id' => $contentObjectId));
        if ($activity->store())
        {
            $list = self::generateActivitiesData(array($activity), $sessionUserId);
            return self::handleOk('Activity saved', $list[0]);
        }
        else
        {
            return self::handleError($activity->getErrorMsg());
        }
    }

    /**
     *
     * Set a sticky activity for user
     * Required POST vars: activity_id
     *
     * @param bool|string $activityId
     *
     * @return Response
     *
     */
    public static function addSticky($activityId = false)
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        if (Sticky::exists($activityId, $sessionUserId))
        {
            return self::handleError('Already sticky');
        }

        $sticky = Sticky::create(array('activity_id' => $activityId, 'user_id' => $sessionUserId));

        if ($sticky->store())
        {
            /** @var $activity ActivityModel */
            $activity = ActivityModel::fetch($activityId);
            return self::handleOk('Sticky saved', $activity->getData());
        }
        else
        {
            return self::handleError($sticky->getErrormsg());
        }
    }

    public static function removeSticky($activityId = false)
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        if (!Sticky::exists($activityId, $sessionUserId))
        {
            return self::handleError('Not sticky');
        }

        $sticky = Sticky::create(array('activity_id' => $activityId, 'user_id' => $sessionUserId));

        if ($sticky->remove())
        {
            /** @var $activity ActivityModel */
            $activity = ActivityModel::fetch($activityId);
            return self::handleOk('Sticky removed', $activity->getData());
        }
        else
        {
            return self::handleError($sticky->getErrormsg());
        }
    }

    /**
     *
     * Submit a Like on an activity for logged in user
     * Required POST vars: activity_id
     *
     * @param bool|string $activityId
     *
     * @return Response
     *
     */
    public static function unlikeActivity($activityId = false)
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        $like = Like::create(array('activity_id' => $activityId, 'user_id' => $sessionUserId));

        if ($like->remove())
        {
            /** @var $activity ActivityModel */
            $activity = ActivityModel::fetch($activityId);
            $response = $activity->getData();
            $response['currentuser_id'] = \eZUser::currentUserID();
            return self::handleOk('Like removed', $response);
        }
        else
        {
            return self::handleError($like->getErrormsg());
        }
    }


    /**
     *
     * Submit a Like on an activity for logged in user
     * Required POST vars: activity_id
     *
     * @param bool|string $activityId
     *
     * @return Response
     *
     */
    public static function likeActivity($activityId = false)
    {
        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('User not logged in');
        }

        $like = Like::create(array('activity_id' => $activityId, 'user_id' => $sessionUserId));

        if ($like->store())
        {
            /** @var $activity ActivityModel */
            $activity = ActivityModel::fetch($activityId);
            $response = $activity->getData();
            $response['currentuser_id'] = \eZUser::currentUserID();
            return self::handleOk('Like saved', $response);
        }
        else
        {
            return self::handleError($like->getErrormsg());
        }
    }

    public static function deleteActivity()
    {
        $http = \eZHTTPTool::instance();
        $activityId = $http->variable('activityId');

        $activity = ActivityModel::fetch($activityId);

        $http = \eZHTTPTool::instance();

        if (!$sessionUserId = $http->sessionVariable('eZUserLoggedInID'))
        {
            return self::handleError('Not logged in');
        }

        if ($sessionUserId != $activity->user_id)
        {
            return self::handleError('Access denied');
        }

        if ($activity instanceof \ezexceed\models\activity\Activity)
        {
            $activity->remove();
            return self::handleOk('Removed message');
        }
        return self::handleError('Unable to remove activity');

    }

    /**
     *
     * Returns a successfull response.
     *
     * @param string $message
     * @param array $data
     * @param array $metaData
     *
     * @return Response
     *
     */
    protected static function handleOk($message, $data = array(), $metaData = array())
    {
        $data = array('ok' => true, 'message' => array($message), 'data' => $data, 'metadata' => $metaData);

        return self::response($data, array('type' => 'json'));
    }

    /**
     *
     * Returns an error.
     *
     * @param int $code
     * @return Response
     *
     */
    protected static function handleError($code)
    {
        return self::response(array('ok' => false, 'errors' => array($code)), array('type' => 'json'));
    }
}
