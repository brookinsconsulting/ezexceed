<?php
/**
 * Publish controller
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\publish;

use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;
use \ezexceed\models\Publish as PublishModel;

/**
 *
 * Publish controller
 *
 * @author Bjørnar Helland / bh@keyteq.no
 * @since 13.12.2011
 *
 */
class Publish extends Controller
{
    /** @var PublishModel */
    protected static $model;

    public static function init()
    {
        self::$model = new PublishModel();
    }

    /**
     * Publish all content supplied
     *
     * @return Response
     *
     */
    public static function run()
    {
        $objects = self::$http->postVariable('objects', array());
        $response = self::$model->publishObjects($objects);

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Get status for node. Returns all content that will be affected by publish.
     *
     * @param int $objectId
     * @param null $languages
     *
     * @return Response
     */
    public static function status($objectId, $languages = null)
    {
        $object = \eZContentObject::fetch($objectId);

        if (!$object) {
            $response = array(
                'ok' => false,
                'error' => \ezpI18n::tr('eze-publish', 'Object does not exist')
            );
            return self::response($response, array('type' => 'json'));
        }

        return self::response(self::$model->status($object, $languages), array('type' => 'json'));
    }
}

Publish::init();