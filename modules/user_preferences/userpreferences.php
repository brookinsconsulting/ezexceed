<?php
/**
 * Retrieves rendered html for editing user-settings
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\user_preferences;

use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;
use \ezexceed\models\content\Object;

use \eZUser;
use \eZContentObject;
use \eZMail;

/**
 *
 * Description
 *
 * Retrieves rendered html for editing user-settings.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 08.12.2011
 *
 */

class UserPreferences extends Controller
{
    /**
     * Returns the rendered template.
     *
     * @param int $id
     *
     * @return Response
     */
    public static function get($id)
    {
        $tpl = \eZTemplate::factory();

        /** @var $user \eZUser */
        $user = eZUser::fetch($id);
        $object = $user->contentObject();
        $attributes = $object->dataMap();

        $heading = \ezpI18n::tr('eze', 'Settings for') . ' ' . $user->attribute('contentobject')->name();
        $tpl->setVariable('user', $user);
        $tpl->setVariable('attributes', $attributes);

        $html = $tpl->fetch('design:userpreferences/preferences.tpl');

        return self::response(compact('heading', 'html'), array('type' => 'json'));
    }

    /**
     *
     * Saves user-properties.
     *
     * @return Response
     *
     */
    public static function properties()
    {
        $fields = self::$http->postVariable('fields');

        $objectId = $fields['objectId'];
        $attributes = $fields['attributes'];

        $ok = Object::saveUser($objectId, $attributes, true);

        $contentObject = eZContentObject::fetch($objectId);
        $name = $contentObject->name();

        $response = array('ok' => $ok, 'name' => $name, 'data' => $attributes);

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Update user authentication information
     *
     * @param int $id
     * @return object
     *
     */
    public static function auth($id)
    {
        $error = '';

        /** @var $user \eZUser */
        $user = eZUser::fetch($id);

        $username = self::$http->variable('username');
        $email = self::$http->variable('email');
        $password = self::$http->variable('password');
        $repeatPassword = self::$http->variable('repeatpassword');
        $changedUsername = $username !== $user->attribute('login');
        $ok = false;

        if (!$username) {
            $error = \ezpI18n::tr('eze', 'Username is empty');
        }
        else if (!eZUser::validateLoginName($username, $error)) {
            $error = \ezpI18n::tr('eze', 'Invalid username!');
        }
        else if (!eZMail::validate($email))
        {
            $error = \ezpI18n::tr('eze', 'Invalid email');
        }
        else if (($password || $repeatPassword) && $password !== $repeatPassword)
        {
            $error = \ezpI18n::tr('eze', 'Repeated password does not match password');
        }
        else if ($password && !ezUser::validatePassword($password))
        {
            $error = \ezpI18n::tr('eze', 'Password is too short');
        }
        else if ($changedUsername && strlen($password) == 0 || strlen($repeatPassword) == 0) {
            $error = \ezpI18n::tr('eze', 'You must supply your password if you are altering your username!');
        }
        else {
            if ($password)
                $user->setInformation($id, $username, $email, $password, $repeatPassword);
            else
            {
                $user->setAttribute("contentobject_id", $id);
                $user->setAttribute("email", $email);
                $user->setAttribute("login", $username);
            }
            $user->store();

            $name = $user->contentObject()->name();
            $data = array(
                'username' => $username,
                'email' => $email
            );
            $ok = true;
        }

        $response = compact('ok', 'error', 'name', 'data');

        return self::response($response, array('type' => 'json'));
    }

    /**
     *
     * Saves message-preferences.
     *
     * @return Response
     *
     */
    public static function saveMessages()
    {
        $user = eZUser::currentUser();
        $address = $user->attribute('email');

        $db = \eZDB::instance();
        $db->begin();

        $settings = \eZGeneralDigestUserSettings::fetchForUser($address);

        if (self::$http->hasPostVariable('combinedDigest'))
            $reciveDigest = 1;
        else
            $reciveDigest = 0;

        $digestType = self::$http->postVariable('digestType');
        $dailyTime = self::$http->postVariable('dailyTime');

        $dayValue = '';

        if ($digestType == 1)
            $dayValue = self::$http->postVariable('weekday');
        else if($digestType == 2)
            $dayValue = self::$http->postVariable('dayOfMonth');

        $settings->setAttribute('receive_digest', $reciveDigest);
        $settings->setAttribute('day', $dayValue);
        $settings->setAttribute('digest_type', $digestType);
        $settings->setAttribute('time', $dailyTime);

        $settings->store();

        \eZCollaborationNotificationRule::removeByUserID($user->id());

        if (self::$http->hasPostVariable('collaboration'))
        {
            $rule = \eZCollaborationNotificationRule::create(self::$http->postVariable('collaboration'));
            $rule->store();
        }

        $db->commit();

        return self::response(array('ok' => true), array('type' => 'json'));
    }
}
