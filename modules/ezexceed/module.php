<?php

$Module = array('name' => 'ezexeceed', 'variable_params' => true);

$ViewList = array();
$ViewList['preview'] = array(
    'functions' => array('preview'),
    'script' => 'preview.php',
    'params' => array('ObjectID', 'EditVersion', 'LanguageCode'),
    'unordered_params' => array(
        'language' => 'Language',
        'site_access' => 'SiteAccess',
        'key' => 'Key',
        'placementid' => 'placementID'
    )
);
$FunctionList = array();
$FunctionList['preview'] = array();

?>