<?php
/**
 * Retrieves html for modifying blocks
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\block;

use \ezexceed\classes\ote\Response;
use ezexceed\operators\eZPencil;
use ezexceed\models\pencil\Pencil;
use \ezexceed\classes\ote\Controller;
use \ezexceed\models\BlockModel;
use \ezexceed\models\content\Object;

/**
 *
 * Description
 *
 * Retrieves html for modifying blocks.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 05.01.2012
 *
 */

class Block extends Controller
{
    /** @var BlockModel */
    protected static $model;

    /**
     * Loads the model data utilized by the different functions.
     */
    public static function init()
    {
        $objectId = self::$http->variable('objectId');

        if (strlen($objectId) > 0 && is_numeric($objectId))
        {
            self::$model = new BlockModel($objectId, self::$http->variable('language'));
            self::$model->load(self::$http->variable('zoneIndex'), self::$http->variable('blockId'));
        }
    }

    /**
     * Renders the template.
     *
     * @param bool $returnTemplates
     *
     * @return Response
     */
    public static function edit($returnTemplates = true)
    {
        $html = '';

        if ((bool)$returnTemplates) {

            $tpl = \eZTemplate::factory();

            $tpl->setVariable('zone_id', self::$model->pageModel->zoneIndex);
            $tpl->setVariable('zone', self::$model->pageModel->zone);
            $tpl->setVariable('block_id', self::$model->pageModel->blockId);
            $tpl->setVariable('block', self::$model->block);
            $tpl->setVariable('objectId', self::$model->pageModel->objectId);

            $html = $tpl->fetch('design:block/edit.tpl');
        }

        $name = self::$model->blockName;
        $nodes = self::$model->getAllNodes();
        $allowedClasses = self::$model->allowedClasses;

        return self::response(compact('html', 'name', 'nodes', 'allowedClasses'), array('type' => 'json'));
    }

    /**
     * Adds an item to the block.
     *
     * @param $nodeId
     * @param $objectId
     *
     * @return Response
     */
    public static function addItem($nodeId, $objectId)
    {
        self::$model->checkAddItem($nodeId, $objectId);
        self::$model->store();

        return self::getItemsHtml();
    }

    /**
     * Renders a pencil.
     *
     * @return Response
     */
    public static function renderPencil()
    {
        $model = new Pencil(self::$model->pageModel->contentObjectAttribute);
        $model->fill(self::$model->block);

        $operator = new eZPencil();
        $html = trim($operator->buildPencil($model));

        return self::response(compact('html'), array('type' => 'json'));
    }

    /**
     * Removes an item.
     *
     * @param $objectId
     *
     * @return Response
     */
    public static function removeItem($objectId)
    {
        self::$model->removeItem($objectId);
        self::$model->store();

        return self::response(array(), array('type' => 'json'));
    }

    /**
     * Saves rotation-values.
     *
     * @param $attributeKey
     * @param string $attributeValue
     *
     * @return Response
     */
    public static function saveRotationAttribute($attributeKey, $attributeValue = '')
    {
        self::$model->updateRotation($attributeKey, $attributeValue);
        self::$model->store();

        return self::getItemsHtml();
    }

    /**
     * Retrieves html for all items.
     *
     * @return string
     */
    protected static function getItemsHtml()
    {
        $tpl = \eZTemplate::factory();

        $tpl->setVariable('block', self::$model->block);

        $html = $tpl->fetch('design:block/edit/standard.tpl');

        return self::response($html, array('type' => 'json'));
    }

    /**
     * Updates fetch-parameters for a block.
     *
     * @param $attributeKey
     * @param string $attributeValue
     *
     * @return Response
     */
    public static function saveFetchAttribute($attributeKey, $attributeValue = '')
    {
        self::$model->updateFetchParameters($attributeKey, $attributeValue);
        self::$model->store();

        return self::edit();
    }

    /**
     * Saves a custom-attribute.
     *
     * @param $attributeKey
     * @param string $attributeValue
     *
     * @return Response
     */
    public static function saveCustomAttribute($attributeKey, $attributeValue = '')
    {
        self::$model->updateCustomAttribute($attributeKey, $attributeValue);
        self::$model->store();

        $tpl = \eZTemplate::factory();
        $tpl->setVariable('block', self::$model->block);

        $html = $tpl->fetch('design:block/edit/_attributes.tpl');

        return self::response(compact('html'), array('type' => 'json'));
    }

    /**
     * Sorts the elements.
     *
     * @param $itemType
     *
     * @return Response
     */
    public static function sortItem($itemType)
    {
        $sortResults = self::$http->variable('sortResults');

        self::$model->sortItems($itemType, $sortResults);

        self::$model->store();

        return self::response(array(), array('type' => 'json'));
    }

    /**
     * Sets date and time for publication of an element.
     *
     * @param $itemType
     * @param $itemIndex
     * @param $date
     * @param $time
     *
     * @return Response
     */
    public static function setPublicationTimestamp($itemType, $itemIndex, $date, $time)
    {
        $modified = self::$model->updatePublicationTimestamp($itemType, $itemIndex, $date, $time);

        self::$model->store();

        return self::response(compact('modified'), array('type' => 'json'));
    }
}

Block::init();
