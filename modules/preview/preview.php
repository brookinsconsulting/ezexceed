<?php
/**
 * Generate preview url for object version
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\preview;

use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;
use \ezexceed\models\content\Object;
use \ezexceed\classes\utilities\PreviewUtilities;
use \ezexceed\classes\utilities\IniUtilities;

use \eZContentObject;

/**
 *
 * Description
 *
 * Generate preview url for object version.
 *
 * @author Bjørnar Helland / bh@keyteq.no
 * @since 08.10.2012
 *
 */

class Preview extends Controller
{
    /**
     * Returns the preview url
     *
     * @param int $id
     *
     * @return Response
     */
    public static function url($objectId, $languageCode = false, $siteAccess = false)
    {
        $ok = false;
        $url = '';

        $eZExceedINI = \eZINI::instance('ezexceed.ini');

        if (IniUtilities::checkGetSetting($eZExceedINI, 'eZExceedPushNotification', 'SignKey', false)
                && $contentObject = eZContentObject::fetch($objectId))
        {
            $contentINI = \eZINI::instance('content.ini');
            $displayMode = $contentINI->variable('VersionManagement', 'DisplayMode');
            $versionId = $contentObject->attribute('current_version');
            if ($displayMode == 'latest' && is_callable('eZContentObjectTreeNode::setUseCurrentUserDraft')) {
                if ($language = \eZContentLanguage::fetchByLocale($languageCode)) {
                    $draft = \eZContentObjectVersion::fetchLatestUserDraft(
                        $contentObject->attribute('id'),
                        \eZUser::currentUserID(),
                        $language->attribute('id'),
                        $contentObject->attribute('modified')
                    );

                    if ($draft instanceof \eZContentObjectVersion)
                        $versionId = $draft->attribute('version');
                }
            }

            $versionObject = $contentObject->version($versionId);
            if ($versionObject && $versionObject->attribute('can_read'))
            {
                $hash = PreviewUtilities::createKey(\eZSys::hostname(), $objectId, $versionId, $languageCode);

                $url = "/ezexceed/preview/$objectId/$versionId";
                if ($languageCode)
                    $url .= "/$languageCode";
                $url .= "/key/$hash";

                if ($siteAccess)
                    $url .= "/site_access/$siteAccess";

                \eZURI::transformURI($url, false, 'full');
                $ok = true;
            }
        }
        $response = array('ok' => $ok, 'url' => $url);

        return self::response($response, array('type' => 'json'));
    }
}
