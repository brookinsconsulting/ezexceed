<?php
/**
 * Handles the nodestructure
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\nodestructure;

use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;
use \ezexceed\models\nodestructure\NodeTree;
use \ezexceed\models\nodestructure\NodeStructureOperations;
use \ezexceed\models\nodestructure\NodeOperations;
use \ezexceed\classes\utilities\Objects;
use \ezexceed\models\content\Object;

/**
 * Description
 *
 * Handles the nodestructure.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 22.03.2012
 */
class NodeStructure extends Controller
{
    /** @var NodeStructureOperations */
    protected static $model;

    /**
     * Initializes the model.
     *
     * @return void
     */
    public static function init()
    {
        self::$model = new NodeStructureOperations();
    }

    /**
     * Retrieves a single node.
     *
     * @param $nodeId
     * @param bool $containersOnly
     *
     * @return Response
     */
    public static function get($nodeId, $containersOnly = false)
    {
        $nodeTree = new NodeTree();

        /** @var $Params array */
        $nodeTree->init($nodeId, null, $containersOnly);
        
        /** @var $items array */
        $q = self::$http->getVariable('q');
        if (empty($q)) {
            $q = false;
        }
        
        $response = $nodeTree->buildTreeArray(true, $q);
        $response['success'] = true;

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Toggles the hidden-status of a node.
     *
     * @param $nodeId
     *
     * @return Response
     */
    public static function toggleVisibility($nodeId)
    {
        if (\eZOperationHandler::operationIsAvailable('content_hide'))
            \eZOperationHandler::execute('content', 'hide', array('node_id' => $nodeId), null, true);
        else
            \eZContentOperationCollection::changeHideStatus($nodeId);

        return self::response(array(), array('type' => 'json'));
    }

    /**
     * Copies an object.
     *
     * @return Response
     */
    public static function copy()
    {
        if ($response = self::$model->fetchCopyData())
            return self::response($response, array('type' => 'json'));

        $db = \eZDB::instance();
        $db->begin();

        /** Do not copy all versions. Only current. */
        $newObject = self::$model->object->copy(true);
        $newObject->setAttribute('published', 0);
        $newObject->store();

        $db->commit();

        $object = self::$model->object;

        $parentNodes = $object->parentNodes();
        foreach ($parentNodes as $node)
        {
            NodeOperations::addNode($newObject, $node->attribute('node_id'), $node->attribute('is_main'), $node->attribute('is_hidden'));
        }

        /**
         * Since all versions get the same modified timestamp and will be older than the contentobject, we
         * must check if the user has newer unpublished versions of the copied object
         */
        $translations = \eZContentObject::translationList();
        $versions = array();

        /** @var $language \eZLocale */
        foreach ($translations as $language)
        {
            $localeCode = $language->attribute('locale_code');
            if ($object->canEdit(false, false, false, $localeCode))
            {
                $languageObject = \eZContentLanguage::fetchByLocale($localeCode);
                $draft = Object::fetchLatestUserDraft($object->attribute('id'), \eZUser::currentUserID(), $languageObject->attribute('id'), false, $object->attribute('modified'));
                if (is_object($draft))
                    $versions[] = $draft->attribute('version');
            }
        }
        /**
         * Make sure the modified attribute is larger than the all other version objects
         */
        $modified = time() + 1;

        foreach ($versions as $version)
        {
            $versionObject = \eZContentObjectVersion::fetchVersion($version, $newObject->attribute('id'));
            $versionObject->setAttribute('modified', $modified);
            $versionObject->store();
        }

        return self::get($newObject->attribute('main_node_id'));
    }

    /**
     * A response indicating a successfull action.
     *
     * @return Response
     */
    protected static function success()
    {
        return self::response(array('success' => true), array('type' => 'json'));
    }

    /**
     * Deletes a node.
     *
     * @return Response
     */
    public static function delete()
    {
        if ($response = self::$model->fetchDeleteData())
            return self::response($response, array('type' => 'json'));

        if (\eZOperationHandler::operationIsAvailable('content_delete'))
            \eZOperationHandler::execute('content', 'delete', array('node_id_list' => self::$model->deleteIDArray, 'move_to_trash' => objects::moveToTrash()), null, true);
        else
            \eZContentOperationCollection::deleteObject(self::$model->deleteIDArray, Objects::moveToTrash());

        return self::success();
    }

    /**
     * Stores a nodes default sorting and updates priority of nodes if list is supplied
     *
     * @static
     * @param $nodeId
     * @return \ezexceed\classes\ote\Response
     */
    public static function setSort($nodeId)
    {
        /** @var $items array */
        $items = self::$http->postVariable('items');
        $sortfield = self::$http->postVariable('sortfield');
        /** @var $sortorder bool */
        $sortorder = self::$http->postVariable('sortorder');

        /** @var $node \eZContentObjectTreeNode */
        $node = \eZContentObjectTreeNode::fetch($nodeId);

        if (!is_object($node))
            return self::response(array('ok' => false, 'error' => 'Node not available'), array('type' => 'json'));
        if (!$node->canEdit())
            return self::response(array('ok' => false, 'error' => 'Access denied'), array('type' => 'json'));

        $response = array('ok' => true);

        if ($sortfield && $sortorder !== null)
        {
            /** Update the default sorting. */
            \eZContentOperationCollection::changeSortOrder($nodeId, $sortfield, $sortorder);
            $response['subtree'] = NodeTree::getSubTree($nodeId, array(
                    'limit' => 25,
                    'sort' => \eZContentObjectTreeNode::sortFieldName($sortfield),
                    'order' => $sortorder
                )
            );
        }

        if ($items) {
            /** Update priority to a list of nodes. */
            if ($node->attribute('sort_field') == \eZContentObjectTreeNode::SORT_FIELD_PRIORITY) {
                $sortOrder = (int)$node->attribute('sort_order');
                $priorityArray = array_keys($items);

                /** Find the next node after the last item of the submitted list. */
                $split = NodeTree::getSubTree($nodeId, array(
                        'limit' => 1,
                        'sort' => \eZContentObjectTreeNode::sortFieldName($node->attribute('sort_field')),
                        'order' => $sortOrder,
                        'offset' => count($items)
                    )
                );

                if (count($split['list']))
                {
                    $splitNode = $split['list'][0];
                    $splitPriority = (int) $splitNode['priority'];

                    $appendValue = ($sortOrder == 1) ? -1 : 1;

                    foreach ($priorityArray as &$value)
                    {
                        $value = $splitPriority + $appendValue;
                        if ($sortOrder == 1)
                            $appendValue--;
                        else
                            $appendValue++;
                    }
                    $priorityArray = array_reverse($priorityArray);
                }
                elseif ($sortOrder == \eZContentObjectTreeNode::SORT_ORDER_DESC) {
                    $priorityArray = array_reverse($priorityArray);
                }

                \eZContentOperationCollection::updatePriority($nodeId, $priorityArray, $items);

                foreach ($items as $id) {
                    \eZContentCacheManager::clearContentCache($id);
                }
            }
        }

        return self::response($response, array('type' => 'json'));
    }

    /**
     * Moves a node.
     *
     * @return Response
     */
    public static function move()
    {
        if ($response = self::$model->fetchMoveData())
            return self::response($response, array('type' => 'json'));

        /**
         * Check if new parent is container
         */
        $newParentNode = \eZContentObjectTreeNode::fetch(self::$model->newParentNodeId);
        if (!$newParentNode || !$newParentNode->attribute('is_container'))
            return self::response(array('error' => true, 'msg' => 'This content does not support subcontent'), array('type' => 'json'));

        if (\eZOperationHandler::operationIsAvailable('content_move'))
        {
            $db = \eZDB::instance();
            $db->begin();

            \eZOperationHandler::execute('content', 'move', array('node_id' => self::$model->nodeId, 'object_id' => self::$model->objectId, 'new_parent_node_id' => self::$model->newParentNodeId), null, true);

            $db->commit();
        }
        else
            \eZContentOperationCollection::moveNode(self::$model->nodeId, self::$model->objectId, self::$model->newParentNodeId);

        return self::get(self::$model->nodeId);
    }

    /**
     * Nodes with classes that is containers only.
     *
     * @param $containersOnly
     * @return array
     */
    public function nodeFilter($containersOnly)
    {
        $joins = '(ezcontentobject.published > 0 OR owner_id = ' . \ezuser::currentUserID() . ') AND ';

        if ($containersOnly) {
            $joins .= 'is_container = 1 AND';
        }

        $tables = null;

        return compact('joins', 'tables');
    }
}

NodeStructure::init();