<?php
/**
 * Simple controller for handling user-searches
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\SearchHandler;

use \ezexceed\models\search\HistoryFetcher;
use \ezexceed\models\search\Result;
use \ezexceed\models\Search;
use \ezexceed\classes\ote\Controller;
use \ezexceed\classes\ote\Response;

/**
 *
 * Description
 *
 * Simple controller for handling user-searches.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 24.10.2011
 *
 */
class SearchHandler extends Controller
{
    /**
     *
     * Returns a jsonencoded objekt of a searchResultModel
     *
     * @param string $q
     *
     * @return Response
     *
     */
    public static function search($q)
    {
        $search = new Search($q);

        $http = \eZHTTPTool::instance();
        $limit = $http->variable('limit');
        $classFilter = $http->variable('classFilter');

        if (!is_array($classFilter))
        {
            $broad = $search->performSearch($limit, $classFilter);
            if ($classFilter !== 'false') $classFilter = $broad->facetIds;
        }
        else $broad = $search->performSearch($limit);

        $facets = array();

        if (is_array($classFilter)) foreach($classFilter as $filter)
            $facets[] = $search->performSearch($limit, $filter);

        return self::response(compact('broad', 'facets'), array('type' => 'json'));
    }

    /**
     *
     * Retrieves or updates searchhistory based on the value of q.
     *
     * @return Response
     *
     */
    public static function history()
    {
        $q = \eZHTTPTool::instance()->variable('q');

        if ($q !== null)
            $update = HistoryFetcher::update($q);
        else
            $update = HistoryFetcher::retrieve(true);

        return self::response($update, array('type' => 'json'));
    }

    /**
     *
     * Clears the history for the current user.
     *
     * @return Response
     *
     */
    public static function clearHistory()
    {
        $update = HistoryFetcher::clear();

        return self::response($update, array('type' => 'json'));
    }
}
