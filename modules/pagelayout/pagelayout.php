<?php
/**
 * Class for retrieval of html for pagelayout-changes
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\modules\pagelayout;

use \eZTemplate;
use \ezexceed\models\layout\Layout;
use \eZHTTPTool;
use \ezexceed\classes\ote\Response;
use \ezexceed\classes\ote\Controller;

/**
 * Class for retrieval of html for pagelayout-changes.
 */
class Pagelayout extends Controller
{
    /** @var Layout */
    protected static $model;

    /**
     *
     * Retrieves available parameters and initializes model.
     *
     * @return void
     *
     */
    public static function initialize()
    {
        if (self::$http->hasPostVariable('objectId'))
            self::loadModel(self::$http->postVariable('objectId'), self::$http->postVariable('language'), self::$http->postVariable('zoneIndex'), self::$http->postVariable('blockId'));
    }

    /**
     *
     * Prepares the model for use.
     *
     * @param $objectId
     * @param $language
     * @param null $zoneIndex
     * @param null $blockIndex
     *
     */
    protected static function loadModel($objectId, $language, $zoneIndex = null, $blockIndex = null)
    {
        self::$model = new Layout($objectId, $language);
        self::$model->load($zoneIndex, $blockIndex);
    }

    /**
     *
     * Retrieves html for changing layout.
     *
     * @param $objectId
     * @param $language
     *
     * @return Response
     *
     */
    public static function edit($objectId, $language)
    {
        $tpl = eZTemplate::factory();

        try {
            self::loadModel($objectId, $language);
            $tpl->setVariable('model', self::$model);

            $html = $tpl->fetch('design:pagelayout/edit.tpl');
        }
        catch (\Exception $e) {
            $html = '<p>' . $e->getMessage() . '</p>';
        }

        return self::response(compact('html'), array('type' => 'json'));
    }

    /**
     *
     * Moves a block by drag and drop.
     *
     * @param int $positions
     *
     * @return Response
     *
     */
    public static function moveBlock($positions)
    {
        self::$model->moveBlock($positions);

        self::$model->store();

        return self::response(array(), array('type' => 'json'));
    }

    /**
     *
     * Removes a block.
     *
     * @return Response
     *
     */
    public static function removeBlock()
    {
        self::$model->removeBlock();

        self::$model->store();

        return self::response(array(), array('type' => 'json'));
    }

    /**
     *
     * Adds a block.
     *
     * @param $blockType
     *
     * @return Response
     *
     */
    public static function addBlock($blockType)
    {
        $block = self::$model->addBlock($blockType);
        self::$model->store();

        $tpl = eZTemplate::factory();

        $tpl->setVariable('zone', self::$model->getZone());
        $tpl->setVariable('block', $block);
        $tpl->setVariable('objectId', self::$model->getObjectId());

        $html = $tpl->fetch('design:pagelayout/block.tpl');

        $response = array
        (
            'html' => $html,
            'blockId' => $block->attribute('id'),
            'type' => $blockType
        );

        return self::response($response, array('type' => 'json'));
    }

    /**
     *
     * Autosaves a block-property.
     *
     * @param $attributeKey
     * @param $attributeValue
     *
     * @return Response
     *
     */
    public static function autosaveBlockAttribute($attributeKey, $attributeValue = '')
    {
        self::$model->saveBlockProperty($attributeKey, $attributeValue);

        self::$model->store();

        return self::response(array(), array('type' => 'json'));
    }

    /**
     *
     * Autosaves layout.
     *
     * @param $layout
     *
     * @return Response
     */
    public static function autosaveLayout($layout)
    {
        self::$model->saveLayout($layout);

        self::$model->store();

        return self::response(array(), array('type' => 'json'));
    }
}

PageLayout::initialize();