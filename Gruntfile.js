/* global __dirname, module */
var walkdir = require('walkdir');
var path = require('path');
var _ = require('lodash');

var baseUrl = 'design/ezexceed/javascript/';
var cssBase = 'design/ezexceed/stylesheets';

var hbsFiles = walkdir
    .sync(baseUrl + 'src/')
    .filter(function(f) { return f.match(/\.hbs$/); })
    .map(function(f) { return path.relative(__dirname, f); })
    .sort(function(a, b) { return a > b; });

var jsifySuffix = function(filename) { return filename.replace('.hbs', '.js'); };
var hbs = _.object(_.map(hbsFiles, jsifySuffix), hbsFiles);
var hbsModules = _.map(_.keys(hbs), function(f) {
    return f.replace(baseUrl + 'src/', '').replace('.js', '');
});
var sourceFiles = [
    baseUrl + 'src/**/*.js',
    baseUrl + 'libs/ezexceedlink/*.js',
    '!**/templates.js', '!**/templates/**/*js'
];


module.exports = function(grunt) {
    grunt.initConfig({
        jshint: {
            all: sourceFiles.concat(['Gruntfile.js']),
            options: {
                jshintrc: '.jshintrc'
            }
        },

        handlebars: {
            all: {
                options : {
                    amd: true,
                    wrapped : true,
                    namespace: false
                },
                files : hbs
            }
        },

        clean: {
            build: {
                src: baseUrl + 'compiled/'
            }
        },

        requirejs: {
            templates: {
                options: {
                    baseUrl: baseUrl + 'src/',
                    paths: {
                        handlebars: 'empty:'
                    },
                    out: baseUrl + 'src/templates.js',
                    include: hbsModules
                }
            },
            compile: {
                options: {
                    mainConfigFile: baseUrl + '/app.build.js',
                    removeCombined: false,
                    modules: [
                        {
                            name: 'ezexceed/main'
                        }
                    ]
                }
            }
        },

        watch: {
            hbs: {
                files: ['**/*.handlebar'],
                tasks: 'handlebars'
            },
            sass: {
                files: [cssBase + '/**/*.sass'],
                tasks: ['sass:dist']
            }
        },

        sass: {
            dist: {
                files: {
                    'design/ezexceed/stylesheets/eze.css': cssBase + '/sass/eze.sass' 
                }
            }
        },

        plato: {
            exceed: {
                options: {
                    jshint : grunt.file.readJSON('.jshintrc')
                },
                files: {
                    'reports/plato': sourceFiles
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-plato');

    grunt.registerTask('default', ['jshint', 'handlebars', 'sass', 'requirejs:templates']);
    grunt.registerTask('build', ['default', 'clean', 'requirejs:compile']);
};
