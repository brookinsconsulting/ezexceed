DROP TABLE `ezexceed_userdevice`;
CREATE TABLE `ezexceed_userdevice` (
    user_id int(11) NOT NULL,
    device_id varchar(128) NOT NULL,
    domain varchar(128) NOT NULL,
    name varchar(255) NOT NULL,
    `type` varchar(32) NOT NULL,
    os varchar(32) NOT NULL,
    os_version varchar(32) NOT NULL,
    notify int(1) NOT NULL,
    `uniquekey` varchar(128) NOT NULL,
    siteaccess varchar(255) NOT NULL,
    PRIMARY KEY (user_id, domain, device_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;