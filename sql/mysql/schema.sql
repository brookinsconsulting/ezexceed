CREATE TABLE `ezexceed_userdevice` (
    user_id int(11) NOT NULL,
    device_id varchar(128) NOT NULL,
    domain varchar(128) NOT NULL,
    name varchar(255) NOT NULL,
    `type` varchar(32) NOT NULL,
    os varchar(32) NOT NULL,
    os_version varchar(32) NOT NULL,
    notify int(1) NOT NULL,
    `uniquekey` varchar(128) NOT NULL,
    siteaccess varchar(255) NOT NULL,
    PRIMARY KEY (user_id, domain, device_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `ezexceed_activity` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `activitystring` text COLLATE utf8_bin NOT NULL,
  `time` int(11) NOT NULL,
  `user_id` int(20) NOT NULL,
  `replyto_id` int(20) NOT NULL,
  `contentobject_id` int(11) NOT NULL,
  `has_replies` int(1) NOT NULL,
  `has_likes` int(1) NOT NULL,
  `addressedto_id` INT( 11 ) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_contentobject_id` (`contentobject_id`,`replyto_id`,`id`),
  KEY `replyto_id` (`replyto_id`,`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `ezexceed_activity_like` (
  `activity_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`activity_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `ezexceed_activity_mention` (
  `activity_id` int(11) NOT NULL,
  `mentioned_user_id` int(11) NOT NULL,
  PRIMARY KEY (`activity_id`,`mentioned_user_id`),
  KEY `mentioned_user_id` (`mentioned_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

CREATE TABLE `ezexceed_activity_tag` (
  `activity_id` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`activity_id`,`tag`),
  KEY `tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


CREATE TABLE `ezexceed_activity_sticky` (
  `user_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
