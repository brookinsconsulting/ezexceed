requirejs({
    appDir: './',
    baseUrl: 'src/',
    dir: 'compiled/',
    map : {
        'select2' : {
            'jquery' : 'jquery-safe'
        },
        'jquery.timepicker' : {
            'jquery' : 'jquery-safe'
        }
    },
    paths: {
        'jquery' : 'empty:',
        'jqueryui' : 'empty:',
        'jqueryui/draggable' : 'empty:',
        'jquerypp' : '../libs/jquerypp',
        'jquery-safe' : '../libs/jquery.noconflict',
        'funky' : 'shared/funky',
        'select2' : '../libs/select2/select2',
        'jquery.timepicker' : '../libs/jquery.timepicker',
        'jquery-ui.touch' : '../libs/jquery.ui.touch-punch.min',
        'jquery.placeholder' : '../libs/jquery.placeholder.min',
        'underscore' : '../libs/underscore',
        'moment' : '../libs/moment',
        'date' : '../libs/date',
        'backbone.touch' : '../libs/backbone.touch',
        'backbone' : '../libs/backbone',
        'handlebars' : '../libs/handlebars.runtime'
    },
    shim: {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery-safe'],
            exports: 'Backbone'
        },
        'backbone.touch': {
            deps: ['backbone'],
            exports: 'Backbone'
        },
        'chosen': {
            deps: ['jquery-safe'],
            exports: 'jQuery.fn.chosen'
        },
        'jqueryui' : ['jquery'],
        'jquery.placeholder' : {
            deps : ['jquery-safe'],
            exports : 'jQuery.fn.placeholder'
        },
        'jquery.timepicker': {
            deps: ['jquery-safe'],
            exports: 'jQuery.fn.timePicker'
        },
        'jquery-ui.touch' : ['jqueryui'],
        'handlebars': {
            exports: 'Handlebars'
        }
    }
});
