/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone","shared/node"],function(e,t){return e.Model.extend({idAttribute:"parentNodeId",PRIORITY:8,DEFAULT_ORDER:0,searchString:"",defaults:function(){return{list:new t.collection}},initialize:function(){_.bindAll(this)},url:function(){var e="/ObjectEdit/getSubtree/"+this.id;return this.searchString.length>0&&(e+="/?searchString="+this.searchString),e},save:function(t){return e.sync("create",{url:"/NodeStructure/setSort/"+this.id},{data:t})},getSortedBy:function(){var e=_(this.get("sortMap")).invert();return parseInt(e[this.get("sort")],10)},more:function(){var t=this.get("limit"),n=["","ObjectEdit","getSubtree",this.get("parentNodeId"),this.get("offset")+t,t].join("/");return e.sync("read",{url:n},{}).done(this.onMore)},onMore:function(e){this.set({offset:this.get("offset")+e.subtree.count},{silent:!0}),this.get("list").add(e.subtree.list,{parse:!0})},parse:function(e){return e.hasOwnProperty("subtree")&&(e=e.subtree),e.hasOwnProperty("list")&&(e.list=new t.collection(e.list)),e}})});