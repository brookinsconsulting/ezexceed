/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view"],function(e){return e.extend({views:null,frozen:!0,init:function(){if(this.hasOwnProperty("freeze")&&this.hasOwnProperty("unFreeze")){var e=_.wrap(this.unFreeze,this.unFreezeWrapper);this.on({freeze:_.wrap(this.freeze,this.freezeWrapper),unfreeze:e}),e()}},freezeWrapper:function(e){if(this.frozen)return;_(this.views).invoke("trigger","freeze"),this.undelegateEvents(),e(),this.frozen=!0},unFreezeWrapper:function(e){if(!this.frozen)return;_(this.views).invoke("trigger","unfreeze"),this.delegateEvents(),e(),this.frozen=!1},remove:function(){_.invoke(this.views,"remove"),e.prototype.remove.apply(this)}})});