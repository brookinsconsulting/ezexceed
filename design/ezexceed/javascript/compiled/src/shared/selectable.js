/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

var dep="ontouchstart"in window?null:"select2";define([dep],function(){var e={width:"100%",containerCssClass:"eze",dropdownCssClass:"eze",dropdownCss:{"z-index":"2000000000"},minimumResultsForSearch:30};return function(t,n){return dep?t.select2(_.defaults(n||{},e)):!1}});