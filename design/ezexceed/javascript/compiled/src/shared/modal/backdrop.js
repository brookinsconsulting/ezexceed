/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","./templates/backdrop"],function(e,t){return e.extend({tagName:"section",className:"eze-bigmodal",tpl:"#tpl-kp-backdropmodal",view:null,initialize:function(e){_.bindAll(this),e=e||{},_.extend(this,_.pick(e,["view","tpl"]))},render:function(){return this.$el.html(t({})),this.view.$el=this.$(".eze-modal-content"),this},setRefreshOnClose:function(){this.options.closeRefresh=!0}})});