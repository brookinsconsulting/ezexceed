/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone"],function(e){var t=e.Model.extend({defaults:function(){return{name:"",identifier:"",iconFile:"/3d.png"}},initialize:function(){_.bindAll(this)},icon:function(e){var t=this.get("iconFile");return t[0]!=="/"&&(t="/"+t),this.collection.iconBaseUrl+"/"+[e,e].join("x")+t},parse:function(e){return e.hasOwnProperty("iconFile")&&(e.iconFile=e.iconFile.split("/").pop()),e}}),n=e.Collection.extend({model:t,iconBaseUrl:"",parse:function(e,t){var n=this.model.prototype.parse;return _.map(e,function(e){return e=n(e),_.isFunction(t)&&t(e),e})},byIdentifier:function(e){return this.find(function(t){return t.get("identifier")===e})}});return{collection:n,model:t}});