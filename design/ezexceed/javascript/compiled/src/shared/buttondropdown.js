/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","jquery-safe"],function(e,t){return e.extend({openClass:"open",initialize:function(){_.bindAll(this)},events:{"click .dropdown-toggle":"toggle"},enable:function(){this.$(".dropdown-toggle").removeAttr("disabled").removeClass("disabled")},disable:function(){this.$(".dropdown-toggle").attr("disabled",1).addClass("disabled")},toggle:function(e){e.stopPropagation();if(this.$el.hasClass(this.openClass)||this.$(e.currentTarget).attr("disabled"))return this.close();this.$el.addClass(this.openClass);var n=t(window).scrollTop(),r=n+t(window).height(),i=this.$(".dropdown-menu"),s=i.offset().top,o=s+i.outerHeight();o>r&&(i.css("bottom","100%"),i.css("top","auto")),_.delay(this.delayedBind,0)},delayedBind:function(){t(document).on("click",this.close)},close:function(){t(document).off("click",this.close),this.$el.removeClass(this.openClass)}})});