/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone"],function(e){var t=e.Model.extend({url:"/Object/preferences/",defaults:function(){return{type:"",data:[]}},initialize:function(){_.bindAll(this)},save:function(t){var n=_.extend(this.attributes,t);return e.sync("create",{url:this.url},{data:n})},parse:function(e){return e}}),n=e.Collection.extend({model:t});return{collection:n,model:t}});