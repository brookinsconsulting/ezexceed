/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","shared/templates/error"],function(e,t){return e.extend({base:!1,editor:null,attributeId:null,language:null,initialize:function(e){_.bindAll(this),this.init(e)},init:function(e){var t=this.$el.data();return _.extend(this,_.pick(t,["attributeBase","id","language","version"])),this.base=this.attributeBase,this.attributeId=this.id,_.extend(this,_.pick(e,["editor","classes","objectId"])),this},buildName:function(e){return this.base+"_"+e+"_"+this.attributeId},selectByInputName:function(e){return this.$('input[name$="'+this.buildName(e)+'"]')},parseEdited:function(e){var t=e.closest(".attribute-content"),n=":input:enabled, :checkbox",r=t.find(n);return e.is(n)||r.push(e),_(r).map(function(e){return{name:e.name,value:e.type==="checkbox"?+e.checked:this.$(e).val()}},this)},error:function(e){return this.$(".alert").remove(),this.$("label.heading").after(t({message:{text:e}})),this},success:function(){this.$(".alert").remove()},version:function(){return this.editor.getVersion(this.language)}})});