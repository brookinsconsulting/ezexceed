/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/views/action"],function(e){return e.extend({actionText:"Add content to the block",parentModel:null,initialize:function(e){return _.bindAll(this),_.extend(this,_.pick(e,["parentModel","actionText","validate"])),this.parentModel.blockNodes.on("add remove",this.render),this},validate:function(e){var t=this.parentModel.get("allowedClasses"),n=_(t).any(function(t){return e.get("class").get("identifier")===t});return t.length===0&&(n=!0),!this.parentModel.nodeExists(e)&&n}})});