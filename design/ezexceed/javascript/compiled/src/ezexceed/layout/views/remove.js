/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["./add"],function(e){return e.extend({parentModel:null,statusText:"The content is already added",actionText:"Remove from the block",confirm:!0,primary:!0,validate:function(e){return this.parentModel.nodeExists(e)}})});