/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","./model","./asyncstatus"],function(e,t,n){return e.extend({currentStatus:null,asyncStatusView:null,initialize:function(e){_.bindAll(this),this.model=new t,_.extend(this.model,_.pick(e,["objectId","language"])),this.model.on("change",this.render),this.on("change",this.statusChanged);var r={callback:this.model.fetch,el:this.$el};return this.asyncStatusView=new n(r),this},statusChanged:function(e){if(this.currentStatus==="changed"&&e)return;this.model.fetch()},render:function(){this.currentStatus=this.model.get("globalStatus"),this.$el.removeClass("new changed published"),this.currentStatus==="publishing"?this.asyncStatusView.render():(this.asyncStatusView.stopPulsing(),this.$el.addClass(this.currentStatus))}})});