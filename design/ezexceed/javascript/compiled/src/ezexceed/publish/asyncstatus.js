/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","shared/flash"],function(e,t){return e.extend({callback:null,initialize:function(e){_.bindAll(this),_.extend(this,_.pick(e,["callback"]))},render:function(){this.$el.addClass("changed");var e={fadeInDuration:1500,fadeOutDuration:1e3,minOpacity:0};return this.$el.data("pulsing")||t.startPulsing(this.$el,e),_.delay(this.callback,2e3),this},stopPulsing:function(){t.stopPulsing(this.$el)}})});