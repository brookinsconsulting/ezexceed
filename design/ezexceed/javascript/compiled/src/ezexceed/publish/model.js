/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone","funky"],function(e,t){return e.Model.extend({objectId:"",language:"",defaults:function(){return{published:0,objects:new e.Collection}},initialize:function(){_.bindAll(this)},url:function(){return"/Publish/status/"+this.objectId+"/"+this.language},getGlobalStatus:function(e){var n="published";e.length>1&&(n="changed");if(e.length>0){var r=e.at(0);n=r.get("status"),e.filter(t.equalProp("status","publishing","get")).length&&(n="publishing")}return n},parse:function(t){return t.hasOwnProperty("objects")&&(t.objects=new e.Collection(t.objects),t.globalStatus=this.getGlobalStatus(t.objects)),t}})});