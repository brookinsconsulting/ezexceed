/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/views/action"],function(e){return e.extend({actionText:"Insert link",parentModel:null,locations:null,version:null,selfBlocking:!0,initialize:function(e){return e=e||{},_.bindAll(this),_.extend(this,_.pick(e,["parentModel","actionText","validate","version","locations"])),this},primaryAction:function(){eZExceed.stack.pop();var e=this,t=this.model.id;this.listenTo(eZExceed,"stack:popped",function(){e.trigger("select",t),e.stopListening()})}})});