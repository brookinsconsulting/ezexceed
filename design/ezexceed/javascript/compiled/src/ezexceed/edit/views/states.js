/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","module","selectable"],function(e,t,n){return e.extend({version:null,initialize:function(e){_.bindAll(this),_.extend(this,_.pick(e,["version"]))},events:{"change select":"changeState"},render:function(){return n(this.$("select")),this},changeState:function(e){var t=this.$(e.currentTarget);this.model.attr("states",this.version,[t.val()]),eZExceed.trigger("edit:state:change",{id:this.model.id,val:t.val()})}})});