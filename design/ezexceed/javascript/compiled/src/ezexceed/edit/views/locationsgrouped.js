/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone","shared/node","module"],function(e,t,n){var r=e.Model.extend({idAttribute:"classId",addLocation:function(e){this.get("locations").add(e),e.get("isMain")&&this.set("hasMain",!0)}}),i=e.Collection.extend({model:r,classes:null,initialize:function(){return this.classes=n.config().classes,this},comparator:function(e){return-e.get("locations").length},addLocations:function(e){var t,n;return e.each(function(e){t=e.get("parent").classId,n=this.get(t)||this.locationGroup(e,t),n.addLocation(e),this.get(t)||this.add(n)},this),this.sort(),this},locationGroup:function(e,n){var i=this.classes.get(n)?this.classes.get(n).get("name"):"";return new r({classId:n,name:i,hasMain:e.get("isMain"),locations:new t.collection})}});return i});