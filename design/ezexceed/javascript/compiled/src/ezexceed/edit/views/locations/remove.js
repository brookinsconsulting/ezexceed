/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/views/action"],function(e){return e.extend({statusText:"The content is placed on this page",actionText:"Remove from this page",confirm:!0,parentModel:null,locations:null,primary:!1,selfBlocking:!1,initialize:function(e){return _.bindAll(this),_.extend(this,_.pick(e,["parentModel","actionText","validate","version","locations"])),this.parentModel.on("autosave.saved",this.unLock),this},validate:function(e){return this.locations.length<=1?!1:this.locations.where({parentId:e.get("id")}).length>0},primaryAction:function(){var e=this.model.get("id");this.locations.each(function(t){t.get("parentId")===e&&this.removeLocation(t.id)},this)},removeLocation:function(e){if(this.locked)return!1;this.lock(),this.toggleActive(),this.selfBlocking||this.locations.remove(e),this.parentModel.once("autosave.saved",this.toggleActive),e===eZExceed.config.currentNodeId&&(eZExceed.currentNodeChanges.removed=!0);var t=[{action:"remove",location:e}];this.parentModel.attr("locations",this.version,t)}})});