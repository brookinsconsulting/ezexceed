/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","shared/node","./locations/add","./locations/remove","./locationsgrouped","module","ezexceed/edit/templates/locationsgrouped","shared/templates/objectlistitem"],function(e,t,n,r,i,s,o,u){return e.extend({objectEditView:null,language:null,locations:null,addActionView:null,removeActionView:null,finder:null,GROUP_LIMIT:8,$ezeItems:null,initialize:function(e){_.bindAll(this),_.extend(this,_.pick(e,["objectEditView","language"])),this.locations=this.model.get("locations"),this.locations.bind("add remove reset",this.render),this.initializeCustomActions()},initializeCustomActions:function(){var e={parentModel:this.model,version:this.objectEditView.getVersion(this.language),locations:this.locations};this.addActionView=new n(e),this.removeActionView=new r(e)},events:{"click button.add":"addLocation","click a.remove":"confirmRemoveLocation","click .eze-confirm-action":"confirmRemove","click .eze-confirm-abort":"confirmAbort","change input.radioselect":"changeMain","click .expand":"toggleGroup"},getClassGroup:function(e){return this.$(".groupedLocations-"+e)},toggleGroup:function(e){var t=this.$(e.currentTarget),n=t.data("classId"),r=this.getClassGroup(n),i=!r.hasClass("hide");t.children(".show").toggleClass("hide",!i),t.children(".hide-group").toggleClass("hide",i),r.toggleClass("hide",i)},renderLocationGroup:function(e){var t=e.get("classId"),n={classId:t,name:e.get("name"),hasMain:e.get("hasMain"),count:e.get("locations").length};this.$ezeItems.append(o(n)),this.getClassGroup(t).children("ul").html(this.renderLocations(e.get("locations")))},renderLocations:function(e){return e.map(function(e){var t={id:e.get("id"),classId:e.get("parent").classId,name:e.get("parent").name,url:e.get("url"),pathFormatted:e.get("pathFormatted"),removable:!1};return this.locations.length>0&&_.extend(t,{removable:!e.get("isMain"),radioselect:"Main",radioselectname:"mainnode",radioselectselected:e.get("isMain"),removeconfirmtext:"This location has subcontent! Removing it will also remove all subcontent. Are you sure you want to remove"}),u(t)},this)},render:function(){this.$ezeItems=this.$(".eze-items").html("");var e=(new i).addLocations(this.locations);e.each(function(e){var t=e.get("locations");t.length>this.GROUP_LIMIT?this.renderLocationGroup(e):this.$ezeItems.append(this.renderLocations(t))},this);var t=this.$("button.add"),n=t.length!==0,r=!n||this.locations.length===1;return this.$("a.remove").toggleClass("hide",r),this},getRootNodeId:function(){var e=s.config(),t=e.classRootNodes,n=this.model.get("class").get("identifier"),r=e.sitemapRootNodeId;return _(t).each(function(e){e.identifier===n&&(r=e.id)}),r},addLocation:function(e){e.preventDefault(),require(["ezexceed/finder"],this.loadFinder)},changeMain:function(e){var t=this.$(e.currentTarget).val();this.locations.invoke("set","isMain",!1),this.locations.get(t).set("isMain",!0),this.model.attr("locations",this.objectEditView.getVersion(this.language),{mainNode:t}),this.render()},loadFinder:function(e){this.removeActionView.setSelfBlocking(!0);var t={rootNodeId:this.getRootNodeId(),customActions:{add:this.addActionView,remove:this.removeActionView},containersOnly:!0},n={heading:["Add location to",{text:this.model.get("name"),quotes:!0}],icon:"Documents",render:!1};this.finder=eZExceed.stack.push(e.view,t,n).on("destruct",this.finderDestruct),this.model.on("autosave.saved",this.updateFinder,this),this.locations.on("add remove reset",this.addActionView.render,this),this.locations.on("add remove reset",this.removeActionView.render,this),eZExceed.trigger("edit:locations:add",{id:this.model.id})},updateFinder:function(){this.locations.reset(this.model.get("locations").models);var e=this.finder.state.objects.marked;e&&e.load({clearCache:!0})},finderDestruct:function(){this.locations.off("add remove",this.addActionView.render),this.locations.off("add remove",this.removeActionView.render),this.model.off("autosave.saved",this.updateFinder,this),this.removeActionView.setSelfBlocking(!1),this.finder=null},confirmRemoveLocation:function(e){e.preventDefault();var t=this.$(e.currentTarget),n=t.closest("li").data("id"),r=this.locations.get(n);parseInt(r.get("childrenCount"),10)>0?this.show(t.siblings(".confirm-remove")):this.removeActionView.removeLocation(n)},confirmRemove:function(e){var t=this.$(e.currentTarget).data("id");this.removeActionView.removeLocation(t),this.confirmAbort(e)},confirmAbort:function(e){var t=this.$(e.currentTarget);this.hide(t.closest(".confirm-remove"))}})});