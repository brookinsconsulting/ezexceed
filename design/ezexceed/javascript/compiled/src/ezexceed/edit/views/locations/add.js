/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/views/action"],function(e){return e.extend({actionText:"Place the content on this page",parentModel:null,locations:null,version:null,selfBlocking:!0,initialize:function(e){return _.bindAll(this),_.extend(this,_.pick(e,["parentModel","actionText","validate","version","locations"])),this.parentModel.on("autosave.saved",this.unLock),this},validate:function(e){var t=!0;return this.locations.each(function(n){var r=parseInt(this.model.get("id"),10),i=parseInt(n.get("id"),10),s=parseInt(n.get("parentId"),10);r===i&&(t=!1),s===r&&(t=!1);var o=this.splitPath(e.get("pathString"));_(o).indexOf(i.toString())!==-1&&(t=!1)},this),t},splitPath:function(e){return _(e.split("/")).filter(function(e){return _.isString(e)&&e.length>0})},primaryAction:function(){if(this.locked)return;this.lock(),this.toggleActive();var e=[{action:"add",location:this.model.id}];this.parentModel.once("autosave.saved",this.toggleActive),this.parentModel.attr("locations",this.version,e)}})});