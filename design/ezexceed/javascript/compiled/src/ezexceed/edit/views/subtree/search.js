/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","jquery-safe","ezexceed/edit/templates/subtree/search"],function(e,t,n){return e.extend({tagName:"form",className:"form-search",initialize:function(){return _.bindAll(this),this.search=_.debounce(this.search,250),this},events:{"keyup input":"search"},search:function(e){var n=t.trim(this.$(e.currentTarget).val());this.trigger("search",n)},focus:function(e){this.$("input").focus().val(e)},render:function(){return this.$el.html(n({})),this}})});