/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["jquery-safe","shared/datatype"],function(e,t){return t.extend({initialize:function(e){this.init(e),_.bindAll(this,"parseEdited")},parseEdited:function(){var t=[],n=null;return this.$("div.element input:enabled").each(function(r,i){i=e(i),n={name:i.attr("name"),value:i.attr("value")},t.push(n)}),t}})});