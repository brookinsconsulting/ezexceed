/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/datatype","module","selectable"],function(e,t,n){return e.extend({initialize:function(e){_.bindAll(this),this.init(e)},render:function(){return n(this.$("select")),this},parseEdited:function(){var e=_.pluck(this.$(":selected"),"value");return _.isArray(e)||(e=[e]),[{name:this.buildName("country"),value:e}]}})});