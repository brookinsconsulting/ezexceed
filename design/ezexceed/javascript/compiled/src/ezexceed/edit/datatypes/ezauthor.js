/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/datatype","jquery-safe","ezexceed/edit/templates/datatypes/ezauthor"],function(e,t,n){return e.extend({editor:null,initialize:function(e){this.init(e)},events:{"click .remove":"onRemove","click .add":"onAdd"},parseEdited:function(){var e=this.attributeBase,n=this.attributeId,r=[];return this.$("li").each(function(i){var s=t(this),o={};o[e+"_data_author_id_"+n]=i,o[e+"_data_author_name_"+n]=s.find(".eze-name").val(),o[e+"_data_author_email_"+n]=s.find(".eze-email").val(),r.push(o)}),r},onRemove:function(e){return e.preventDefault(),this.$(e.currentTarget).parents("li").remove(),this.model.attr(this.attributeId,this.version,this.parseEdited()),this},onAdd:function(e){return e.preventDefault(),this.$(".eze-items").append(n({})),this}})});