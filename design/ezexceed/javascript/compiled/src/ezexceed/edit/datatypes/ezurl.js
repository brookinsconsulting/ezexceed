/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/datatype"],function(e){return e.extend({urlElement:null,textElement:null,initialize:function(e){this.init(e),this.urlElement=this.selectByInputName("ezurl_url"),this.textElement=this.selectByInputName("ezurl_text")},parseEdited:function(){return[{name:this.urlElement.attr("name"),value:this.urlElement.val()},{name:this.textElement.attr("name"),value:this.textElement.val()}]}})});