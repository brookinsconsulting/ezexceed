/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/datatype","selectable"],function(e,t){return e.extend({initialize:function(e){_.bindAll(this),this.init(e)},parseEdited:function(){return _.map(this.$(".eze-save"),function(e){return e=this.$(e),{name:e.attr("name"),value:e.val()}},this)},render:function(){return t(this.$("select")),this}})});