/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone","shared/datatype","jquery-safe"],function(e,t,n){return t.extend({cache:{},initialize:function(e){return _.bindAll(this),this.init(e),this.inputEl=this.selectByInputName("ezkeyword_data_text"),this},render:function(){var t=this;this.inputEl.bind("keydown",function(e){e.keyCode===n.ui.keyCode.TAB&&n(this).data("autocomplete").menu.active&&e.preventDefault()}).autocomplete({minLength:0,search:function(){var e=t.extractLast(this.value);if(e.length<2)return!1},source:function(r,i){var s=t.extractLast(r.term);if(_(t.cache).has(s)){i(t.cache[s]);return}var o={url:"/Object/findKeywords/"+s};return e.sync("read",o).done(function(e){t.cache[s]=e,i(n.ui.autocomplete.filter(e,t.extractLast(s)))})},focus:function(){return!1},select:function(e,n){var r=t.split(this.value);return r.pop(),r.push(n.item.value),r.push(""),this.value=r.join(", "),t.editor.autosave({currentTarget:t.inputEl}),!1}})},split:function(e){return e.split(/,\s*/)},extractLast:function(e){return this.split(e).pop()}})});