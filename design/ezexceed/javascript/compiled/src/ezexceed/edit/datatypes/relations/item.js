/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["ezexceed/grid/views/item","ezexceed/edit/templates/datatypes/relations/item"],function(e,t){return e.extend({multiple:null,selected:!1,initialize:function(e){_.bindAll(this),_.extend(this,_.pick(e,["selected","multiple"])),this.selected&&this.model.set("selected",!0,{silent:!0}),this.listenTo(this.model,{"change:selected":this.toggleChecked,"change:name change:published change:modified published":this.render})},toggleChecked:function(e,t){return this.$(".select").attr("checked",t),this},render:function(){var e=this.model.get("class"),n=e?e.get("name"):"Unknown",r=e?e.id:!1,i=_.defaults({name:this.model.get("name")||"No name",classId:r,type:n,multiple:this.multiple},this.model.toJSON());return this.$el.html(t(i)),this}})});