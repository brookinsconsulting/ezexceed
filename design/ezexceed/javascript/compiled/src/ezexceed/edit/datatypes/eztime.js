/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/datatype","shared/timepicker"],function(e,t){return e.extend({$input:null,initialize:function(e){return _.bindAll(this),this.init(e),this},parseEdited:function(){var e=this.$input.val(),t="",n="",r="";if(e){var i=this.$input.val().split(":");i[0]&&(t=i[0]),i[1]&&(n=i[1]),i[2]&&(r=i[2])}return[{name:this.base+"_time_hour_"+this.attributeId,value:t},{name:this.base+"_time_minute_"+this.attributeId,value:n},{name:this.base+"_time_second_"+this.attributeId,value:r}]},render:function(){return this.$input=this.selectByInputName("time"),t(this.$input),this}})});