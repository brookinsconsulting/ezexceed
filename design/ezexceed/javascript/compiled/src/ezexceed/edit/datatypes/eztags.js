/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["jquery-safe","shared/datatype"],function(e,t){return t.extend({template:e("#tmpl-eztag-add"),initalize:function(e){_.bindAll(this,"render","addTag","removeTag"),this.init(e)},events:{"click .button-add-tag":"addTag","click .eztag-remove":"removeTag"},addTag:function(){var t=this.selectByInputName("eztags_tag"),n=t.val(),r=!1;this.$(".eztag-tags .eztag-tag").each(function(t,i){e.trim(e(i).html())===n&&(r=!0)});var i=this.$(".eztag-tags");n.length>0&&r===!1&&(i.append(this.template.tmpl({keyword:n})),t.val(""))},removeTag:function(t){t.preventDefault(),e(t.currentTarget).parent().parent().remove()},parseEdited:function(){},render:function(){this.$(".input-tag-name").unbind(),this.delegateEvents()}})});