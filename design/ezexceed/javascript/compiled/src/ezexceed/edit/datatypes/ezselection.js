/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["jquery-safe","shared/datatype","module","selectable"],function(e,t,n,r){return t.extend({initialize:function(e){return this.init(e),this.render()},render:function(){return r(this.$("select")),this},parseEdited:function(){var t=[],n=this.$("input:checked");n.length||(n=this.$("select option:selected"));if(n.length){var r=this;n.each(function(){var n={};n[r.base+"_ezselect_selected_array_"+r.attributeId]=e(this).val(),t.push(n)})}else t.push({name:this.base+"_ezselect_selected_array_"+this.attributeId,value:""});return t}})});