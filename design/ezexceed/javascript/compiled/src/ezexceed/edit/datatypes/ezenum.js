/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/datatype","module","selectable"],function(e,t,n){return e.extend({initialize:function(e){_.bindAll(this),this.init(e)},render:function(){return n(this.$("select")),this},parseEdited:function(){var e={id:[],values:[],elems:[]};return _.each(this.$(":selected"),function(t){t=this.$(t);var n=t.data();e.id.push(n.id),e.values.push(t.val()),e.elems.push(n.value)},this),[{name:this.buildName("data_enumvalue"),value:e.values},{name:this.buildName("select_data_enumelement"),value:e.values},{name:this.buildName("data_enumelement"),value:e.elems},{name:this.buildName("data_enumid"),value:e.id}]}})});