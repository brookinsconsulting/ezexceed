/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/datatype"],function(e){return e.extend({initialize:function(e){return _.bindAll(this),this.init(e),this},parseEdited:function(e){var t;return e.is(":checked")?t={name:e.attr("name"),value:e.attr("value")}:t={name:e.attr("name")+"x"},[t]},render:function(){return this}})});