/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","jquery-safe","ezexceed/notification/templates/refresh","jquerypp/animate"],function(e,t,n){return e.extend({tagName:"section",className:"eze-notification",appendTo:".eze-main",rendered:!1,textHeading:"",textDescription:"",displayTime:15e3,accepts:["appendTo","textHeading","textDescription","displayTime"],initialize:function(e){_.bindAll(this),e=e||{},_.extend(this,_.pick(e,this.accepts)),e={textHeading:this.textHeading,textDescription:this.textDescription},this.$el.html(n(e))},events:{click:"action"},render:function(){if(!this.rendered){this.hide(),this.$el.appendTo(this.appendTo),this.rendered=!0;var e=this.$el.height()+parseInt(this.$el.css("top"),10),t="-"+e+"px";this.$el.css("top",t),this.show();var n={top:"10px"},r=this;this.$el.animate(n,1e3,function(){_.delay(r.close,r.displayTime)})}return this},action:function(){this.trigger("action")},close:function(){return this.rendered&&(this.$el.detach(),this.remove(),this.rendered=!1),this.trigger("closed"),this}})});