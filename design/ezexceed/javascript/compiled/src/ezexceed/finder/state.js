/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone"],function(e){return e.Model.extend({objects:null,frozen:null,initialize:function(){_.bindAll(this),this.objects={marked:null,clipboard:null}},freeze:function(){if(this.frozen)return;this.objects.marked.off("fetched",this.onFetched,this),this.frozen=!0},unFreeze:function(){if(!this.frozen)return;this.objects.marked.on("fetched",this.onFetched,this),this.frozen=!1},mark:function(e,t){t=t||{},this.objects.marked&&this.objects.marked.off("fetched",this.onFetched,this),this.objects.marked=e,this.objects.marked.on("fetched",this.onFetched,this);if(!t.hasOwnProperty("silent")||!t.silent)this.trigger("mark",this.objects.marked),this.objects.marked.load()},onFetched:function(e){this.trigger("update",e)},clipboard:function(e,t){if(!e)return this.objects.clipboard;this.objects.clipboard={node:e,type:t},this.trigger("clipboard",e)},emptyClipboard:function(){this.objects.clipboard=null,this.trigger("clipboard")},setError:function(e,t){this.trigger("error",{msg:e,node:t})}})});