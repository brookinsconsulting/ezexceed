/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone","funky"],function(e,t){var n=e.Collection.extend({getActive:function(e){var n=this.find(t.truthyProp("active","get"));if(!n)return;return e?n.get(e):n},byChildId:function(e){return this.find(function(t){return t.get("view").model.get("children").contains(e)})},indexOfId:function(e){var t=this.find(function(t){return t.get("view").model.id===e});return t?t.get("view").index:-1}});return{collection:n}});