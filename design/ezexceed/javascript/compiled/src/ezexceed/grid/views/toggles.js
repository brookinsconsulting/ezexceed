/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["shared/view","ezexceed/grid/templates/toggles"],function(e,t){return e.extend({classes:null,$buttons:null,sort:"date",toggles:{trash:!1,mine:!1,published:!0},initialize:function(){_.bindAll(this)},events:{"click .sort-by button":"onSort","click .show button":"toggleShow"},toggleShow:function(e){this.$(".show .active").removeClass("active"),this.toggles=this.$(e.currentTarget).addClass("active").data(),eZExceed.trigger("find:show",{toggles:this.toggles})},onSort:function(e){this.$(".sort-by .active").removeClass("active"),this.sort=this.$(e.currentTarget).addClass("active").data("sort"),eZExceed.trigger("find:sort",{sort:this.sort})},render:function(){var e=this.getActive(),n=this.getSortBy(),r={trashEnabled:eZExceed.config.trashEnabled,sortby:{date:n==="date",author:n==="author",type:n==="type"},toggles:{published:e==="published",trash:e==="trash",mine:e==="mine"}};return this.$el.html(t(r)),this.$buttons=this.$("button"),this},getActive:function(){return this.toggles.trash?"trash":this.toggles.mine?"mine":"published"},getSortBy:function(){return this.sort}})});