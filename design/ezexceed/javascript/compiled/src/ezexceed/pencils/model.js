/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(["backbone"],function(e){return e.Model.extend({defaults:function(){return{attributeId:null,objectId:null,identifier:null,renderedOutput:null,language:null,version:null,versions:null}},initialize:function(){_.bindAll(this)},url:function(){var e=[this.get("objectId"),this.get("identifier"),this.get("language")];return"/ObjectEdit/getAttribute/"+e.join("/")},parse:function(t){return"versions"in t&&(t.versions=new e.Collection(t.versions)),t},save:function(t,n,r){e.sync("create",{url:n},{data:t,success:r})},removeInternalDrafts:function(){var t=this.get("versions").where({status:5});if(!t.length)return;var n={versions:_.pluck(t,"id")},r={url:"/Object/removeInternalDrafts/"+this.id};return e.sync("create",r,{data:n})}})});