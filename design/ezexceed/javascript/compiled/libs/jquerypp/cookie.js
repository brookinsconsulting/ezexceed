/**
     * @function jQuery.cookie
     * @parent jquerypp
     * @plugin jquery/dom/cookie
     * @author Klaus Hartl/klaus.hartl@stilbuero.de
     *
     * `jQuery.cookie(name, [value], [options])` lets you create, read and remove cookies. It is the
     * [jQuery cookie plugin](https://github.com/carhartl/jquery-cookie) written by [Klaus Hartl](stilbuero.de)
     * and dual licensed under the [MIT](http://www.opensource.org/licenses/mit-license.php)
     * and [GPL](http://www.gnu.org/licenses/gpl.html) licenses.
     *
	 * ## Examples
	 * 
	 * Set the value of a cookie.
	 *  
	 *      $.cookie('the_cookie', 'the_value');
	 * 
	 * Create a cookie with all available options.
	 *
     *      $.cookie('the_cookie', 'the_value', {
     *          expires: 7,
     *          path: '/',
     *          domain: 'jquery.com',
     *          secure: true
     *      });
	 *
	 * Create a session cookie.
	 *
     *      $.cookie('the_cookie', 'the_value');
	 *
	 * Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
	 * used when the cookie was set.
	 *
     *      $.cookie('the_cookie', null);
	 *
	 * Get the value of a cookie.
     *
	 *      $.cookie('the_cookie');
     *
     * @param {String} [name] The name of the cookie.
     * @param {String} [value] The value of the cookie.
     * @param {Object} [options] An object literal containing key/value pairs to provide optional cookie attributes. Values can be:
     *
     * - `expires` - Either an integer specifying the expiration date from now on in days or a Date object. If a negative value is specified (e.g. a date in the past), the cookie will be deleted. If set to null or omitted, the cookie will be a session cookie and will not be retained when the the browser exits.
     * - `domain` - The domain name
     * - `path` - The value of the path atribute of the cookie (default: path of page that created the cookie).
     * - `secure` - If true, the secure attribute of the cookie will be set and the cookie transmission will require a secure protocol (like HTTPS).
     *
     * @return {String} the value of the cookie or {undefined} when setting the cookie.
     */

define(["jquerypp/util/json","jquery"],function(e,t){(function(){t.cookie=function(e,n,r){if(typeof n=="undefined"){var f=null;if(document.cookie&&document.cookie!=""){var l=document.cookie.split(";");for(var c=0;c<l.length;c++){var h=t.trim(l[c]);if(h.substring(0,e.length+1)==e+"="){f=decodeURIComponent(h.substring(e.length+1));break}}}if(t.evalJSON&&f&&f.match(/^\s*\{/))try{f=t.evalJSON(f)}catch(p){}return f}r=r||{},n===null&&(n="",r.expires=-1),typeof n=="object"&&t.toJSON&&(n=t.toJSON(n));var i="";if(r.expires&&(typeof r.expires=="number"||r.expires.toUTCString)){var s;typeof r.expires=="number"?(s=new Date,s.setTime(s.getTime()+r.expires*24*60*60*1e3)):s=r.expires,i="; expires="+s.toUTCString()}var o=r.path?"; path="+r.path:"",u=r.domain?"; domain="+r.domain:"",a=r.secure?"; secure":"";document.cookie=[e,"=",encodeURIComponent(n),i,o,u,a].join("")}})(t)});