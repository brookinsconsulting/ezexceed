/*!
 * jQuery UI Progressbar @VERSION
 * http://jqueryui.com
 *
 * Copyright 2012 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * http://api.jqueryui.com/progressbar/
 *
 * Depends:
 *   jquery.ui.core.js
 *   jquery.ui.widget.js
 */

define(["jquery","./core","./widget"],function(e){(function(e,t){e.widget("ui.progressbar",{version:"@VERSION",options:{value:0,max:100},min:0,_create:function(){this.options.value=this._constrainedValue(),this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({role:"progressbar","aria-valuemin":this.min,"aria-valuemax":this.options.max,"aria-valuenow":this.options.value}),this.valueDiv=e("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element),this.oldValue=this.options.value,this._refreshValue()},_destroy:function(){this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.valueDiv.remove()},value:function(e){return e===t?this.options.value:(this._setOption("value",this._constrainedValue(e)),this)},_constrainedValue:function(e){var n;return e===t?n=this.options.value:n=e,typeof n!="number"&&(n=0),Math.min(this.options.max,Math.max(this.min,n))},_setOptions:function(e){var n=e.value;delete e.value,this._super(e),n!==t&&this._setOption("value",n)},_setOption:function(e,t){e==="max"&&(this.options.max=Math.max(this.min,t),this.options.value=this._constrainedValue()),e==="value"?this.options.value=this._constrainedValue(t):this._super(e,t),this._refreshValue()},_percentage:function(){return 100*this.options.value/this.options.max},_refreshValue:function(){var e=this._percentage();this.oldValue!==this.options.value&&(this.oldValue=this.options.value,this._trigger("change")),this.options.value===this.options.max&&this._trigger("complete"),this.valueDiv.toggle(this.options.value>this.min).toggleClass("ui-corner-right",this.options.value===this.options.max).width(e.toFixed(0)+"%"),this.element.attr("aria-valuemax",this.options.max),this.element.attr("aria-valuenow",this.options.value)}})})(e)});