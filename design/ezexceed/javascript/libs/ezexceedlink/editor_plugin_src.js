/* global tinymce */
(function()
{
    tinymce.create('tinymce.plugins.eZExceedLinkPlugin', {
        init : function(ed)
        {
            var linkView, modal;
            // Register commands
            ed.addCommand('mceeZExceedLink', function()
            {
                var se = ed.selection;

                // No selection and not in link
                if (se.isCollapsed() && !ed.dom.getParent(se.getNode(), 'A'))
                {
                    return;
                }

                require(['modal', 'jquery-safe'], function(Modals, $)
                {
                    var buttonId = ed.controlManager.get('ezexceedlink').id;
                    var modalOptions = {
                        pinTo : $('#' + buttonId),
                        direction : 'right'
                    };
                    modal = new Modals.regular(modalOptions);

                    require(['ezoe/link'], function(Link)
                    {
                        var options = {
                            el : modal.$content,
                            editor : ed
                        };

                        linkView = new Link(options);
                        modal.display(linkView, true);
                    });
                });
            });

            // Register button
            ed.addButton('ezexceedlink', {title : 'advanced.link_desc', cmd : 'mceeZExceedLink'});

            ed.onNodeChange.add(function(ed, cm, n, co)
            {
                cm.setDisabled('ezexceedlink', co && n.nodeName != 'A');
                cm.setActive('ezexceedlink', n.nodeName == 'A' && !n.name);
            });

            ed.onRemove.add(function()
            {
                if (modal) {
                    modal.close();
                }
            });
        },

        getInfo : function()
        {
            return {
                longname : 'eZExceed link',
                author : 'Keyteq AS',
                authorurl : 'http://www.keyteq.no',
                infourl : 'http://www.keyteq.no',
                version : tinymce.majorVersion + "." + tinymce.minorVersion
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add('ezexceedlink', tinymce.plugins.eZExceedLinkPlugin);
})();
