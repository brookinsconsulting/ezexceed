/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['backbone', 'shared/node'], function(Backbone, Node)
{
    /**
     * @class Shared.Subtree
     */
    return Backbone.Model.extend({
        idAttribute : 'parentNodeId',

        PRIORITY : 8,
        DEFAULT_ORDER : 0,
        searchString : '',

        defaults : function()
        {
            return {
                list : new Node.collection()
            };
        },

        initialize : function()
        {
            _.bindAll(this);
        },

        url : function()
        {
            var url = '/ObjectEdit/getSubtree/' + this.id;

            if (this.searchString.length > 0) {
                url += '/?searchString=' + this.searchString;
            }

            return url;
        },

        save : function(data)
        {
            return Backbone.sync('create', {url: '/NodeStructure/setSort/' + this.id}, {data: data});
        },

        getSortedBy : function()
        {
            var sortMap = _(this.get('sortMap')).invert();

            return parseInt(sortMap[this.get('sort')], 10);
        },

        more : function()
        {
            var limit = this.get('limit');
            var url = ['',
                'ObjectEdit',
                'getSubtree',
                this.get('parentNodeId'),
                this.get('offset') + limit,
                limit
            ].join('/');

            return Backbone.sync('read', {url : url}, {}).done(this.onMore);
        },

        onMore : function(response)
        {
            this.set({
                offset : this.get('offset') + response.subtree.count
            }, {silent: true});
            this.get('list').add(response.subtree.list, {parse : true});
        },

        parse : function(data)
        {
            if (data.hasOwnProperty('subtree')) {
                data = data.subtree;
            }

            if (data.hasOwnProperty('list')) {
                data.list = new Node.collection(data.list);
            }

            return data;
        }
    });
});
