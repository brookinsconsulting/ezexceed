/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
var dep = 'ontouchstart' in window ? null : 'select2';
define([dep], function()
{
    var selectableConfig = {
        width : '100%',
        containerCssClass: 'eze',
        dropdownCssClass: 'eze',
        dropdownCss : {
            'z-index':'2000000000'
        },
        minimumResultsForSearch : 30
    };
    return function(el, config)
    {
        return dep ? el.select2(_.defaults((config || {}), selectableConfig)) : false;
    };
});
