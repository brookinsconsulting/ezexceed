define(['jquery.timepicker'], function()
{
    return function(el, options)
    {
        options = _.defaults((options || {}), {
            className : 'eze eze-ontop',
            timeFormat : 'H:i',
            appendTo : 'body'
        });
        el.timepicker(options);
        return el;
    };
});
