/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

 define(['shared/view'], function(View)
 {
     /**
      * @class Shared.ContainerView
      */
    return View.extend(
    {
        views : null,
        frozen : true,

        init : function()
        {
            if (this.hasOwnProperty('freeze') && this.hasOwnProperty('unFreeze')) {

                var wrappedUnfreeze = _.wrap(this.unFreeze, this.unFreezeWrapper);

                this.on({
                    'freeze' : _.wrap(this.freeze, this.freezeWrapper),
                    'unfreeze' : wrappedUnfreeze
                });

                wrappedUnfreeze();
            }
        },

        freezeWrapper : function(func)
        {
            if (this.frozen) {
                return;
            }

            _(this.views).invoke('trigger', 'freeze');
            this.undelegateEvents();
            func();

            this.frozen = true;
        },

        unFreezeWrapper : function(func)
        {
            if (!this.frozen) {
                return;
            }

            _(this.views).invoke('trigger', 'unfreeze');
            this.delegateEvents();
            func();

            this.frozen = false;
        },

        remove : function()
        {
            _.invoke(this.views, 'remove');
            View.prototype.remove.apply(this);
        }
    });
 });
