var backboneDep = 'ontouchstart' in window ? 'backbone.touch' : 'backbone.keys';
define([backboneDep, 'shared/templates/loader'], function(Backbone, Loader)
{
    function canAnimate()
    {
        var el = document.body;
        var prefixes = 'Webkit Moz O'.split(' ');
        if (!el.style.animationName) {
            return _.any(prefixes, function(pre)
            {
                return el.style[pre + 'AnimationName' ] !== undefined;
            });
        }
        return true;
    }

    return Backbone.View.extend({
        animated : canAnimate(),
        animationTime : 400,
        _noTouch : [
            "html",
            "header button.pop-stack",
            ".eze-block-pencil .edit a",
            ".eze-block-pencil .add a",
            ".eze-block-pencil .eze-node a",
            ".eze-block-pencil>div",
            ".attribute-direct-edit",
            "header button.close-stack",
            ".alert .close",
            ".attr-group",
            ".enableEditor"
        ],
        hide : function(el)
        {
            el = el || this.$el;
            return el.addClass('hide');
        },
        show : function(el)
        {
            el = el || this.$el;
            return el.removeClass('hide');
        },
        fadeIn : function(el, duration, cb)
        {
            el = el || this.$el;
            duration = duration || 100;
            this.show(el).animate({opacity:1}, duration, cb);
            return el;
        },

        fadeOut : function(el, duration, cb)
        {
            el = el || this.$el;
            duration = duration || 100;
            el.animate({opacity:0}, duration, cb);
            return el;
        },

        // Deprecated, serves as a no op for now
        template: function()
        {
            return '';
        },

        // Render loader template
        _loader: function(options)
        {
            return Loader(options);
        },

        _useTouchHandlers : function(eventName, selector)
        {
            return this.isTouch && eventName === 'click' && this._noTouch.indexOf(selector) === -1;
        }
    });
});
