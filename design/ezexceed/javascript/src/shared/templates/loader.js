define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this, functionType="function";

function program1(depth0,data) {
  
  
  return "\n    <div class=\"loading-wrap eze-loader\">\n";
  }

function program3(depth0,data) {
  
  var stack1, options;
  options = {hash:{
    'size': (depth0.size)
  },data:data};
  return escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, depth0.loaderIcon, options) : helperMissing.call(depth0, "icon", depth0.loaderIcon, options)));
  }

function program5(depth0,data) {
  
  var stack1, options;
  options = {hash:{
    'size': (depth0.size)
  },data:data};
  return escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, "Loader", options) : helperMissing.call(depth0, "icon", "Loader", options)));
  }

function program7(depth0,data) {
  
  
  return "\n    </div>\n";
  }

  stack1 = helpers['if'].call(depth0, depth0.wrap, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n    <img alt=\"";
  if (stack1 = helpers.text) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.text; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"\n         src=\"";
  stack1 = helpers['if'].call(depth0, depth0.loaderIcon, {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"\n         class=\"loader eze-loader ";
  if (stack1 = helpers.className) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.className; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"/>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.statusText, options) : helperMissing.call(depth0, "translate", depth0.statusText, options)))
    + "\n\n";
  stack2 = helpers['if'].call(depth0, depth0.wrap, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n";
  return buffer;
  })

});