define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, self=this, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  
  return " btn-primary";
  }

  buffer += "    <div class=\"btn-group\">\n        <a class=\"btn dropdown-toggle";
  stack1 = helpers['if'].call(depth0, depth0.primary, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" data-toggle=\"dropdown\">\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.actionText, options) : helperMissing.call(depth0, "translate", depth0.actionText, options)))
    + "\n            <span class=\"caret\"></span>\n        </a>\n        <ul class=\"dropdown-menu\">\n            <li><p>";
  options = {hash:{},data:data};
  stack2 = ((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.statusText, options) : helperMissing.call(depth0, "translate", depth0.statusText, options));
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</p></li>\n            <li class=\"divider\"></li>\n            <li>\n                <a class=\"eze-confirm-action\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Yes, remove it", options) : helperMissing.call(depth0, "translate", "Yes, remove it", options)))
    + "</a>\n            </li>\n            <li>\n                <a>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "No, keep it", options) : helperMissing.call(depth0, "translate", "No, keep it", options)))
    + "</a>\n            </li>\n        </ul>\n    </div>\n";
  return buffer;
  })

});