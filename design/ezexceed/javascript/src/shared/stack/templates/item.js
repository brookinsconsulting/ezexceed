define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this, functionType="function";

function program1(depth0,data) {
  
  
  return " first";
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                <img class=\"icon-32\" src=\"";
  options = {hash:{
    'id': (depth0.classId),
    'size': (32)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\">\n                <img class=\"icon-16\" src=\"";
  options = {hash:{
    'id': (depth0.classId),
    'size': (16)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\">\n                ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n                    ";
  stack1 = helpers['if'].call(depth0, depth0.classIdentifier, {hash:{},inverse:self.program(8, program8, data),fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n                ";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                    <img class=\"icon-32\" src=\"";
  options = {hash:{
    'identifier': (depth0.classIdentifier),
    'size': (32)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\">\n                    <img class=\"icon-16\" src=\"";
  options = {hash:{
    'identifier': (depth0.classIdentifier),
    'size': (16)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\">\n                    ";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n                        ";
  stack1 = helpers['if'].call(depth0, depth0.icon, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n                    ";
  return buffer;
  }
function program9(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                        <img class=\"icon-32\" src=\"";
  options = {hash:{
    'size': (32)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, depth0.icon, options) : helperMissing.call(depth0, "icon", depth0.icon, options)))
    + "\"/>\n                        <img class=\"icon-16\" src=\"";
  options = {hash:{
    'size': (16)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, depth0.icon, options) : helperMissing.call(depth0, "icon", depth0.icon, options)))
    + "\"/>\n                        ";
  return buffer;
  }

function program11(depth0,data) {
  
  
  return "<p></p>";
  }

function program13(depth0,data) {
  
  
  return " close-stack";
  }

function program15(depth0,data) {
  
  
  return " pop-stack";
  }

function program17(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                    <img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, "StackClose", options) : helperMissing.call(depth0, "icon", "StackClose", options)))
    + "\"><span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Close", options) : helperMissing.call(depth0, "translate", "Close", options)))
    + "</span>\n                ";
  return buffer;
  }

function program19(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                    <img src=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, "StackBack", options) : helperMissing.call(depth0, "icon", "StackBack", options)))
    + "\"><span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Go back", options) : helperMissing.call(depth0, "translate", "Go back", options)))
    + "</span>\n                ";
  return buffer;
  }

  buffer += "<section class=\"stack-item ";
  stack1 = helpers['if'].call(depth0, depth0.first, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">\n    <header>\n        <h1>\n            <div class=\"iconwrap\">\n\n                ";
  stack1 = helpers['if'].call(depth0, depth0.classId, {hash:{},inverse:self.program(5, program5, data),fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n            </div>\n            <span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.heading, options) : helperMissing.call(depth0, "translate", depth0.heading, options)))
    + "</span>\n        </h1>\n\n\n        <div class=\"eze-autosave\">\n            ";
  stack2 = helpers['if'].call(depth0, depth0.autosave, {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            <button class=\"go-back";
  stack2 = helpers['if'].call(depth0, depth0.first, {hash:{},inverse:self.program(15, program15, data),fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"\n                data-multiple=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Close all", options) : helperMissing.call(depth0, "translate", "Close all", options)))
    + "\" data-one=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Close", options) : helperMissing.call(depth0, "translate", "Close", options)))
    + "\">\n\n                ";
  stack2 = helpers['if'].call(depth0, depth0.first, {hash:{},inverse:self.program(19, program19, data),fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n            </button>\n        </div>\n    </header>\n    <section class=\"eze-stack-content ";
  if (stack2 = helpers.className) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.className; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  if (stack2 = helpers.content) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.content; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "</section>\n</section>\n";
  return buffer;
  })

});