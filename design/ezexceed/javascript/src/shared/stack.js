define(['shared/view', 'backbone', 'jquery-safe', 'shared/flash', './templates/loader', './stack/templates/item'],
    function(View, Backbone, $, Flash, LoaderTemplate, ItemTemplate)
{
    return View.extend({
        tagName : 'section',
        id : 'eze-stack',
        headerHeight : 31,

        initialize : function()
        {
            _.bindAll(this);

            this.collection = new Backbone.Collection();
            this.on('destruct', this.close);

            this.loaderHtml = LoaderTemplate({
                loaderIcon : 'LoaderStack',
                className : 'icon-16',
                size: 16
            });

            return this;
        },

        events : {
            'click header button.pop-stack' : function() { this.pop(); },
            'click header button.close-stack' : 'closeAll'
        },

        closeAll : function()
        {
            if (this.collection.length > 0) {
                this.notifyViews('destruct', true, true);
                this.collection.reset();
            }
        },

        depth : function()
        {
            return this.collection.length;
        },

        replace : function(view, options, context)
        {
            this.closeAll();

            return this.push(view, options, context);
        },

        push : function(view, options, context)
        {
            _.defaults(context, {autosave : true});
            context.first = this.collection.length === 0;

            var wrapper = $(ItemTemplate(context));
            var heading = wrapper.find('header h1 span');

            // Insist on putting view inside of stacks content
            options.el = wrapper.find('.eze-stack-content');
            options.heading = heading;

            var stackedView = new view(options);
            stackedView.on('loaded', this.viewLoaded);

            if (context.autosave) {
                stackedView.on({
                    'stack.spinner.show save': this.spinnerShow,
                    'stack.spinner.remove saved': this.spinnerRemove
                });
            }

            this.notifyViews('freeze', false);

            var last = this.collection.last();
            var model = this.collection.push({
                view : stackedView,
                render : (context.hasOwnProperty('render') && context.render),
                wrapper : wrapper
            });

            this.pushToDOM(model, last);

            this.delegateEvents();

            this.trigger('push', stackedView);

            eZExceed.trigger('stack:push', this.getCurrentPath(), stackedView, options, context);

            return stackedView;
        },

        pop : function()
        {
            var model = this.collection.pop();

            var view = model.get('view');
            var args = arguments.length > 0 ? _.toArray(arguments) : [view.model];
            view.trigger.apply(view, ['destruct'].concat(args));

            if (this.collection.length > 0) {
                var next = this.collection.last();

                var wrapper = model.get('wrapper').addClass('move-out');
                var delay = this.animated ? 200 : 0;

                eZExceed.trigger('stack:pop', this.getCurrentPath(), view);
                _.delay(function(headingEl, headings)
                {
                    wrapper.removeClass('last');
                    next.get('wrapper').addClass('last');

                    headingEl.text(headings.one);
                    wrapper.remove();

                    // A view might need to know when the stack pop animation is completed. (KeyMedia)
                    view.trigger.apply(view, ['stack.popped'].concat(args));
                    eZExceed.trigger('stack:popped');
                    next.get('view').trigger('unfreeze');
                }, delay, this.firstHeading, this.headings);
            }
            return this;
        },

        // When new views are pushed to the stack, this method
        // responds and handles all DOM modifications
        pushToDOM : function(model)
        {
            var first = this.$('.last').removeClass('last move-in').length === 0;

            var classes = ['last'];
            if (!first) {
                classes.push('move-in');
            }

            var wrapper = model.get('wrapper')
                .addClass(classes.join(' '))
                .css('top', (this.collection.length - 1) * this.headerHeight);

            this.$el.append(wrapper);

            if (first) {
                this.firstHeading = this.$('.close-stack span');
                this.headings = this.$('.close-stack').data();
            }
            else {
                // Update previous stack-elements close-button
                this.firstHeading.text(this.headings.multiple);
            }

            // Pass down size of stack wrapper for this stack-content
            // to the view itself, this allows components like
            // Finder that requires this knowledge do this without
            // taking their own measurements
            var contentElement = wrapper.find('.eze-stack-content');
            var view = model.get('view');
            view.trigger('resize', {
                width : contentElement.outerWidth(),
                height : contentElement.outerHeight()
            });

            // Once animation (if any) is ended signal to the item
            // that this is a good time to display its content
            _.delay(function()
            {
                view.trigger('stacked');
            }, this.animated ? 500 : 0);

            if (model.get('render')) {
                view.render();
            }

            return this;
        },

        close : function()
        {
            this.notifyViews('destruct');
            this.$el.html('');
            this.collection.reset();
        },

        getAutosaveObject : function()
        {
            var last = this.collection.last();
            if (!last) {
                return false;
            }
            var element = last.get('wrapper').find('div.eze-autosave');
            var text = element.find('p');

            return {
                element : element,
                text : text
            };
        },

        spinnerShow : function()
        {
            var autosave = this.getAutosaveObject();
            if (!autosave) {
                return false;
            }

            autosave.element.addClass('saving');
            autosave.text.html(this.loaderHtml + ' ' + eZExceed.translate('Saving') + '…');
        },

        spinnerRemove : function(text)
        {
            var autosave = this.getAutosaveObject();
            if (!autosave) {
                return false;
            }

            autosave.element.removeClass('saving');
            autosave.text.text(
                text || '√ ' + eZExceed.translate('All changes saved')
            );

            // Remove all changes saved text after a while
            if (!autosave.text.data('pending')) {
                autosave.text.data('pending', _.delay(function()
                {
                    autosave.text.text('').data('pending', null);
                }, 1500));
            }

            eZExceed.publishStatus.trigger('change', true);
        },

        flashCloseAll : function()
        {
            Flash.bg(this.$('.close-stack'), {
                primaryColor : '#FF9C00',
                secondaryColor : '#fff',
                count : 4
            });
        },

        notifyViews : function(event, passModel, closeAll)
        {
            this.collection.each(function(model)
            {
                var view = model.get('view');

                if (_(view).isObject()) {
                    if (passModel) {
                        view.trigger(event, model.get('view').model, closeAll);
                    }
                    else {
                        view.trigger(event, null, closeAll);
                    }
                }
            });
        },

        viewLoaded : function()
        {
            this.trigger('loaded');
        },

        getCurrentPath : function()
        {
            return '/' + this.collection.map(function(item)
            {
                return item.get('view').name || 'undefined';
            }).join("/");
        }
    });
});
