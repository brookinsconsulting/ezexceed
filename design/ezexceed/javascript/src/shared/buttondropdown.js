/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
 
 define(['shared/view', 'jquery-safe'], function(View, $)
 {
    return View.extend({

        openClass : 'open',

        initialize : function()
        {
            _.bindAll(this);
        },

        events : {
            'click .dropdown-toggle' : 'toggle'
        },

        enable : function()
        {
            this.$('.dropdown-toggle').removeAttr('disabled').removeClass('disabled');
        },

        disable : function()
        {
            this.$('.dropdown-toggle').attr('disabled', 1).addClass('disabled');
        },

        toggle : function(e)
        {
            e.stopPropagation();

            if (this.$el.hasClass(this.openClass) || this.$(e.currentTarget).attr('disabled')) {
                return this.close();
            }

            this.$el.addClass(this.openClass);

            /**
             * Check if confirm-box is below view-port
             */
            var docViewTop = $(window).scrollTop(),
                docViewBottom = docViewTop + $(window).height(),
                confirm = this.$('.dropdown-menu');

            var elemTop = confirm.offset().top;
            var elemBottom = elemTop + confirm.outerHeight();

            if (elemBottom > docViewBottom) {
                confirm.css('bottom', '100%');
                confirm.css('top', 'auto');
            }

            _.delay(this.delayedBind, 0);
        },

        delayedBind : function()
        {
            $(document).on('click', this.close);
        },

        close : function()
        {
            $(document).off('click', this.close);
            this.$el.removeClass(this.openClass);
        }
    });
 });
