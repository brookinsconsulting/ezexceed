define([], function()
{
    return {
        truthyProp : function(prop, getter)
        {
            return function(model) { return getter ? model[getter](prop) : model[prop]; };
        },

        toggleProp : function(prop)
        {
            return function() { this[prop] = !this[prop]; };
        },

        equalProp : function(prop, value, getter)
        {
            return function(model) { return getter ? model[getter](prop) === value : model[prop] === value; };
        }
    };
});
