/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'shared/templates/objectlistitem'], function(View, Template)
{
    return View.extend({
        tagName : 'li',
        sortable : false,
        removable : true,
        editable : true,
        skip : false,

        attributes : function()
        {
            return {
                'class': 'object-by-id-' + this.model.id,
                'data-id': this.model.id,
                'data-object-id': this.model.id
            };
        },

        initialize : function(options)
        {
            _.bindAll(this);
            this.skip = {};
            _.extend(this, _.pick(options, ['sortable', 'removable', 'editable', 'skip']));
            this.model.on('fetched', this.render);
        },

        events : {
            'click .edit-btn' : 'onEdit',
            'click .remove' : 'onRemove'
        },

        render : function()
        {
            this.$el.html(Template(this.data()));
            return this;
        },

        onRemove : function(e)
        {
            e.preventDefault();
            this.collection.remove(this.model);
            return this;
        },

        onEdit : function(e)
        {
            var target = this.$(e.currentTarget);
            var data = target.data();
            var model = this.model;
            var update = this.update;
            require(['ezexceed/edit'], function(Edit)
            {
                var context = {
                    heading : {
                        text : data.name
                    },
                    classId : data.classId
                };

                eZExceed.stack.push(
                    Edit.view,
                    {model : model},
                    context
                ).on('destruct', update);
            });
            return this;
        },

        data : function()
        {
            var data =  {
                id : this.model.id,
                sortable : this.sortable,
                removable : this.removable,
                editable : this.editable,
                name : this.model.get('name'),
                pathFormatted : this.model.get('pathFormatted')
            };
            var contentClass = this.model.get('class');

            if (!_.contains(this.skip, 'url'))
                data.url = this.model.get('url');

            if (!_.contains(this.skip, 'className'))
                data.className = contentClass.get('name');

            if (!_.contains(this.skip, 'classId'))
                data.classId = contentClass.id;

            if (!_.contains(this.skip, 'status'))
                data.published = this.model.get('status');

            return data;
        },

        update : function()
        {
            this.model.fetch();
        }
    });
});
