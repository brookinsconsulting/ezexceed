define(['shared/view', 'shared/buttondropdown', 'shared/templates/action', 'shared/templates/confirm'],
    function(View, ButtonDropdown, ActionTemplate, ConfirmTemplate)
{
    /**
     * @class Shared.Action
     */
    return View.extend(
    {
        statusText : null,
        actionText : null,
        tagName : 'li',
        confirm : null,
        primary : true,
        selfBlocking : false,
        locked : false,
        buttonDropdown : null,
        validate : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['statusText', 'actionText', 'validate', 'confirm', 'primary']));
        },

        events : {
            'click .eze-confirm-action' : 'primaryAction',
            'click button.btn' : 'primaryAction'
        },

        toggleActive : function()
        {
            this.$('button').toggleClass('active');
        },

        setSelfBlocking : function(selfBlocking)
        {
            this.selfBlocking = selfBlocking;
        },

        lock : function()
        {
            if (this.selfBlocking) {
                this.locked = true;
            }
        },

        unLock : function()
        {
            if (this.selfBlocking) {
                this.locked = false;
            }
        },

        primaryAction : function(e)
        {
            this.trigger('action', e, this.model);

            this.lock();
        },

        validateModel : function()
        {
            if (this.validate !== null) {
                return this.validate(this.model);
            }

            return true;
        },

        render : function()
        {
            var options = {
                statusText : (_(this.statusText).isFunction()) ? this.statusText(this.model) : this.statusText,
                actionText : (_(this.actionText).isFunction()) ? this.actionText(this.model) : this.actionText,
                primary : this.primary
            };

            var template = ActionTemplate;
            if ((_(this.confirm).isFunction() && this.confirm(this.model)) || !_(this.confirm).isEmpty()) {
                template = ConfirmTemplate;
            }

            if (this.validateModel()) {
                this.$el.html(template(options));
                this.show();
            }
            else {
                this.hide();
            }

            this.delegateEvents();

            this.buttonDropdown = new ButtonDropdown({el : this.$('.btn-group')});

            return this;
        }
    });
});
