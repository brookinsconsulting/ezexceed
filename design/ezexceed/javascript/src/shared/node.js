/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone'], function(Backbone)
{
    /** @class Shared.Node */
    var Model = Backbone.Model.extend(
    {
        selected : false,
        nodeCache : null,
        containersOnly : false,
        searchString : '',

        defaults : function()
        {
            return {
                id : false,
                name : '',
                icon : '',
                hasChildren : true,
                childrenCount : 0,
                parentId : 0,
                objectId : 0,
                url : '',
                access : {
                    read : false,
                    edit : false,
                    remove : false,
                    move : false
                },
                children : new Collection()
            };
        },

        initialize : function(options)
        {
            _.bindAll(this);

            options = (options || {});

            _.extend(this, _.pick(options, ['nodeCache', 'containersOnly']));
        },

        url : function()
        {
            return '/NodeStructure/get/' + this.get('id');
        },

        /** Override Backbone.Model.fetch() to trigger a 'fetched' event. This is used for rendering the model async. */
        load : function(options)
        {
            options = _.clone(options || {});

            var cached = this.nodeCache.get(this.id);
            if (options.clearCache && cached) {
                this.nodeCache.remove(cached);
                cached = false;
                delete options.clearCache;
            }

            this.trigger('fetch');
            if (cached) {
                return this.fetched();
            }

            var url = '/NodeStructure/get/' + this.get('id');

            if (this.containersOnly) {
                url += '/1';
            }

            if (this.searchString) {
                url += '?q=' + this.searchString;
            }
            this.url = url;

            // We need to pass success as an option here even though that IS NOT
            // the recommended way of doing this. Normally chaining to .done() is correct
            // but only passed success/error handlers are transferred over to
            // the new ajax call that happens if this ajax call indicates the user
            // as being logged out.
            // If you rewrite this the Finder will not render after logging back in
            options = _.defaults(options, {
                parse: true,
                success: this.fetched
            });

            return this.fetch(options);

        },

        fetched : function()
        {
            var cached = this.nodeCache.get(this.id);

            if (!cached) {
                this.nodeCache.add(this);
            }
            else {
                this.set(cached.toJSON());
            }

            this.trigger('fetched', this);

            return this;
        },

        parent : function()
        {
            var parentId = this.get('parentId');
            if (parentId <= 1) {
                return false;
            }

            if (!this.nodeCache.get(parentId)) {
                var node = new Model({id : parentId, nodeCache : this.nodeCache});
                node.load();

                return node;
            }

            return this.nodeCache.get(parentId);
        },

        parse : function(resp)
        {
            var node;
            if (resp.hasOwnProperty('node')) {
                node = resp.node;
            }
            else {
                node = resp;
            }

            if (node.hasOwnProperty('hasChildren') && node.hasChildren) {
                node.children = new Collection(node.children, {parse: true});

                var cache = this.nodeCache;
                node.children.each(function(child)
                {
                    child.nodeCache = cache;
                    child.containersOnly = this.containersOnly;
                }, this);
            }

            if (node.hasOwnProperty('classId')) {
                node['class'] = eZExceed.classes.get(node.classId);
            }

            return node;
        },

        /**
        * Remove a child
        * @param model
        */
        removeChild : function(model)
        {
            var children = this.get('children');
            if (children && children.get(model.id)) {
                children.remove(model);
                this.set({childrenCount : children.length});
            }
        },

        /**
        * Add a child
        * @param model
        */
        addChild : function(model)
        {
            var children = this.get('children');
            if (children) {
                children.add(model);
                this.set({childrenCount : this.get('childrenCount') + 1});
            }
        },

        /**
        * Check if this node can be moved to node
        * @param node
        */
        canMoveTo : function(node)
        {
            if (node.id === this.id) {
                return false;
            }

            if (!node.get('isContainer')) {
                return false;
            }
            /** Traverse and check if is ancestor. */
            var tmpNode = node;
            while (tmpNode)
            {
                if (tmpNode.get('parentId') === this.id) {
                    return false;
                }

                tmpNode = tmpNode.parent();
            }

            return true;
        }

    });

    var Collection = Backbone.Collection.extend({
        model : Model,

        indexById : function(id) {
            var index = null;
            this.each(function(item, key) {
                if (item.id === id) {
                    index = key;
                }
            }, this);
            return index;
        }
    });

    return {
        model: Model,
        collection: Collection
    };
});
