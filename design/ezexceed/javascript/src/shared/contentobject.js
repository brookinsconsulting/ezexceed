/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['module', 'backbone', './node', './subtree', 'jquery-safe'],
    function(Module, Backbone, Node, Subtree, $)
{
    var classes = Module.config().classes;
    var Model = Backbone.Model.extend({
        defaults : function() {
            return {
                name : null,
                published : 0,
                modified : 0,
                ownerId : 0,
                currentVersion : 0,
                'status' : -1,
                'class' : null,
                locations : new Node.collection(),
                subtree : new Subtree(),
                versions : new Versions(),
                initialLanguage : null,
                versionsChanged : {}
            };
        },

        saveQueue : null,
        xhrQueue : null,

        initialize : function()
        {
            _.bindAll(this);

            this.saveQueue = {};
            this.xhrQueue = $({});
            this.versions = new Versions();
            this.selectedTranslations = new Versions();
            this.listenTo(this.versions, {change: this.onChange});
        },

        // Update a single attribute with values
        // Id should be the int id of the attribute
        // value can be anything from a simple string
        // to a complex object as the REST API spec
        // for that attribute
        attr : function(id, version, value, options)
        {
            if (typeof value === 'undefined') return false;

            options = (options || {});
            _.defaults(options, {silent : false});

            version = parseInt(version, 10);

            var model = this.versions.get(version);

            if (!model) {
                this.versions.add({id: version}, options);
                model = this.versions.get(version);
                model.set(id, value);
            }
            else {
                var previous = model.previous(id);
                if (_.isEqual(previous, value)) {
                    this.trigger('autosave.saved', this, false);
                }
                else {
                    model.set(id, value);
                }
            }
            return this;
        },

        getQueue: function() {
            return this.saveQueue;
        },

        // Mark attribute to be saved
        toBeSaved : function(id, version, field)
        {
            if (!this.saveQueue[version]) {
                this.saveQueue[version] = {};
            }

            if (!this.saveQueue[version][id]) {
                this.saveQueue[version][id] = field;
                return true;
            }

            return false;
        },

        // Handle raw updates on the data model
        // Final gate that handles creating a http
        // request and thus leaving the rest to the
        // backend
        onChange : function(model, changes, data)
        {
            if (!this.postUrl)
                throw "Object edit has no post url to use for saving";

            if (this.versions.length === 0)
                return;

            this.trigger('autosave.saving');

            var queue = this.versions.map(function(version)
            {
                return {
                    url : [this.postUrl, version.id].join('/'),
                    attributes : _(version.attributes).omit('id')
                };
            }, this);

            if (queue.length) {
                data = data || ({});
                _.each(queue, function(item)
                {
                    data.attributes = item.attributes;
                    this.xhrQueue.queue(_.bind(function(next)
                    {
                        this.trigger('autosave.saving');
                        _.defer(Backbone.sync, 'create', {url : item.url}, {
                            data : data,
                            success : this.onAutosaveDone,
                            error : this.onAutosaveFailed,
                            complete : next
                        });
                    }, this));
                }, this);
            }
            else
                this.xhrQueue.dequeue();

            return this;
        },

        // Fires after an autosave fails
        onAutosaveFailed : function(response)
        {
            this.trigger('autosave.failed', this, response.errors);
        },

        // Cleanup method after saving is done
        // Handles removing any now-saved attributes from the
        // changes-made model that is normally sent to backend
        onAutosaveDone : function(response)
        {
            if (!response.ok) {
                this.trigger('autosave.failed', this, response.errors);
                return false;
            }

            this.set(this.parse(response));
            this.trigger('autosave.saved', this, response);

            // Needed for views/publish to catch up?
            var attributes = response.attributes;
            attributes.id = parseInt(response.version, 10);
            this.versions.add(attributes);
            this.clearModel();

            // Update version for translation that received new draft
            var versions = this.get('versions');
            _.each(response.status.versions, function(version, lang)
            {
                var versionsToUpdate = versions.where({language: lang});
                _.invoke(versionsToUpdate, 'set', 'id', version);
            });
        },

        // Some times we need to clear the model to avoid
        // autosave on out-of-date content after publish, and similar actions
        clearModel : function()
        {
            this.saveQueue = {};
            this.versions.reset([], {silent: true});
        },

        // delete object
        remove : function(permanent)
        {
            if (typeof permanent === "undefined") permanent = false;
            this.url = '/Object/remove/' + this.id + '/' + (+permanent);
            return this.destroy();
        },

        removeUnused : function()
        {
            this.url = '/Object/removeUnused/' + this.id;
            return this.destroy();
        },

        removeInternalDrafts : function()
        {
            var versions = this.get('versions').where({status : 5});

            if (!versions.length)
                return;

            var data = {
                versions : _.pluck(versions, 'id')
            };
            var config = {url : '/Object/removeInternalDrafts/' + this.id};
            return Backbone.sync('create', config, {data : data});
        },

        removeDrafts : function(versions)
        {
            var data = {
                versions : versions
            };
            var config = {url : '/Object/removeDrafts/' + this.id};
            return Backbone.sync('create', config, {data : data});
        },

        // publish this object
        publish : function(versions, options)
        {
            versions = versions || ([]);
            options = options || ({});

            if (versions.length === 0) {
                versions = this.versionsChanged ? this.versionsChanged.pluck('version') : [this.get('changedVersion')];
            }
            
            if (versions.length === 0) {
                return;
            }

            options.data = {
                userId : this.get('ownerId'),
                versions : versions
            };
            var config = {url : '/Object/publish/' + this.id};
            var onPublished = this.onPublished;
            var beforePublish = _.bind(this.trigger, this, 'publish');
            this.xhrQueue.queue(function(next)
            {
                beforePublish();
                options.complete = next;
                Backbone.sync('create', config, options).done(onPublished);
            });
        },

        onPublished : function(response)
        {
            this.set(this.parse(response))
                .trigger('published', this);
        },

        // Restore object
        restore : function()
        {
            this.trigger('restore');
            var config = {url : '/Object/restore/' + this.id};
            var model = this;
            return Backbone.sync('create', config, {data: {}})
                .done(function(response)
                {
                    model.set(model.parse(response))
                        .trigger('restored', model);
                });
        },

        hasAnyChildren : function()
        {
            return this.get('locations').any(function(location)
            {
                return location.get('childrenCount') > 0;
            });
        },

        // create and save a new object
        create : function(data)
        {
            this.set(data);
            var model = this;

            var config = {url : this.url('create')};
            Backbone.sync('create', config, {data: data})
                .done(this.contentFetched, function()
                {
                    model.trigger('created', model);
                });
        },

        _url : false,
        url : function(action)
        {
            if (this._url) return _.result(this, '_url');
            action = (action || 'objects');
            return '/Object/' + action + '/' + (this.id || '');
        },

        fetch : function()
        {
            return Backbone.sync('read', this).done(this.contentFetched);
        },

        contentFetched : function(data)
        {
            this.set(this.parse(data));
            this.trigger('fetched', this);
        },

        parse : function(resp)
        {
            var data = null;
            if (resp.hasOwnProperty('object')) {
                data = resp.object;
            }
            else if (resp.length > 0) {
                data = resp[0];
            }
            else {
                data = resp;
            }

            if (data.hasOwnProperty('class')) {
                data['class'] = classes.get(data['class']);
            }
            else if (data.hasOwnProperty('classId')) {
                data['class'] = classes.get(data.classId);
            }
            else if (data.hasOwnProperty('contentClassId')) {
                data['class'] = classes.get(data.contentClassId);
            }

            /** A location was added. */
            if (resp.hasOwnProperty('location') && resp.location.ok) {
                if (this.has('locations') && resp.location.hasOwnProperty('node')) {
                    this.get('locations').add(resp.location.node);
                }
            }

            if (resp.hasOwnProperty('location') && resp.location.removed && resp.location.ok) {
                this.get('locations').remove(resp.location.removed);
            }

            /** First-run only. */
            if (data.hasOwnProperty('locations')) {
                data.locations = new Node.collection(data.locations);
            }

            if (data.hasOwnProperty('versions')) {
                var versions = new Versions(data.versions, {parse: true});

                /** Save returns only the versions altered. This causes internal-drafts in other languages not to be removed. */
                if (this.has('versions')) {
                    this.get('versions').each(function(version)
                    {
                        if (!versions.get(version.id)) {
                            versions.add(version);
                        }
                    });

                    data.versions = versions;
                }
            }

            if (data.hasOwnProperty('subtree')) {
                data.subtree = new Subtree(data.subtree, {parse : true});
            }

            return data;
        }
    });

    var Collection = Backbone.Collection.extend({
        model : Model,

        sortBy : 'date',

        searchCache : null,

        xhr : null,

        initialize : function()
        {
            _.bindAll(this);
            this.clearCache();
        },

        indexById : function(id)
        {
            var index = null;

            this.each(function(item, key)
            {
                if (item.id === id)
                    index = key;
            }, this);

            return index;
        },

        parse : function(data)
        {
            if (typeof data === 'object' && data.hasOwnProperty('objects')) {
                var objects = data.objects;
                if ('total' in data) this.total = data.total;
                if ('time' in data) this.time = data.time;
                return objects;
            }
            else if (typeof data === 'array')
                return data;
        },

        lastSearch : null,

        search : function(criteria)
        {
            var cacheKey = JSON.stringify(criteria);
            this.trigger('loading').trigger('search');
            this.lastSearch = criteria;
            if (cacheKey in this.searchCache) {
                this.onSearch(this.searchCache[cacheKey]);
                return false;
            }
            else {
                if (this.xhr) {
                    this.xhr.abort();
                }
                var cache = this.searchCache;
                // Coerces false => 0, true => 1
                var key;
                for (key in criteria) {
                    if (_.isBoolean(criteria[key])) {
                        criteria[key] = +criteria[key];
                    }
                }
                this.xhr = this.fetch({data: criteria})
                    .done(function(resp) { cache[cacheKey] = resp; }, this.onSearch);
                return this.xhr;
            }
        },

        more : function()
        {
            if (this.xhr) this.xhr.abort();
            if (this.total > this.length) {
                this.trigger('loading');
                var criteria = {};
                _.defaults(criteria, this.lastSearch);
                criteria.offset = this.length;
                this.xhr = Backbone.sync('read', this, {data: criteria})
                    .done(this.onMore);
                return this.xhr;
            }
        },

        onMore : function(resp)
        {
            return this.add(resp, {parse:true});
        },

        onSearch : function(resp)
        {
            this.trigger('fetched', resp);
            this.total = 0;
            return this.reset(resp, {parse:true});
        },

        clearCache : function()
        {
            this.searchCache = {};
        }
    });

    var Versions = Backbone.Collection.extend({
        model: Backbone.Model.extend({
            parse: function(data)
            {
                if (data) {
                    data.id = parseInt(data.id, 10);
                }
                return data;
            }
        })
    });

    return {
        model: Model,
        collection: Collection
    };
});
