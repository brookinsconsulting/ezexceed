define(['jquery-safe'], function($)
{
    var animateBackgroundColor = function(el, val, time)
    {
        return el.queue('flash', function(next)
        {
            el.animate({'background-color': val}, time);
            next();
        }).delay(time, 'flash');
    };

    return {
        bg : function(el, options)
        {
            options = options || ({});
            _.defaults(options, {
                primaryColor : '#fff',
                secondaryColor : '#000',
                time : 100,
                count : 3
            });

            el.css({'background-color' : options.primaryColor});

            // Animate with X flashing white background-effects
            while (options.count >= 0) {
                animateBackgroundColor(el, options.secondaryColor, options.time);
                animateBackgroundColor(el, options.primaryColor, options.time);
                options.count -= 1;
            }

            return el.dequeue('flash');
        },

        startPulsing : function(el, options)
        {
            el.data('pulsing', true).addClass('eze-pulsing');
            this.pulseAnimation(el, options);
            return el;
        },

        stopPulsing : function(el)
        {
            el = el || $('.eze-pulsing');
            return el.data('pulsing', '').stop().css('opacity', 1).removeClass('eze-pulsing');
        },

        pulseAnimation : function(el, options)
        {
            options = (options || {});
            var _this = this;

            _.defaults(options, {
                minOpacity : '.33',
                fadeOutDuration : 650,
                fadeInDuration : 650
            });

            el.animate({
                opacity : options.minOpacity
            }, options.fadeOutDuration, function()
            {
                el.animate({
                    opacity : 1
                }, options.fadeInDuration, function()
                {
                    if (el.data('pulsing')) {
                        _this.pulseAnimation(el, options);
                    }
                });
            });
        }
    };
});