define(['shared/view', 'modernizr', 'module', 'jquery-safe', 'jqueryui/datepicker'], function(View, Modernizr, Module, $)
{
    return View.extend({
        format : null,
        dateFormat : null,
        nativeSupport : false,
        iOS : false,
        init: false,

        initialize : function()
        {
            _.bindAll(this);
            _.extend(this, _.pick(Module.config(), ['format', 'dateFormat']));
            this.iOS = navigator.platform.indexOf("iPad") != -1 || navigator.platform.indexOf("iPhone") != -1;
            if (this.options.init) this.setup();
        },

        /**
         *
        * Initialize datepicker on a input type=date element.
        * Uses native html5 if supported
        *
        * @param el
        * @param options
         *
        */
        setup : function(el, options)
        {
            options = (options || {});
            if (el) {
                this.setElement(el);
            }

            this.nativeSupport = this.iOS || (Modernizr.inputtypes.date && this.el.type === 'date');

            var dateOptions = _.extend({
                dateFormat : this.dateFormat[this.format],
                disabled : true,
                firstDay : 1
            }, options);

            if (!this.nativeSupport) {
                var datepickerEl = this.$el.datepicker(dateOptions).datepicker("widget");
                var exceedJui = 'exceed-jui';
                if (!datepickerEl.parent().hasClass(exceedJui))
                    datepickerEl.wrap('<div class="' + exceedJui + '"></div>');
                this.$el.datepicker('enable');
                if (eZExceed.locale === 'nor-NO') {
                    $.datepicker.regional['nor-NO'] = {
                        clearText : "Tøm", clearStatus : "",
                        closeText : "Lukk", closeStatus : "",
                        prevText : "&laquo;Forrige", prevStatus : "",
                        prevBigText : "&#x3c;&#x3c;", prevBigStatus : "",
                        nextText : "Neste&raquo;", nextStatus : "",
                        nextBigText : "&#x3e;&#x3e;", nextBigStatus : "",
                        currentText : "I dag", currentStatus : "",
                        monthNames : ["Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"],
                        monthNamesShort : ["Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"],
                        monthStatus : "", yearStatus : "",
                        weekHeader : "Uke", weekStatus : "",
                        dayNamesShort : ["Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"],
                        dayNames : ["Søndag", "Mandag", "Tirsdag", "Onsdag", "Torsdag", "Fredag", "Lørdag"],
                        dayNamesMin : ["Sø", "Ma", "Ti", "On", "To", "Fr", "Lø"],
                        dayStatus : "DD", dateStatus : "D, M d",
                        dateFormat : "yy-mm-dd",
                        firstDay : 1,
                        initStatus : "", isRTL : false
                    };
                    $.datepicker.setDefaults($.datepicker.regional['nor-NO']);
                }
                else {
                    $.datepicker.setDefaults({
                        dateFormat : "yy-mm-dd"
                    });
                }
            }
            else {
                var dateValue = this.$el.data('value');
                if (dateValue) {
                    var date = new Date(dateValue);
                    if (date)
                        this.el.valueAsDate = date;
                }
            }
            return this;
        },

        /**
        * Get date as Date object from element
        * @param el
        */
        getDate : function(asObject)
        {
            var timestamp;
            if (this.nativeSupport) {
                timestamp = this.el.valueAsDate;
            }
            else {
                var val = this.$el.val();
                timestamp = this.$el.datepicker('getDate');
                if (!timestamp && val)
                    timestamp = Date.parse(val);
            }
            return asObject ? new Date(timestamp) : timestamp;
        }
    });
});
