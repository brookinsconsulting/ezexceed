define(['shared/modal/regular', 'shared/modal/backdrop'], function(Regular, Backdrop)
{
    return {
        regular: Regular,
        backdrop: Backdrop
    };
});
