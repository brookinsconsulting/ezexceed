/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['backbone'], function(Backbone)
{
    var Model = Backbone.Model.extend({
        url : '/Object/preferences/',

        defaults : function()
        {
            return {
                type : '',
                data : []
            };
        },

        initialize : function()
        {
            _.bindAll(this);
        },

        // Normal Backbone._sync should have worked here, but somehow
        // some strange bug in how backbone sync hands it off
        // makes it crash, so doing it old-style
        save : function(attributes)
        {
            var data = _.extend(this.attributes, attributes);
            return Backbone.sync('create', {url : this.url}, {data : data});
        },

        parse : function(data)
        {
            return data;
        }
    });

    var Collection = Backbone.Collection.extend({
        model : Model
    });

    return {
        collection: Collection,
        model: Model
    };
});
