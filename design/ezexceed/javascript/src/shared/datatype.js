/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'shared/templates/error'], function(View, ErrorTemplate)
{
    return View.extend({
        base : false,
        editor : null,
        attributeId : null,
        language : null,

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);
        },

        init : function(options)
        {
            var data = this.$el.data();

            _.extend(this, _.pick(data, ['attributeBase', 'id', 'language', 'version']));
            this.base = this.attributeBase;
            this.attributeId = this.id;

            _.extend(this, _.pick(options, ['editor', 'classes', 'objectId']));

            return this;
        },

        /**
        * Builds an input name following eZ-standards.
        *
        * @param name
        * @return {String}
        */
        buildName : function(name)
        {
            return this.base + '_' + name + '_' + this.attributeId;
        },

        /**
        * Retrieves an input by ez-standardname.
        *
        * @param name
        */
        selectByInputName : function(name)
        {
            return this.$('input[name$="' + this.buildName(name) + '"]');
        },

        // Receive the edited node and return the full
        // value for this datatype as needed for the model
        // before pushing to the REST API
        //
        // The full value can be a complex object derived from multiple values
        // from input fields, ajax call etc. Make sure to
        // deliver everything when this method is called,
        // regardless of what node is passed
        //
        // For simple nodes (text string, bool etc) the base method
        // should suffice
        parseEdited : function(node)
        {
            var containerEl = node.closest('.attribute-content');
            var selector = ':input:enabled, :checkbox';
            var elements = containerEl.find(selector);

            /** Make sure the original property is included. */
            if (!node.is(selector)) {
                elements.push(node);
            }

            return _(elements).map(function(el)
            {
                return {
                    name : el.name,
                    value : el.type === 'checkbox' ? +el.checked : this.$(el).val()
                };

            }, this);
        },

        error : function(message)
        {
            this.$('.alert').remove();
            this.$('label.heading').after(
                ErrorTemplate({message: {text : message}})
            );
            return this;
        },

        success : function()
        {
            this.$('.alert').remove();
        },

        version : function()
        {
            return this.editor.getVersion(this.language);
        }
    });
});
