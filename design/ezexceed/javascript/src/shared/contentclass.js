/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['backbone'], function(Backbone)
{
    var Model = Backbone.Model.extend({

        defaults : function()
        {
            return {
                name : '',
                identifier : '',
                iconFile : '/3d.png'
            };
        },

        initialize : function()
        {
            _.bindAll(this);
        },

        icon : function(size)
        {
            var iconFile = this.get('iconFile');
            if (iconFile[0] !== '/') iconFile = '/' + iconFile;
            return this.collection.iconBaseUrl + '/' + [size, size].join('x') + iconFile;
        },

        parse : function(data)
        {
            if (data.hasOwnProperty('iconFile')) {
                data.iconFile = data.iconFile.split('/').pop();
            }
            return data;
        }
    });

    var Collection = Backbone.Collection.extend({
        model : Model,
        iconBaseUrl : '',

        parse : function(models, filter)
        {
            var parse = this.model.prototype.parse;
            return _.map(models, function(model)
            {
                model = parse(model);
                if (_.isFunction(filter)) filter(model);
                return model;
            });
        },

        byIdentifier: function(identifier)
        {
            return this.find(function(model)
            {
                return model.get('identifier') === identifier;
            });
        }
    });

    return {
        collection: Collection,
        model: Model
    };
});
