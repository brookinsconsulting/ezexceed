/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', './templates/backdrop'], function(View, Template)
{
    return View.extend(
    {
        tagName : 'section',
        className: 'eze-bigmodal',
        tpl : '#tpl-kp-backdropmodal',
        view : null,

        initialize : function(options)
        {
            _.bindAll(this);
            options = (options || {});
            _.extend(this, _.pick(options, ['view', 'tpl']));
        },

        render : function()
        {
            this.$el.html(Template({}));
            this.view.$el = this.$('.eze-modal-content');
            return this;
        },

        setRefreshOnClose : function()
        {
            this.options.closeRefresh = true;
        }
    });
});
