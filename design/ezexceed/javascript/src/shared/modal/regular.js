/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'jquery-safe', './templates/regular'], function(View, $, ModalTemplate)
{
    return View.extend({
        tpl : 'modal/regular',
        tagName : 'div',
        className : 'popover',
        appendTo : '.eze-main',
        direction : 'right',
        _view : null,
        rendered : false,
        MARGIN : 20,
        frozen : null,

        accepts : ['tpl', 'appendTo', 'pinTo', 'direction'],

        initialize : function(options)
        {
            _.bindAll(this);
            options = (options || {});

            _.extend(this, _.pick(options, this.accepts));

            this.$el.addClass(this.direction).html(ModalTemplate({}));

            this.$content = this.$('.eze-content');
            $(document).bind('documentClicked', this.close);
        },

        render : function()
        {
            if (!this.rendered) {
                this.$el.appendTo(this.appendTo);
                this.rendered = true;
            }
            return this;
        },

        // Render a given view instance into this view
        display : function(view, render)
        {
            this._view = view;
            this.listenTo(view, {
                close : this.close,
                freeze : this.freeze,
                unfreeze : this.unfreeze
            });
            this.$('.eze-title').text(eZExceed.translate(_.result(this._view, 'heading')));
            this.render();

            this.$el.show();

            // Must be positioned after it's shown, else calculations is wrong
            if (render) view.render();

            this.position();

            return this;
        },

        freeze : function()
        {
            this.frozen = true;
            /**
             * hide method has been overridden in this
             */
            var view = new View();
            view.hide(this.$el);
        },

        unfreeze : function()
        {
            this.frozen = false;
            this.show();
        },

        position : function()
        {
            var resizeContent = false;
            var win = $(window);
            var viewportHeight = win.height();

            if (this.pinTo) {
                var offset;
                if (this.pinTo.parents('#eze-toolbar').length > 0) {
                    offset = this.pinTo.position();
                    if (offset.top === 0 && offset.left === 0)
                        offset = this.pinTo.offset();
                }
                else {
                    offset = this.pinTo.offset();
                }
                var width = this.$el.outerWidth();
                var height = this.$el.outerHeight();

                var css = {
                    'top' : offset.top,
                    'left' : offset.left
                };
                switch (this.direction) {
                    case 'left':
                        css.left = offset.left - width - this.MARGIN / 2;
                        css.top = offset.top - (height / 2) + (this.pinTo.outerHeight() / 2);
                        break;
                    case 'right':
                        css.left = offset.left + this.pinTo.outerWidth();
                        css.top = offset.top - (height / 2) + (this.pinTo.outerHeight() / 2);
                        break;
                    case 'top':
                        // not implemented
                        break;
                    case 'bottom':
                        css.left = offset.left - (width / 2) + (this.pinTo.outerWidth() / 2);
                        css.top = offset.top + this.pinTo.outerHeight();
                        if (css.top + height > viewportHeight) {
                            css.bottom = this.MARGIN;
                            resizeContent = true;
                        }
                        break;
                }

                var arrow = this.$('.arrow');
                // Make sure popover isn't outside browser's right edge
                var viewRightPos = win.width() - this.MARGIN;
                var rightPos = css.left + width;
                if (rightPos > viewRightPos) {
                    var diff = rightPos - viewRightPos;
                    css.left = css.left - diff;
                    // Adjust arrow
                    var arrowLeft = parseInt((width / 2) - (arrow.width() / 2) + diff, 10);
                    arrow.css('left', arrowLeft);
                }

                // Popover is above document start and thus partially hidden
                // we need to move it down, and adjust its arrow equally amounts up
                if (css.top < this.MARGIN) {
                    arrow.css('margin-top', css.top - this.MARGIN);
                    css.top = this.MARGIN;
                }

                // Popover is to the left of screen start
                // Adjust by adding the relevant margin
                if (css.left < this.MARGIN) {
                    arrow.css('margin-left', css.left - this.MARGIN);
                    css.left = this.MARGIN;
                }
                css['z-index'] = 9000001;
                this.$el.css(css);
            }

            if (resizeContent) {
                var content = this.$('.eze-content');
                var heading = this.$('.eze-title');
                content.css({
                    height : viewportHeight - content.offset().top - heading.outerHeight() - this.MARGIN
                });
            }

            return this;
        },

        hide : function()
        {
            if (this.rendered) {
                this.$el.detach();
                this.rendered = false;
            }

            this.trigger('closed');

            return this;
        },

        // Close and remove the modal from DOM
        // Triggers the `close` event
        close : function(e, target)
        {
            if (!this.rendered || this.frozen) {
                return this;
            }

            if (!target || this.$el.find(target).length === 0 && !this.$el.is(target)) {
                $(document).off('documentClicked', this.close);
                this.trigger('closed');
                if (this._view) this._view.remove();
                this.remove();
                this.rendered = false;
            }

            return this;
        }
    });
});
