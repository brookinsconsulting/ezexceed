define([
    'backbone',
    'shared/datatype',
    './datatypes/ezauthor',
    './datatypes/ezxmltext',
    './datatypes/ezbinaryfile',
    './datatypes/ezgmaplocation',
    './datatypes/ezimage',
    './datatypes/ezdatetime',
    './datatypes/ezdate',
    './datatypes/eztime',
    './datatypes/ezobjectrelation',
    './datatypes/ezobjectrelationlist',
    './datatypes/ezcountry',
    './datatypes/ezselection',
    './datatypes/ezmatrix',
    './datatypes/ezprice',
    './datatypes/ezoption',
    './datatypes/ezenum',
    './datatypes/ezkeyword',
    './datatypes/ezsrrating'
],
function(Backbone,
    Base, Author, XMLText, BinaryFile,
    Gmap, Image, eZDateTime, eZDate, Time, Relation, Relations,
    Country, Selection, Matrix, Price, Option, Enum, Keyword, eZSrRating)
{
    var Model = Backbone.Model.extend({
        _loaded : false,
        _handlers : null,

        // Return a handler for this type (ezauthor, ezstring etc)
        // and bind the passed node to that handler
        // Defaults to using `_base_` for every datatype
        // without a specific handler
        handler : function(type, node, options, cacheHandler)
        {
            options = (options || {});
            var handler = false;

            if (node) {
                var base = node.find('.attribute-base');
                if (base.length > 0 && base.data('handler')) {
                    var data = base.data();
                    handler = data.handler;

                    var config = _.pick(data, ['paths', 'shim']);
                    if (!_.isEmpty(config)) require.config(config);
                }
            }

            if (handler.length > 0) {
                if (handler.match(/\./)) {
                    handler = this.traverseNamespace(handler, window);
                }
                else {
                    // Handler is neatly tucked into a require js module!
                    handler = require([handler], function(Handler)
                    {
                        var result = new Handler(options);
                        result.type = type;
                        result.render();
                        if (cacheHandler) cacheHandler(result);
                    });
                    return;
                }
            }
            else {
                if (!this.has(type))
                    type = '_base_';

                handler = this.get(type);
            }

            if (handler) {
                var result = new handler(options);
                result.type = type;
                result.render();
                if (cacheHandler) cacheHandler(result);
            }
        },

        traverseNamespace : function(path, container)
        {
            var parts = path.split('.');
            var handler = container[parts.shift()];
            if (handler) {
                while (handler && parts.length > 0)
                    handler = handler[parts.shift()];
            }
            return handler;
        }
    });

    // These are the hardcoded-to-load Exceed overridden datatype handlers
    return new Model({
        _base_ : Base,
        ezauthor : Author,
        ezxmltext : XMLText,
        ezbinaryfile : BinaryFile,
        ezgmaplocation : Gmap,
        ezimage : Image,
        ezdatetime : eZDateTime,
        ezdate : eZDate,
        eztime : Time,
        ezobjectrelation : Relation,
        ezobjectrelationlist : Relations,
        ezcountry : Country,
        ezselection : Selection,
        ezmatrix : Matrix,
        ezprice : Price,
        ezoption : Option,
        ezenum : Enum,
        ezkeyword : Keyword,
        ezsrrating : eZSrRating
    });
});
