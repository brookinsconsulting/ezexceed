define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;

function program1(depth0,data) {
  
  
  return " hide";
  }

  buffer += "<li>\n    <div class=\"wrap\">\n        <img class=\"icon-16\" src=\"";
  options = {hash:{
    'id': (depth0.classId),
    'size': (16)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\">\n        <span class=\"object-name\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " (";
  if (stack2 = helpers.count) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.count; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + ")</span>\n        <div class=\"right-align\">\n            <a class=\"btn btn-mini expand\" data-class-id=\"";
  if (stack2 = helpers.classId) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classId; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">\n                <span class=\"show";
  stack2 = helpers['if'].call(depth0, depth0.hasMain, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Show all", options) : helperMissing.call(depth0, "translate", "Show all", options)))
    + "</span>\n                <span class=\"hide-group";
  stack2 = helpers.unless.call(depth0, depth0.hasMain, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Hide", options) : helperMissing.call(depth0, "translate", "Hide", options)))
    + "</span>\n            </a>\n        </div>\n    </div>\n</li>\n\n<li class=\"groupedLocations-";
  if (stack2 = helpers.classId) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.classId; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " locationGroup";
  stack2 = helpers.unless.call(depth0, depth0.hasMain, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">\n    <ul class=\"location-group-list\"></ul>\n</li>";
  return buffer;
  })

});