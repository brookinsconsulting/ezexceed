define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n    <td>\n        <input class=\"box\" type=\"text\" name=\"";
  if (stack1 = helpers.base) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.base; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "_ezmatrix_cell_";
  if (stack1 = helpers.attributeId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.attributeId; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "[]\" value=\"\" placeholder=\"...\" />\n    </td>\n    ";
  return buffer;
  }

  buffer += "<tr>\n    <td>\n        <input class=\"rowSelect\" type=\"checkbox\" name=\"";
  if (stack1 = helpers.base) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.base; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "_data_matrix_remove_";
  if (stack1 = helpers.attributeId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.attributeId; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "[]\" value=\"";
  if (stack1 = helpers.rowIndex) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.rowIndex; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"\n        title=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Select row for removal", options) : helperMissing.call(depth0, "translate", "Select row for removal", options)))
    + "\" />\n    </td>\n    ";
  stack2 = helpers.each.call(depth0, depth0.cells, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</tr>";
  return buffer;
  })

});