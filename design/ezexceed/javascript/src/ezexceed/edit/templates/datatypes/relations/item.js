define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<div class=\"thumb\" style=\"background:url("
    + escapeExpression(((stack1 = ((stack1 = depth0.images),stack1 == null || stack1 === false ? stack1 : stack1.thumb)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ") center center no-repeat;background-size:cover\">\n";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<div class=\"thumb\">\n    <img class=\"class-icon icon-48\" src=\"";
  options = {hash:{
    'id': (depth0.classId),
    'size': (48)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\">\n";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n            <input class=\"select\" type=\"checkbox\" ";
  stack1 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n            ";
  return buffer;
  }
function program6(depth0,data) {
  
  
  return " checked";
  }

function program8(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n            <input class=\"select\" name=\"eze-find-select\" type=\"radio\" ";
  stack1 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n            ";
  return buffer;
  }

  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.images),stack1 == null || stack1 === false ? stack1 : stack1.thumb), {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</div>\n\n\n<h2><a class=\"edit\">";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</a></h2>\n<p>\n    <img src=\"";
  options = {hash:{
    'id': (depth0.classId),
    'size': (16)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\" alt=\"";
  if (stack2 = helpers.type) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.type; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"icon-16\">\n    ";
  if (stack2 = helpers.type) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.type; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\n</p>\n\n<div class=\"actions\">\n    <div class=\"well well-small option-controller\">\n        <label>\n            ";
  stack2 = helpers['if'].call(depth0, depth0.multiple, {hash:{},inverse:self.program(8, program8, data),fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Select this content", options) : helperMissing.call(depth0, "translate", "Select this content", options)))
    + "\n        </label>\n    </div>\n</div>\n";
  return buffer;
  })

});