define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n    <td>\n        <input type=\"text\"\n            class=\"eze-save price input-small\"\n            name=\"";
  if (stack1 = helpers.base) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.base; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "_data_option_additional_price_";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n    </td>\n    ";
  return buffer;
  }

  buffer += "<tr>\n    <td>\n        <input type=\"text\" class=\"eze-save option input-medium\"\n            name=\"";
  if (stack1 = helpers.base) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.base; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "_data_option_value_";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n    </td>\n\n    ";
  stack2 = helpers.unless.call(depth0, ((stack1 = depth0.config),stack1 == null || stack1 === false ? stack1 : stack1.informationCollection), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n    <td>\n        <button type=\"button\" class=\"btn btn-danger btn-mini remove\" data-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Remove", options) : helperMissing.call(depth0, "translate", "Remove", options)))
    + "\n        </button>\n\n        <input type=\"hidden\"\n            class=\"eze-save id\"\n            value=\"";
  if (stack2 = helpers.index) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.index; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"\n            name=\"";
  if (stack2 = helpers.base) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.base; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "_data_option_id_";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">\n    </td>\n</tr>\n";
  return buffer;
  })

});