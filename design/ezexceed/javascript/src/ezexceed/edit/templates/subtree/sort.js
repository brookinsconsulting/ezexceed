define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression, self=this, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        <option value=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  stack1 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.cap),stack1 ? stack1.call(depth0, depth0.name, true, options) : helperMissing.call(depth0, "cap", depth0.name, true, options)))
    + "</option>\n    ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return " selected";
  }

function program4(depth0,data) {
  
  
  return " active";
  }

function program6(depth0,data) {
  
  
  return "disabled=\"disabled\" ";
  }

  buffer += "<select class=\"chzn-select\">\n    ";
  stack1 = helpers.each.call(depth0, depth0.sortOptions, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</select>\n<div class=\"pull-right btn-group order\">\n    <button type=\"button\" class=\"btn btn-small asc";
  stack1 = helpers['if'].call(depth0, depth0.asc, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" data-value=\"1\" ";
  stack1 = helpers['if'].call(depth0, depth0.asc, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">↑</button>\n    <button type=\"button\" class=\"btn btn-small desc";
  stack1 = helpers['if'].call(depth0, depth0.desc, {hash:{},inverse:self.noop,fn:self.program(4, program4, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\" data-value=\"0\" ";
  stack1 = helpers['if'].call(depth0, depth0.desc, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">↓</button>\n</div>\n";
  return buffer;
  })

});