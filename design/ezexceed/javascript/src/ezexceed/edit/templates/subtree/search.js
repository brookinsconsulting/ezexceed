define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  


  return "<div class=\"input-prepend\">\n    <span class=\"add-on\">\n        <i class=\"icon-search\"></i>\n    </span>\n    <input class=\"input-medium\" type=\"text\" placeholder=\"Type to search\" name=\"q\" />\n</div>";
  })

});