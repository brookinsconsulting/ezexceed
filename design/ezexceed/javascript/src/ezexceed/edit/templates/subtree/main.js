define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, self=this, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n    <div class=\"options-menu\">\n        <ul class=\"kp-edit-block-elements\">\n            ";
  stack1 = helpers.each.call(depth0, depth0.locations, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n            <li class=\"separator\"></li>\n        </ul>\n    </div>\n";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                <li class=\"location";
  stack1 = helpers['if'].call(depth0, depth0.checked, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">\n                    <a class=\"item\" data-id=\""
    + escapeExpression(((stack1 = depth0.id),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\">\n                        <span class=\"eze-checkbox\">\n                            <span></span>\n                        </span>\n                        <strong>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Show", options) : helperMissing.call(depth0, "translate", "Show", options)))
    + "</strong> ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "subcontent below", options) : helperMissing.call(depth0, "translate", "subcontent below", options)))
    + " "
    + escapeExpression(((stack1 = depth0.name),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\n                    </a>\n                </li>\n            ";
  return buffer;
  }
function program3(depth0,data) {
  
  
  return " active";
  }

function program5(depth0,data) {
  
  
  return "<ul class=\"eze-items\"></ul>";
  }

  stack1 = helpers['if'].call(depth0, depth0.showLocations, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n<h1>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Content under", options) : helperMissing.call(depth0, "translate", "Content under", options)))
    + " ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.currentLocation, options) : helperMissing.call(depth0, "translate", depth0.currentLocation, options)))
    + "</h1>\n<p class=\"description\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Edit and sort content under this page", options) : helperMissing.call(depth0, "translate", "Edit and sort content under this page", options)))
    + "</p>\n\n";
  stack2 = helpers['if'].call(depth0, depth0.hasAnyChildren, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n<button type=\"button\" class=\"btn show-more hide\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Show more", options) : helperMissing.call(depth0, "translate", "Show more", options)))
    + "</button>";
  return buffer;
  })

});