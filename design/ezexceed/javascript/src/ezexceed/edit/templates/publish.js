define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, self=this, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        <i class=\"status-icon ";
  stack1 = helpers['if'].call(depth0, depth0.notPublished, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\"></i>\n        <span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Publish this content", options) : helperMissing.call(depth0, "translate", "Publish this content", options)))
    + "</span>\n    ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "new";
  }

function program4(depth0,data) {
  
  
  return "changed";
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n        <i class=\"status-icon published\"></i>\n        <span>\n            ";
  stack1 = helpers['if'].call(depth0, depth0.publishing, {hash:{},inverse:self.program(9, program9, data),fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n        </span>\n    ";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "This content is being published", options) : helperMissing.call(depth0, "translate", "This content is being published", options)))
    + "…\n            ";
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "This content is published", options) : helperMissing.call(depth0, "translate", "This content is published", options)))
    + "\n            ";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n    <span class=\"text\">";
  options = {hash:{
    'context': ("eze")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "It has never been published", options) : helperMissing.call(depth0, "translate", "It has never been published", options)))
    + "</span>\n";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n    <span class=\"text\">";
  options = {hash:{
    'context': ("eze")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Last published", options) : helperMissing.call(depth0, "translate", "Last published", options)))
    + "</span>\n    <time datetime=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.dateFormat),stack1 ? stack1.call(depth0, depth0.modified, options) : helperMissing.call(depth0, "dateFormat", depth0.modified, options)))
    + "\">\n        ";
  options = {hash:{
    'format': ("dddd DD. MMM YY HH:mm")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.dateFormat),stack1 ? stack1.call(depth0, depth0.modified, options) : helperMissing.call(depth0, "dateFormat", depth0.modified, options)))
    + "\n    </time>\n";
  return buffer;
  }

function program15(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<button type=\"button\" class=\"btn btn-primary publish\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Publish now", options) : helperMissing.call(depth0, "translate", "Publish now", options)))
    + "</button>\n";
  return buffer;
  }

function program17(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<p class=\"revert\"><a>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Revert all changes", options) : helperMissing.call(depth0, "translate", "Revert all changes", options)))
    + "…</a></p>\n";
  return buffer;
  }

  buffer += "<h1>\n    ";
  stack1 = helpers['if'].call(depth0, depth0.publishable, {hash:{},inverse:self.program(6, program6, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</h1>\n\n<p class=\"description\">\n";
  stack1 = helpers['if'].call(depth0, depth0.notPublished, {hash:{},inverse:self.program(13, program13, data),fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</p>\n\n";
  stack1 = helpers['if'].call(depth0, depth0.publishable, {hash:{},inverse:self.noop,fn:self.program(15, program15, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  stack1 = helpers['if'].call(depth0, depth0.revert, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  return buffer;
  })

});