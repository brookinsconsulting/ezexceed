/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['shared/view', 'module', 'selectable'], function(View, Module, selectable)
{
    return View.extend({
        version : null,

        initialize : function(options)
        {
            _.bindAll(this);
            _.extend(this, _.pick(options, ['version']));
        },

        events : {
            'change select' : 'changeState'
        },

        render : function()
        {
            selectable(this.$('select'));
            return this;
        },

        changeState : function(e)
        {
            var target = this.$(e.currentTarget);
            this.model.attr('states', this.version, [target.val()]);
            eZExceed.trigger('edit:state:change', {
                id : this.model.id,
                val : target.val()
            });
        }
    });
});
