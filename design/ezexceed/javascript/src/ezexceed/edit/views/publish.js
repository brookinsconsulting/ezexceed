/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'ezexceed/publish/asyncstatus', 'ezexceed/edit/templates/publish'],
    function(View, ASyncStatus, Template)
{
    return View.extend({
        asyncStatusView : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['editor']));

            this.listenTo(this.model, {
                'autosave.saved' : this.editorSaved,
                published : this.render
            });
            this.listenTo(this.model.selectedTranslations, {
                'add remove reset' : this.render
            });
        },

        events : {
            'click .publish' : 'publish',
            'click .revert a' : 'revert'
        },

        publish : function()
        {
            this.$('.publish')
                .prop('disabled', true)
                .addClass('disabled')
                .after(this._loader({className:'icon-16',size:16}));

            var languages = this.model.selectedTranslations.pluck('id');
            var ids = this.model
                .get('versions')
                .chain()
                .filter(function(v) {
                    return _.contains(languages, v.get('language'));
                }).pluck('id').value();

            this.model.publish(ids, {success : this.update, error : this.onError});

            eZExceed.trigger('edit:publish', {id : this.model.id});
        },

        revert : function(e)
        {
            this.$(e.currentTarget).after(this._loader({className : 'icon-16', size : 16}));
            var languages = this.model.selectedTranslations.pluck('id');
            var versions = this.model.get('versions').filter(function(version)
            {
                return _.contains(languages, version.get('language'));
            });
            var ids = _.pluck(versions, 'id');

            this.model.removeDrafts(ids)
                .done(this.model.fetch)
                .fail(this.onError);
            eZExceed.trigger('edit:revert', {id : this.model.id});
        },

        update : function()
        {
            if (this.asyncStatusView === null) {
                eZExceed.publishStatus.trigger('change');
            }

            this.model.clearModel();
        },

        editorSaved : function()
        {
            return this.render();
        },

        onError : function(resp)
        {
            this.show(this.$('button.publish'));
            if (resp.errors && resp.errors.publish && resp.errors.publish.error) {
                this.show(this.$('.publish-errors').html(resp.errors.publish.error));
            }
        },

        renderAsyncStatus : function()
        {
            var asyncOptions = {
                callback : this.publish,
                el : this.$('i.status-icon')
            };

            this.asyncStatusView = new ASyncStatus(asyncOptions).render();
        },

        render : function()
        {
            var published = this.model.get('published');
            var publishing = this.model.get('publishing');
            var modified = this.model.get('modified');
            var languages = this.model.selectedTranslations.pluck('id');

            var changedVersions = _(this.model.get('versionsChanged')).pluck('language');
            var changed = _(languages).intersection(changedVersions).length;
            var notPublished = (!published || published === 0);
            var revert = false;

            if (changed) {
                revert = this.model.get('versions').any(function(v)
                {
                    return _.has(languages, v.get('language')) && v.get('status') === 0;
                });
            }

            var context = {
                published : published,
                publishing : publishing,
                modified : modified,
                notPublished : notPublished,
                publishable : (notPublished || changed),
                revert : revert
            };

            this.$el.html(Template(context));

            if (publishing) {
                this.renderAsyncStatus();
                this.asyncStatusView.$el.removeClass('new published');
            }
            else {
                this.asyncStatusView = null;
            }

            return this;
        }
    });
});
