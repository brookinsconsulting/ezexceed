/* global console */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['shared/view', 'shared/contentobject', 'jquery-safe', 
    './publish', 'ezexceed/activity', './subtree/main', './states', './locations', '../datatypes', 'shared/preferences', 'module'],
    function(View, Models, $, Publish, Activity, Subtree, States, Locations, Datatypes, Preferences, Module)
{
    return View.extend({
        requireAutosave : true,
        className : 'edit-object',
        translationVersions : null,
        mainLanguage : false,

        $scrollTarget : null,
        scrollTop : null,

        locations : null,
        states : null,
        publish : null,

        heading : null,

        name : 'edit',

        // Handlers for every attribute in the current edit view
        // These handlers are used for most of the interacting with
        // the attribute/datatype
        _handlers : null,

        events : {
            'change .eze-object-translations input' : 'showTranslations',

            'change .eze-object-attribute input[type=checkbox]' : 'autosave',
            'change .eze-object-attribute input[type=date]' : 'autosave',
            'change .eze-object-attribute select' : 'autosave',

            'keyup .eze-object-attribute :input' : 'debouncedAutosave',
            'change .eze-object-attribute :input' : 'debouncedAutosave',
            'focusout .eze-object-attribute :input' : 'autosave',

            /** eZXMLText */
            'change textarea' : 'autosave',

            // Alert boxes shall be closable
            'click .alert .close' : 'dismissAlert',

            // Toggle grouped attributes
            'click .attr-group' : 'toggleAttributeGroup'
        },

        initialize : function(options)
        {
            _.bindAll(this);
            this.$el.html(this._loader({
                text:"Loading editor",
                size : 32,
                className:"icon-32"
            }));

            this.datatypes = Datatypes;

            this.translationVersions = {};
            this._handlers = {};

            _.extend(this, _.pick(options, ['heading']));

            /** Only create new model object when an id is passed. */
            var values = _.pick(options, ['id', 'language', 'nodeId']);
            if ('id' in values) {
                values.initialLanguage = values.language;
                this.model = new Models.model(values);
            }

            this.listenTo(this.model, {
                'autosave.failed': this.onFailed,
                'autosave.saving': this.onSaving,
                'autosave.saved': this.onSaved,
                'change:name': this.updateHeading,
                'close': this.close,
                'fetched': this.render
            });

            var urlParams = ['ObjectEdit', 'get', this.model.id, this.model.get('initialLanguage')];
            this.model._url = '/' + urlParams.join('/');
            this.model.fetch();

            this.on({
                destruct: this.close,
                freeze: this.freeze,
                unfreeze: this.unfreeze
            });

            this.debouncedAutosave = _.debounce(this.autosave, 50);
            this.debouncedSave = _.debounce(this.saveAllVersions, 500);
        },

        unfreeze : function()
        {
            if (this.scrollTop) {
                this.$scrollTarget.scrollTop(this.scrollTop);
            }
        },

        freeze : function()
        {
            this.scrollTop = this.$scrollTarget.scrollTop();
        },

        close : function()
        {
            _.invoke(this._handlers, 'trigger', 'destruct');
            // If this is a new object that is created and closed without editing,
            // it can be removed
            if (this.model.get('status') === 0 && !this.model.get('versionsChanged').length)
                this.model.removeUnused();
            else {
                // Delete all internal drafts
                this.model.removeInternalDrafts();
            }
            this.model.off(null, null, this);
        },

        render : function()
        {
            var renderedOutput = this.model.get('renderedOutput');
            if (!renderedOutput || renderedOutput.length < 10) {
                if (console && console.log) {
                    console.log('ObjectEdit.render failed for object: ' + this.model.id);
                }
                return this;
            }

            _.invoke(this._handlers, 'trigger', 'destruct');
            this._handlers = {};

            this.$el.html(renderedOutput);

            // Store original value of every input as a data attributte
            // makes autosaving only on change a possibility
            _.map(this.$(':input'), this.storeOriginalValue);

            this.$('input[type="text"]').first().focus();

            /** Add post url to editObject model. */
            this.model.postUrl = this.$('#eze-edit-form').attr('action');

            if (this.model.has('versions')) {
                var mainVersion = this.model.get('versions').at(0);

                this.mainLanguage = mainVersion.get('language');
                this.mainVersion = parseInt(mainVersion.id, 10);
                this.model.version = parseInt(mainVersion.id, 10);
            }

            // Render subviews
            this.renderPublish()
                .renderTranslations()
                .renderLocations()
                .renderActivity()
                .renderSubtree()
                .renderStates();

            this.legacyRender();

            this.$scrollTarget = this.$('.eze-object-attributes');
            this.unfreeze();

            this.trigger('loaded');
            return this;
        },

        renderTranslations : function()
        {
            var translations = this.$('.eze-object-translations input');

            if (translations.length) {
                _(translations).each(function(field)
                {
                    field = this.$(field);
                    var data = field.data();
                    this.setVersion(data.language, data.version);
                }, this);
            }
            else {
                this.setVersion(this.mainLanguage, this.mainVersion);
            }

            if (!this.model.selectedTranslations.length) {
                var changedVersions = _(this.model.get('versionsChanged')).pluck('language');
                var versions = _(changedVersions).map(function(language)
                {
                    return {
                        id : language
                    };
                });
                this.model.selectedTranslations.add(versions);
            }

            this.updateTranslationSelections();

            // If this is an update of the edit view,
            // we must trigger checked on the selected languages
            _.each(this.model.selectedTranslations.pluck('id'), function(language)
            {
                var langEl = this.$('#eze-edit-language-' + language);
                if (langEl.not(':checked'))
                    langEl.click();
            }, this);

            return this;
        },

        renderPublish : function()
        {
            this.publish = new Publish({
                model : this.model,
                el : this.$('.eze-publish'),
                editor : this
            }).render();

            return this;
        },

        renderLocations : function()
        {
            var options = {
                el : this.$('.eze-locations'),
                model : this.model,
                language : this.mainLanguage,
                objectEditView : this
            };

            var locations = new Locations(options);

            this._handlers.locations = locations.render();

            return this;
        },

        renderSubtree : function()
        {
            /** Object doesn't have sub-content. */
            if (eZExceed.classes.get(this.model.get('class')).get('isContainer') !== 1) {
                return this;
            }

            var el = this.$('.subtree');

            if (el.length > 0) {

                this._handlers.subtree = new Subtree({
                    model : this.model,
                    el : el
                }).render();

                this.listenTo(this._handlers.subtree, {
                    save : this.subtreeSave,
                    saved : this.subtreeSaved
                });
            }

            return this;
        },

        subtreeSave : function()
        {
            this.trigger('stack.spinner.show');
        },

        subtreeSaved : function()
        {
            this.trigger('stack.spinner.remove');
        },

        renderStates : function()
        {
            this.states = new States({
                version : this.getVersion(),
                model : this.model,
                el : this.$('.states')
            }).render();
            return this;
        },

        renderActivity : function()
        {
            this._handlers.activity = new Activity.view({
                objectId : this.model.id,
                el : this.$('.eze-activity'),
                showHeading : true,
                showTabs : false
            });

            return this;
        },

        getAttributeGroup : function(named)
        {
            return this.$('#eze-attribute-group-' + named);
        },

        toggleAttributeGroup : function(e)
        {
            var heading = this.$(e.currentTarget);
            var target = this.getAttributeGroup(heading.attr('target'));
            if (target.hasClass('hide')) {
                target.removeClass('hide');
                heading.find('i').attr('class', 'icon-caret-down');
                heading.removeClass('closed').addClass('open');

                // Render each datatype as we open the grouping
                _.each(target.find('.eze-object-attribute:not(.hide)'), function(attrEl)
                {
                    attrEl = this.$(attrEl);
                    var handler = this._handlers[attrEl.data('id')];
                    if (handler) handler.render();
                }, this);
                eZExceed.trigger('edit:attributegroup:show', {id : this.model.id});
            }
            else {
                target.addClass('hide');
                heading.find('i').attr('class', 'icon-caret-right');
                heading.removeClass('open').addClass('closed');
                eZExceed.trigger('edit:attributegroup:hide', {id : this.model.id});
            }

            // Save preferences
            var settings = {};
            _.each(this.$('.attr-group'), function(el)
            {
                var node = $(el);
                var target = node.attr('target');
                settings[target] = this.getAttributeGroup(target).is(':visible') ? 1 : 0;
            }, this);

            var identifier = this.model.get('class').get('identifier');
            var pref = new Preferences.model();
            pref.save({
                type : 'eZExceed_category_group_' + identifier,
                data : settings
            });
            return this;
        },

        // Triggered on changes in the DOM on any attributes
        // regular form field.
        // Uses the handler for that attribute to parse the field
        // and return a final value for the attribute
        // to be sent to the model.
        autosave : function(e)
        {
            var field = $(e.currentTarget);
            if (!this.skipAutosave(field)) {

                var data = field.parents('.eze-object-attribute').data();
                var changed = this.model.toBeSaved(data.id, this.getVersion(data.language), field);
                var hasData = this.model.saveQueue && !_.isEmpty(this.model.saveQueue);

                if (changed || hasData) {
                    this.trigger('stack.spinner.show');
                    this.debouncedSave();
                }
            }
        },

        // Takes all queued up changes and moves them from the
        // queue to the actual model
        saveAllVersions : function()
        {
            var versions = this.model.getQueue();
            var changes = false;

            if (!_.isEmpty(versions)) {
                var handlers = this._handlers;
                var model = this.model;

                _(versions).each(function(ids, version)
                {
                    _(ids).each(function(field, id)
                    {
                        var handler = handlers[id];
                        if (handler && 'parseEdited' in handler) {
                            changes = true;
                            model.attr(id, version, handler.parseEdited(field));
                        }
                    });
                });
            }

            // If there were no changes make sure we dont leave anything hanging
            // in a bad state by thinking its still saving
            if (!changes) this.trigger('stack.spinner.remove');

            return this;
        },

        onSaving : function()
        {
            this.trigger('stack.spinner.show');
        },

        updateHeading : function()
        {
            this.heading.text(this.model.get('name'));
        },

        onSaved : function(model, response)
        {
            this.trigger('stack.spinner.remove');

            if (!response) return;

            this.$('.alert').remove();

            if (response && response.hasOwnProperty('name')) {
                this.model.set('name', response.name);
            }

            /** Notify attributes. */
            if (response && _(response).has('attributes')) {
                var attr = response.attributes;
                var handlers = this._handlers;
                _(attr).each(function(value, key)
                {
                    if (handlers[key] && handlers[key].success)
                        handlers[key].success(value, response);
                });
            }

            model.get('versions').each(function(version)
            {
                this.setVersion(version.language, version.id);
            }, this);
            this.mainVersion = response.version;

            /** If this is current node, mark it as changed. */
            if (model.id === eZExceed.config.currentObjectId)
                eZExceed.currentNodeChanges.changed = true;
        },

        onFailed : function(model, errors)
        {
            var handler;

            _(errors).each(function(error, key) {
                handler = this._handlers[key];
                if (handler && !_.isUndefined(handler.error)) {
                    handler.error(error.error);
                }

            }, this);

            this.trigger('stack.spinner.remove', eZExceed.translate('Error saving!'));
        },

        typeHandler : function(type, id, options)
        {
            if (this._handlers.hasOwnProperty(id))
                return this._handlers[id];

            _.extend(options, {
                classes : Module.config().classes,
                model : this.model,
                editor : this,
                objectId : this.model.id
            });
            var cache = this._handlers;
            var cacheHandler = function(handler)
            {
                cache[id] = handler;
            };
            var handler = this.datatypes.handler(type, options.el, options, cacheHandler);

            if (handler) {
                this._handlers[id] = handler;
                return handler;
            }
        },

        legacyRender : function()
        {
            var hider = this.hide;
            this.$('fieldset.ezcca-collapsible').each(function()
            {
                var node = $(this);
                if (node.find('label.message-error').length === 0)
                    hider(node.addClass('ezcca-collapsed').find('div.ezcca-collapsible-fieldset-content'));
            });

            return this;
        },

        dismissAlert : function(e)
        {
            this.$(e.currentTarget).parent('.alert').remove();
            return this;
        },

        showTranslations : function(e)
        {
            var target = $(e.currentTarget),
                language = target.data('language'),
                selector = '.attribute.language-' + language;

            var on = target.is(':checked');

            if (on) {
                this.model.selectedTranslations.add({id : language});
            }
            else {
                var model = this.model.selectedTranslations.get(language);
                if (model)
                    this.model.selectedTranslations.remove(model);
            }

            var selection = this.$(selector);
            var command = on ? 'show' : 'hide';
            this[command](selection);
            var options = {
                selection : selection
            };
            options[command] = language;
            this.updateTranslationSelections(options);
        },

        updateTranslationSelections : function(options)
        {
            options = options || ({});
            var languages = this.$('.eze-language');
            var selection;
            if (languages.length === 0) {
                // No translations implies 1 language
                selection = this.$('.eze-object-attribute');
            }
            else {
                var checked = this.$('.eze-object-translations input:checked');
                var flags = this.$('.eze-object-attribute .language');
                if (checked.length === 1) {
                    checked.attr('disabled', true)
                        .parents('li').addClass('disabled');

                    this.hide(flags);
                }
                else
                {
                    checked.removeAttr('disabled')
                        .parents('li').removeClass('disabled');

                    this.show(flags);
                }
                if (options.hasOwnProperty('show')) {
                    selection = options.selection;
                }
                else {
                    var languageScopedSelector = _.map(checked, function(node) {
                        var lang =  $(node).data('language');
                        this.model.selectedTranslations.add({id : lang});
                        return '.attribute.language-' + lang;
                    }, this).join(', ');
                    selection = this.$(languageScopedSelector);
                }
            }
            if (selection) {
                var editor = this;
                selection.each(function()
                {
                    var node = $(this);
                    var data = node.data();
                    if ('datatype' in data && 'id' in data) {
                        editor.typeHandler(data.datatype, data.id, {
                            el : node,
                            version : data.version
                        });
                    }
                });
            }
        },

        getVersion : function(language)
        {
            if (_(this.translationVersions).has(language))
                return this.translationVersions[language];
            return this.mainVersion;
        },

        setVersion : function(language, version)
        {
            this.translationVersions[language] = parseInt(version, 10);
        },

        skipAutosave : function(field)
        {
            var data = field.data();
            if ('autosave' in data && data.autosave === 'off') return true;

            if (!('autosave' in data) || data.autosave !== 'off') {
                if (!data.hasOwnProperty('originalValue')) return false;
                var val = field.val();
                if (field.is(':checkbox'))
                    val = field.is(':checked');
                if (val !== data.originalValue) {
                    field.data('originalValue', val);
                    return false;
                }
                return true;
            }
            return false;
        },

        storeOriginalValue : function(field)
        {
            field = this.$(field);
            field.data('original-value', field.val());
        }
    });
});
