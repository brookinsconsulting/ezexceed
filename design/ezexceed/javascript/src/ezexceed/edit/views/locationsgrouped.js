/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'shared/node', 'module'], function(Backbone, Node, Module)
{
    var Model = Backbone.Model.extend(
    {
        idAttribute : 'classId',

        addLocation : function(location)
        {
            this.get('locations').add(location);

            if (location.get('isMain')) {
                this.set('hasMain', true);
            }
        }
    });

    var Collection = Backbone.Collection.extend(
    {
        model : Model,
        classes : null,

        initialize : function()
        {
            this.classes = Module.config().classes;

            return this;
        },

        comparator : function(model)
        {
            return -model.get('locations').length;
        },

        addLocations : function(locations)
        {
            var classId, locationGroup;

            locations.each(function(location)
            {
                classId = location.get('parent').classId;
                locationGroup = this.get(classId) || this.locationGroup(location, classId);

                locationGroup.addLocation(location);

                if (!this.get(classId)) {
                    this.add(locationGroup);
                }
            }, this);

            this.sort();

            return this;
        },

        locationGroup : function(location, classId)
        {
            var name = this.classes.get(classId) ? this.classes.get(classId).get('name') : '';

            return new Model({
                classId : classId,
                name : name,
                hasMain : location.get('isMain'),
                locations : new Node.collection()
            });
        }
    });

    return Collection;
});