/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'shared/node', './locations/add', './locations/remove', './locationsgrouped', 'module', 'ezexceed/edit/templates/locationsgrouped', 'shared/templates/objectlistitem'],
    function(View, Node, Add, Remove, LocationsGrouped, Module, LocationsTemplate, ObjectListItemTemplate)
{
    return View.extend(
    {
        objectEditView : null,
        language : null,
        locations : null,
        addActionView : null,
        /** @type Locations.Remove */
        removeActionView : null,
        finder : null,
        GROUP_LIMIT : 8,
        $ezeItems : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['objectEditView', 'language']));

            this.locations = this.model.get('locations');
            this.locations.bind('add remove reset', this.render);

            this.initializeCustomActions();
        },

        initializeCustomActions : function()
        {
            var options = {
                parentModel : this.model,
                version : this.objectEditView.getVersion(this.language),
                locations : this.locations
            };

            this.addActionView = new Add(options);
            this.removeActionView = new Remove(options);
        },

        events : {
            'click button.add' : 'addLocation',
            'click a.remove' : 'confirmRemoveLocation',
            'click .eze-confirm-action' : 'confirmRemove',
            'click .eze-confirm-abort' : 'confirmAbort',
            'change input.radioselect' : 'changeMain',
            'click .expand' : 'toggleGroup'
        },

        getClassGroup : function(classId)
        {
            return this.$('.groupedLocations-' + classId);
        },

        toggleGroup : function(e)
        {
            var target = this.$(e.currentTarget);
            var classId = target.data('classId');
            var classGroup = this.getClassGroup(classId);
            var visible = !classGroup.hasClass('hide');

            target.children('.show').toggleClass('hide', !visible);
            target.children('.hide-group').toggleClass('hide', visible);
            classGroup.toggleClass('hide', visible);
        },

        renderLocationGroup : function(locationGroup)
        {
            var classId = locationGroup.get('classId');

            var context = {
                classId : classId,
                name : locationGroup.get('name'),
                hasMain : locationGroup.get('hasMain'),
                count : locationGroup.get('locations').length
            };

            this.$ezeItems.append(LocationsTemplate(context));

            this.getClassGroup(classId).children('ul').html(this.renderLocations(locationGroup.get('locations')));
        },

        renderLocations : function(locations)
        {
            return locations.map(function(node)
            {
                var data = {
                    id : node.get('id'),
                    classId : node.get('parent').classId,
                    name : node.get('parent').name,
                    url : node.get('url'),
                    pathFormatted : node.get('pathFormatted'),
                    removable : false
                };

                if (this.locations.length > 0) {
                    _.extend(data, {
                        removable : !node.get('isMain'),
                        radioselect : 'Main',
                        radioselectname : 'mainnode',
                        radioselectselected : node.get('isMain'),
                        removeconfirmtext : 'This location has subcontent! Removing it will also remove all subcontent. Are you sure you want to remove'
                    });
                }

                return ObjectListItemTemplate(data);
            }, this);
        },

        render : function()
        {
            this.$ezeItems = this.$('.eze-items').html('');

            var locationsGrouped = new LocationsGrouped().addLocations(this.locations);

            locationsGrouped.each(function(locationGroup)
            {
                var locations = locationGroup.get('locations');

                if (locations.length > this.GROUP_LIMIT) {
                    this.renderLocationGroup(locationGroup);
                }
                else {
                    this.$ezeItems.append(this.renderLocations(locations));
                }
            }, this);

            var addButton = this.$('button.add');
            var addExists = addButton.length !== 0;

            var toggle = !addExists || this.locations.length === 1;

            this.$('a.remove').toggleClass('hide', toggle);

            return this;
        },

        getRootNodeId : function()
        {
            var config = Module.config();
            var classRootNodes = config.classRootNodes;
            var classIdentifier = this.model.get('class').get('identifier');
            var rootNodeId = config.sitemapRootNodeId;

            _(classRootNodes).each(function(rootNode)
            {
                if (rootNode.identifier === classIdentifier) {
                    rootNodeId = rootNode.id;
                }
            });

            return rootNodeId;
        },

        addLocation : function(e)
        {
            e.preventDefault();

            require(['ezexceed/finder'], this.loadFinder);
        },

        changeMain : function(e)
        {
            var mainNodeId = this.$(e.currentTarget).val();

            this.locations.invoke('set', 'isMain', false);
            this.locations.get(mainNodeId).set('isMain', true);

            this.model.attr('locations', this.objectEditView.getVersion(this.language), {mainNode : mainNodeId});

            this.render();
        },

        loadFinder : function(Finder)
        {
            /** Double-clicking is possible from finder. */
            this.removeActionView.setSelfBlocking(true);

            var options = {
                rootNodeId : this.getRootNodeId(),
                customActions : {
                    add : this.addActionView,
                    remove : this.removeActionView
                },
                containersOnly : true
            };

            var context = {
                heading : [
                    'Add location to',
                    {
                        text : this.model.get('name'),
                        quotes : true
                    }
                ],
                icon : 'Documents',
                render : false
            };

            this.finder = eZExceed.stack.push(Finder.view, options, context).on('destruct', this.finderDestruct);

            this.model.on('autosave.saved', this.updateFinder, this);
            this.locations.on('add remove reset', this.addActionView.render, this);
            this.locations.on('add remove reset', this.removeActionView.render, this);

            eZExceed.trigger('edit:locations:add', {id : this.model.id});
        },

        updateFinder : function()
        {
            /** Must update locations with new data. */
            this.locations.reset(this.model.get('locations').models);

            var node = this.finder.state.objects.marked;
            if (node) {
                node.load({clearCache : true});
            }
        },

        finderDestruct : function()
        {
            this.locations.off('add remove', this.addActionView.render);
            this.locations.off('add remove', this.removeActionView.render);
            this.model.off('autosave.saved', this.updateFinder, this);

            this.removeActionView.setSelfBlocking(false);
            this.finder = null;
        },

        confirmRemoveLocation : function(e)
        {
            e.preventDefault();

            var target = this.$(e.currentTarget);
            var nodeId = target.closest('li').data('id');
            var node = this.locations.get(nodeId);

            if (parseInt(node.get('childrenCount'), 10) > 0) {
                this.show(target.siblings('.confirm-remove'));
            }
            else {
                this.removeActionView.removeLocation(nodeId);
            }
        },

        confirmRemove : function(e)
        {
            var nodeId = this.$(e.currentTarget).data('id');

            this.removeActionView.removeLocation(nodeId);
            this.confirmAbort(e);
        },

        confirmAbort : function(e)
        {
            var target = this.$(e.currentTarget);
            this.hide(target.closest('.confirm-remove'));
        }
    });
});
