/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['shared/view', 'shared/views/objectlistitem', 'shared/contentobject'],
    function(View, ObjectListItem, ContentObject)
{
    return View.extend({

        scrollTop : null,
        scrollTarget : null,

        initialize : function()
        {
            _.bindAll(this);

            this.collection = this.model.get('list');
            this.collection.on('add', _.debounce(this.render, 10));

            return this;
        },

        sortable : function(remove)
        {
            if (remove) {
                if (this.$el.hasClass('sortable')) {
                    this.$el.removeClass('sortable').sortable('destroy');
                }
            }
            else {
                this.$el.addClass('sortable').sortable({
                    axis : 'y',
                    update : this.savePriority,
                    create : this.sortCreate,
                    start : this.sortStart
                });
            }
        },

        sortCreate : function()
        {
            this.$el.css('min-height', this.$el.css('height'));
            /** Set fixed height. */
            this.$el.height(this.$el.height());
        },

        sortStart : function()
        {
            this.$el.sortable('refreshPositions');
        },

        savePriority : function()
        {
            // Find the new position
            var items = _.map(this.$('li'), function(element)
            {
                return this.$(element).data('node');
            }, this);

            this.model.trigger('save');
            this.model.save({items : items}).done(this.prioritySaved);

            return this;
        },

        prioritySaved : function()
        {
            this.model.trigger('saved');
        },

        render : function()
        {
            var sortedBy = this.model.getSortedBy();
            var sortable = (sortedBy === this.model.PRIORITY && this.model.searchString.length === 0);

            var items = this.model.get('list').map(function(item)
            {
                var model = new ContentObject.model();
                model.set(model.parse({
                    id : item.get('objectId'),
                    name : item.get('name'),
                    'class' : item.get('classId')
                })).set('className', false);

                var view = new ObjectListItem({
                    sortable : sortable,
                    removable : false,
                    skip : ['className'],
                    model : model
                }).render();

                view.$el.data('node', item.get('nodeId'));
                return view.$el;
            }, this);

            this.$el.html(items);

            this.sortable(!sortable);

            return this;
        }
    });
});