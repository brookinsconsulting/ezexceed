define(['shared/containerview', 'ezexceed/addcontent/widget', './sort', './search', 'funky', 'jquery-safe', 'ezexceed/edit/templates/subtree/main'],
    function(View, Widget, Sort, Search, Funky, $, Template)
{
    return View.extend(
    {
        /** @type Shared.Subtree */
        subtree : null,
        $optionsMenu : null,
        $showMore : null,
        $itemsElement : null,

        initialize : function()
        {
            _.bindAll(this);

            this.views = {};
            this.subtree = this.model.get('subtree');

            this.listenTo(this.subtree, {
                change : this.render,
                load : this.showLoader,
                save : this.onSave,
                saved : this.onSaved
            });

            this.listenTo(this.model, {'change:locations' : this.render});
            this.listenTo(this, {'destruct' : this.remove});
        },

        events : {
            'click ul.kp-edit-block-elements' : 'openMenu',
            'click li.location' : 'selectLocation',
            'click .show-more' : 'showMore'
        },

        onSave : function()
        {
            this.trigger('save');
        },

        onSaved : function()
        {
            this.trigger('saved');
        },

        showMore : function()
        {
            this.$showMore.after(this._loader({size: 16, className : 'icon-16'}));
            this.subtree.more().done(this.toggleShowMore);
        },

        toggleShowMore : function()
        {
            if (this.subtree.get('total') > this.subtree.get('list').length && this.subtree.searchString.length === 0) {
                this.show(this.$showMore);
            }
            else {
                this.hide(this.$showMore);
            }

            this.$('img.loader').remove();
        },

        documentClicked : function(e, target)
        {
            if (this.$optionsMenu.find(target).length === 0 && !this.$optionsMenu.is(target)) {
                this.close();
            }
        },

        close : function()
        {
            this.$optionsMenu.removeClass('active');
            $(document).off('documentClicked', this.documentClicked);
        },

        openMenu : function()
        {
            this.$optionsMenu.addClass('active');
            $(document).on('documentClicked', this.documentClicked);
        },

        selectLocation : function(e)
        {
            e.stopPropagation();

            this.close();

            var target = this.$(e.currentTarget);
            var id = target.children('a.item').data('id');

            if (this.subtree.id === id) {
                return true;
            }

            this.$('li.location').removeClass('active');
            target.addClass('active');

            this.showLoader();

            this.subtree.id = id;
            this.subtree.searchString = '';
            this.subtree.fetch();
        },

        showLoader : function()
        {
            var loaderOptions = {
                size : 16,
                className : 'icon-16'
            };

            this.$itemsElement.html(this._loader(loaderOptions));
        },

        renderSortView : function()
        {
            if (this.views.sortView) {
                this.views.sortView.off('close', this.close);
                this.views.sortView.remove();
                this.views.sortView = null;
            }

            var options = {
                model : this.subtree,
                itemsElement : this.$itemsElement
            };

            this.views.sortView = new Sort(options).on('close', this.close);
            this.$('ul.kp-edit-block-elements').append(this.views.sortView.render().$el);
        },

        /**
        * Filter and format locations so only
        * the main node or nodes with children are returned
        * and formatted
        */
        filterLocations : function(locations)
        {
            if (locations.length === 0) {
                return [];
            }

            return locations.chain()
                .filter(this.locationFilter)
                .map(this.formatLocation)
                .value();
        },

        locationFilter : function(location)
        {
            return location.get('isMain') || location.get('childrenCount') > 0;
        },

        formatLocation : function(location)
        {
            return {
                id : location.id,
                name : location.get('parent').name,
                checked : location.id === this.subtree.id
            };
        },

        selectedLocation : function(locations)
        {
            return locations.find(Funky.equalProp('id', this.subtree.id));
        },

        ensureHasValidSubtree : function(locations)
        {
            var selectedLocation = this.selectedLocation(locations);

            if (!selectedLocation)
            {
                this.showLoader();

                var location = locations.find(Funky.truthyProp('isMain', 'get'));
                this.subtree.id = location.id;
                this.subtree.fetch();
            }
        },

        renderCreate : function()
        {
            if (this.views.widget) {
                this.views.widget.off('edit:done', this.createDone);
                this.views.widget.remove();
                this.views.widget = null;
            }

            var options = {
                className : 'btn create-new',
                config : {
                    direction : 'left'
                }
            };

            this.views.widget = new Widget(options).render();
            this.views.widget.nodeId = this.subtree.id;
            this.views.widget.on('edit:done', this.createDone);

            this.$('p.description').after(this.views.widget.$el);
        },

        createDone : function(model)
        {
            /** @type {Shared.Node} */
            var selectedLocation = this.selectedLocation(this.model.get('locations'));
            /** One or more locations for the new subtree-content. */
            var newChildLocation = model.get('locations').find(Funky.equalProp('parentId', this.subtree.id, 'get'));

            if (newChildLocation) {
                selectedLocation.addChild(newChildLocation);
            }

            this.showLoader();
            this.subtree.fetch();
        },

        renderSearch : function()
        {
            this.views.search = new Search({searchString : this.subtree.searchString}).render();
            this.$('button.create-new').after(this.views.search.$el);
            this.views.search.on('search', this.performSearch);

            if (this.subtree.searchString.length > 0) {
                this.views.search.focus(this.subtree.searchString);
            }
        },

        performSearch : function(searchString)
        {
            this.showLoader();
            this.subtree.searchString = searchString;
            this.subtree.fetch();
        },

        render : function()
        {
            var locations = this.model.get('locations');

            /** Current subtree might have been deleted. If so fetch a new one! */
            this.ensureHasValidSubtree(locations);

            var context = {
                showLocations : locations.length > 0,
                locations : this.filterLocations(locations),
                hasAnyChildren : this.model.hasAnyChildren(),
                currentLocation : {
                    text : locations.get(this.subtree.id).get('parent').name,
                    quotes : true
                }
            };

            this.$el.html(Template(context));

            this.$optionsMenu = this.$('div.options-menu');
            this.$itemsElement = this.$('ul.eze-items');

            this.renderCreate();

            if (this.subtree.get('total') > this.subtree.get('limit') || this.subtree.searchString.length > 0) {
                this.renderSearch();
            }

            if (context.hasAnyChildren) {
                this.renderSortView();
            }

            this.$showMore = this.$('.show-more');
            this.toggleShowMore();

            return this;
        }
    });
});
