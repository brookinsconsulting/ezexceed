/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
 
 define(['shared/view', 'jquery-safe', 'ezexceed/edit/templates/subtree/search'], function(View, $, Template)
 {
    return View.extend(
    {
        tagName : 'form',
        className : 'form-search',

        initialize : function()
        {
            _.bindAll(this);

            this.search = _.debounce(this.search, 250);

            return this;
        },

        events : {
            'keyup input' : 'search'
        },

        search : function(e)
        {
            var searchString = $.trim(this.$(e.currentTarget).val());
            this.trigger('search', searchString);
        },

        focus : function(searchString)
        {
            this.$('input').focus().val(searchString);
        },

        render : function()
        {
            this.$el.html(Template({}));

            return this;
        }
    });
 });
