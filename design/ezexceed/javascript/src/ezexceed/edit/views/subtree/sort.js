define(['shared/view', './subtree', 'selectable', 'ezexceed/edit/templates/subtree/sort'],
    function(View, Subtree, selectable, Template)
{
    return View.extend(
    {
        tagName : 'li',
        className : 'sort-options',
        itemView : null,
        itemsElement : null,
        loader : null,

        initialize : function(options)
        {
            _.bindAll(this);
            _.extend(this, _.pick(options, ['itemsElement']));

            return this;
        },

        events : {
            'change select' : 'saveSorting',
            'click .order button': 'saveOrder',
            /** Prevents click on the select from reopening the settings-menu. */
            'click select' : function(e) {e.stopPropagation();}
        },

        saveOrder : function(e)
        {
            var node = this.$(e.currentTarget);
            this.$('.order .active').removeClass('active').attr('disabled', null);
            node.addClass('active').attr('disabled', true);

            this.saveSorting(e);
        },

        saveSorting : function(e)
        {
            e.stopPropagation();

            this.model.trigger('load');
            this.model.trigger('save');
            this.trigger('close');

            var data = {
                sortfield : this.$('select').val(),
                sortorder : (this.$('.order button.active').data('value') || this.model.DEFAULT_ORDER)
            };

            this.model.save(data).done(this.sortSaved);
        },

        sortSaved : function(response)
        {
            if (response.ok && response.subtree) {
                this.model.set(this.model.parse(response.subtree));
            }

            this.model.trigger('saved');
        },

        getSortOptions : function(sortedBy)
        {
            var sortMap = this.model.get('sortMap');

            return _(sortMap).map(function(value, key)
            {
                var id = parseInt(key, 10);

                return {
                    id : id,
                    name : value,
                    selected : sortedBy === id
                };
            });
        },

        renderSubtree : function()
        {
            var options = {
                el : this.itemsElement,
                model : this.model
            };

            this.itemView = new Subtree(options).render();
        },

        render : function()
        {
            var sortedBy = this.model.getSortedBy();
            var order = parseInt(this.model.get('order'), 10);

            var context = {
                asc : (order === 1),
                desc : (order === 0),
                order : (sortedBy !== this.model.PRIORITY),
                sortOptions : this.getSortOptions(sortedBy)
            };

            this.$el.html(Template(context));

            selectable(this.$('select'), {width : '70%'});

            this.renderSubtree();

            return this;
        }
    });
});
