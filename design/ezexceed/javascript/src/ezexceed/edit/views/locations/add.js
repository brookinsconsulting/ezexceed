/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/views/action'], function(Action)
{
    return Action.extend(
    {
        actionText : 'Place the content on this page',
        parentModel : null,
        locations : null,
        version : null,
        selfBlocking : true,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['parentModel', 'actionText', 'validate', 'version', 'locations']));

            this.parentModel.on('autosave.saved', this.unLock);

            return this;
        },

        validate : function(node)
        {
            var canAdd = true;

            this.locations.each(function(location)
            {
                var activeNodeId = parseInt(this.model.get('id'), 10);
                var locationId = parseInt(location.get('id'), 10);
                var locationParentId = parseInt(location.get('parentId'), 10);

                /** You cannot put a node under itself. */
                if (activeNodeId === locationId) {
                    canAdd = false;
                }

                /** You cannot add the same node twice. */
                if (locationParentId === activeNodeId) {
                    canAdd = false;
                }

                var locationNodeArray = this.splitPath(node.get('pathString'));

                /** Cycles are not allowed. */
                if (_(locationNodeArray).indexOf(locationId.toString()) !== -1) {
                    canAdd = false;
                }
            }, this);

            return canAdd;
        },

        splitPath : function(path)
        {
            return _(path.split('/')).filter(function(value)
            {
                return _.isString(value) && value.length > 0;
            });
        },

        primaryAction : function()
        {
            if (this.locked) {
                return;
            }

            this.lock();
            this.toggleActive();

            var values = [{action : 'add', location : this.model.id}];

            this.parentModel.once('autosave.saved', this.toggleActive);
            this.parentModel.attr('locations', this.version, values);
        }
    });
});
