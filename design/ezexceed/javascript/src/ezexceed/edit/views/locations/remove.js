/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/views/action'], function(Action)
{
    /**
     * @class Locations.Remove
     * @extends Shared.Action
     */
    return Action.extend(
    {
        statusText : 'The content is placed on this page',
        actionText : 'Remove from this page',
        confirm : true,
        parentModel : null,
        locations : null,
        primary: false,
        selfBlocking : false,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['parentModel', 'actionText', 'validate', 'version', 'locations']));

            this.parentModel.on('autosave.saved', this.unLock);

            return this;
        },

        validate : function(node)
        {
            if (this.locations.length <= 1) {
                return false;
            }

            return this.locations.where({parentId : node.get('id')}).length > 0;
        },

        primaryAction : function()
        {
            var parentId = this.model.get('id');

            this.locations.each(function(location)
            {
                if (location.get('parentId') === parentId) {
                    this.removeLocation(location.id);
                }
            }, this);
        },

        removeLocation : function(nodeId)
        {
            if (this.locked) {
                return false;
            }

            this.lock();
            this.toggleActive();

            if (!this.selfBlocking) {
                this.locations.remove(nodeId);
            }


            this.parentModel.once('autosave.saved', this.toggleActive);

            /** If this is current node, mark it as removed. */
            if (nodeId === eZExceed.config.currentNodeId) {
                eZExceed.currentNodeChanges.removed = true;
            }

            var values = [{action : 'remove', location : nodeId}];
            this.parentModel.attr('locations', this.version, values);
        }
    });
});