/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'shared/datatype', 'jquery-safe'], function(Backbone, View, $)
{
    return View.extend({
        /**
         * Global autocomplete cache for holding search terms with matching keywords
         */
        cache : {},

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);
            this.inputEl = this.selectByInputName('ezkeyword_data_text');

            return this;
        },

        render : function()
        {
            var _this = this;
            this.inputEl.bind("keydown", function(event)
                {
                    if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active)
                    {
                        event.preventDefault();
                    }
                })
                .autocomplete({
                    minLength : 0,
                    search : function()
                    {
                        // custom minLength
                        var term = _this.extractLast(this.value);
                        if (term.length < 2)
                            return false;
                    },

                    source : function(request, response)
                    {
                        var term = _this.extractLast(request.term);
                        if (_(_this.cache).has(term)) {
                            response(_this.cache[ term ]);
                            return;
                        }

                        var config = {url : '/Object/findKeywords/' + term};
                        return Backbone.sync('read', config)
                            .done(function(data)
                            {
                                _this.cache[term] = data;
                                // delegate back to autocomplete, but extract the last term
                                response($.ui.autocomplete.filter(
                                    data, _this.extractLast(term)));
                            });
                    },
                    focus : function()
                    {
                        // prevent value inserted on focus
                        return false;
                    },
                    select : function(event, ui)
                    {
                        var terms = _this.split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        // add placeholder to get the comma-and-space at the end
                        terms.push("");
                        this.value = terms.join(", ");
                        _this.editor.autosave({
                            currentTarget : _this.inputEl
                        });
                        return false;
                    }
                });
        },

        split : function(val)
        {
            return val.split(/,\s*/);
        },

        extractLast : function(term)
        {
            return this.split(term).pop();
        }
    });
});
