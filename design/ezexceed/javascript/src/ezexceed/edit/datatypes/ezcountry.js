/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'module', 'selectable'], function(View, Module, selectable)
{
    return View.extend({

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);
        },

        render : function()
        {
            selectable(this.$('select'));
            return this;
        },

        parseEdited : function()
        {
            var values = _.pluck(this.$(':selected'), 'value');

            if (!_.isArray(values)) {
                values = [values];
            }

            return [
                {name : this.buildName('country'), value : values}
            ];
        }
    });
});
