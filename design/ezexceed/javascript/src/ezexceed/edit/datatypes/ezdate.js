/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'shared/datepicker'], function(View, DatePicker)
{
    return View.extend({
        datePicker : null,
        $date : null,
        isEZDemo : false,

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);

            this.$date = this.$('input');

            this.isEZDemo = !this.selectByInputName('date_date');
            if (!this.isEZDemo) {
                // Change id of input el to avoid possible conflicts
                // down the stack when the same object is opened twice
                this.$date.attr('id', _.uniqueId(this.$date.attr('id')));
                this.datePicker = new DatePicker({
                    el : this.$date
                });
            }
        },

        events : {
            'click .clear' : 'clear'
        },

        clear : function()
        {
            this.$date.val('');
            this.model.attr(this.attributeId, this.version, this.parseEdited());
        },

        parseEdited : function()
        {
            if (this.isEZDemo) {
                return this.parseEzDemoValues();
            }

            var date = this.datePicker.getDate();

            if (!date) {
                return [
                    {name : this.base + '_date_year_' + this.attributeId, value : ''},
                    {name : this.base + '_date_month_' + this.attributeId, value : ''},
                    {name : this.base + '_date_day_' + this.attributeId, value : ''}
                ];
            }

            return [
                {name : this.base + '_date_year_' + this.attributeId, value : date.getFullYear()},
                {name : this.base + '_date_month_' + this.attributeId, value : date.getMonth() + 1},
                {name : this.base + '_date_day_' + this.attributeId, value : date.getDate()}
            ];
        },

        // Backwards support for ez 4.7 demo design
        // that overrides the ezdatetime edit template
        parseEzDemoValues : function()
        {
            return _.map(this.$(".attribute-content input"), function(el)
            {
                return {
                    name : el.name,
                    value : el.value
                };
            });
        },

        render : function()
        {
            this.datePicker.setup(false, {onSelect : this.onSelect});
            return this;
        },

        onSelect : function()
        {
            this.model.attr(this.attributeId, this.version, this.parseEdited());
        }
    });
});
