/* global ezie */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['./upload_base', 'jquery-safe', 'module'], function(View, $)
{
    return View.extend(
    {
        browseButton : null,
        importUrl : '',
        editor : null,
        ezIeLoaded : false,
        _ezieEnabled : null,

        initialize : function(options)
        {
            this.init(options);

            _.bindAll(this);

            this.model.on('autosave.saved', this.update, this);

            this.enableEzIe();

            this.filelistWrapper = 'eze-upload-new-image-' + this.attributeId;
            this.browseButton = 'eze-image-browser-' + this.attributeId;

            this.maxFileSize = parseInt(this.$('.ezimage-max-file-size').val(), 10);

            this.sharedInitialize();

            return this;
        },

        enableEzIe : function()
        {
            var jq = window.$ || (window.jQuery);
            if (!jq) {
                return false;
            }

            var button = jq('.edit-image', this.el);
            this._ezieEnabled = (button.length > 0 && jq.fn.ezie) ? button : false;

            if (this._ezieEnabled) {
                /** Must be sure that only one ezie exists on the button, else it will cause problems. */
                this._ezieEnabled.unbind().ezie();
            }
            else {
                this.hide(this.$('.edit-image'));
            }
        },

        events : {
            'click .close' : 'removeImage'
        },

        removeImage : function(e)
        {
            e.preventDefault();

            var target = $(e.currentTarget);

            var remove = {
                name : target.attr('name'),
                value : target.val()
            };

            var altText = {
                name : [this.base, 'data_imagealttext', this.attributeId].join('_'),
                value : ''
            };

            var extras = {
                returnTemplates : true
            };
            extras[target.attr('name')] = 1;

            this.model.attr(this.attributeId, this.version, [remove, altText], {silent: true});
            this.model.onChange(this.model, true, extras);
        },

        render : function()
        {
            if (this._ezieEnabled) {
                ezie.gui.config.bind.reload_saved = this.overrideEzIE;
            }

            this.loadUpload();
            this.enableEzIe();

            return this;
        },

        overrideEzIE : function(new_block)
        {
            /** Must trigger a save of version since eZImage does not update version, hence it's not flagged as changed. */
            var attr = [{
                name : 'triggerVersionUpdate',
                value : 1
            }];

            this.model.attr('triggerVersionUpdate', this.version, attr);

            /** Prevents input-field from duplicating. */
            this.$('input.image-description').remove();
            this.$('div.thumbnail').replaceWith(new_block);

            this.loadUpload();
            this.enableEzIe();
        },

        filesAdded : function()
        {
            this.uploader.settings.multipart_params = _.defaults({
                attributes : this.attributeId,
                inputFormName : this.base + '_data_imagename_' + this.attributeId,
                returnTemplates : true
            }, this.uploader.settings.multipart_params);

            this.uploader.start();

            this.$('div.thumbnail').append(this._loader({size : 16, className : 'icon-16'}));
            this.show(this.$('div.thumbnail'));
            this.hide(this.$('div.upload'));
        },

        fileUploaded : function(up, file, info)
        {
            this.$('div.thumbnail').html('');

            var response = JSON.parse(info.response);
            if (response.ok) {
                this.update(this.model, response);
            }
        },

        update : function(model, response)
        {
            if (response && response.templates && response.templates.hasOwnProperty(this.attributeId)) {
                if (this.uploader) {
                    this.uploader.destroy();
                }

                var html = $(response.templates[this.attributeId]);

                /** Must add timestamp to imageurl to clear cached image. */
                var image = html.find('img');
                var now = new Date();
                image.attr('src', image.attr('src') + '?' + now.getTime());

                this.$('.attribute-content').html(html);

                this.loadUpload();
                this.enableEzIe();
            }
            else if((response[0] && response[0].hasOwnProperty('imageRemove')) || response.hasOwnProperty('imageRemove')) {
                this.$('.alt-text').remove();
                this.$('img').remove();
                this.show(this.$('.new-wrap'));
                this.$('.image-container').removeClass('with-image');
            }
        }
    });
});
