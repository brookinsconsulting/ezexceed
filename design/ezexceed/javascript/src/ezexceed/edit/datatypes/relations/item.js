/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['ezexceed/grid/views/item', 'ezexceed/edit/templates/datatypes/relations/item'], function(ItemView, Template)
{
    return ItemView.extend({

        multiple : null,

        selected : false,

        initialize : function(options)
        {
            _.bindAll(this);
            _.extend(this, _.pick(options, ['selected', 'multiple']));
            if (this.selected) {
                this.model.set('selected', true, {silent: true});
            }

            this.listenTo(this.model, {
                'change:selected' : this.toggleChecked,
                'change:name change:published change:modified published' : this.render
            });
        },

        toggleChecked : function(model, state)
        {
            this.$('.select').attr('checked', state);
            return this;
        },

        render : function()
        {
            var contentClass = this.model.get('class');
            var type = contentClass ? contentClass.get('name') : 'Unknown';
            var classId = contentClass ? contentClass.id : false;

            var context = _.defaults({
                name : this.model.get('name') || 'No name',
                classId : classId,
                type : type,
                multiple: this.multiple
            }, this.model.toJSON());

            this.$el.html(Template(context));
            return this;
        }
    });
});
