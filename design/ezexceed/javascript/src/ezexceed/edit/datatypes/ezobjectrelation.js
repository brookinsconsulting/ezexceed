/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'shared/contentobject', 'shared/views/objectlistitem', 'jquery-safe', 'funky'],
    function(View, ContentObject, ObjectListItem, $, Funky)
{
    return View.extend({
        $list : null,

        initialize : function(options)
        {
            _.bindAll(this);
            this.sharedInitialize(options);
        },

        sharedInitialize : function(options)
        {
            _.extend(this, _.pick(options, ['classes']));
            this.listView = ObjectListItem;
            this.$list = this.$('.eze-items');
            this.collection = new ContentObject.collection();

            this.listenTo(this.collection, {
                'remove' : this.onRemove,
                'add' : this.onAdd,
                'reset add remove' : this.save
            });

            var collection = this.collection;
            var ul = this.$list;

            var getListView = this.getListView;
            this.$list.find('li').each(function(index, el)
            {
                var model = new collection.model({id: $(el).data('id')});
                model.listview = getListView({
                    model : model,
                    el : ul.find('.object-by-id-' + model.id)
                });
                collection.add(model, {silent: true});
            });
            this.init(options);
        },

        events : {
            'click button.add' : 'add',
            'click button.remove' : 'remove'
        },

        parseEdited : function()
        {
            return [{
                name: this.base + '_data_object_relation_id_' + this.attributeId,
                value: (this.collection.length > 0 ? this.collection.at(0).id : 0)
            }];
        },

        remove : function(e)
        {
            this.collection.remove(
                this.$(e.currentTarget).data('id')
            );
            eZExceed.trigger('edit:ezobjectrelation:add', {id : this.model.id});
        },


        add : function(e)
        {
            var callback = this.lockAction(e);

            var options = {
                cb : this.onSelected,
                classes : this.classes.chain()
                    .filter(Funky.truthyProp('canCreate'))
                    .pluck('id')
                    .value(),
                list : {
                    item : {
                        multiple: false
                    }
                }
            };

            var context = {
                heading : ['Set relation for', {text: ' ' + this.model.get('name')}],
                classId : this.model.get('class').id,
                render : true
            };

            this.loadGrid(options, context, callback);
            eZExceed.trigger('edit:ezobjectrelation:add', {id : this.model.id});
        },

        loadGrid : function(options, context, closeCb)
        {
            var dependencies = [
                'ezexceed/grid',
                'ezexceed/edit/datatypes/relations/item'
            ];

            require(dependencies, _.bind(function(Grid, ItemView)
            {
                var onSelected = options.cb;
                delete options.cb;

                _.defaults(options, {
                    nodeId : (this.$('button.add').data('nodeId') || this.gridView.nodeId),
                    list : {},
                    actions : []
                });

                _.defaults(options.list, {
                    view : ItemView,
                    selected : this.collection.pluck('id')
                });

                this.gridView = eZExceed.stack.push(
                    Grid.view,
                    options, 
                    context
                ).once('destruct', closeCb);

                this.listenTo(this.gridView.collection, {
                    'change:selected' : onSelected
                });
            }, this));
        },

        // Listener for when child view with object selector destructs
        onSelected : function(model)
        {
            this.$list.html('');
            this.collection.reset([model]);
            this.onAdd(model);
            return this;
        },

        onAdd : function(model)
        {
            model.listview = this.getListView({
                model: model
            }).render();
            this.$list.append(model.listview.el);
        },

        onRemove : function(model)
        {
            model.listview.remove();
        },

        save : function()
        {
            this.model.attr(this.attributeId, this.version, this.parseEdited());
            return this;
        },

        getListView : function(options)
        {
            _.defaults(options, {
                collection : this.collection,
                sortable : ('sortable' in this)
            });
            options.collection = this.collection;
            return new this.listView(options);
        },

        lockAction : function(e)
        {
            e.preventDefault();
            var target = this.$(e.currentTarget).attr('disabled', true).addClass('disabled');
            return function()
            {
                target.attr('disabled', null).removeClass('disabled');
            };
        }
    });
});
