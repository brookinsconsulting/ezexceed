/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'module'], function(View, Module)
{
    return View.extend(
    {
        uploadStatus : {},
        filelistWrapper : null,
        browseButton : null,
        importUrl : '',

        maxFileSize : null,

        sharedInitialize : function()
        {
            this.importUrl = eZExceed.transformUrl(this.model.postUrl + '/' + this.version) + '?returnTemplates=1';
        },

        loadUpload : function()
        {
            require(['plupload/plupload'], this.enableUpload);
        },

        enableUpload : function(plupload)
        {
            var $button = this.$('#' + this.browseButton);
            if (!$button.length)
                return;

            /** Reset upload status. */
            this.uploadStatus.errors = 0;
            this.uploadStatus.imported = 0;

            var uploadOptions = {
                browse_button : this.browseButton,
                container : this.filelistWrapper,
                multipart_params : {
                    returnTemplates : true
                },
                url : this.importUrl
            };

            _.defaults(uploadOptions, Module.config().plupload);

            if (eZExceed.formtoken) {
                uploadOptions.multipart_params.ezxform_token = eZExceed.formtoken;
            }

            if (this.maxFileSize > 0) {
                uploadOptions.max_file_size = this.maxFileSize + 'b';
            }

            this.uploader = new plupload.Uploader(uploadOptions);

            this.uploader.init();

            this.uploader.bind('FileUploaded', this.fileUploaded);
            this.uploader.bind('FilesAdded', this.filesAdded);
            this.uploader.bind('Error', this.uploadError);
        },

        uploadError : function(uploader, error)
        {
            var errors = {};

            errors[this.attributeId] = {
                attributeId : this.attributeId,
                name : _(this.$el.find('legend').html()),
                error : error.message
            };

            this.editor.model.trigger('autosave.failed', this.editor.model, errors);
        }
    });
});
