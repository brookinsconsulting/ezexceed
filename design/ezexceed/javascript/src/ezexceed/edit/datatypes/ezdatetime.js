/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'underscore', 'shared/datepicker', 'shared/timepicker'], function(View, _, Datepicker, Timepicker)
{
    return View.extend({
        $date : null,
        $time : null,

        datePicker : null,
        isEZDemo : false,

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);

            this.$date = this.selectByInputName('datetime_date');
            this.isEZDemo = !this.$date.length;

            if (!this.isEZDemo) {
                this.datePicker = new Datepicker({
                    el : this.$date
                });
                // Change id of input el to avoid possible conflicts
                // down the stack when the same object is opened twice
                this.$time = this.selectByInputName('datetime_time');
                this.$date.attr('id', _.uniqueId(this.$date.attr('id')));
                this.$time.bind('change', this.onTimeChange);
            }
        },

        events : {
            'click .clear' : 'clear'
        },

        clear : function()
        {
            this.$date.val('');
            this.$time.val('');
            this.model.attr(this.attributeId, this.version, this.parseEdited());
        },

        parseEdited : function()
        {
            if (this.isEZDemo) {
                return this.parseEzDemoValues();
            }

            var date = this.datePicker.getDate(this.$date);
            if (!date) {
                return [
                    {name : this.base + '_datetime_year_' + this.attributeId, value : ''},
                    {name : this.base + '_datetime_month_' + this.attributeId, value : ''},
                    {name : this.base + '_datetime_day_' + this.attributeId, value : ''},
                    {name : this.base + '_datetime_hour_' + this.attributeId, value : ''},
                    {name : this.base + '_datetime_minute_' + this.attributeId, value : ''}
                ];
            }

            var hour = 0;
            var minute = 0;
            var time = this.$time.val();
            if (time) {
                var split = time.split(':');
                if (split[0]) {
                    hour = split[0];
                }
                if (split[1]) {
                    minute = split[1];
                }
            }
            return [
                {name : this.base + '_datetime_year_' + this.attributeId, value : date.getFullYear()},
                {name : this.base + '_datetime_month_' + this.attributeId, value : date.getMonth() + 1},
                {name : this.base + '_datetime_day_' + this.attributeId, value : date.getDate()},
                {name : this.base + '_datetime_hour_' + this.attributeId, value : hour},
                {name : this.base + '_datetime_minute_' + this.attributeId, value : minute}
            ];
        },

        // Backwards support for ez 4.7 demo design
        // that overrides the ezdatetime edit template
        parseEzDemoValues : function()
        {
            return _.map(this.$(".attribute-content input"), function(el)
            {
                return {
                    name : el.name,
                    value : el.value
                };
            });
        },

        render : function()
        {
            /** The null check is for the eZDemo-design. */
            if (this.$date && this.datePicker !== null) {
                this.datePicker.setup(false, {onSelect : this.onDateChange});
            }
            if (this.$time) {
                Timepicker(this.$time);
            }
            return this;
        },

        onDateChange : function()
        {
            this.model.attr(this.attributeId, this.version, this.parseEdited());
        },

        onTimeChange : function()
        {
            this.editor.autosave({
                currentTarget : this.$time
            });
        }
    });
});
