/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'jquery-safe', 'ezexceed/edit/templates/datatypes/ezoption', 'jquerypp/form_params'],
    function(View, $, Template)
{
    return View.extend({

        priceTest : null,

        config : null,

        initialize : function(options)
        {
            _.bindAll(this);

            this.priceTest = /^[\-|+]?[0-9]+\.?[0-9]{0,2}$/;
            this.config = this.$('.bootstrap').data();

            this.init(options);
        },

        events : {
            'click .add' : 'add',
            'click .remove' : 'remove'
        },

        add : function(e)
        {
            e.preventDefault();

            // Save a custom action button to store the new option in the db
            var attr = {};
            attr["CustomActionButton[" + this.attributeId + "_new_option]"] = 1;

            this.model.attr(this.attributeId, this.version, [], {silent: true});
            this.model.onChange(this.model, true, attr);

            var context = {
                id : this.attributeId,
                config : this.config,
                index : this.$('tr').length - 1,
                base : this.base
            };
            this.$('.table')
                .append(Template(context))
                .find('.option')
                .focus();
        },

        remove : function(e)
        {
            // Save a custom action button to store the new option in the db
            var attr = {};
            attr["CustomActionButton[" + this.attributeId + "_remove_selected]"] = 1;
            attr[this.buildName('data_option_remove')] = [this.$(e.currentTarget).data('id')];

            this.model.attr(this.attributeId, this.version, [], {silent: true});
            this.model.onChange(this.model, true, attr);

            this.$(e.currentTarget).closest('tr').remove();

            // Perform regular save
            this.model.attr(this.attributeId, this.version, this.parseEdited());
        },

        render : function()
        {
            return this;
        },

        parseEdited : function()
        {
            var values = [
                this.buildValue('data_option_name', this.$('.name'), true),
                this.buildValue('data_option_id', this.$('.id')),
                this.buildValue('data_option_value', this.$('.option')),
                this.buildValue('data_option_additional_price', this.$('.price'))
            ];

            return values;
        },

        getValue : function(el)
        {
            return this.$(el).val();
        },

        buildValue : function(name, nodes, single)
        {
            return {
                name : this.buildName(name),
                value : (single ? nodes.val() : _.map(nodes, this.getValue))
            };
        },

        validate : function(el)
        {
            el = this.$(el);
            var val = el.val();
            if (el.hasClass('price')) {
                return val.match(this.priceTest);
            }
            return val !== "";
        }
    });
});
