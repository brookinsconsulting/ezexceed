/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['shared/datatype', 'jquery-safe', 'ezexceed/edit/templates/datatypes/ezmatrix'],
    function(View, $, Template)
{
    return View.extend({
        templates :
        {
            cell : '#tmpl-ezmatrix-cell',
            checkbox : '#tmpl-ezmatrix-checkbox'
        },

        rowCount : null,
        columnCount : null,
        dataElement : null,


        initialize : function(options)
        {
            _.bindAll(this, 'addRows');

            this.init(options);

            this.dataElement = this.$('.attribute-base');
            this.hasDummyRow = this.$('.dummy').length;
            this.updateData();
        },

        events :
        {
            'click .addMatrixRows' : 'addRows',
            'click .removeSelectedRows' : 'removeRows',
            'change .numMatrixRows' : 'numChanged',
            'change .rowSelect' : 'rowSelected'
        },

        rowSelected : function(e)
        {
            /** We don't wish to trigger save everytime a row is selected. */
            e.stopPropagation();
        },

        numChanged : function(e)
        {
            /** We also can't add rows everytime the count of rows to be added changes. */
            e.stopPropagation();
        },

        updateData : function(change)
        {
            if (change)
                this.dataElement.data('row-count', this.rowCount + parseInt(change, 10));

            this.rowCount = parseInt(this.dataElement.data('row-count'), 10);
            this.columnCount = parseInt(this.dataElement.data('column-count'), 10);

            var hasRows = (this.rowCount > 0);

            this.$('.removeSelectedRows').toggle(hasRows);
        },

        addRows : function(e)
        {
            e.preventDefault();

            var actionElement = this.$(e.currentTarget),
                rowCountSelect = this.$('.numMatrixRows'),
                attr = {};

            this.renderNewRows(rowCountSelect.val());

            var newRows = parseInt(rowCountSelect.val(), 10);

            /**
             * If there is a dummy row, add it to the total count of new rows
             */
            if (this.hasDummyRow) {
                newRows++;
                this.hasDummyRow = 0;
            }

            // Save a custom action button to store the new option in the db
            attr[actionElement.attr('name')] = newRows;
            attr[this.buildName('data_matrix_add_count')] = newRows;

            this.model.attr(this.attributeId, this.version, [], {silent : true});
            this.model.onChange(this.model, true, attr);
        },

        removeRows : function(e)
        {
            e.preventDefault();

            var attr = {},
                actionElement = this.$(e.currentTarget),
                checked = [];

            _(this.$('input.rowSelect')).each(function(el, index)
            {
                el = this.$(el);
                if (el.is(':checked')) {
                    checked.push(index);
                    el.closest('tr').remove();
                }
            }, this);

            if (checked.length === 0)
                return;

            this.updateData(-checked.length);

            if (this.hasDummyRow) {
                this.hasDummyRow = 0;
                /**
                 * No need to save the removal of the dummy row
                 */
                return;
            }

            // Save a custom action button to store the new option in the db
            attr[actionElement.attr('name')] = 1;
            attr[this.buildName('data_matrix_remove')] = checked;

            this.model.attr(this.attributeId, this.version, [], {silent : true});
            this.model.onChange(this.model, true, attr);
        },

        parseEdited : function()
        {
            var values = [], indexName = this.buildName('ezmatrix_cell');

            if (this.hasDummyRow)
            {
                /**
                 * Must add new row manually if the first one is the dummy row
                 */
                var attr = {};
                attr['CustomActionButton[' + this.attributeId + '_new_row]'] = 1;
                attr[this.buildName('data_matrix_add_count')] = 1;
                this.hasDummyRow = 0;

                this.model.attr(this.attributeId, this.version, [], {silent : true});
                this.model.onChange(this.model, true, attr);
            }

            this.$('td input:text').each(function()
            {
                var value = {};

                value[indexName] = $(this).val();

                values.push(value);
            });

            return values;
        },

        renderNewRows : function(numNewRows)
        {
            var data = {
                base : this.base,
                attributeId : this.attributeId,
                cells : []
            };

            /**
             * Fill the cells array as many times as columnCount
             * Needed in handlebar template
             */
            var i;
            for (i = 0; i < this.columnCount; i++) {
                data.cells[i] = true;
            }

            for (i=0; i < numNewRows; i++) {
                data.rowIndex = this.rowCount + i;
                this.$('.table-matrix').append(Template(data));
            }

            this.updateData(numNewRows);
            this.delegateEvents();
        }
    });
});
