/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'jquery-safe', 'ezexceed/edit/templates/datatypes/ezauthor'], function(View, $, Template)
{
    return View.extend({
        editor : null,

        initialize : function(options)
        {
            this.init(options);
        },

        events : {
            'click .remove' : 'onRemove',
            'click .add' : 'onAdd'
        },

        parseEdited : function()
        {
            var base = this.attributeBase;
            var id = this.attributeId;
            var values = [];

            this.$('li').each(function(index)
            {
                var node = $(this);
                var author = {};
                author[base + '_data_author_id_' + id] = index;
                author[base + '_data_author_name_' + id] = node.find('.eze-name').val();
                author[base + '_data_author_email_' + id] = node.find('.eze-email').val();
                values.push(author);
            });

            return values;
        },

        onRemove : function(e)
        {
            e.preventDefault();
            this.$(e.currentTarget).parents('li').remove();
            this.model.attr(this.attributeId, this.version, this.parseEdited());
            return this;
        },

        onAdd : function(e)
        {
            e.preventDefault();
            this.$('.eze-items').append(Template({}));
            return this;
        }
    });
});
