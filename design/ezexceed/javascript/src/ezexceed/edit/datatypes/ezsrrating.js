/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
 
 define(['shared/datatype'], function(View)
 {
    return View.extend(
    {
        initialize : function(options)
        {
            _.bindAll(this);

            this.init(options);

            return this;
        },

        parseEdited : function(node)
        {
            var value;

            if (node.is(':checked')) {
                value = {
                    name : node.attr('name'),
                    value : node.attr('value')
                };
            }
            else {
                value = {
                    name : node.attr('name') + 'x'
                };
            }

            return [value];
        },

        render : function()
        {
            return this;
        }
    });
 });
