/* global google */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype'], function(View)
{
    return View.extend({
        filelistWrapper : 'kp-upload-new-image-',
        editor : null,
        eZFormTokenelementId : '#ezxform_token_js',
        geocoder : null,
        maxZoom : 13,

        // DOM node caches
        $lat : null,
        $lng : null,
        $map : null,

        // Gmaps API instances
        map : null,
        marker : null,

        // What google map type to use
        MAP_TYPE : 'ROADMAP',

        // Wether or not map is draggable
        MAP_DRAGGABLE : true,

        // Error message for empty address
        ERR_NO_ADDRESS : 'Please enter an address',
        
        // Error message for invalid address
        ERR_INVALID_ADDRESS : 'Please enter a valid address',
        
        // Error message for when geolocation fails or is rejected
        ERR_CURRENT_POSITION : 'Geolocation failed',

        initialize : function(options)
        {
            _.bindAll(this);
            
            // Make sure error messages are translated
            this.ERR_NO_ADDRESS = eZExceed.translate(this.ERR_NO_ADDRESS);
            this.ERR_INVALID_ADDRESS = eZExceed.translate(this.ERR_INVALID_ADDRESS);
            this.ERR_CURRENT_POSITION = eZExceed.translate(this.ERR_CURRENT_POSITION);
            
            this.init(options);
        },

        events : {
            'click .find-address' : 'findAddress',
            'click .clear-address' : 'clearPosition',
            'keyup .address' : function(e) {
                if (e.keyCode === 13)
                    this.findAddress(e);
                return;
            },
            'click .current-pos' : function() {
                navigator.geolocation.getCurrentPosition(
                    this.onCurrentPositionSuccess,
                    this.onCurrentPositionError,
                    {'gearsRequestAddress': true}
                );
            },
            'click .map' : 'enableScroll'
        },

        findAddress : function(e)
        {
            e.preventDefault();
            
            var address = this.$addr.val();
            var previousAddress = this.$addr.data('previousAddress');
            
            if(address !== previousAddress) {
                this.$addr.data('previousAddress', address);
                
                this.show(this.$('.find-address-loader'));
                
                if (this.geocoder) {
                    this.geocoder.geocode({'address' : address}, this.onGeocode);
                }
            }
        },

        clearPosition : function()
        {
            var point = new google.maps.LatLng(0, 0);
            this.map.setOptions({
                zoom : 2
            });
            this.updateLatLngFields(point, true);
            this.resetAddressField();
            return this.save();
        },

        enableScroll : function(e)
        {
            e.preventDefault();
            this.map.setOptions({
                scrollwheel : true
            });
        },

        render : function()
        {
            // Caches all used DOM nodes
            this.$lat = this.$('.lat');
            this.$lng = this.$('.lng');
            this.$map = this.$('.map');
            this.$map.width(this.$el.width());
            this.$addr = this.$('.address');
            this.$nav = this.$('.current-pos');

            if (typeof google.maps === 'undefined')
            {
                google.load('maps', '3', {
                    callback : this.mapInitializer,
                    other_params : 'sensor=' + this.$('.actions').data('sensor')
                });
            }
            else
                this.mapInitializer();
            return this;
        },

        parseEdited : function()
        {
            return _(['$lat','$lng','$addr']).map(function(field)
            {
                return {
                    name : this[field].attr('name'),
                    value : this[field].val()
                };
            }, this);
        },

        mapInitializer : function()
        {
            if (!('maps' in google) || !('LatLng' in google.maps))
                return;

            var lat = this.$lat.val() || (0);
            var lng = this.$lng.val() || (0);

            if (lat === '0') lat = 0;
            if (lng === '0') lng = 0;
            
            this.toggleAddressClearButton(lat, lng);

            var startPoint = new google.maps.LatLng(lat, lng);

            this.map = new google.maps.Map(this.$map[0], {
                center: startPoint,
                zoom : (lat || lng) ? this.maxZoom : 0,
                scrollwheel : false,
                mapTypeId: google.maps.MapTypeId[this.MAP_TYPE]
            });
            this.marker = new google.maps.Marker({
                map: this.map,
                position: startPoint,
                draggable: this.MAP_DRAGGABLE
            });
            this.geocoder = new google.maps.Geocoder();
            
            google.maps.event.addListener(this.map, 'tilesloaded', this.hideFindAddressLoader);

            google.maps.event.addListener(this.marker, 'dragend', this.onDragend);
            google.maps.event.addListener(this.map, 'click', this.onClick);

            if (!navigator.geolocation)
                this.$nav.addClass('hide');
            return this;
        },

        onGeocode : function(results, status)
        {
            if (status !== google.maps.GeocoderStatus.OK)
            {
                this.hideFindAddressLoader();
                
                if (this.$addr.val() === '')
                    this.error(this.ERR_NO_ADDRESS);
                else
                    this.error(this.ERR_INVALID_ADDRESS);
                
                this.$addr.focus();
                return this;
            }
            this.map.setOptions({
                center: results[0].geometry.location,
                zoom : this.maxZoom
            });
            this.updateLatLngFields(results[0].geometry.location, true);
            
            // Trigger editor save after geocoding is done
            return this.save();
        },

        onCurrentPositionSuccess : function(position)
        {
            var location = '';
            var point = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            if (navigator.geolocation.type == 'Gears' && position.gearsAddress)
                location = [position.gearsAddress.city, position.gearsAddress.region, position.gearsAddress.country].join(', ');
            else if (navigator.geolocation.type == 'ClientLocation')
                location = [position.address.city, position.address.region, position.address.country].join(', ');

            this.$addr.val(location);
            this.map.setOptions({
                zoom: this.maxZoom,
                center: point
            });
            this.updateLatLngFields(point, true);
            return this.save();
        },

        onCurrentPositionError : function()
        {
            this.error(this.ERR_CURRENT_POSITION);
        },

        onDragend : function(e)
        {
            this.updateLatLngFields(e.latLng);
            this.resetAddressField();
            return this.save();
        },

        onClick : function(e)
        {
            this.map.panTo(e.latLng);
            this.updateLatLngFields(e.latLng, true);
            this.resetAddressField();
            return this.save();
        },

        save : function()
        {
            this.model.attr(this.attributeId, this.version, this.parseEdited());
            this.toggleAddressClearButton();
            
            return this;
        },
        
        toggleAddressClearButton : function(lat, lng)
        {
            if (typeof lat === 'undefined')
                lat = this.$lat.val() || (0);
            
            if (typeof lng === 'undefined')
                lng = this.$lng.val() || (0);
            
            if (lat === '0') lat = 0;
            if (lng === '0') lng = 0;
            
            // If coordinates are set, leave the button enabled. Else disable it.
            this.$('.clear-address')[0].disabled = !(lat && lng);
        },
        
        hideFindAddressLoader : function()
        {
            this.hide(this.$('.find-address-loader'));
        },
        
        updateLatLngFields : function(point, onMap)
        {
            this.$lat.val(point.lat());
            this.$lng.val(point.lng());
            if (onMap)
                this.marker.setPosition(point);
            return this;
        },
        
        resetAddressField : function()
        {
            this.$addr
                .data('previousAddress', '')
                .val('');
        }
    });
});
