/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['jquery-safe', 'shared/datatype', 'module', 'selectable'], function($, View, Module, selectable)
{
    return View.extend({
        initialize : function(options)
        {
            this.init(options);

            return this.render();
        },

        render : function()
        {
            selectable(this.$('select'));

            return this;
        },

        parseEdited : function()
        {
            var values = [];
            var selected = this.$('input:checked');

            if (!selected.length)
                selected = this.$("select option:selected");

            if (selected.length) {
                var _this = this;
                selected.each(function()
                {
                    var value = {
                    };
                    value[_this.base + '_ezselect_selected_array_' + _this.attributeId] = $(this).val();
                    values.push(value);
                });
            }
            else
                values.push({name : this.base + '_ezselect_selected_array_' + this.attributeId, value : ''});

            return values;
        }
    });
});
