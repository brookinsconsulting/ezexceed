/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['./ezobjectrelation', 'jqueryui/sortable', 'jquery-ui.touch'], function(View)
{
    return View.extend({
        initialize : function(options)
        {
            _.bindAll(this);
            this.sharedInitialize(options);
            this.collection.comparator = function(first, second)
            {
                return (first.get('index') > second.get('index')) ? 1 : -1;
            };
        },

        sortable : function()
        {
            this.$('.sortable', this.el).sortable({
                axis : 'y',
                handle : '.reorder',
                update : this.reorder
            });
        },

        reorder : function()
        {
            _.each(this.$('.eze-items > li'), function(el, index)
            {
                var id = this.$(el).data('id');
                if (id) {
                    var model = this.collection.get(id);
                    model.set('index', index, {silent: true});
                }
            }, this);
            this.collection.sort();
            this.save();
            eZExceed.trigger('edit:ezobjectrelation:reorder', {id : this.model.id});
        },

        render : function()
        {
            this.sortable();
            return this;
        },

        parseEdited : function()
        {
            var values = [];
            var inputName = this.buildName('data_object_relation_list');

            if (this.collection.length > 0) {
                values = this.collection.map(function(model)
                {
                    //return model.id;
                    var obj = {};
                    obj[inputName] = model.id;
                    return obj;
                });
            }
            else {
                var value = {};
                value[inputName] = 'no_relation';
                values = [value];
            }
            return values;
        },

        add : function(e)
        {
            var callback = this.lockAction(e);

            var options = {
                cb : this.onSelected,
                classConstraints : this.getContentClassConstraints(),
                list : {
                    item : {
                        multiple: true
                    }
                }
            };

            var context = {
                heading : ['Add relations to', {text: ' ' + this.model.get('name')}],
                classId : this.model.get('class').id,
                render : true
            };

            this.loadGrid(options, context, callback);

            eZExceed.trigger('edit:ezobjectrelation:add', {id : this.model.id});
        },

        onSelected : function(model, selected)
        {
            if (selected) {
                this.collection.add(model);
            }
            else {
                this.collection.remove(model);
            }
            return;
        },

        // Get the content class constraints (if any) for this relation list
        // Will look in the DOM for an input with a name containing `this.attributeId`
        // that contains a string of content class names that it will translate to ids
        // and return as a simple array — or empty array if none is found
        getContentClassConstraints : function()
        {
            var el = this.$('.constraints');
            if (el.length === 0)
                return [];

            var names = el.val().split(',');
            return _(names).map(function(identifier)
            {
                return this.classes.byIdentifier(identifier).id;
            }, this);
        }
    });
});
