/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['./upload_base', 'jquery-safe'], function(View, $)
{
    return View.extend({
        filelistWrapper : 'eze-upload-new-file-',
        browseButton : 'eze-file-browser-',
        editor : null,

        initialize : function(options)
        {
            this.init(options);
            _.bindAll(this);

            this.model.on('autosave.saved', this.saved, this);

            this.filelistWrapper = this.filelistWrapper + this.attributeId;
            this.browseButton = this.browseButton + this.attributeId;

            this.sharedInitialize();

            this.loadUpload();
            return this;
        },

        events : {
            'click .remove' : 'onRemove'
        },

        onRemove : function(e)
        {
            e.preventDefault();
            var target = this.$(e.currentTarget);
            var data = {
                name : target.attr('name'),
                value : 'Remove this file',
                binaryRemove : 1
            };
            var extras = {};
            extras[target.attr('name')] = 1;
            this.model.attr(this.attributeId, this.version, [data], {silent: true});
            this.model.onChange(this.model, true, extras);
        },

        filesAdded : function()
        {
            this.uploader.settings.multipart_params = _.defaults({
                attributes : this.attributeId,
                inputFormName : this.base + '_data_binaryfilename_' + this.attributeId,
                returnTemplates : 1
            }, this.uploader.settings.multipart_params);

            this.uploader.start();

            // Show loader
            this.$('div.wrap').append(this._loader({size : 16, className : 'icon-16'}));
            this.hide(this.$('p.file'));
            this.hide(this.$('button.remove'));
            this.hide(this.$('div.upload'));
        },

        fileUploaded : function(up, file, info)
        {
            var response = $.parseJSON(info.response);
            if (response.ok)
                this.saved(this.model, response);
            else
                this.error();
        },

        saved : function(model, response)
        {
            if (response.hasOwnProperty('templates') && response.templates[this.attributeId]) {
                this.uploader.destroy();
                this.$(".wrap").replaceWith(response.templates[this.attributeId]);
                this.delegateEvents();
                this.loadUpload();
            }
            else if (response.attributes && response.attributes[this.attributeId]) {
                if (response.attributes[this.attributeId][0].hasOwnProperty('binaryRemove')) {
                    this.hide(this.$('.file, .remove'));
                }
            }
        }
    });
});
