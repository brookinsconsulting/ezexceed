/* global tinyMCE */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'shared/datatype', 'jquery-safe'], function(Backbone, View, $)
{
    var languageCache = {};
    return View.extend({
        textarea: null,
        editorId: null,

        config : null,

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);

            this.config = eZExceed.config.eZOeGlobalSettings || {};

            if (!window.$)
                window.$ = $;
            if (!window.jQuery)
                window.jQuery = $;
        },

        events : {
            'click .enableEditor' : 'toggleEditor'
        },

        render : function()
        {
            if (!this.config)
                return;

            if (typeof tinyMCE === 'undefined') {
                var scripts = $('#ezexceed-ezoe').text().split(',');
                var init = this.initEditor;
                scripts = _.filter(scripts, function(item)
                {
                    return item.match(/(^http)|(\.js$)/) && item !== '';
                });

                var languagePack = this.$('.oe-window').data('languagePack');
                if (languagePack in languageCache)
                    languagePack = languageCache[languagePack];

                require([scripts.shift()], function()
                {
                    require(scripts, function()
                    {
                        if (_.isString(languagePack)) {
                            Backbone.sync('read', {url: '/ObjectEdit/tinyMceLanguagePack/' + languagePack}, {})
                                .done(function(language)
                                {
                                    languageCache[languagePack] = language;
                                    tinyMCE.addI18n(language);
                                    init();
                                });
                        }
                        else {
                            tinyMCE.addI18n(languagePack);
                            init();
                        }
                    });
                });
            }
            else {
                this.initEditor();
            }

            return this;
        },

        preInit : function()
        {
            // Change id of textarea to avoid possible conflicts
            // down the stack when the same object is opened twice
            this.textarea = this.$('textarea');
            this.editorId = _.uniqueId(this.textarea.attr('id'));
            this.textarea.attr('id', this.editorId);

            var uri = '//' + document.location.host + this.config.ez_tinymce_url;
            var tps = this.config.plugins.split(',');
            var pm = tinyMCE.PluginManager;

            tinyMCE.ScriptLoader.markDone(uri.replace('tiny_mce', 'langs/' + this.config.language));

            _.each(tps, function(plugin)
            {
                pm.urls[plugin] = uri.replace('tiny_mce.js', 'plugins/' + plugin);
            });

            this.config.paste_preprocess = this.pastePreProcess;
            this.config.paste_postprocess = this.pastePostProcess;

            var editorChanged = _.debounce(this.save, 500);
            var _this = this;

            this.config.setup = function(ed)
            {
                ed.onInit.add(function(ed)
                {
                    tinyMCE.dom.Event.add(ed.getWin(), "blur", function()
                    {
                        if (ed.isDirty()) {
                            ed.save();
                            $(ed.getElement()).trigger('focusout');
                        }
                    });
                    _this.addDisableButton();

                });

                ed.onPaste.add(function(editor)
                {
                    editorChanged(editor);
                });

                ed.onChange.add(function(editor)
                {
                    editorChanged(editor);
                });

                ed.onKeyUp.add(function(editor)
                {
                    editorChanged(editor);
                });
            };
        },

        save : function(editor)
        {
            /**
             * Need a try/catch here cause this could happen when closing the editor and a save trigger
             * would be triggered
             */
            try {
                if (editor.isDirty()) {
                    editor.save();
                    this.$('textarea').trigger('change');
                }
            }
            catch (e) {
            }

        },

        initEditor : function()
        {
            this.preInit();
            var data = this.$('.hiddenData').data();

            var eZOeAttributeSettings = this.config;

            eZOeAttributeSettings.ez_attribute_id = data.attributeid;
            eZOeAttributeSettings.ez_contentobject_id = data.contentobjectid;
            eZOeAttributeSettings.ez_contentobject_version = data.version;
            eZOeAttributeSettings.theme_advanced_buttons1 = data.buttons;
            eZOeAttributeSettings.theme_advanced_statusbar_location = data.pathlocation;
            eZOeAttributeSettings.theme_advanced_toolbar_location = data.toolbarlocation;
            eZOeAttributeSettings.spellchecker_languages = data.spelllanguages;

            if (this.textarea.length > 0) {
                if (tinyMCE.getInstanceById(this.editorId) === undefined) {
                    tinyMCE.baseURL = '//' + document.location.host + '/extension/ezoe/design/standard/javascript';
                    new tinyMCE.Editor(this.editorId, eZOeAttributeSettings).render();
                    tinyMCE.init(this.config);
                }
                else {
                    tinyMCE.execCommand('mceRemoveControl', false, this.textarea.attr('id'));
                }
            }
        },

        pastePreProcess : function(pl, o)
        {
            /** Strip <a> HTML tags from clipboard content (Happens on Internet Explorer). */
            o.content = o.content.replace( /(\s[a-z]+=")<a\s[^>]+>([^<]+)<\/a>/gi, '$1$2' );
        },

        pastePostProcess : function(pl, o)
        {
            /** removes \n after <br />, this is for paste of text with soft carriage return from Word in Firefox. See issue http://issues.ez.no/18702. */
            o.node.innerHTML = o.node.innerHTML.replace(/<br\s?.*\/?>\n/gi,'<br>');
        },

        addDisableButton : function()
        {
            var disableButton = this.$('#' + this.textarea.attr('id') + '_disable');
            disableButton.on('click', this.toggleEditor);
        },

        toggleEditor : function()
        {
            var editor = tinyMCE.getInstanceById(this.editorId);
            if (editor)
            {
                editor.remove();
                this.show(this.$('.enableEditor').parent());
            }
            else {
                this.hide(this.$('.enableEditor').parent());
                this.initEditor();
            }
        }

    });
});
