/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'selectable'], function(View, selectable)
{
    return View.extend({
        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);
        },

        parseEdited : function()
        {
            return _.map(this.$('.eze-save'), function(el)
            {
                el = this.$(el);
                return {
                    name : el.attr('name'),
                    value : el.val()
                };
            }, this);
        },

        render : function()
        {
            selectable(this.$('select'));
            return this;
        }
    });
});
