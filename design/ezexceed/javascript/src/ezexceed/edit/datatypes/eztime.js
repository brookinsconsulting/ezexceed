/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'shared/timepicker'], function(View, timepicker)
{
    return View.extend({
        $input : null,

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);
            return this;
        },

        parseEdited : function()
        {
            var timeString = this.$input.val();
            var hour = '';
            var minute = '';
            var second = '';

            if (timeString) {
                var split = this.$input.val().split(':');

                if (split[0])
                    hour = split[0];
                if (split[1])
                    minute = split[1];
                if (split[2])
                    second = split[2];
            }
            return [
                {name : this.base + '_time_hour_' + this.attributeId, value : hour},
                {name : this.base + '_time_minute_' + this.attributeId, value : minute},
                {name : this.base + '_time_second_' + this.attributeId, value : second}
            ];
        },

        render : function()
        {
            this.$input = this.selectByInputName('time');
            timepicker(this.$input);
            return this;
        }
    });
});
