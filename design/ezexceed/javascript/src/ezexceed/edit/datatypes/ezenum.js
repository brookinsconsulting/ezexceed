/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/datatype', 'module', 'selectable'], function(View, Module, selectable)
{
    return View.extend({

        initialize : function(options)
        {
            _.bindAll(this);
            this.init(options);
        },

        render : function()
        {
            selectable(this.$('select'));
            return this;
        },

        parseEdited : function()
        {
            var values = {
                id : [],
                values : [],
                elems : []
            };

            _.each(this.$(':selected'), function(el)
            {
                el = this.$(el);
                var data = el.data();
                values.id.push(data.id);
                values.values.push(el.val());
                values.elems.push(data.value);

            }, this);

            return [
                {name : this.buildName('data_enumvalue'), value : values.values},
                {name : this.buildName('select_data_enumelement'), value : values.values},
                {name : this.buildName('data_enumelement'), value : values.elems},
                {name : this.buildName('data_enumid'), value : values.id}
            ];
        }
    });
});
