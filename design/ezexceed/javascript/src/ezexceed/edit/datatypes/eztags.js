/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['jquery-safe', 'shared/datatype'], function($, View)
{
    return View.extend({
        template : $('#tmpl-eztag-add'),

        initalize : function(options)
        {
            _.bindAll(this, 'render', 'addTag', 'removeTag');

            this.init(options);
        },

        events :
        {
            'click .button-add-tag' : 'addTag',
            'click .eztag-remove' : 'removeTag'
        },

        addTag : function()
        {
            var tagInput = this.selectByInputName('eztags_tag');
            var tagName = tagInput.val();
            var exists = false;

            this.$('.eztag-tags .eztag-tag').each(function(i, el)
            {
                if ($.trim($(el).html()) === tagName)
                    exists = true;
            });

            var ul = this.$('.eztag-tags');

            if(tagName.length > 0 && exists === false)
            {
                ul.append(this.template.tmpl({keyword : tagName}));
                tagInput.val('');
            }
        },

        removeTag : function(e)
        {
            e.preventDefault();

            $(e.currentTarget).parent().parent().remove();
        },

        parseEdited : function()
        {

        },

        render : function()
        {
            this.$('.input-tag-name').unbind();
            this.delegateEvents();
        }
    });
});
