/* global _errs */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/modal/regular', 'jquery-safe', 'shared/flash', 'ezexceed/main/templates/errors'],
    function(View, $, Flash, Template)
{
    return View.extend({
        className : 'eze-blackout',
        tpl : 'main/errors',

        active : false,

        // Pattern used to match the error message from ajax calls
        // to decide if the user was logged out
        errorPattern : /.*/,

        // Pattern used to match a special element in the DOM that contains
        // the new formtoken value
        formtokenPattern : /<span.*?ezxform_token_js.*?><\/span>/,

        // A reference to the app global syncBuffer
        syncBuffer : null,

        accepts : ['tpl', 'appendTo', 'errorPattern', 'syncBuffer'],

        initialize : function(options)
        {
            _.bindAll(this);
            this.syncBuffer = [];
            _.extend(this, _.pick(options, this.accepts));
        },

        events : {
            'submit form' : 'signin',
            'click .dismiss': 'dismiss',
            'click': function(e) { e.stopPropagation(); }
        },

        500 : function(xhr, type)
        {
            // Create a newline separated string of generated urls in syncBuffer
            // The _errs variable exists if errorception is enabled for the site
            // using the errorbeacon.tpl template
            // The _errs takes metadata that will be passed alongside the exception
            // we throw in the end of this method
            if (typeof _errs !== 'undefined') {
                var urls = _.reduce(this.syncBuffer, function(memo, val)
                {
                    return memo + '\n' + (val[2].url || _.result(val[1], 'url'));
                }, '');
                _errs.meta = {
                    urls : urls
                };
            }

            this.syncBuffer.pop();

            if (type === 'abort') {
                return;
            }

            this.active = true;
            var messages = {
                500: 'An internal server error occurred, this has been reported',
                504: 'A network error occurred, consult your IT department regarding misbehaving gateways'
            };
            var xhrStatus = xhr.status || 500;
            this.render({
                message : '' + xhrStatus + ': ' + messages[xhrStatus]
            });

            // Stop the toolbar pulsing now and set that item as non-active
            Flash.stopPulsing().removeClass('active');

            // Throws an error for our beacon to forward to a centralized logger
            // if that is set up
            throw 'Internal server error 500';
        },

        signin : function(e)
        {
            e.preventDefault();
            e.stopPropagation();
            $.ajax({
                url: '/user/login',
                type: 'POST',
                crossDomain: false,
                cache: false,
                data: {
                    Login : this.$('[name=login]').val(),
                    Password : this.$('[name=password]').val(),
                    RedirectURI: '',
                    LoginButton :'Logg inn'
                }
            }).always(this.checkSignedIn);
        },

        checkSignedOut : function(resp, type, xhr, def)
        {
            if (resp.hasOwnProperty('error_text') && resp.error_text.match(this.errorPattern)) {
                this.active = true;
                this.render({
                    message : "You've been logged out",
                    username: eZExceed.user.username
                });
                this.$('[name=password]').focus();
                return def;
            }
            this.syncBuffer.pop();
            return def.resolve(resp);
        },

        checkSignedIn : function(resp)
        {
            // Check for #eze-toolbar in response = equals signed in
            if (resp.indexOf('eze-toolbar') !== -1) {
                // Replace formtoken
                var formtokenEl = resp.match(this.formtokenPattern);
                if (formtokenEl) {
                    eZExceed.formtoken = $(formtokenEl[0]).attr('title');
                }

                this.active = false;
                this.trigger('signedin');
                this.$el.detach();
            }
        },

        dismiss : function()
        {
            this.active = false;
            this.$el.detach();
        },

        render : function(context)
        {
            this.$el
                .html(Template(context || {}))
                .appendTo(this.appendTo);

            return this;
        }
    });
});
