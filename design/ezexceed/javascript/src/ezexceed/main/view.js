/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', './toolbar', 'ezexceed/pencils', 'shared/modal', 'jquery-safe', 'ezexceed/notification/main'],
    function(View, Toolbar, Pencils, Modals, $, Notification)
{
    // Outer view for all of the view mode (not all content)
    return View.extend(
    {
        views : null,

        originalBodyCss : null,

        scrollTop : 0,

        // Left side margin to add to body to make room for
        // the toolbar
        MARGIN_LEFT : 63,

        // Store original marging left
        marginLeft: 0,

        initialize : function()
        {
            _.bindAll(this);

            this.views = {};

            this.listenTo(eZExceed.stack.collection, {
                'add remove': this.bindStackToModal,
                'reset': this.removeStack
            });
            this.listenTo(eZExceed, 'resize', this.onResize);

            this.$doc = $(document);
            this.$body = this.$('body');
            this.$html = this.$('html');
            this.marginLeft = this.$body.data('margin-left');
            this.originalBodyCss = {
                'overflow' : this.$body.css('overflow'),
                'overflow-y' : this.$body.css('overflow-y')
            };
            this.originalHtmlCss = {
                'overflow' : this.$html.css('overflow'),
                'overflow-y' : this.$html.css('overflow-y')
            };
        },

        events : {
            'click html' : 'documentClicked'
        },

        render : function()
        {
            this.$toolbar = this.$(this.options.toolbarId);
            this.views.toolbar = new Toolbar({el : this.$toolbar}).render();
            this.views.pencils = new Pencils.manager({el : this.$body}).render();

            return this;
        },

        onResize : function()
        {
            this.views.pencils.render();
        },

        bindStackToModal : function(model, collection)
        {
            /** First item. */
            if (!this.views.hasOwnProperty('modal')) {
                this.scrollTop = this.$doc.scrollTop();
                this.$doc.scrollTop(0);
                this.views.modal = new Modals.backdrop({view: eZExceed.stack});
                this.views.modal.$el.appendTo(this.$('.eze-main'));
                var css = {'overflow':'hidden'};
                this.$body.css(css);
                this.$html.css(css);
                this.views.modal.render();
            }

            /** Last item removed. */
            if (collection.length === 0)
                this.removeStack();
        },

        removeStack : function()
        {
            this.views.modal.$el.addClass('hide');
            this.views.modal.view.remove();
            this.views.modal.remove();
            this.$body.css(this.originalBodyCss);
            this.$html.css(this.originalHtmlCss);
            this.$doc.scrollTop(this.scrollTop);
            delete this.views.modal;

            var showNotification = false,
                text = {},
                reloadParent = false;

            /**
             * If there are changes to current viewed node, we must display a reload confirm
             */
            if (eZExceed.currentNodeChanges.hasOwnProperty('removed')) {
                /**
                 * Show removed message. Must refresh parent page
                 */
                text.textHeading = 'You have deleted the page you are viewing';
                text.textDescription = 'The parent page must be refreshed in order to update the view';
                showNotification = true;
                reloadParent = true;
            }
            else if ('changed' in eZExceed.currentNodeChanges) {
                /**
                 * Show changed message. Must refresh current page
                 */
                text.textHeading = 'You have changed the page you are viewing';
                text.textDescription = 'The page must be refreshed in order to update the view';
                showNotification = true;
            }

            if (showNotification) {
                var notification = new Notification(text);
                notification.on('action',function()
                {
                    if (reloadParent) {
                        var pathArr = window.location.pathname.split('/');
                        pathArr.pop();
                        window.location = window.location.protocol + '//' + window.location.hostname + pathArr.join('/');
                    }
                    else
                        window.location.reload();
                }).render();
            }
        },

        documentClicked : function(e)
        {
            this.$el.trigger('documentClicked', e.target);
        },

        hide : function()
        {
            this.$toolbar.addClass('hide');
            this.$body.css('margin-left', this.marginLeft);
        },

        show : function()
        {
            this.$toolbar.removeClass('hide');
            this.$body.css('margin-left', this.marginLeft + this.MARGIN_LEFT);
        }
    });
});
