define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n    <form onsubmit=\"return false;\">\n        <input name=\"login\" type=\"text\" placeholder=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Username", options) : helperMissing.call(depth0, "translate", "Username", options)))
    + "\" value=\"";
  if (stack2 = helpers.username) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.username; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" disabled/>\n        <input name=\"password\" type=\"password\" placeholder=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Password", options) : helperMissing.call(depth0, "translate", "Password", options)))
    + "\" autofocus />\n        <button type=\"submit\" class=\"btn\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Login and resume", options) : helperMissing.call(depth0, "translate", "Login and resume", options)))
    + "</button>\n    </form>\n    ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        <button type=\"submit\" class=\"btn dismiss\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Dismiss", options) : helperMissing.call(depth0, "translate", "Dismiss", options)))
    + "</button>\n    ";
  return buffer;
  }

  buffer += "<div class=\"alert-wrapper\">\n    <img class=\"alert-icon\" src=\"";
  options = {hash:{
    'size': (48),
    'white': (1)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, "Alert", options) : helperMissing.call(depth0, "icon", "Alert", options)))
    + "\" />\n    <p class=\"alert-message\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.message, options) : helperMissing.call(depth0, "translate", depth0.message, options)))
    + "</p>\n    ";
  stack2 = helpers['if'].call(depth0, depth0.username, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</div>\n";
  return buffer;
  })

});