/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'shared/modal', 'shared/flash', 'ezexceed/publish/globalstatus'],
    function(View, Modals, Flash, GlobalStatus)
{
    /** Toolbar view, responsible for controlling actions and creating new views based on it. */
    return View.extend({
        nodeId : null,
        objectId : null,
        nodeIdPath : null,
        name : '',
        classId: null,
        devicesView : null,
        currentOpenView : null,
        activeClasses : 'active',

        initialize : function()
        {
            _.bindAll(this);

            var keys = ['nodeId', 'objectId', 'nodeIdPath', 'language', 'name', 'classId'];
            _.extend(this, _.pick(this.$el.data(), keys));

            // When close all in stack is hit a reset on stack is triggered
            // use this to remove any active states as there cant possibly
            // be anything active afterwards
            eZExceed.stack.collection.on('reset', this.disableActiveButtons, this);

            this.initPublishStatus();
        },

        initPublishStatus : function()
        {
            var options = {
                el : this.$('li.publish').find('span.status-icon'),
                objectId : this.objectId,
                language : this.language
            };

            eZExceed.publishStatus = new GlobalStatus(options).trigger('change');
        },

        events : {
            'click .edit' : 'editObject',
            'click .publish' : 'publish',
            'click .layout' : 'layout',
            'click .create' : 'addContent',
            'click .sitemap' : 'finder',
            'click .find' : 'grid',
            'click .eze-user-preferences' : 'userPreferences',
            'click .activity' : 'activity',
            'click .preview' : 'preview',
            'click .extension a' : 'loadExtension'
        },

        loadExtension : function(e)
        {
            var target = this.$(e.currentTarget);
            var data = target.data();
            e.preventDefault();

            var options = data;
            // If namespace is set we should map it to its path
            if (data.ns && data.path) {
                var paths = {};
                paths[data.ns] = data.path;
                require.config({paths: paths});
            }
            else {
                options.url = target.attr('href');
            }

            var context = {
                heading: data.name,
                icon: data.icon,
                render: true
            };

            var moduleOptions = {
                endLoading: this.endLoading,
                target: target,
                module: data.module || 'ezexceed/extension',
                destruct: null
            };

            var loaders = {
                'popover' : this.loadViewInPopover,
                'stack' : this.loadViewInStack
            };
            loaders[data.type](data['class'], options, moduleOptions, context);
        },

        buildModuleOptions : function(e, module)
        {
            e.preventDefault();
            e.stopPropagation();

            return {
                endLoading : this.endLoading,
                target : this.$(e.currentTarget),
                module : module,
                destruct : null
            };
        },

        endLoading : function(moduleOptions)
        {
            moduleOptions = (moduleOptions || {});
            if (_(moduleOptions).has('target')) {
                Flash.stopPulsing(this.$(moduleOptions.target));
            }
        },

        finder : function(e)
        {
            var options = {
                nodeIdPath : this.nodeIdPath,
                actions : {
                    read : true,
                    copy : true,
                    remove : true,
                    edit : true,
                    hide : true
                }
            };

            var context = {
                heading : 'Sitemap',
                icon : 'Sitemap',
                render : false
            };

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/finder');

            this.loadViewInStack('ezexceed/finder', options, moduleOptions, context);
        },

        grid: function(e)
        {
            var options = {
                objectId : this.objectId,
                language : this.language
            };

            var context = {
                heading : 'Find content',
                icon : 'Grid',
                render : true
            };

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/grid');

            this.loadViewInStack('ezexceed/grid', options, moduleOptions, context);
        },

        layout: function(e)
        {
            var options = {
                objectId : this.objectId,
                language : this.language
            };

            var context = {
                heading : [
                    'Layout for',
                    {
                        text : this.name,
                        quotes : true
                    }
                ],
                classId : this.classId,
                render : false
            };

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/layout');

            this.loadViewInStack('ezexceed/layout', options, moduleOptions, context);
        },

        loadViewInStack : function(name, options, moduleOptions, context)
        {
            this.disableActiveButtons();
            // Close stack when clicking same button again and stack depth is 1
            var depth = eZExceed.stack.depth();
            if (depth > 1) {
                eZExceed.stack.flashCloseAll();
                return false;
            }
            else if (depth === 1 && name === this.currentOpenView) {
                this.currentOpenView = null;
                eZExceed.stack.closeAll();
                return false;
            }

            Flash.startPulsing(moduleOptions.target);
            this.currentOpenView = name;

            var activeClasses = this.activeClasses;

            require([moduleOptions.module], function(Module)
            {
                var view = (Module.view || Module);
                eZExceed.stack
                    .once('loaded', function(){moduleOptions.endLoading(moduleOptions);})
                    .replace(view, options, context)
                    .on('destruct', moduleOptions.destruct);
                moduleOptions.target.addClass(activeClasses);
            });

            return true;
        },

        userPreferences : function(e)
        {
            var options = {userId : this.$(e.currentTarget).data('userId')};

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/userpreferences');
            moduleOptions.className = 'eze-dashboard-modal';
            this.loadViewInPopover('ezexceed/userpreferences', options, moduleOptions);
        },

        editObject : function(e)
        {
            var options = {
                id : this.objectId,
                language : this.language
            };

            var context = {
                heading : {text : this.name},
                classId : this.classId
            };

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/edit');
            moduleOptions.destruct = this.onEditComplete;

            this.loadViewInStack('ezexceed/edit', options, moduleOptions, context);
        },

        onEditComplete : function(model)
        {
            this.name = model.get('name');
        },

        loadViewInPopover : function(name, options, moduleOptions, context)
        {
            context = context || false;
            // Close stack when clicking same button again and stack depth is 1
            var depth = eZExceed.stack.depth();
            if (depth > 1) {
                eZExceed.stack.flashCloseAll();
                this.disableActiveButtons();
                return false;
            }

            this.disableActiveButtons();
            if (name === this.currentOpenView) {
                this.trigger('modal:close');
                this.currentOpenView = null;
                return false;
            }

            eZExceed.stack.closeAll();

            this.currentOpenView = name;

            delete options.e;
            Flash.startPulsing(moduleOptions.target);
            moduleOptions.target.addClass(this.activeClasses);

            var modal = new Modals.regular({
                pinTo : moduleOptions.target
            });

            if (moduleOptions.className) {
                modal.$el.addClass(moduleOptions.className);
            }

            var toolbar = this;
            require([moduleOptions.module], function(Module)
            {
                options.el = modal.$content;
                options.container = modal;

                var ctor = Module.view || Module;
                var view = new ctor(options);
                // Propagate heading from context if view doesnt have a hardcoded one
                if (context && context.heading && !view.heading) {
                    view.heading = context.heading;
                }
                view.once('loaded', function()
                {
                    moduleOptions.endLoading(moduleOptions);
                });

                toolbar.trigger('modal:close').on('modal:close', modal.remove);

                modal.on('closed', function()
                {
                    toolbar.off('modal:close');
                    toolbar.disableActiveButtons();
                    toolbar.currentOpenView = null;
                    eZExceed.stack.off(null, null, modal);
                });

                eZExceed.stack.on('push', modal.remove, modal);

                if (context.render) {
                    modal.display(view, true);
                }
            });

            return true;
        },

        addContent : function(e)
        {
            var options = {nodeId : this.nodeId};

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/addcontent');
            moduleOptions.className = 'eze-add-content-modal';
            this.loadViewInPopover('ezexceed/addcontent', options, moduleOptions);
        },

        publish : function(e)
        {
            var options = {
                objectId : this.objectId,
                language : this.language
            };

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/publish');
            moduleOptions.className = 'eze-publish-modal';
            this.loadViewInPopover('ezexceed/publish', options, moduleOptions);
        },

        activity : function(e)
        {
            var options = {
                focus : true,
			objectId : this.objectId,
			render : false
            };

            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/activity');
            moduleOptions.className = 'eze-activity-modal';
            this.loadViewInPopover('ezexceed/activity', options, moduleOptions);
        },

        preview : function(e)
        {
            var target = this.$(e.currentTarget);
            var options = {
                objectId : this.objectId,
                language : this.language,
                timeline : target.data('timeline')
            };
            var moduleOptions = this.buildModuleOptions(e, 'ezexceed/preview');
            moduleOptions.className = 'eze-preview-modal';
            this.loadViewInPopover('ezexceed/preview', options, moduleOptions);
        },

        disableActiveButtons : function()
        {
            var target = this.$('.active');
            target.removeClass('active');
            Flash.stopPulsing(target);
        }
    });
});
