define(['ezexceed/preview/main', 'ezexceed/preview/models'], function(MainView, Models)
{
    return {
        view: MainView,
        collection: Models.collection,
        model: Models.Model
    };
});
