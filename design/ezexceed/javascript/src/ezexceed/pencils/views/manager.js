/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', './attribute', '../model', 'shared/views/action', 'module', 'jquery-safe'], function(View, AttributeView, Model, Action, Module, $)
{
    return View.extend({
        current : null,
        lastClicked : null,

        initialize : function()
        {
            _.bindAll(this);

            $(document).on('documentClicked', this.close);
        },

        events : {
            'click .attribute-direct-edit' : 'editAttribute',
            'click .eze-block-pencil>div' : 'expand',
            'click .eze-block-pencil .edit a' : 'editBlock',
            'click .eze-block-pencil .add a' : 'addToBlock',
            'click .eze-block-pencil .eze-node a' : 'editNode'
        },

        addToBlock : function(e)
        {
            e.preventDefault();

            require(['ezexceed/pencils/views/addtoblock'], function(AddToBlock)
            {
                var target = $(e.currentTarget).find('li.add a');
                var addToBlock = new AddToBlock({target : target});
                addToBlock.on('saved', function()
                {
                    /**
                     * Mark currentNode as changed
                     */
                    eZExceed.currentNodeChanges.changed = true;
                });
            });
        },

        render : function()
        {
            var silverPencils = this.$('.eze-pencil.attribute-direct-edit'),
                blockPencils = this.$('.eze-pencil:not(.attribute-direct-edit)');
            /**
             * Position silver pencils first. If overlapping pencils, silver pencils should appear first
             */
            silverPencils.each(this.position);
            this.$pencils = silverPencils.add(blockPencils);
            blockPencils.each(this.position);

            this.$pencils.removeClass('invisible');

            return this;
        },

        expand : function(e)
        {
            e.stopPropagation();
            this.$pencils.children('div.kp-edit-block-box').removeClass('active');
            $(e.currentTarget).addClass('active');

            return this;
        },

        editNode : function(e)
        {
            var target = $(e.currentTarget);

            var options = {
                id : target.data('objectId'),
                language : target.data('language')
            };

            var context = {classIdentifier : target.data('classIdentifier')};

            require(['ezexceed/edit'], function(Edit)
            {
                eZExceed.stack.push(Edit.view, options, context);
            });
        },

        editBlock : function(e)
        {
            var target = $(e.currentTarget);
            var onDestruct = this.closePencils;

            require(['ezexceed/layout/views/block'], function(Block)
            {
                var options = {
                    objectId : target.data('objectId'),
                    zoneIndex : target.data('zoneIndex'),
                    blockId :  target.data('blockId'),
                    language : target.data('language')
                };

                var context = {
                    heading : [
                        'Settings for',
                        {
                            text  : target.data('blockName'),
                            quotes : true
                        }
                    ],
                    render : false,
                    icon : 'Documents'
                };

                var blockView = eZExceed.stack.push(Block, options, context).on('destruct', onDestruct);
                blockView.on('saved', function() {
                    /**
                     * Mark currentNode as changed
                     */
                    eZExceed.currentNodeChanges.changed = true;
                });
            });
        },

        closePencils : function()
        {
            this.$pencils.children('div.active').removeClass('active');
        },

        editAttribute : function(e)
        {
            e.preventDefault();
            e.stopPropagation();

            var target = $(e.currentTarget), data = target.data();

            if (this.current) {
                // Dont do anything if the requested attribute is open
                if (this.lastClicked && target[0] === this.lastClicked[0]) {
                    return;
                }

                this.close();
            }

            this.model = new Model();

            this.model.clear({silent : true});

            this.model.set({
                identifier : data.attribute,
                type : data.type,
                objectId : data.objectId,
                language : data.language,
                version : data.version
            }, {silent : true});

            this.current = new AttributeView({
                trigger : target,
                datatypes : Module.config().classes,
                model : this.model,
                button : target,
                stickTo : target
            });

            this.current.on('destruct', this.onEntityDestruct);

            target
                .data('original', target.html())
                .html(this._loader({className : 'icon-16',size : 16}));

            this.model.on('change', function()
            {
                target.html(target.data('original')).addClass('active');
            }).fetch();

            this.lastClicked = target;

            return this;
        },

        onEntityDestruct : function()
        {
            this.current = null;
        },

        close : function(e, target)
        {
            /** TinyMCE places the HTML for the style-dropdown outside this element. This prevents the directEdit-window from closing. */
            if ($(target).closest('div.mceListBoxMenu').length > 0) {
                return;
            }

            if (this.current) {
                this.current.trigger('destruct', e);
            }

            this.closePencils();
        },

        position : function(i, el)
        {
            el = $(el);
            if (el.hasClass('attribute-direct-edit')) {
                el.closest('.eze-update-on-edit').css({
                    position: 'relative'
                });
            }
            else {
                var overlappingElements = this.overlapping(el);
                var silverPencils = overlappingElements.filter('.attribute-direct-edit').length;
                var index = 0;

                overlappingElements.each(function(i, el)
                {
                    index = silverPencils + i;
                    el = $(el);
                    var ul = el.find('ul');
                    var left = (ul.width() + 6) * index;
                    var wrapper = ul.parent();

                    if (wrapper.length > 0) {
                        wrapper.css('left', wrapper.position().left + left);
                        el.addClass('expanded');
                    }
                });
            }
        },

        overlapping : function(el)
        {
            el = $(el);
            if (el.hasClass('expanded')) {
                return $();
            }

            var getPositions = function(elem)
            {
                var pos, width, height;
                pos = elem.offset();
                width = elem.width();
                height = elem.height();
                return [
                    [ pos.left, pos.left + width ],
                    [ pos.top, pos.top + height ]
                ];
            };

            var comparePositions = function(p1, p2)
            {
                var r1, r2;
                r1 = p1[0] < p2[0] ? p1 : p2;
                r2 = p1[0] < p2[0] ? p2 : p1;
                return r1[1] > r2[0] || r1[0] === r2[0];
            };

            var pos1 = getPositions(el),
                pos2;

            return this.$pencils.filter(function(i, el)
            {
                pos2 = getPositions($(el));
                return comparePositions(pos1[0], pos2[0]) && comparePositions(pos1[1], pos2[1]);
            });
        }
    });
});
