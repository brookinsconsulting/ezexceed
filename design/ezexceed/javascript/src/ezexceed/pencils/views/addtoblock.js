/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['jquery-safe', 'shared/view', 'ezexceed/layout/model', 'ezexceed/layout/views/add', 'ezexceed/layout/views/remove', 'ezexceed/finder'], function($, View, Model, Add, Remove, Finder)
{
    return View.extend({
        target : null,
        finder : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['target']));

            this.initializeModel();
        },

        initializeModel : function()
        {
            var options = this.target.data();
            options.returnItems = 0;
            options.returnTemplates = 0;

            this.model = new Model(options);
            this.model.fetch().done(this.render);
        },

        /** Replaces the existing pencil with an updated one. */
        contentAdded : function(response)
        {
            var targetClass = 'ul.kp-edit-block-elements';
            var items = $(response.html).find(targetClass);

            this.target.closest('div.eze')
                .find(targetClass).replaceWith(items)
                .find('div.kp-edit-block-box').addClass('active');
        },

        getPencil : function()
        {
            this.model.getPencil().done(this.contentAdded);
        },

        onAdd : function(e, node)
        {
            this.finder.trigger('save');
            this.model.addItem(node.id, node.get('objectId'), this.saved);
        },

        onRemove : function(e, node)
        {
            this.finder.trigger('save');
            var objectId = node.get('objectId');
            this.model.removeItem(objectId, this.saved);
        },

        saved : function()
        {
            this.trigger('saved');
            this.finder.trigger('saved');
        },

        render : function()
        {
            var addAction = new Add({parentModel : this.model}).on('action', this.onAdd);
            var removeAction = new Remove({parentModel : this.model}).on('action', this.onRemove);

            var options = {
                customActions : {
                    add : addAction,
                    remove : removeAction
                }
            };

            var context = {
                heading : [
                    'Add item to',
                    {
                        text : this.target.data('blockName'),
                        quotes : true
                    }
                ],
                icon : 'Layout',
                render : false
            };

            this.finder = eZExceed.stack.push(Finder.view, options, context).on('destruct', this.getPencil);
        }
    });
});
