/* global tinyMCE */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'jquery-safe', 'jqueryui/draggable', 'jquery-ui.touch'], function(View, $)
{
    return View.extend({
        className : 'popover semi-direct',
        handler : null,
        values : null,
        changed : false,
        options : null,
        $attr : null,
        appendTo : '.eze-main',

        initialize : function(options)
        {
            _.bindAll(this);

            this.options = (options || {});
            this.handler = options.handler;

            this.values = {};

            this.model.on('change', this.render);
            this.on('destruct', this.destruct);
        },

        events : {
            'click a.publish' : 'saveAndPublish',
            // Using trigger over direct call is intended
            'click a.discard, button.close' : function(e) { this.trigger('destruct', e); },

            // Make sure clicks inside this view does not trigger
            // the handling in the outer view that closes this view
            'click' : function(e) { e.stopPropagation(); },

            // Autosave bindings
            'focusout textarea' : 'queueForSave',
            'change .attribute input[type=checkbox]' : 'queueForSave',
            'change .attribute select' : 'queueForSave',
            'focusout .attribute :input' : 'queueForSave'
        },

        render : function()
        {
            var output = this.model.get('renderedOutput');

            this.$el.html(output);

            /** Append attribute to heading because it is not calculated until later. */
            this.$('.modal-header h3').append(this.$('label.heading').html());

            var options = {
                el : this.$el,
                objectId : this.model.get('objectId'),
                editor : this,
                model : this.model
            };

            var type = this.model.get('type');

            this.$el.appendTo($(this.appendTo));
            this.stickTo();
            this.$el.draggable({
                handle : '.modal-header'
            });

            require(['ezexceed/edit/datatypes'], function(Datatypes)
            {
                Datatypes.handler(type, false, options);
            });

            return this;
        },

        updateDOM : function(resp)
        {
            var update = this.options.trigger.parents('.eze-update-on-edit');

            if (update) {

                update.each(function()
                {
                    var node = $(this);
                    var attributeId = node.data('id');

                    if (resp.attributes.hasOwnProperty(attributeId)) {
                        var attribute = resp.attributes[attributeId].shift();

                        if (attribute) {
                            var inner = node.find('.eze-inner');
                            inner.html(attribute.value);
                        }
                    }
                });
            }

            this.options.button.html(this.options.button.data('original'));

            eZExceed.publishStatus.trigger('change');
        },

        stickTo : function()
        {
            this.$el.css({
                top : '15%',
                left : '50%',
                'margin-left' : '-290px',
                display: 'block'
            });

            return this;
        },

        destruct : function(e, options)
        {
            options = (options || {});
            var target = null,
                removeInternal = false;
            if (e) {
                target = this.$(e.currentTarget);
            }

            if (!target || (target && !target.hasClass('discard'))) {
                removeInternal = !this.save(options);
            }
            else {
                removeInternal = true;
            }

            if (removeInternal) {
                this.model.removeInternalDrafts();
            }

            if (this.options.button) {
                this.options.button.removeClass('active');
            }

            this.remove();

            return this;
        },

        // Overrides remove to also remove any active tinyMCE
        remove : function()
        {
            if ((tinyMCE !== undefined) && tinyMCE.activeEditor) {
                tinyMCE.activeEditor.remove();
            }

            this.trigger('remove');

            this.$el.remove();
        },

        saveAndPublish : function(e)
        {
            e.preventDefault();
            e.stopPropagation();

            this.options.button
                .data('original', this.options.button.html());

            /** No autoloader unless the user actually changed something. */
            if (!_.isEmpty(this.values)) {
                this.options.button.html(this._loader({className : 'icon-16',size : 16}));
            }

            this.trigger('destruct', e, {publish : true});

            return this;
        },

        // Triggered on changes in the DOM on any attributes
        // regular form field. Maintains a queue of values
        // to save
        queueForSave : function(e)
        {
            var field = $(e.currentTarget);

            if (field.data('autosave') === 'off') {
                return false;
            }

            var attributeId = this.$('.eze-attribute-id').data('attributeId');

            if (this.handler && this.handler.hasOwnProperty('parseEdited')) {
                this.values = this.handler.parseEdited(field);
            }
            else {
                this.values[field.attr('name')] = field.val();

                this.values.attributes = {};
                this.values.attributes[attributeId] = [{
                    name : field.attr('name'),
                    value : field.val()
                }];
            }
        },

        save : function(options)
        {
            if (_.isEmpty(this.values)) {
                return false;
            }

            options = (options || {});
            var form = this.$('form');
            var language = this.model.get('language');
            var version = this.model.get('versions').where({language : language});
            var mainVersion = version[0];
            var url = [form.attr('action'), mainVersion.id].join('/');

            // Need a clone so we dont manipulate our state in the coming step
            var values = _.clone(this.values);

            if (options.hasOwnProperty('publish')) {
                values.publishChanges = options.publish;
            }

            this.model.save(values, url, this.updateDOM);

            return true;
        }
    });
});
