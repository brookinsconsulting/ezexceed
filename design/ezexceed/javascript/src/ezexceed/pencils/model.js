/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone'], function(Backbone)
{
    return Backbone.Model.extend({
        defaults : function()
        {
            return {
                attributeId : null,
                objectId : null,
                identifier : null,
                renderedOutput : null,
                language : null,
                version : null,
                versions : null
            };
        },

        initialize : function()
        {
            _.bindAll(this);
        },

        url : function()
        {
            var data = [
                this.get('objectId'),
                this.get('identifier'),
                this.get('language')
            ];

            return '/ObjectEdit/getAttribute/' + data.join('/');
        },

        parse : function(data)
        {
            if ('versions' in data) {
                data.versions = new Backbone.Collection(data.versions);
            }
            return data;
        },

        save : function(values, url, success)
        {
            Backbone.sync('create', {url : url}, {data : values, success : success});
        },

        removeInternalDrafts : function()
        {
            var versions = this.get('versions').where({status : 5});

            if (!versions.length)
                return;

            var data = {
                versions : _.pluck(versions, 'id')
            };
            var config = {url : '/Object/removeInternalDrafts/' + this.id};
            return Backbone.sync('create', config, {data : data});
        }
    });
});
