/* global console, _eZExceedJSInjectionBootstrap */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'handlebars', 'ezexceed/backbone.sync', 'shared/contentclass', 'ezexceed/main/view',
    'shared/stack', 'moment'],
    function(Backbone, Handlebars, Sync, ContentClass, View, Stack, moment)
{
    Handlebars.registerHelper('translate-context', function(context)
    {
        if (context)
            this.translateContext = context;
    });

    var translateString = function(key)
    {
        if (key in window.eZExceed.config.Translations) {
            return window.eZExceed.config.Translations[key];
        }
        else {
            if (console) {
                console.log('Unregistered translation: ', key);
            }
        }
        return key;
    };

    Handlebars.registerHelper('translate', function(value)
    {
        var translateEntity = function(value)
        {
            /** Simple string that should be translated. */
            if (_(value).isString())
                return translateString(value);

            /** Single object. Return original text. */
            if (_(value).isObject()) {
                if (value.hasOwnProperty('quotes') && value.quotes)
                    return '«' + value.text + '»';
                else
                    return value.text;
            }
        };

        if (_(value).isArray())
            return _(value).map(translateEntity).join(' ');
        else
            return translateEntity(value);
    });

    Handlebars.registerHelper('cap', function(value, translate)
    {
        if (!value) { return false; }
        if (translate) {
            value = translateString(value);
        }
        return value[0].toUpperCase() + value.slice(1);
    });

    //  Format a timestamp
    //  usage: {{dateFormat date format="d.M.yyyy"}}
    Handlebars.registerHelper('dateFormat', function(context, options)
    {
        var defaultFormat = 'DD. MMM. YYYY';
        _.defaults(options.hash, {format : defaultFormat});
        var format = options.hash.format;
        var date = moment(context * 1000);
        var now = moment();
        if (format === 'pretty') {
            if (date.diff(now, 'days') === 0) {
                format = "LT";
            }
            else {
                format = defaultFormat;
            }
        }
        return date.format(format);
    });

    var isRetina = window.devicePixelRatio > 1 ? true : false;
    var retinaSizes = {
        16 : 32,
        24 : 48,
        32 : 48,
        48 : 128,
        128 : 128
    };

    Handlebars.registerHelper('classIcon', function(options)
    {
        _.defaults(options.hash, {size : 32});

        if (isRetina) {
            options.hash.size = retinaSizes[options.hash.size];
        }

        var contentClass;
        if (_.has(options.hash, 'id')) {
            contentClass = eZExceed.classes.get(options.hash.id);
        }
        else if (_.has(options.hash, 'identifier')) {
            contentClass = eZExceed.classes.byIdentifier(options.hash.identifier);
        }
        else {
            throw "classIcon helper requires either id or identifier as an argument";
        }

        return contentClass ? contentClass.icon(options.hash.size) : '';
    });

    Handlebars.registerHelper('icon', function(name, options)
    {
        _.defaults(options.hash, {size : 32, white : false});
        if (isRetina) {
            options.hash.size = retinaSizes[options.hash.size];
        }
        var size = 'x' + options.hash.size;
        var images = window.eZExceed.config.images;

        // Legacy support direct references to a path
        if (name.match(/\//)) return name;

        if (images.hasOwnProperty(size) && images[size].hasOwnProperty(name)) {
            var path = images[size][name];
            if (options.hash.white) {
                var parts = path.split('/');
                parts.splice(parts.length - 1, 0, 'white');
                path = parts.join('/');
            }
            return path;
        }
        else {
            size = options.hash.size;
            return [window.eZExceed.config.iconBaseUrl, size+'x'+size, name + '.png'].join('/');
        }
    });

    /**
     * Root-namespace
     *
     * @class eZExceed
     */
    return _.extend({
        contentClasses : [],

        config : {},

        currentNodeChanges : {},

        transformUrl : Sync.transformUrl,

        navigateTo : function(url)
        {
            if (this.stack.collection.length <= 1) {
                window.location = url;
            }
        },

        translate : function(value)
        {
            var translateEntity = function(value)
            {
                /** Simple string that should be translated. */
                if (_(value).isString())
                    return translateString(value);

                /** Single object. Return original text. */
                if (_(value).isObject()) {
                    if (value.hasOwnProperty('quotes') && value.quotes)
                        return '«' + value.text + '»';
                    else
                        return value.text;
                }
            };

            if (_(value).isArray())
                return _(value).map(translateEntity).join(' ');
            else
                return translateEntity(value);
        },

        /**
         * This function will log all identical(untranslated) text-strings.
         */
        validateTranslations : function()
        {
            _(this.config.Translations).each(function(value, key){
                if (value === key) {
                    console.log('Source and translation is identical: ', key);
                }
            });
        },

        init : function(options)
        {
            // This method bridges template generated content from ezp into
            // the client layer of exceed. Its usefull for including template operator
            // results for convenient use later
            _.extend(this.config, _eZExceedJSInjectionBootstrap());

            switch (this.config.locale) {
                case 'nor-NO':
                    require(['moment/lang/nb'], function()
                    {
                        moment.lang('nb');
                    });
                    break;
                default:
                    moment.lang('en');
                    break;
            }

            // Override moment date formatting for relative (2 days ago) formatting
            // so that "one hour ago" is the smallest possible unit
            var smallestTimeUnit = moment.relativeTime.h;
            moment.relativeTime = _.extend(moment.relativeTime, {
                s : smallestTimeUnit,
                m : smallestTimeUnit,
                mm : smallestTimeUnit
            });

            if (options.root !== '/') {
                Backbone.siteRoot = options.root;
                this.config.siteRoot = options.root;
            }

            ContentClass.collection.prototype.iconBaseUrl = this.config.iconBaseUrl;

            // Reference to content classes in system
            var classes = new ContentClass.collection();

            var creatableClasses = this.config.canCreateClasses;
            classes.reset(
                classes.parse(this.config._contentClasses, function(model)
                {
                    model.canCreate = (model.id in creatableClasses);
                })
            );

            this.classes = classes;

            delete this.config.canCreateClasses;
            delete this.config._contentClasses;

            /** Module-specific config. This is how it is done! */
            require.config({
                config : {
                    'shared/datepicker' : {
                        format : this.config.dateFormatCurrent,
                        dateFormat : this.config.dateFormat
                    },
                    'shared/contentobject' : {
                        classes : this.classes
                    },
                    'ezexceed/edit/views/main' : {
                        classes : this.classes
                    },
                    'ezexceed/pencils/views/manager' : {
                        classes : this.classes
                    },
                    'ezexceed/finder/views/main' : {
                        rootNodeId : this.config.rootNodes.sitemapRootNode.id
                    },
                    'ezexceed/finder/views/rootselector' : {
                        rootNodes : this.config.rootNodes
                    },
                    'ezexceed/finder/views/multiupload' : {
                        plupload : this.config.plupload
                    },
                    'ezexceed/finder/views/actions' : {
                        trashEnabled : this.config.trashEnabled
                    },
                    'ezexceed/grid/views/main' : {
                        classes : this.classes,
                        rootNodeId : this.config.rootNodes.sitemapRootNode.id
                    },
                    'ezexceed/edit/views/locations' : {
                        sitemapRootNodeId : this.config.rootNodes.sitemapRootNode.id,
                        classRootNodes : this.config.rootNodes.classRootNodes
                    },
                    'ezexceed/edit/datatypes/ezimage' : {
                        plupload : this.config.plupload
                    },
                    'ezexceed/edit/datatypes/upload_base' : {
                        plupload : this.config.plupload
                    },
                    'ezexceed/edit/views/locationsgrouped' : {
                        classes : this.classes
                    }
                }
            });

            // Create stack
            this.stack = new Stack();

            // This is ground zero for everything of any visual value
            // the rest of this init method is purely around for convenience and optimizations
            this.view = new View({
                el : options.$doc,
                toolbarId : '.eze #eze-toolbar'
            }).render();

            // Alias the hide/show methods so they can be exposed outside of this context
            // we know that the initialize needs this
            this.hideExceed = this.view.hide;
            this.showExceed = this.view.show;

            return this;
        }
    }, Backbone.Events);
});
