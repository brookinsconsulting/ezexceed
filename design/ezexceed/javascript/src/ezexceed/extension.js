define(['shared/view'], function(Base)
{
    var View = Base.extend({
        render: function()
        {
            this.$el.html(
                '<iframe style="width:100%; height:100%;"' +
                    'src="' + this.options.url + '">' +
                '</iframe>'
            );
            this.trigger('loaded');
            return this;
        }
    });
    return {
        view : View
    };
});
