define(['shared/containerview', 'shared/modal', 'ezexceed/addcontent/templates/widget'], function(View, Modal, Template)
{
    return View.extend({

        config : null,
        tagName : 'button',
        className : 'btn btn-primary create-new',

        nodeId : null,

        initialize : function(options)
        {
            options = options || ({});

            _.bindAll(this);

            this.config = {};
            this.views = {};

            _.extend(this, _.pick(options, ['config', 'nodeId']));

            _.defaults(this.config, {
                rootNodeId : 1,
                direction : 'bottom'
            });

            if (!this.nodeId) {
                this.nodeId = this.rootNodeId;
            }
        },

        events : {
            'click' : function()
            {
                require(['ezexceed/addcontent'], this.create);
            }
        },

        create : function(AddContent)
        {
            var popover = new Modal.regular({
                direction : this.config.direction,
                pinTo : this.$el
            });

            popover.$el.addClass('eze-add-content-modal');

            this.views.view = new AddContent.view({
                container : popover,
                action : 'push',
                el : popover.$content,
                nodeId : this.nodeId
            });

            this.views.popover = popover;

            this.listenTo(this.views.view, {'created' : this.onCreate});
        },

        onCreate : function()
        {
            this.views.popover.close();
            this.listenTo(this.views.view, {'edit:done' : this.editDone});
        },

        editDone : function(model)
        {
            this.trigger('edit:done', model);
        },

        render : function()
        {
            this.$el.html(Template({
                left : this.config.direction === 'left'
            }));
            return this;
        }
    });
});
