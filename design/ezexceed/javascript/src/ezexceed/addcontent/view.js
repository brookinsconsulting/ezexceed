/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'shared/contentclass', 'shared/contentobject', 'ezexceed/addcontent/templates/view'], function(View, ContentClass, ContentObject, Template)
{
    return View.extend(
    {
        isCreating : false,
        action : 'replace',
        heading : "Create new content",
        container : null,
        nodeId : null,

        initialize : function(options)
        {
            _.bindAll(this);

            this.$el.html(this._loader({size: 16}));

            _.extend(this, _.pick(options, ['nodeId', 'action', 'container']));

            if (!this.collection) {
                this.collection = new ContentClass.collection();
                this.collection.url = '/AddContent/get/' + this.nodeId;
                this.collection.on('reset', this.render).fetch({reset: true});
            }
        },

        events : {
            'click li' : 'createObject'
        },

        createObject : function(e)
        {
            if (this.isCreating) {
                return;
            }

            var target = this.$(e.currentTarget);
            target.find('img').replaceWith(
                this._loader({size: 16, className: 'icon-16'})
            );

            this.isCreating = true;

            var data = target.data();


            this.storeNewObject(ContentObject.model, data);

            return this;
        },

        render : function()
        {
            this.$el.html(Template(this.collection.toJSON()));
            this.container.display(this);
            this.position();
            this.trigger('loaded');
            eZExceed.trigger('addcontent:show');

            return this;
        },

        // Called after create on a class is called
        // and after the edit module is loaded and passed in
        storeNewObject : function(Model, data)
        {
            eZExceed.trigger('addcontent:create', data);
            new Model()
                .once('created', this.renderEdit)
                .create({
                    typeId : data.classId,
                    nodeId : this.nodeId,
                    name : [eZExceed.translate('New'), data.name.toLowerCase()].join(' ')
                });
        },

        renderEdit : function(model)
        {
            this.trigger('created');
            require(['ezexceed/edit'], _.bind(this.pushEdit, this, model));
        },

        pushEdit : function(model, Edit)
        {
            var options = {
                id : model.id,
                language : model.get('initialLanguage')
            };

            var context = {
                heading : {
                    text : model.get('name'),
                    quotes : true
                },
                classId : model.get('class')
            };

            eZExceed.stack[this.action](Edit.view, options, context).once('destruct', this.editDone);
        },

        editDone : function(model)
        {
            this.trigger('edit:done', model);
        },

        position : function()
        {
            var innerEl = this.$el.children(':first');
            if (this.$el.height() < innerEl.outerHeight()) {
                var newHeight = innerEl.height() - (innerEl.outerHeight() - this.$el.height());
                innerEl.css('height', newHeight);
            }
        }
    });
});
