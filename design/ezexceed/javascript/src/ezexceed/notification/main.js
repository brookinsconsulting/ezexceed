/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'jquery-safe', 'ezexceed/notification/templates/refresh', 'jquerypp/animate'],
    function(View, $, Template)
{
    return View.extend({
        tagName : 'section',
        className : 'eze-notification',
        appendTo : '.eze-main',
        rendered : false,
        textHeading : '',
        textDescription : '',
        displayTime : 15000,

        accepts : ['appendTo', 'textHeading', 'textDescription', 'displayTime'],

        initialize : function(options)
        {
            _.bindAll(this);
            options = (options || {});

            _.extend(this, _.pick(options, this.accepts));

            options = {
                textHeading : this.textHeading,
                textDescription : this.textDescription
            };
            this.$el.html(Template(options));
        },

        events : {
            'click' : 'action'
        },

        render : function()
        {
            if (!this.rendered)
            {
                this.hide();
                this.$el.appendTo(this.appendTo);
                this.rendered = true;

                /**
                 * Make the element start outside of viewport
                 * and animate it in from top
                 */
                var height = this.$el.height() + parseInt(this.$el.css('top'), 10);
                var top = '-' + height + 'px';
                this.$el.css('top', top);
                this.show();
                var options = {
                    top : '10px'
                };
                var _this = this;
                this.$el.animate(options, 1000, function(){
                    _.delay(_this.close, _this.displayTime);
                });
            }
            return this;
        },

        action : function()
        {
            this.trigger('action');
        },

        close : function()
        {
            if (this.rendered)
            {
                this.$el.detach();
                this.remove();
                this.rendered = false;
            }

            this.trigger('closed');

            return this;
        }
    });
});
