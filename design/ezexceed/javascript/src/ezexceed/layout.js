/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['ezexceed/layout/views/main'], function(View)
{
    return {
        view : View
    };
});
