/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

/** Overrides sync-method to add support for custom routing. */
define(['backbone', 'ezexceed/main/views/errors', 'jquery-safe'], function(Backbone, ErrorView, $)
{
    Backbone.$ = $;

    Backbone.emulateHTTP = true;
    Backbone.emulateJSON = true;

    Backbone._sync = Backbone.sync;
    Backbone.siteRoot = '';

    var transformUrl = function(url)
    {
        var parameters = ['Router', 'handle'];
        var urlParts = url.split('?');

        /** Remove empty strings. */
        var classMethod = _.filter(urlParts[0].split('/'), function(value)
        {
            return value && value.length > 0;
        });

        parameters = parameters.concat(classMethod);

        url = parameters.join('::');

        if (urlParts.length === 2)
            url += '/?' + urlParts[1];

        return Backbone.siteRoot  + '/ezjscore/call/' + url;
    };

    var Errors = new ErrorView({
        appendTo : '.eze-main',
        syncBuffer : [],
        errorPattern : /Not a valid ezjscServerRouter argument/
    });

    Errors.on('signedin', function()
    {
        // Snapshot syncBuffer in this instance
        var queue = _.clone(Errors.syncBuffer);
        Errors.syncBuffer = [];
        _.each(queue, function(args)
        {
            // The original deferred, now we can finally resolve this (hopefully)
            var def = args[args.length - 1];
            Backbone.sync.apply(Backbone, args).done(function()
            {
                def.resolve.apply(def, arguments);
            });
        });
    });

    // For debugging you know
    window.syncBuffer = Errors.syncBuffer;

    Backbone.sync = function(method, model, options)
    {
        options = options || ({});

        var doneCallback = false;
        // Remove the success callback from options so we can handle it at the right time
        if (options.hasOwnProperty('success') && options.success) {
            doneCallback = options.success;
            delete options.success;
        }

        var failCallback = false;
        // Remove the error callback from options so we can handle it at the right time
        if (options.hasOwnProperty('error') && options.error) {
            failCallback = options.error;
            delete options.error;
        }

        Errors.syncBuffer.push([method, model, _.clone(options)]);

        if (Errors.active) {
            var deferred = $.Deferred();
            _.last(Errors.syncBuffer).push(deferred);
            return deferred;
        }

        // If no url is passed, get it from the Backbone.Model
        if (!options.url) {
            options.url = _.result(model, 'url');
        }

        // Supports passing `transform: false` to have urls used as-is
        if (!options.hasOwnProperty('transform') || options.transform) {
            options.url = transformUrl(options.url);
        }

        // If we found a form token, and its not a GET request
        // make sure the formtoken is patched onto the data sent
        if (method !== 'read' && eZExceed.formtoken) {
            options.data = options.data || ({});
            _.extend(options.data, {
                ezxform_token : eZExceed.formtoken
            });
        }

        if (!('trigger' in model)) _.extend(model, Backbone.Events);

        // Get reference to xhr object by first doing the sync method
        var xhr = Backbone._sync(method, model, options);

        // Pipe through the logged out detector, this will return a $.Deferred, not the jquery xhr superset
        var def2 = new $.Deferred();
        var def = xhr.then(function(resp, type, xhr)
        {
            return Errors.checkSignedOut(resp, type, xhr, def2);
        }, function(xhr, type) {
            if (type === 'abort') return xhr;
            if (failCallback) failCallback.call(this, arguments);
            Errors[500].call(this, arguments);
        });


        _.last(Errors.syncBuffer).push(def2);

        // Merge the xhr object back into the Deferred and return it
        var result = _.extend(xhr, def);

        // Re-apply success/error callbacks sent through options
        if (doneCallback) result.done(doneCallback);
        if (failCallback) result.fail(failCallback);

        return result;
    };

    return {
        transformUrl : transformUrl
    };
});
