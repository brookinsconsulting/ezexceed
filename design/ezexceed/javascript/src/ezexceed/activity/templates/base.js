define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<p class=\"entry\">\n    <span class=\"date\">\n        <strong>";
  if (stack1 = helpers.user_fullname) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.user_fullname; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</strong>\n        ";
  if (stack1 = helpers.timeformatted) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.timeformatted; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n    </span>\n    <br />\n    ";
  if (stack1 = helpers.activitystring) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.activitystring; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</p>\n";
  return buffer;
  })

});