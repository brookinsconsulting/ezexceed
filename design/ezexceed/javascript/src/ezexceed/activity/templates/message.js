define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var stack1;
  if (stack1 = helpers.avatar) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.avatar; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  return escapeExpression(stack1);
  }

function program3(depth0,data) {
  
  var stack1, options;
  options = {hash:{
    'size': (32)
  },data:data};
  return escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, "DefaultUser", options) : helperMissing.call(depth0, "icon", "DefaultUser", options)));
  }

  buffer += "<img class=\"user-image icon-32\"\n    src=\"";
  stack1 = helpers['if'].call(depth0, depth0.avatar, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\">\n\n<p class=\"entry\">\n    <span class=\"date\">\n        <strong>";
  if (stack1 = helpers.user_fullname) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.user_fullname; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</strong>\n        ";
  if (stack1 = helpers.timeformatted) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.timeformatted; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n    </span>\n    <br />\n    ";
  if (stack1 = helpers.activitystring) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.activitystring; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n</p>\n\n<div class=\"input-append add-comment hide\">\n    <input data-reply-to-id=\""
    + escapeExpression(((stack1 = depth0.id),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"comment-input\" type=\"text\" placeholder=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Write message", options) : helperMissing.call(depth0, "translate", "Write message", options)))
    + "…\" />\n    <button type=\"button\" class=\"btn btn-mini\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Share", options) : helperMissing.call(depth0, "translate", "Share", options)))
    + "</button>\n    <img class=\"icon-16 hide\" src=\"/extension/ezexceed/design/ezexceed/images/loader.gif\" />\n</div>\n\n<button type=\"button\" class=\"btn btn-mini add-comment-button\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Comment", options) : helperMissing.call(depth0, "translate", "Comment", options)))
    + "</button>\n\n<div class=\"comments-container\"></div>\n";
  return buffer;
  })

});