define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<h1>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Activity", options) : helperMissing.call(depth0, "translate", "Activity", options)))
    + "</h1>\n<p class=\"description\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Comments on this content", options) : helperMissing.call(depth0, "translate", "Comments on this content", options)))
    + "</p>\n";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n    <div class=\"btn-group\" data-toggle=\"buttons-radio\">\n        <button type=\"button\" class=\"btn active\" data-object-id=\"";
  if (stack1 = helpers.objectId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.objectId; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "This page", options) : helperMissing.call(depth0, "translate", "This page", options)))
    + "</button>\n        <button type=\"button\" class=\"btn\" data-object-id=\"null\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "All activity", options) : helperMissing.call(depth0, "translate", "All activity", options)))
    + "</button>\n        <button type=\"button\" class=\"btn\" data-mentions=\"1\">@";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "me", options) : helperMissing.call(depth0, "translate", "me", options)))
    + "</button>\n    </div>\n";
  return buffer;
  }

  stack1 = helpers['if'].call(depth0, depth0.showHeading, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n";
  stack1 = helpers['if'].call(depth0, depth0.showTabs, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n<div class=\"input-append add-message\">\n    <input class=\"message-input write-post\" type=\"text\" placeholder=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Write message", options) : helperMissing.call(depth0, "translate", "Write message", options)))
    + "…\" size=\"16\" />\n    <button type=\"button\" class=\"btn\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Share", options) : helperMissing.call(depth0, "translate", "Share", options)))
    + "</button>\n    <img class=\"icon-16 hide\" src=\"/extension/ezexceed/design/ezexceed/images/loader.gif\" />\n</div>\n\n<div class=\"comments-container\"></div>\n\n<button type=\"button\" class=\"btn show-more hide\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Show more", options) : helperMissing.call(depth0, "translate", "Show more", options)))
    + "</button>\n";
  return buffer;
  })

});