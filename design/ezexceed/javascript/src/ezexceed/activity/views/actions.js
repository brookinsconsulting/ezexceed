define(['shared/containerview', '../models', 'ezexceed/activity/templates/actions'], function(View, Models, ActionsTemplate)
{
    return View.extend(
    {
        tagName : 'p',
        className : 'post-actions',
        activityId : null,
        userId : null,
        viewData : null,
        likes : null,
        comments : null,
        appendAfter : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['activityId', 'userId', 'likes', 'appendAfter']));

            this.userId = this.model.get('currentuser_id');
            this.likes = this.model.get('likes');
            if (this.likes) {
                this.listenTo(this.likes, {
                    'reset remove': this.render
                });
            }

            this.comments = this.model.get('comments');
            if (this.comments) {
                this.listenTo(this.comments, {
                    'add remove': this.updateRemove
                });
            }

            if (this.model) {
                this.listenTo(this.model, {
                    'change': this.render
                });
            }
        },

        events : {
            'click a.like' : 'likeMessage',
            'click a.unlike' : 'unlikeMessage',
            'click a.remove-entry' : 'removeEntry'
        },

        updateRemove : function()
        {
            this.model.set({
                canRemove : this.comments.length === 0 && this.model.get('isMine')
            });
        },

        removeEntry : function(e)
        {
            e.stopPropagation();

            this.model.remove();
            this.model.collection.remove(this.model);
        },

        likeMessage : function()
        {
            var model = new Models.like({activity_id : this.model.id, id : this.userId});

            var data = {user_id : this.userId};

            this.likes.create(model, {wait : true, data : data});
            eZExceed.trigger('activity:like', data);
        },

        unlikeMessage : function(e)
        {
            /** The link clicked is removed from the DOM. This makes the modal close. */
            e.stopPropagation();

            var model = this.likes.get(this.userId);
            this.likes.remove(model);

            model.unLike(this.model.id);
            eZExceed.trigger('activity:unlike', {
                id : this.model.id
            });
        },

        youLike : function()
        {
            return this.likes.any(function(model)
            {
                return this.userId === model.id;
            }, this);
        },

        likeText : function()
        {
            if (this.likes.length === 1) {
                var like = this.likes.at(0);

                if (like.id === this.userId) {
                    return 'You';
                }
                else {
                    return {text : like.get('name')};
                }
            }
            else if (this.likes.length > 1) {
                return [{text : this.likes.length}, 'people'];
            }
            return '';
        },

        render : function()
        {
            /** No actions for this entry exists. */
            if (!this.model.get('canRemove') && this.model.get('isMine') && this.likes.length === 0) {
                this.remove();
                return this;
            }

            this.$el.html(ActionsTemplate(this.context()));
            if (this.appendAfter.length > 0) {
                this.appendAfter.after(this.$el);
            }

            return this;
        },

        context : function()
        {
            return {
                likeCount : this.likes.length,
                hasLikes : this.likes.length > 0,
                youLike : this.youLike(),
                likeText : this.likeText(),
                entry : this.model.toJSON()
            };
        }

    });
});
