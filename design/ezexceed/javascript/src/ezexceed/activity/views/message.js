define(['./autocomplete', './base', './list', 'ezexceed/activity/templates/message'], function(Autocomplete, Base, List, MessageTemplate)
{
    return Base.extend(
    {
        tagName : 'li',
        className : 'clearfix',
        commentsView : null,
        objectId : null,
        comments : null,
        $removeButton : null,
        autocompleteOptions : null,

        initialize : function(options)
        {
            _.bindAll(this);

            this.autocompleteOptions = {};
            this.views = {};

            _.extend(this, _.pick(options, ['autocompleteOptions']));

            this.comments = this.model.get('comments');
            this.listenTo(this.comments, {
                add: this.toggleComment
            });
        },

        events : {
            'click button.add-comment-button' : 'toggleComment'
        },

        toggleComment : function()
        {
            var className = 'hide';
            var input = this.$('div.input-append');
            var isHidden = input.hasClass(className);
            var button = this.$('button.add-comment-button');

            if (isHidden) {
                input.removeClass(className);
                button.addClass(className);
                this.renderAutocomplete();
                input.find('input').focus();
                eZExceed.trigger('activity:comment:show');
            }
            else {
                input.addClass(className);
                button.removeClass(className);
                this.views.autocompleteView.remove();
                delete this.views.autocompleteView;
                eZExceed.trigger('activity:comment:hide');
            }
        },

        renderComments : function()
        {
            var listOptions = {
                tagName : 'ol',
                className : 'eze-replies',
                collection : this.comments,
                view : Base,
                container : this.$('.comments-container')
            };

            this.views.commentListView = new List(listOptions).render();
            return this;
        },

        renderAutocomplete : function()
        {
            this.views.autocompleteView = new Autocomplete(_.extend({}, this.autocompleteOptions, {
                el : this.$('div.input-append:not(.hide)'),
                collection : this.comments
            })).render();
            return this;
        },

        render : function()
        {
            this.$el.html(MessageTemplate(this.model.toJSON()));
            this.$removeButton = this.$('a.remove-message');

            this.renderActions()
                .renderComments();

            return this;
        }
    });
});
