define(['shared/containerview', './list', './autocomplete', '../models', './message', 'ezexceed/activity/templates/main'],
    function(View, List, Autocomplete, Models, Message, MainTemplate)
{
    return View.extend(
    {
        objectId : null,
        container : null,
        focus : false,
        showTabs : true,
        showHeading : false,
        autocompleteOptions : null,
        heading : "Activity",

        id : null,

        displayed : false,

        name : "activity",

        initialize : function(options)
        {
            _.bindAll(this);

            // Generate a unique id for this container
            this.id = _.uniqueId('eze-activity');
            this.views = {};

            _.extend(this, _.pick(options, ['objectId', 'container', 'showTabs', 'showHeading', 'focus']));

            this.autocompleteOptions = {
                appendTo : '#' + this.id
            };

            this.collection = new Models.messages();
            this.collection.objectId = this.objectId;
            this.listenTo(this.collection, {
                'add reset': this.toggleMore
            });

            var render = options.hasOwnProperty('render') ? options.render : true;

            if (render) {
                this.render();
            }

            this.collection.fetch().done(this.display);
        },

        events : {
            'click button.show-more' : 'showMore',
            'click div.btn-group button' : 'toggleView'
        },

        toggleMore : function()
        {
            var button = this.$('button.show-more');
            button.toggleClass('hide', !this.collection.more);
            button.next().remove();
        },

        toggleView : function(e)
        {
            var target = this.$(e.currentTarget);

            if (target.hasClass('active')) {
                return;
            }

            this.collection.objectId = target.data('objectId');

            target
                .addClass('active')
                .siblings().removeClass('active');

            if (!_.isUndefined(target.data('mentions'))) {
                this.collection.mentions = 1;
                this.$('div.add-message').addClass('hide');
            }
            else {
                this.collection.mentions = 0;
                this.$('div.add-message').removeClass('hide');
            }
            eZExceed.trigger('activity:toggle');
            this.collection.resetOffset();
            this.collection.fetch({reset: true}).done(this.display);
        },

        // Build template context
        context : function()
        {
            return {
                tagName : 'ol',
                className : 'eze-comments',
                collection : this.collection,
                view : Message,
                autocompleteOptions : _.extend({}, this.autocompleteOptions),
                container : this.$('.comments-container')
            };
        },

        showMore : function()
        {
            this.$('button.show-more').after(
                this._loader({size:16, className:'icon-16'})
            );
            this.collection.increaseOffset();
            this.collection.fetch({remove: false, update : true}).done(this.toggleMore);
            eZExceed.trigger('activity:showmore');
        },

        display : function()
        {
            if (!this.displayed) {
                /** The display method should only be called once. */
                this.displayed = true;
                if (this.container) {
                    this.container.display(this, true);
                    this.container.$el.attr('id', this.id);
                }
                else {
                    this.$el.attr('id', this.id);
                }
                this.renderAutocomplete();
            }

            if (this.container) {
                this.container.position();
            }
        },

        renderMessages : function()
        {
            this.views.messageListView = new List(this.context()).render();
            return this;
        },

        renderAutocomplete : function()
        {
            this.views.autocompleteView = new Autocomplete(_.extend({}, this.autocompleteOptions, {
                el : this.$('div.add-message'),
                appendTo : '#' + this.id,
                collection : this.collection
            })).render();
            return this;
        },

        render : function()
        {
            var options = {
                objectId : this.objectId,
                showTabs : this.showTabs,
                showHeading : this.showHeading
            };

            this.$el.html(MainTemplate(options));
            var $input = this.$('input');

            if (this.focus) {
                $input.focus();
            }

            this.renderMessages();

            this.trigger('loaded');

            return this;
        }
    });
});
