define(['shared/containerview', './actions', 'ezexceed/activity/templates/base'], function(View, Actions, BaseTemplate)
{
    return View.extend(
    {
        tagName : 'li',
        className : 'sub-comment',
        userId : null,
        actions : null,

        renderActions : function()
        {
            var options = {
                appendAfter : this.$('p.entry'),
                model : this.model
            };

            this.actions = new Actions(options).render();
            return this;
        },

        render : function()
        {
            this.$el.html(BaseTemplate(this.model.toJSON()));

            return this.renderActions();
        }
    });
});
