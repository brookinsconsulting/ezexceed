define(['shared/view', '../models', 'jquery-safe', 'jqueryui/autocomplete'], function(View, Models, $)
{
    return View.extend(
    {
        $inputElement : null,
        users : null,
        bindKeysScoped : true,
        appendTo : '.eze-main',
        rendered : false,

        initialize : function(options)
        {
            _.bindAll(this);

            this.$inputElement = this.$('input:text');

            _.extend(this, _.pick(options, ['appendTo']));

            this.listenTo(this.collection, {
                'reset add remove': this.loader
            });

            this.users = {};
        },

        events : {
            'click button.btn' : 'postEntry'
        },

        keys : {
            'enter' : 'postEntry'
        },

        postEntry : function(e)
        {
            e.stopPropagation();
            e.preventDefault();

            var entry = this.getParsedEntry();

            /** We do not post empty entries. */
            if (entry.length === 0) {
                return;
            }

            this.loader();

            var data = {
                activitystring : entry,
                replyto_id : this.$inputElement.data('replyToId'),
                contentobject_id: this.collection.objectId
            };

            var config = {
                wait: true,
                data: data
            };
            if (!data.replyto_id) {
                config.at = 0;
            }

            eZExceed.trigger('activity:post', config);

            this.collection.create(data, config);
            this.$inputElement.val('');
        },

        loader : function(hide)
        {
            if (hide) {
                this.$('img').remove();
                this.show(this.$('button'));
            }
            else {
                this.$('button').after(this._loader({size : 16, className : 'icon-16'}));
                this.hide(this.$('button'));
            }
        },

        getParsedEntry : function()
        {
            var message = this.$inputElement.val();

            message = _.reduce(this.users, function(memo, userId, key)
            {
                return memo.replace(key, '@{' + userId + '}');
            }, message, this);

            return message;
        },

        getUserKey : function(element, term)
        {
            var position = this.getCursorPosition(element);

            // Search backwards to find the beginning @ position
            var revString = term.split('').reverse().join('');
            var startPos = position - revString.indexOf('@') - 1;

            return term.substr(startPos, position);
        },

        getCursorPosition : function(element)
        {
            var pos = 0;

            if (!_.isUndefined(element.selectionStart)) {
                pos = element.selectionStart;
            }
            else if(!_.isUndefined(document.selection)) {
                element.focus();
                var selection = document.selection.createRange();
                var selectionLength = document.selection.createRange().text.length;
                selection.moveStart('character', - element.value.length);
                pos = selection.text.length - selectionLength;
            }

            return pos;
        },

        source : function(request, response)
        {
            var user = this.getUserKey(this.$inputElement[0], request.term);

            if (user.match(/^@/))
            {
                var name = user.replace(/@/, '');

                var model = new Models.autocomplete();
                model.on('change', function()
                {
                    var users = _(model.get('users')).filter(function(user)
                    {
                        name = $.ui.autocomplete.escapeRegex(name);
                        return new RegExp(name, 'i').test(user.label);
                    });
                    eZExceed.trigger('activity:autocomplete', {
                        input : user.label,
                        matches : users.length
                    });

                    response(users);
                }).fetch();
            }
        },

        focus : function(e, ui)
        {
            var element = this.$inputElement;

            var lastWord = this.getUserKey(element[0], element.val());

            var key = '@' + ui.item.label;
            element.val(element.val().replace(lastWord, key));

            this.users[key] = ui.item.value;

            return false;
        },

        select : function(e)
        {
            /** Prevents post from happening. */
            e.preventDefault();
            e.stopPropagation();
        },

        remove : function()
        {
            if (this.rendered) {
                this.$inputElement.autocomplete('destroy');
            }
            View.prototype.remove.apply(this);
        },

        render : function()
        {
            this.$inputElement.autocomplete({
                appendTo : this.appendTo,
                source : this.source,
                focus : this.focus,
                select : this.select
            });
            this.rendered = true;

            return this;
        }
    });
});
