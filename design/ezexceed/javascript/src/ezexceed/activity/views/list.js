define(['shared/containerview'], function(View)
{
    return View.extend(
    {
        view : null,
        container : null,
        inserted : false,
        autocompleteOptions : null,

        initialize : function(options)
        {
            _.bindAll(this);

            this.autocompleteOptions = {};

            _.extend(this, _.pick(options, ['view', 'container', 'autocompleteOptions']));

            this.listenTo(this.collection, {
                'reset add remove': this.render
            });
        },

        render : function()
        {
            if (this.collection.length === 0) {
                this.$el.empty();
                this.inserted = false;
                return this;
            }

            /** Anonymous function is used to filter function-arguments. */
            var content = this.collection.map(function(model)
            {
                return new this.view({
                    model: model,
                    autocompleteOptions : _.extend({}, this.autocompleteOptions)
                }).render().el;
            }, this);

            this.$el.html(content);

            if (!this.inserted) {
                this.inserted = true;
                this.container.html(this.$el);
            }

            return this;
        }
    });
});
