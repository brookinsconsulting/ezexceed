/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['backbone', 'module', 'jquery-safe'], function(Backbone, Module, $)
{
    var Autocomplete = Backbone.Model.extend(
    {
        parse : function(response)
        {
            var userArray = _.map(response.data, function(item)
            {
                return {
                    label: item.name,
                    value: item.id
                };
            });

            return {users : userArray};
        },

        url : '/Activity/lookUpUser/'
    });

    var Like = Backbone.Model.extend(
    {
        unLike : function(activityId)
        {
            var url = '/Activity/unlikeActivity/' + activityId;
            var data = {user_id : this.id};

            Backbone.sync('create', {url : url}, {data : data});
        },

        url : function()
        {
            return '/Activity/likeActivity/' + this.get('activity_id');
        },

        parse : function(response)
        {
            if (response.hasOwnProperty('data')) {
                this.collection.reset(response, {parse : true});
            }
            else {
                return response;
            }
        }
    });

    var Likes = Backbone.Collection.extend({

        model : Like,

        parse : function(response)
        {
            if (response.hasOwnProperty('data') && response.data.hasOwnProperty('likes')) {
                response = response.data.likes;
            }

            response = _(response).map(function(name, id)
            {
               return {
                   id : id,
                   name : name
               };
            });

            return response;
        }
    });

    var Message = Backbone.Model.extend({

        url : '/Activity/activityEndpoint/',

        parse : function(response)
        {
            if (response.hasOwnProperty('data')) {
                response = response.data;
            }

            response.likes = new Likes(response.likes, {parse : true});

            response.comments = new Messages(response.replies, {parse: true});
            delete response.replies;

            response.isMine = response.user_id === response.currentuser_id;
            response.isComment = response.replyto_id > 0;
            response.canRemove = response.comments.length === 0 && response.isMine;

            response.activitystring = _(response.mentions).reduce(function(memo, name, key)
            {
                return memo.replace('@{'+key+'}', '<strong>' + name + '</strong>');
            }, response.activitystring);

            return response;
        },

        remove : function()
        {
            var url = '/Activity/deleteActivity/';
            var data = {activityId : this.get('id')};

            Backbone.sync('create', {url : url}, {data : data});
        }
    });

    var Messages = Backbone.Collection.extend(
    {
        objectId : null,
        limit : 25,
        currentOffset : 0,
        model : Message,
        mentions : false,
        more : null,

        url : function()
        {
            var params = {
                object_id : this.objectId,
                /** Retrieve one more than necessary to determine if there are more posts. */
                limit : this.limit + 1,
                offset : this.currentOffset
            };

            var url = '/Activity/';

            if (this.mentions) {
                url += '/mentions/';
            }
            else {
                url += 'activityEndpoint';
            }

            url += '/?' + $.param(params);

            return url;
        },

        resetOffset : function()
        {
            this.currentOffset = 0;
        },

        increaseOffset : function()
        {
            this.currentOffset += this.limit;
        },

        parse : function(response)
        {
            var data = null;
            if (response.data) {
                data = response.data;
                this.more = data.length > this.limit;

                if (this.more) {
                    data.pop();
                }

                return data;
            }

            return response;
        }
    });

    return {
        autocomplete : Autocomplete,
        like : Like,
        message : Message,
        messages : Messages
    };
});
