/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
 define(['shared/view', 'shared/flash'], function(View, Flash)
 {
    return View.extend(
    {
        callback : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['callback']));
        },

        render : function()
        {
            this.$el.addClass('changed');

            var options = {
                fadeInDuration : 1500,
                fadeOutDuration : 1000,
                minOpacity : 0
            };

            if (!this.$el.data('pulsing')) {
                Flash.startPulsing(this.$el, options);
            }

            _.delay(this.callback, 2000);

            return this;
        },

        stopPulsing : function()
        {
            Flash.stopPulsing(this.$el);
        }
    });
 });