/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'funky'], function(Backbone, Funky)
{
    return Backbone.Model.extend(
    {
        objectId : '',
        language : '',

        defaults : function()
        {
            return {
                published : 0,
                objects : new Backbone.Collection()
            };
        },

        initialize : function()
        {
            _.bindAll(this);
        },

        url : function()
        {
            return '/Publish/status/' + this.objectId + '/' + this.language;
        },

        getGlobalStatus : function(objects)
        {
            var status = 'published';

            /** A related object has been changed. */
            if (objects.length > 1) {
                status = 'changed';
            }

            /** Main-object is not published. */
            if (objects.length > 0) {
                var object = objects.at(0);
                status = object.get('status');

                if (objects.filter(Funky.equalProp('status', 'publishing', 'get')).length) {
                    status = 'publishing';
                }
            }

            return status;
        },

        parse : function(response)
        {
            if (response.hasOwnProperty('objects')) {
                response.objects = new Backbone.Collection(response.objects);
                response.globalStatus = this.getGlobalStatus(response.objects);
            }

            return response;
        }
    });
});