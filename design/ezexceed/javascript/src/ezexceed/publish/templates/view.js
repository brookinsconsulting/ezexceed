define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n    <time datetime=\"";
  options = {hash:{
    'format': ("DD.MM.YYYY")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.dateFormat),stack1 ? stack1.call(depth0, depth0.modified, options) : helperMissing.call(depth0, "dateFormat", depth0.modified, options)))
    + "\" pubdate>\n        ";
  options = {hash:{
    'format': ("DD. MMM YY HH:mm")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.dateFormat),stack1 ? stack1.call(depth0, depth0.modified, options) : helperMissing.call(depth0, "dateFormat", depth0.modified, options)))
    + "\n    </time>\n    ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "It has never been published", options) : helperMissing.call(depth0, "translate", "It has never been published", options)))
    + ".\n    ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n\n    ";
  stack1 = helpers['if'].call(depth0, depth0.relatedChanges, {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n    <ul class=\"eze-items\">\n        ";
  stack1 = helpers.each.call(depth0, depth0.objects, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n    </ul>\n    ";
  stack1 = helpers.unless.call(depth0, depth0.active, {hash:{},inverse:self.program(13, program13, data),fn:self.program(11, program11, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n";
  return buffer;
  }
function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n    <label class=\"checkbox\">\n        <input type=\"checkbox\">\n        <strong>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Publish all related pages", options) : helperMissing.call(depth0, "translate", "Publish all related pages", options)))
    + "</strong> "
    + escapeExpression(((stack1 = ((stack1 = depth0.objects),stack1 == null || stack1 === false ? stack1 : stack1.length)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "changed", options) : helperMissing.call(depth0, "translate", "changed", options)))
    + "\n    </label>\n    ";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n        <li>\n            <div class=\"wrap\">\n                <input type=\"checkbox\" data-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"";
  stack1 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(9, program9, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n                <img src=\"";
  options = {hash:{
    'size': (16),
    'id': (depth0.classId)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\" alt=\"";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"icon-16\">\n                ";
  if (stack2 = helpers.name) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.name; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\n                <div class=\"right-align\">\n                    <i class=\"status-icon ";
  if (stack2 = helpers.status) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.status; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"></i>\n                </div>\n            </div>\n        </li>\n        ";
  return buffer;
  }
function program9(depth0,data) {
  
  
  return " checked";
  }

function program11(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n    <button class=\"btn btn-primary disabled\" disabled>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Publish pages", options) : helperMissing.call(depth0, "translate", "Publish pages", options)))
    + "</button>\n    ";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n    <button class=\"btn btn-primary\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Publish pages", options) : helperMissing.call(depth0, "translate", "Publish pages", options)))
    + "</button>\n    ";
  return buffer;
  }

  buffer += "<p class=\"publish-info\">\n    ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Last published", options) : helperMissing.call(depth0, "translate", "Last published", options)))
    + ":\n    ";
  stack2 = helpers['if'].call(depth0, depth0.published, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</p>\n\n";
  stack2 = helpers['if'].call(depth0, depth0.changed, {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n";
  return buffer;
  })

});