/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', './model', './asyncstatus'], function(View, Model, ASyncStatus)
{
    return View.extend(
    {        
        currentStatus : null,
        asyncStatusView : null,

        initialize : function(options)
        {
            _.bindAll(this);

            this.model = new Model();
            _.extend(this.model, _.pick(options, ['objectId', 'language']));

            this.model.on('change', this.render);
            this.on('change', this.statusChanged);

            var asyncOptions = {
                callback : this.model.fetch,
                el : this.$el
            };

            this.asyncStatusView = new ASyncStatus(asyncOptions);

            return this;
        },

        statusChanged : function(isSave)
        {
            /** A save when status already is "changed" does not alter the publish-state. */
            if (this.currentStatus === 'changed' && isSave) {
                return;
            }

            this.model.fetch();
        },

        render : function()
        {
            this.currentStatus = this.model.get('globalStatus');

            this.$el.removeClass('new changed published');

            if (this.currentStatus === 'publishing') {
                this.asyncStatusView.render();
            }
            else {
                this.asyncStatusView.stopPulsing();
                this.$el.addClass(this.currentStatus);
            }
        }
    });
});
