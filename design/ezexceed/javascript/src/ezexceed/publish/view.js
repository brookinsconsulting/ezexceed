/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'shared/view', './model', 'ezexceed/publish/templates/view', 'ezexceed/publish/templates/status'],
    function(Backbone, View, Model, ViewTemplate, StatusTemplate)
{
    return View.extend(
    {
        container : null,
        heading : "Publish this content",

        initialize : function(options)
        {
            _.bindAll(this);
            _.extend(this, _.pick(options, ['container']));

            this.$el.html(this._loader({className : 'icon-16', size : 16}));

            this.model = new Model();

            this.model.objectId = options.objectId;
            this.model.language = options.language;

            this.model.on('change', this.render).fetch();
        },

        events : {
            'click .btn-primary' : 'publish',
            'change label.checkbox input' : 'toggleAll',
            'change .eze-items input' : 'toggle'
        },

        toggle : function(e)
        {
            var checkbox = this.$(e.currentTarget);
            var model = this.model.get('objects').get(checkbox.data('id'));
            model.set({selected: checkbox.is(':checked')});
            this.buttonState();
        },

        render : function()
        {
            var objects = this.model.get('objects');
            if (objects.length === 1) {
                objects.at(0).set('selected', true);
            }

            var data = {
                published : parseInt(this.model.get('published'), 10),
                modified : this.model.get('modified'),
                objects : objects.toJSON(),
                changed : objects.length > 0,
                active : objects.where({selected: true}).length > 0,
                relatedChanges : objects.length - 1
            };

            this.$el.html(ViewTemplate(data));
            this.container.display(this);
            this.trigger('loaded');

            return this;
        },

        toggleAll : function(e)
        {
            var checked = this.$(e.currentTarget).is(':checked');

            this.model.get('objects').invoke('set', {selected: checked});
            this.$('.eze-items').find('input').attr('checked', checked);
            this.buttonState();
        },

        buttonState : function()
        {
            if (this.model.get('objects').where({selected:true}).length === 0) {
                this.$('.btn').addClass('disabled').attr('disabled', 'disabled');
            }
            else {
                this.$('.btn').removeClass('disabled').attr('disabled', null);
            }
        },

        publish : function(e)
        {
            var data = {objects : []};
            var checkedObjects = this.$('.eze-items').find('input:checked');
            var objects = this.model.get('objects');

            _(checkedObjects).each(function(element)
            {
                var id = this.$(element).data('id');
                var object = objects.get(id);

                data.objects.push({
                    id : object.id,
                    versions : object.get('versions')
                });

            }, this);

            if (data.objects.length > 0) {
                this.$(e.currentTarget).attr('disabled', true);

                var url = '/Publish/run';

                this.$('.eze-items').append(StatusTemplate({published : false}));

                Backbone.sync('create', {url : url}, {data : data, success : this.publishSuccess});
            }
        },

        isAsync : function(response)
        {
            return _(response && response.published).any(function(published) {
                return published && published.info && published.info.status === 'publishing';
            });
        },

        publishSuccess : function(response)
        {
            this.$('.button.btn-primary').attr('disabled', true);

            if (!this.isAsync(response)) {
                this.$('.eze-items li.notification').remove();
                this.$('.eze-items').append(StatusTemplate({published : true}));
            }

            /** Close modal. */
            var trigger = _.bind(this.trigger, this);
            _.delay(trigger, 1500, 'close');

            eZExceed.publishStatus.trigger('change');
        }
    });
});
