/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'shared/view', './models', './timeline', 'ezexceed/preview/templates/main'],
    function(Backbone, View, Models, Timeline, Template)
{
    var timeline = null;
    return View.extend({
        container : null,
        heading : "Preview",
        timeline : false,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['container', 'objectId', 'language', 'timeline']));

            this.collection = new Models.collection();
            this.listenTo(this.collection, {
                'screen.sent': this.screenSent,
                'screen.send': this.sendingPreview,
                'remove reset add': this.render
            });
            this.collection.fetch().done(this.render);
        },

        events : {
            'click .preview' : 'preview',
            'change .follow' : 'follow',
            'click .preview-url' : 'cpPreviewUrl',
            'click .remove' : 'removeDevice',
            'click .timeline' : 'showTimeline'
        },

        removeDevice : function(e)
        {
            var id = this.$(e.currentTarget).data('id');
            var model = this.collection.get(id);
            var _this = this;

            var data = {id : id};
            model.url = '/Device/unregister';

            var success = function()
            {
                _this.collection.remove(model);
                eZExceed.trigger('preview:device:remove');
            };

            Backbone.sync('create', model, {data : data}).done(success);
        },

        cpPreviewUrl : function(e)
        {
            this.$(e.currentTarget).select();
            eZExceed.trigger('preview:device:copyurl');
        },

        follow : function(e)
        {
            var node = this.$(e.currentTarget);
            var id = node.data('id');
            var on = node.is(':checked');
            this.collection.get(id).follow(on);
            if (on) {
                eZExceed.trigger('preview:device:follow');
            }
            else {
                eZExceed.trigger('preview:device:unfollow');
            }
        },

        preview : function(e)
        {
            var node = this.$(e.currentTarget);
            var model = this.collection.get(node.data('id'));
            model.sendScreen(document.location.pathname);
            eZExceed.trigger('preview:device:sendpreview');
        },

        sendingPreview : function(id)
        {
            var row = this.$('#eze-device-' + id);
            row.find('.preview').addClass('disabled').attr('disabled', 'disabled');
        },

        screenSent : function(model)
        {
            var button = this.$('#eze-device-' + model.id + ' .preview');
            button.text(button.data('sent'));
        },

        getPreviewUrl : function()
        {
            var url = ['', 'Preview', 'url', this.objectId, this.language, eZExceed.config.siteaccess].join('/');
            Backbone.sync('read', {url : url}).done(this.updatePreviewUrl);
        },

        updatePreviewUrl : function(resp)
        {
            if (resp.ok)
            {
                this.$('.preview-url').val(resp.url);
                this.show(this.$('.preview-url-wrap'));
            }
            else
                this.hide(this.$('.preview-url-wrap'));
        },

        showTimeline : function()
        {
            /**
             * Refresh page to put it back in normal state
             */
            if (timeline) {
                eZExceed.trigger('preview:device:timeline:hide');
                window.location.reload();
                return;
            }
            timeline = new Timeline().render();
            eZExceed.trigger('preview:device:timeline:show');
            this.trigger('close');
        },

        render : function()
        {
            var data = {};
            if (this.collection.length > 0) {
                data.devices = this.collection.invoke('data');
            }
            if (this.timeline) {
                data.timeline = true;
                data.timelineOn = !!timeline;
            }
            this.$el.html(Template(data));
            this.container.display(this);
            this.getPreviewUrl();
            return this.trigger('loaded');
        }
    });
});
