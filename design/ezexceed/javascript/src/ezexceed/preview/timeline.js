/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'shared/datepicker', 'jquery-safe', 'moment', 'ezexceed/preview/templates/timeline', 'ezexceed/preview/templates/pageloader',
    'jqueryui/draggable', 'jqueryui/droppable', 'jquerypp/animate', 'jquery-ui.touch'],
    function(View, Datepicker, $, moment, Template, PageloaderTemplate)
{
    return View.extend({
        tagName : 'section',
        id : 'ezflow-timeline',
        appendTo : '.eze-main',

        initialize : function()
        {
            /** jQuery DnD does not provide the target. This necessitates usage of $(this). In other words, please leave the bindAll-call as is. */
            _.bindAll(this, 'render', 'renderSlider', 'updateBlocks', 'onChange', 'timeSelected', 'getPreview', 'showLoader', 'removeLoader');
            this.datePicker = new Datepicker();
            this.$appendTo = $(this.appendTo);
        },

        events : {
            'change .datepicker' : 'getPreview',
            'time:change .timewrap ul li' : 'onChange',
            'click .timewrap ul li' : 'timeSelected'
        },

        render : function()
        {
            /**
             * Return if timeline is already showing
             */
            if ($('#'+this.id).length)
                return;

            var now = moment();

            var data = {
                date : now.format('DD.MM.YYYY')
            };
            this.$el.html(Template(data));
            this.$el.appendTo(this.$appendTo);

            this.$('.datepicker')[0].valueAsDate = new Date();

            this.dateEl = this.$('.datepicker');
            this.datePicker.setup(this.dateEl);
            this.renderSlider();

            /**
             * Preset the timeline to the next hour
             */
            now.add('hours', 1);
            this.time = now.format('H:00');

            var index = now.hours();
            if (index === 0)
                index = 24;

            this.$('.timewrap ul li:nth-child('+index+')').click();
            return this;
        },

        renderSlider : function()
        {
            var timeElements = this.$('.timewrap ul li');
            var gridWidth = timeElements.first().width();
            var timewrap = this.$('.timewrap ul');
            var x1 = timewrap.position().left - gridWidth;
            var x2 = x1 + timewrap.width();
            var containment = [x1, 0, x2, timewrap.height()];

            var options = {
                axis : "x",
                grid : [gridWidth, 0],
                containment : containment
            };

            this.$('.dragwrap').draggable(options);

            timeElements.droppable({
                tolerance : 'touch',
                drop : this.onSliderChange
            });
        },

        timeSelected : function(e)
        {
            var target = $(e.currentTarget);
            target.trigger('drop');
            var slider = this.$('.dragwrap');

            var margin = (slider.width() - target.width()) / 2;
            var offsetLeft = target.offset().left - margin;
            var options = {
                left : offsetLeft + 'px'
            };
            slider.animate(options, 500);
            this.time = target.text();
            this.getPreview();
        },

        onSliderChange : function(e, ui)
        {
            /**
             * this is bound to the target
             */
            var target = $(this);
            var targetLeftOffset = target.offset().left;
            var droppedLeftOffset = ui.offset.left;

            /**
             * For some reason we must use the 'touch' tolerance on the droppable areas to make it work.
             * Therefore several droppables are dropped at since the slider is wider than the droppables
             * Me must calculate which one that are completly covered by the draggable
             */
            if (targetLeftOffset > droppedLeftOffset && targetLeftOffset < (droppedLeftOffset + target.width()))
                $(this).trigger('time:change', {ui : ui, target : $(this)});
        },

        onChange : function(e, data)
        {
            if (data && 'target' in data)
                this.time = data.target.text();
            this.getPreview();
        },

        getPreview : function()
        {
            var date = this.datePicker.getDate(this.dateEl) || (new Date());

            if (this.time) {
                var split = this.time.split(':');

                date.setHours(split[0]);
                date.setMinutes(split[1]);
            }
            var time = Math.round(date.getTime() / 1000);

            this.showLoader();
            var root = (eZExceed.config.siteRoot || '');
            var url = [root, 'ezflow', 'preview', time, eZExceed.config.currentNodeId].join('/');
            $.getJSON(url, this.updateBlocks);
            eZExceed.trigger('preview:device:timeline:change');
        },

        updateBlocks : function(resp)
        {
            this.removeLoader();
            var html = '';

            _(resp).each(function(block) {
                html = $('<div/>').html(block.xhtml).text();
                $('#address-' + block.objectid).html(html);
            });
            eZExceed.trigger('resize');
        },

        showLoader : function()
        {
            var loader = $(PageloaderTemplate({}));
            loader.insertBefore(this.$el);
        },

        removeLoader: function()
        {
            this.$appendTo.find('.eze-fadeout, .eze-page-loader').remove();
        }
    });
});
