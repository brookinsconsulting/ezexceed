define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  buffer += "<section class=\"eze-fadeout\"></section>\n<section class=\"eze-page-loader\">\n	<img src=\"/extension/ezexceed/design/ezexceed/images/loader-white.gif\" class=\"icon-64\">\n	<h2>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Loading future", options) : helperMissing.call(depth0, "translate", "Loading future", options)))
    + "…</h2>\n</section>";
  return buffer;
  })

});