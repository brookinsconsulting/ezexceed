define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this, functionType="function";

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n<h4>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Preview the future with Timeline", options) : helperMissing.call(depth0, "translate", "Preview the future with Timeline", options)))
    + "</h4>\n";
  stack2 = helpers['if'].call(depth0, depth0.timelineOn, {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n<hr />\n";
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<button class=\"btn timeline\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Turn off timeline", options) : helperMissing.call(depth0, "translate", "Turn off timeline", options)))
    + "</button>\n";
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<button class=\"btn timeline\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Turn on timeline", options) : helperMissing.call(depth0, "translate", "Turn on timeline", options)))
    + "</button>\n";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<table class=\"table\">\n    <tbody>\n    ";
  stack1 = helpers.each.call(depth0, depth0.devices, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n    </tbody>\n</table>\n";
  return buffer;
  }
function program7(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n        <tr data-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" id=\"eze-device-";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n            <td>\n                <p><img src=\"";
  if (stack1 = helpers.image) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.image; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"icon-32\"> ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</p>\n            </td>\n            <td>\n                <button type=\"button\" class=\"btn preview\"\n                    data-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\"\n                    data-sent=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Preview sent", options) : helperMissing.call(depth0, "translate", "Preview sent", options)))
    + "\"\n                >";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Send preview", options) : helperMissing.call(depth0, "translate", "Send preview", options)))
    + "</button>\n            </td>\n            <td>\n                <label class=\"checkbox\">\n                    <input type=\"checkbox\" class=\"follow\" data-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\"";
  stack2 = helpers['if'].call(depth0, depth0.following, {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Mirror browsing", options) : helperMissing.call(depth0, "translate", "Mirror browsing", options)))
    + "\n                </label>\n            </td>\n            <td>\n                <button type=\"button\" class=\"btn remove\" data-id=\"";
  if (stack2 = helpers.id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Remove", options) : helperMissing.call(depth0, "translate", "Remove", options)))
    + "</button>\n            </td>\n        </tr>\n    ";
  return buffer;
  }
function program8(depth0,data) {
  
  
  return " checked";
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<p class=\"get-app\">\n<a class=\"btn btn-primary\" target=\"_blank\"\n    href=\"https://itunes.apple.com/no/app/exceed/id567405821?ls=1&mt=8\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Get the app from the App Store", options) : helperMissing.call(depth0, "translate", "Get the app from the App Store", options)))
    + "</a>\n</p>\n";
  return buffer;
  }

  stack1 = helpers['if'].call(depth0, depth0.timeline, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n<span class=\"preview-url-wrap hide\">\n    <h4>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Preview this page on an URL", options) : helperMissing.call(depth0, "translate", "Preview this page on an URL", options)))
    + "</h4>\n    <input class=\"preview-url\" type=\"text\" readonly=\"readonly\" value=\"\" />\n    <hr />\n</span>\n<h4>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Send preview to devices", options) : helperMissing.call(depth0, "translate", "Send preview to devices", options)))
    + "</h4>\n";
  stack2 = helpers['if'].call(depth0, depth0.devices, {hash:{},inverse:self.program(10, program10, data),fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n";
  return buffer;
  })

});