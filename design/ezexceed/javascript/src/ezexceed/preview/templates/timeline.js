define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<div class=\"wrapper\"></div>\n<input class=\"datepicker\" type=\"date\" value=\"";
  if (stack1 = helpers.date) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.date; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" />\n\n<div class=\"timewrap\">\n    <span class=\"gfx\"></span>\n    <div class=\"dragwrap\">\n        <div class=\"drag\"></div>\n        <div class=\"drag ghost\"></div>\n    </div>\n    <ul>\n        <li class=\"even\">1:00</li>\n        <li>2:00</li>\n        <li class=\"even\">3:00</li>\n        <li>4:00</li>\n        <li class=\"even\">5:00</li>\n        <li>6:00</li>\n        <li class=\"even\">7:00</li>\n        <li>8:00</li>\n        <li class=\"even\">9:00</li>\n        <li>10:00</li>\n        <li class=\"even\">11:00</li>\n        <li>12:00</li>\n        <li class=\"even\">13:00</li>\n        <li>14:00</li>\n        <li class=\"even\">15:00</li>\n        <li>16:00</li>\n        <li class=\"even\">17:00</li>\n        <li>18:00</li>\n        <li class=\"even\">19:00</li>\n        <li>20:00</li>\n        <li class=\"even\">21:00</li>\n        <li>22:00</li>\n        <li class=\"even\">23:00</li>\n        <li>24:00</li>\n    </ul>\n</div>";
  return buffer;
  })

});