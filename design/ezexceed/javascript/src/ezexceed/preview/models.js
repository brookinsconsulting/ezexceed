/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['jquery-safe', 'backbone'], function($, Backbone)
{
    var Model = Backbone.Model.extend({
        defaults : function()
        {
            return {
                id : null,
                name : 'Unnamed device',
                os : 'UnknownOS',
                osVersion : '1.0',
                type : 'aDevice',
                following : false
            };
        },

        urlRoot : '/Device/',

        icon : function(size, white)
        {
            // Default to 16px icons
            size = (size || '32x32');
            // Default to black icons
            white = (white || false);

            var icon;
            switch (this.get('type')) {
                case 'iPad': icon = 'iPad.png'; break;
                case 'iPhone': icon = 'Iphone.png'; break;
                default: icon = 'Mobile-phone.png'; break;
            }
            return '/extension/ezexceed/design/ezexceed/images/kp/' + size + '/' + (white ? 'white/' : '') + icon;
        },

        initialize : function()
        {
            _.bindAll(this);
        },

        follow : function(on)
        {
            var data = {
                follow: (on ? 1 : 0),
                uniqueKey : this.id
            };
            Backbone.sync('create', {url: this.urlRoot + 'follow'}, {data: data})
                .done(this.followed);
            return this;
        },

        followed : function(resp)
        {
            this.trigger('followed', this, resp);
        },

        sendScreen : function(url)
        {
            this.trigger('screen.send', this.id);
            var data = {
                uniqueKey : this.id,
                url : url
            };
            Backbone.sync('create', {url : this.urlRoot + 'preview'}, {data: data})
                .done(this.screenSent);
            return this;
        },

        screenSent : function(resp)
        {
            if (resp.ok) {
                this.trigger('screen.sent', this, resp);
            }
        },

        data : function(iconSize, whiteIcon)
        {
            whiteIcon = (whiteIcon || false);
            iconSize = (iconSize || '24x24');
            return {
                id : this.id,
                name : this.get('name'),
                type : this.get('type'),
                following : this.get('following'),
                image : this.icon(iconSize, whiteIcon)
            };
        }
    });

    var Collection = Backbone.Collection.extend({
        model : Model,

        url : '/Device/find',

        parse : function(response)
        {
            if (response.hasOwnProperty('devices')) {
                return response.devices;
            }
            return response;
        }
    });

    return {
        model: Model,
        collection: Collection
    };
});
