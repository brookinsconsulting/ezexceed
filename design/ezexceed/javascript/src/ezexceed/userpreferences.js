/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['backbone', 'shared/view', 'shared/templates/error', 'shared/templates/loader', 'jquerypp/form_params'],
    function(Backbone, View, ErrorTemplate, LoaderTemplate)
{
    var UserPrefs = View.extend({

        userId : null,
        container : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['userId', 'container']));

            this.autosave = _.debounce(this.autosave, 300);
            this.loadPreferences();
        },

        events : {
            'change .auto :input' : 'autosave',
            'change input[type="password"]' : 'highlightSave',
            'keyup .auto :input' : 'autosave',
            'submit #user-auth' : 'saveAuth',
            'click div.alert button' : 'removeError'
        },

        removeError : function()
        {
            this.$('div.alert').remove();
        },

        highlightSave : function()
        {
            this.$('button.save').addClass('btn-primary');
        },

        autosave : function(e)
        {
            var form = this.$(e.currentTarget).closest('form');
            var url = form.attr('action');
            var data = form.formParams();

            this.show(this.$saving);

            Backbone.sync('create', {url : url}, {data : data}).done(this.saved);
        },

        saved : function()
        {
            this.hide(this.$saving);
            this.show(this.$saved);
            _.delay(function(saved)
            {
                saved.addClass('hide');
            }, 750, this.$saved);

            eZExceed.publishStatus.trigger('change');
        },

        handleError : function(response)
        {
            this.$('.alert').remove();

            if (!response.ok) {
                var errorData = {
                    heading : 'Error!',
                    message : {
                        text : response.error
                    }
                };

                this.$('#user-auth legend').after(ErrorTemplate(errorData));
                this.hide(this.$saving);
            }
            else {
                this.$('#ez_password, #ez_repeatpassword').val('');
                this.saved();
            }
        },

        saveAuth : function(e)
        {
            e.preventDefault();

            this.show(this.$saving);

            var form = this.$('#user-auth');

            var url = form.attr('action');
            var data = form.formParams();

            Backbone.sync('create', {url : url}, {data : data}).done(this.handleError);
        },

        loadPreferences : function()
        {
            this.$el.html(LoaderTemplate({className: 'icon-32'}));

            var url = '/UserPreferences/get/' + this.userId;

            Backbone.sync('read', {url : url}).done(this.render);
        },

        render : function(response)
        {
            if (response) {
                this.heading = {text : response.heading};
                this.$el.html(response.html);
                this.container.display(this);

                this.$saving = this.$('.eze-autosave .saving');
                this.$saved = this.$('.eze-autosave .saved');
            }
            this.trigger('loaded');

            return this;
        }
    });

    return {
        view : UserPrefs
    };
});
