/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/views/action'], function(Action)
{
    return Action.extend(
    {
        actionText : 'Insert link',
        parentModel : null,
        locations : null,
        version : null,
        selfBlocking : true,

        initialize : function(options)
        {
            options = (options || {});
            _.bindAll(this);

            _.extend(this, _.pick(options, ['parentModel', 'actionText', 'validate', 'version', 'locations']));

            return this;
        },

        primaryAction : function()
        {
            eZExceed.stack.pop();
            var _this = this,
                id = this.model.id;
            this.listenTo(eZExceed, 'stack:popped', function(){
                _this.trigger('select', id);
                _this.stopListening();
            });

        }
    });
});
