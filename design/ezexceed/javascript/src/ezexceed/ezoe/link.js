/* global tinymce */
/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', './sitemapadd', 'ezoe/templates/link'], function(View, Add, Template)
{
    return View.extend({
        heading : 'Insert link',
        editor : null,
        bookmark : null,
        addFromSitemapView : null,

        initialize : function(options)
        {
            _.bindAll(this);
            _.extend(this, _.pick(options, ['editor']));

            this.bookmark = this.editor.selection.getBookmark();

            this.addFromSitemapView = new Add();
            this.listenTo(this.addFromSitemapView, 'select', this.insertFromSitemap);
        },

        events : {
            'click .sitemap' : 'showSitemap',
            'click .insert' : 'insertLink'
        },

        render : function()
        {
            var editor = this.editor;
            var elm = editor.selection.getNode();
            elm = editor.dom.getParent(elm, "A");
            if (elm === null) {
                var prospect = editor.dom.create("p", null, editor.selection.getContent());
                if (prospect.childNodes.length === 1) {
                    elm = prospect.firstChild;
                }
            }

            var value = '';
            if (elm !== null && elm.nodeName == "A") {
                value = editor.dom.getAttrib(elm, 'href');
            }

            var data = {
                value : value
            };

            this.$el.html(Template(data));
            return this;
        },

        showSitemap : function()
        {
            var _this = this;
            require(['ezexceed/finder'], function(Finder){
                var options = {
                    customActions : {
                        add : _this.addFromSitemapView
                    }
                };

                var context = {
                    heading : [
                        'Insert link'
                    ],
                    icon : 'Documents',
                    render : false
                };

                eZExceed.stack.push(Finder.view, options, context);
                _this.listenTo(eZExceed, 'stack:popped', function(){_this.trigger('unfreeze');});
                _this.trigger('freeze');
            });
        },

        insertFromSitemap : function(id)
        {
            var _this = this;
            _.delay(function()
            {
                _this.insert('eznode://' + id);
            }, 0);
        },

        insertLink : function()
        {
            var value = this.$('input').val();
            this.insert(value);
        },

        insert : function(value)
        {
            var editor = this.editor;

            var elm, elementArray, i;
            editor.selection.moveToBookmark(this.bookmark);

            elm = editor.selection.getNode();
            //checkPrefix(document.forms[0].href);

            elm = editor.dom.getParent(elm, "A");

            // Remove element if there is no href
            if (!value) {
                editor.dom.remove(elm, 1);
                editor.execCommand("mceEndUndoLevel");
                this.trigger('close');
                return;
            }

            // Create new anchor elements
            if (elm === null) {
                editor.getDoc().execCommand("unlink", false, null);
                editor.execCommand("mceInsertLink", false, "#mce_temp_url#", {skip_undo : 1});

                elementArray = tinymce.grep(editor.dom.select("a"), function(n)
                {
                    return editor.dom.getAttrib(n, 'href') == '#mce_temp_url#';
                });
                for (i = 0; i < elementArray.length; i++) {
                    elm = elementArray[i];
                    editor.dom.setAttrib(elm, 'href', value);
                }
            }
            else {
                editor.dom.setAttrib(elm, 'href', value);
            }

            // Don't move caret if selection was image
            if (elm.childNodes.length != 1 || elm.firstChild.nodeName != 'IMG') {
                editor.focus();
                editor.selection.select(elm);
                editor.selection.collapse(0);
            }

            editor.execCommand("mceEndUndoLevel");
            this.trigger('close');
        }
    });
});
