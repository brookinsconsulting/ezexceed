define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;


  buffer += "<div class=\"tab-content\">\n    <input class=\"input-block-level\" type=\"text\" name=\"url\" value=\"";
  if (stack1 = helpers.value) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.value; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" />\n    <button class=\"btn sitemap pull-left\" type=\"button\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Browse sitemap", options) : helperMissing.call(depth0, "translate", "Browse sitemap", options)))
    + "</button>\n    <button class=\"btn btn-primary insert pull-right\" type=\"button\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Insert link", options) : helperMissing.call(depth0, "translate", "Insert link", options)))
    + "</button>\n</div>";
  return buffer;
  })

});