/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'backbone', 'module', 'selectable', 'jqueryui/sortable'], function(View, Backbone, Module, selectable)
{
    return View.extend(
    {
        startIndex : null,
        postdata : null,
        objectId : null,
        language : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['objectId', 'language']));

            this.$el.html(this._loader({className:'icon-32',size:32}));

            this.load();
        },

        events : {
            'click div.nav a.btn' : 'toggleContainer',
            'click form.add-new button.btn' : 'addBlock',
            'click a.btn.remove-btn' : 'removeBlock',
            'click .zone-type-selector' : 'autosaveLayout',
            'change input.name' : 'autosaveBlockAttribute',
            'click a.edit-btn' : 'editBlock'
        },

        getActiveTab : function()
        {
            return this.$('div.tab-pane.active');
        },

        load : function()
        {
            var url = '/Pagelayout/edit/' + this.objectId + '/' + this.language;

            Backbone.sync('read', {url : url}).done(this.render);
        },

        saved : function()
        {
            this.trigger('saved');
            // Mark currentNode as changed
            eZExceed.currentNodeChanges.changed = true;
        },

        editBlock : function(e)
        {
            e.preventDefault();

            var target = this.$(e.currentTarget);

            var options = {
                objectId : this.objectId,
                language : this.language,
                zoneIndex : this.$('div.tab-pane.active').data('zoneIndex'),
                blockId :  target.parents('li:first').data('blockId')
            };

            var context = {
                heading : [
                    'Settings for',
                    {
                        text : target.closest('div.wrap').children('input.name').val(),
                        quotes : true
                    }
                ],
                render : false,
                icon : 'Documents'
            };
            var _this = this;

            require(['ezexceed/layout/views/block'], function(Block)
            {
                var blockView = eZExceed.stack.push(Block, options, context);
                blockView.on('saved', _this.saved);
            });
        },

        getPostdata : function(blockId)
        {
            if (typeof(blockId) !== 'undefined')
                this.postdata.blockId = blockId;

            return this.postdata;
        },

        autosaveLayout : function(e)
        {
            this.trigger('save');

            var target = this.$(e.currentTarget);

            if (!target.hasClass('active-layout'))
            {
                var url = '/Pagelayout/autosaveLayout/' + target.data('value');
                var data = this.getPostdata();

                Backbone.sync('create', {url : url}, {data : data}).done(this.load, this.saved);
            }
        },

        autosaveBlockAttribute : function(e)
        {
            this.trigger('save');

            var target = this.$(e.currentTarget), optionsByValue = this.optionsByValue;
            var blockId = target.closest('li').data('blockId'), inputName = target.attr('name'), inputValue = target.val();
            var params = [inputName, inputValue];
            var dropdowns = this.dropdowns;

            var url = '/Pagelayout/autosaveBlockAttribute/' + params.join('/');
            var data = this.getPostdata(blockId);

            var success = function()
            {
                    /** Special handling for the name-attribute. */
                    if (inputName === 'name') {
                        optionsByValue(blockId).html(inputValue);
                        dropdowns();
                    }
            };

            Backbone.sync('create', {url : url}, {data : data}).done(success, this.saved);
        },

        addBlock : function(e)
        {
            this.trigger('save');

            e.preventDefault();

            var name = this.$('div.tab-pane.active').find('form.add-new select').val();
            var url = '/Pagelayout/addBlock/' + name;
            var data = this.getPostdata();

            Backbone
                .sync('create', {url : url}, {data : data})
                .done(this.onBlockAdded, this.saved);

            return this;
        },

        onBlockAdded : function(response)
        {
            var activeTab = this.getActiveTab();
            var option = Backbone.$('<option />')
                .val(response.blockId)
                .text(response.type);

            activeTab.find('.blockOverflow').append(option);
            activeTab.children('ul').append(response.html).find('select');
            activeTab.find('.name:last').focus();

            this.hide(activeTab.find('.no-content:visible'));

            this.dropdowns();
        },

        optionsByValue : function(val)
        {
            return this.$('option[value="' + val + '"]');
        },

        removeBlock : function(e)
        {
            this.trigger('save');

            e.preventDefault();

            var block = this.$(e.currentTarget).closest('li');
            var blockId = block.data('blockId');
            var redundantOptions = this.optionsByValue(blockId);
            var activeTab = this.getActiveTab();

            block.remove();
            redundantOptions.remove();
            this.dropdowns();

            if (activeTab.find('li').length === 0)
                activeTab.find('.no-content').show();

            var url = '/Pagelayout/removeBlock/';
            var data = this.getPostdata(blockId);

            Backbone.sync('create', {url : url}, {data : data}).done(this.saved);
        },

        toggleContainer : function(e)
        {
            e.preventDefault();

            var target = this.$(e.currentTarget);
            if (!target.hasClass('active'))
            {
                this.$('div.nav a.btn').removeClass('active');
                target.addClass('active');

                this.$('div.tab-content div.tab-pane').removeClass('active');
                var zoneIndex = target.data('id');
                this.$('div#tab' + (zoneIndex + 1)).addClass('active');

                this.postdata.zoneIndex = zoneIndex;
            }
        },

        sortStop : function(e, ui)
        {
            var item = this.$(ui.item);
            var positions = this.startIndex - item.index(), blockId = item.data('blockId');

            if (positions !== 0)
            {
                this.trigger('save');

                var url = '/Pagelayout/moveBlock/' + positions;
                var data = this.getPostdata(blockId);

                Backbone.sync('create', {url : url}, {data : data}).done(this.saved);

                this.startIndex = null;
            }
        },

        sortStart : function(e, ui)
        {
            this.startIndex = this.$(ui.item).index();
        },

        dropdowns : function()
        {
            selectable(this.$('select'), {
                width : 'off'
            });

            var el = this.$('.eze-items select');
            if (el) {
                el.change(this.autosaveBlockAttribute);
            }

            return this;
        },

        render : function(response)
        {
            this.$el.html(response.html);

            this.$('div.tab-pane ul').sortable({
                start : this.sortStart,
                stop : this.sortStop,
                handle : '.reorder',
                axis : 'y',
                delay : 50
            });

            this.postdata = {
                objectId : this.objectId,
                language : this.language,
                zoneIndex : this.$('div.nav a.active').data('id')
            };

            this.dropdowns();

            this.trigger('loaded');

            return this;
        }
    });
});
