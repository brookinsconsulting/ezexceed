/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/views/action'], function(Action)
{
    return Action.extend(
    {
        actionText : 'Add content to the block',
        parentModel : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['parentModel', 'actionText', 'validate']));

            this.parentModel.blockNodes.on('add remove', this.render);

            return this;
        },

        validate : function(node)
        {
            var allowedClasses = this.parentModel.get('allowedClasses');

            var allowed = _(allowedClasses).any(function(classIdentifier)
            {
                return node.get('class').get('identifier') === classIdentifier;
            });

            if (allowedClasses.length === 0)
                allowed = true;

            return !this.parentModel.nodeExists(node) && allowed;
        }
    });
});
