/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['jquery-safe', 'shared/view', './../model', 'shared/datepicker', './add', './remove', 'module', 'selectable', 'shared/timepicker', 'jqueryui/sortable'],
    function($, View, Model, DatePicker, Add, Remove, Module, selectable, timepicker)
{
    return View.extend({

        startIndex : null,
        valid : null,
        waiting : null,
        datePicker : null,
        objectId : null,
        language : null,
        finder : null,

        dates : null,
        times : null,

        $valid : null,
        $waiting : null,
        $timepicker : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['objectId', 'language']));

            this.dates = [];
            this.times = [];

            this.$el.html(this._loader({className:'icon-32',size: 32}));

            /** @type Block.model */
            this.model = new Model(options);
            this.listenTo(this.model, {
                change: this.render
            });
            this.model.fetch();

            this.on('destruct', this.destruct);
        },

        saved : function()
        {
            this.trigger('saved');
        },

        destruct : function()
        {
            if (this.$timepicker) {
                this.$timepicker.timepicker('remove');
            }
        },

        events : {
            'change .rotationAttribute' : 'saveRotation',
            'change .fetchAttribute' : 'saveFetchAttribute',
            'change .customAttribute' : 'saveCustomAttribute',
            'change .rotationCheckboxAttribute' : 'saveRotation',
            'click button.add-item' : 'addItem',
            'click a.edit-btn' : 'editObject',
            'click a.remove-btn' : 'removeItem',
            'click #block-choose-source' : 'selectSource',
            'click #block-choose-dynamic-source' : 'selectDynamicSource',
            'change .publish-date' : 'updateTimestamp',
            'change .time-picker' : 'updateTimestamp'
        },

        editObject : function(e)
        {
            var target = this.$(e.currentTarget);
            var nameContainer = target.closest('div.wrap').children('span.object-name');

            var options = {
                id : target.data('id'),
                language : this.language
            };

            var context = {
                heading : {
                    text : nameContainer.html(),
                    quotes : true
                },
                classIdentifier : target.data('classIdentifier'),
                render : false
            };

            require(['ezexceed/edit'], function(Edit)
            {
                eZExceed.stack.push(Edit.view, options, context).on('destruct', function(model)
                {
                    nameContainer.html(model.get('name'));
                });
            });
        },

        updateTimestamp : function(e)
        {
            var parent = this.$(e.currentTarget).parent();

            var type = parent.closest('ul').data('item-type');
            var index = parent.closest('li').index();
            var date = this.dates[index].getDate(true);

            var dateString = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
            var time = this.times[index].val();

            this.trigger('save');
            this.model.savePublicationTime([type, index, dateString, time], this.saved);
        },

        selectDynamicSource : function()
        {
            var options = {
                customActions : this.fetchSourceAction(this.onSelectDynamicSource)
            };

            this.showFinder(options, this.fetchSourceContext());
        },

        fetchSourceAction : function(action)
        {
            var fetchOptions = {
                parentModel : this.model,
                actionText : 'Choose this node as source',
                validate : null
            };

            var fetchAction = new Add(fetchOptions).on('action', action);

            return {
                fetch : fetchAction
            };
        },

        fetchSourceContext : function()
        {
            return {
                heading : 'Select source',
                render : false,
                icon : 'Layout'
            };
        },

        selectSource : function(e)
        {
            var attributeName = $(e.currentTarget).data('name');
            var onSelectSource = this.onSelectSource;

            var fetch = function(e, element)
            {
                onSelectSource(element, attributeName);
            };

            var options = {
                customActions : this.fetchSourceAction(fetch)
            };

            this.showFinder(options, this.fetchSourceContext());
        },

        removeItem : function(e)
        {
            this.trigger('save');

            var target = this.$(e.currentTarget);
            target.parent().parent().remove();

            var id = target.data('objectId');
            this.model.removeItem(id, this.saved);
        },

        showFinder : function(options, context)
        {
            var finderLoaded = this.finderLoaded;

            require(['ezexceed/finder'], function(Finder)
            {
                finderLoaded(Finder, options, context);
            });
        },

        finderLoaded : function(Finder, options, context)
        {
            this.finder = eZExceed.stack.push(Finder.view, options, context).on('destruct', this.resetFinder);
        },

        resetFinder : function()
        {
            this.finder = null;
        },

        addItem : function(e)
        {
            e.preventDefault();

            var addAction = new Add({parentModel : this.model}).on('action', this.onAdd);
            var removeAction = new Remove({parentModel : this.model}).on('action', this.removeItemFromFinder);

            var options = {
                customActions : {
                    add : addAction,
                    remove : removeAction
                }
            };

            var context = {
                heading : [
                    'Add item to',
                    {
                        text : this.model.get('name'),
                        quotes : true
                    }
                ],
                render : false,
                icon : 'Layout'
            };

            this.showFinder(options, context);
        },

        onAdd : function(e, element)
        {
            var nodeId = element.id, objectId = element.get('objectId');

            this.finder.trigger('save');
            this.model.addItem(nodeId, objectId, this.itemsAdded);
        },

        itemsAdded : function(response)
        {
            this.finder.trigger('saved');
            this.updateItems(response);
        },

        removeItemFromFinder : function(e, node)
        {
            var objectId = node.get('objectId');
            this.finder.trigger('save');
            this.model.removeItem(objectId, this.itemRemovedFromFinder);
            this.$('li[data-id=' + node.get('objectId') + ']').remove();
        },

        itemRemovedFromFinder : function()
        {
            this.finder.trigger('saved');
        },

        updateItems : function(response)
        {
            this.$('div.itemsContainer').replaceWith(response);
            this.addEvents();
        },

        onSelectSource : function(element, attributeName)
        {
            var nodeId = element.id;

            this.trigger('save');
            this.model.saveCustomAttribute([attributeName, nodeId], this.renderCustomProperties);

            eZExceed.stack.pop();
        },

        renderCustomProperties : function(response)
        {
            this.saved();
            this.$('.customAttributes').html(response.html);
        },

        onSelectDynamicSource : function(e, element)
        {
            var nodeId = element.id, model = this.model, saved = this.saved;

            this.trigger('save');
            this.model.saveFetchAttribute(['Source', nodeId], function()
            {
                saved();
                model.fetch();
            });

            eZExceed.stack.pop();
        },

        saveFetchAttribute : function(e)
        {
            this.trigger('save');
            this.model.saveFetchAttribute(this.getAttributeData(e), this.saved);
        },

        saveRotation : function(e)
        {
            this.trigger('save');
            this.model.saveRotation(this.getAttributeData(e), this.rotationUpdated);
        },

        rotationUpdated : function(response)
        {
            this.trigger('saved');
            this.updateItems(response);
        },

        saveCustomAttribute : function(e)
        {
            this.trigger('save');
            this.model.saveCustomAttribute(this.getAttributeData(e), this.saved);
        },

        getAttributeData : function(e)
        {
            var target = this.$(e.currentTarget), key = target.attr('name'), value;

            if (target.is(':checkbox')) {
                value = target.is(':checked') ? target.val() : 0;
            }
            else {
                value = target.val();
            }

            return [key, value];
        },

        sortStart : function(e, ui)
        {
            this.startIndex = this.$(ui.item).index();
        },

        sortStop : function(e, ui)
        {
            var changed = this.$(ui.item).index() !== this.startIndex;
            this.startIndex = null;

            if (!changed) {
                return;
            }

            var sortResults = [], itemContainer = this.$(ui.item).parent(), itemType = itemContainer.data('item-type');

            _(itemContainer.children('li')).each(function(li)
            {
                li = this.$(li);

                sortResults.push({
                    id : li.data('id'),
                    index : li.index()
                });

            }, this);

            this.trigger('save');
            this.model.saveSort({sortResults : sortResults}, itemType, this.saved);
        },

        render : function()
        {
            this.$el.html(this.model.get('html'));
            selectable(this.$('select'), {width: 'off'});
            this.addEvents();

            return this;
        },

        addEvents : function()
        {
            /** Enable sorting. */
            this.$('ul.eze-items').sortable({
                start : this.sortStart,
                stop : this.sortStop,
                handle : '.reorder',
                axis : 'y',
                delay : 50
            });
            /** Set up date and time-picker. */
            _.each(this.$('[data-item-type="waiting"] li'), function(el, index)
            {
                el = $(el);
                this.dates[index] = new DatePicker({
                    el: el.find('.publish-date'),
                    init: true
                });
                this.times[index] = timepicker(el.find('input.time-picker'));
            }, this);
        }
    });
});
