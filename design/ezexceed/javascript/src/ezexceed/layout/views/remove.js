/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['./add'], function(Add)
{
    return Add.extend(
    {
        parentModel : null,
        statusText : 'The content is already added',
        actionText : 'Remove from the block',
        confirm : true,
        primary : true,

        validate : function(node)
        {
            return this.parentModel.nodeExists(node);
        }
    });
});
