/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 *
 * @class Block.model
 */
define(['backbone', 'shared/node', 'jquery-safe'], function(Backbone, Node, $)
{
    return Backbone.Model.extend(
    {
        urls : null,
        moduleParams : null,
        blockNodes : null,
        returnTemplates : 1,

        initialize : function(options)
        {
            _.bindAll(this);

            this.blockNodes = new Node.collection();

            this.moduleParams = {
                objectId : options.objectId,
                language : options.language,
                zoneIndex : options.zoneIndex,
                blockId : options.blockId
            };

            _.extend(this, _.pick(options, ['returnTemplates']));

            this.setUrls();
        },

        url : function()
        {
            return '/Block/edit/' + this.returnTemplates + '/?' + $.param(this.moduleParams);
        },

        getPencil : function()
        {
            var url = '/Block/renderPencil/?' + $.param(this.moduleParams);
            return Backbone.sync('read', {url : url});
        },

        setUrls : function()
        {
            this.urls = {
                rotation : '/Block/saveRotationAttribute/',
                fetchAttribute : '/Block/saveFetchAttribute/',
                customAttribute : '/Block/saveCustomAttribute/',
                addItem : '/Block/addItem/',
                removeItem : '/Block/removeItem/',
                sortItem : '/Block/sortItem/',
                publicationTime : '/Block/setPublicationTimestamp/'
            };
        },

        nodeExists : function(node)
        {
            return this.blockNodes.where({id : node.get('id')}).length > 0;
        },

        addItem : function(nodeId, objectId, success)
        {
            var urlParams = [nodeId, objectId];
            var url = this.urls.addItem + urlParams.join('/');
            this.performSave(url, this.moduleParams, success);

            var model = new Node.model({id : nodeId, objectId : objectId});
            this.blockNodes.add(model);
        },

        removeItem : function(objectId, success)
        {
            var model = this.blockNodes.where({objectId : objectId});
            this.blockNodes.remove(model);

            var url = this.urls.removeItem + objectId;
            this.performSave(url, this.moduleParams, success);
        },

        saveSort : function(data, itemType, success)
        {
            var url = this.urls.sortItem + itemType;
            this.performSave(url, data, success);
        },

        savePublicationTime : function(data, success)
        {
            var url = this.urls.publicationTime + data.join('/');
            this.performSave(url, this.moduleParams, success);
        },

        saveRotation : function(data, success)
        {
            var url = this.urls.rotation + data.join('/');
            this.performSave(url, this.moduleParams, success);
        },

        saveCustomAttribute : function(data, success)
        {
            var url = this.urls.customAttribute + data.join('/');
            this.performSave(url, this.moduleParams, success);
        },

        saveFetchAttribute : function(data, success)
        {
            var url = this.urls.fetchAttribute + data.join('/');
            this.performSave(url, this.moduleParams, success);
        },

        performSave : function(url, data, success)
        {
            data = _.isEmpty(data) ? this.moduleParams : _.extend(data, this.moduleParams);

            Backbone.sync('create', {url : url}, {data : data, success : success});
        },

        parse : function(response)
        {
            this.blockNodes.reset(response.nodes);
            delete response.nodes;

            return response;
        }
    });
});
