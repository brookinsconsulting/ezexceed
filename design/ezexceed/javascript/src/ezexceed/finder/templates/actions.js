define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n            <div class=\"thumb\" style=\"background:url("
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.object)),stack1 == null || stack1 === false ? stack1 : stack1.thumb)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ") center center no-repeat;background-size:cover\"></div>\n        ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n            <img src=\"";
  options = {hash:{
    'id': (((stack1 = depth0['class']),stack1 == null || stack1 === false ? stack1 : stack1.id)),
    'size': (48)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\" class=\"icon-48\" />\n        ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n                    <li><span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Author", options) : helperMissing.call(depth0, "translate", "Author", options)))
    + "</span>";
  stack2 = helpers['if'].call(depth0, ((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.object)),stack1 == null || stack1 === false ? stack1 : stack1.author), {hash:{},inverse:self.noop,fn:self.program(6, program6, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                    </li>\n                    <li><span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Modified", options) : helperMissing.call(depth0, "translate", "Modified", options)))
    + "</span>"
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.object)),stack1 == null || stack1 === false ? stack1 : stack1.modified)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</li>\n                ";
  return buffer;
  }
function program6(depth0,data) {
  
  var stack1;
  return escapeExpression(((stack1 = ((stack1 = ((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.object)),stack1 == null || stack1 === false ? stack1 : stack1.author)),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1));
  }

function program8(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                    <li>\n                        <span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Status", options) : helperMissing.call(depth0, "translate", "Status", options)))
    + "</span>\n                        <i class=\"status-icon "
    + escapeExpression(((stack1 = ((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.status)),stack1 == null || stack1 === false ? stack1 : stack1.status)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"></i>\n                        ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.cap),stack1 ? stack1.call(depth0, ((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.status)),stack1 == null || stack1 === false ? stack1 : stack1.status), "translate", options) : helperMissing.call(depth0, "cap", ((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.status)),stack1 == null || stack1 === false ? stack1 : stack1.status), "translate", options)))
    + "\n                    </li>\n                ";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        <li>\n            <button href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" class=\"btn show\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "View", options) : helperMissing.call(depth0, "translate", "View", options)))
    + "</button>\n        </li>\n    ";
  return buffer;
  }

function program12(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        <li>\n            <button class=\"btn edit\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Edit", options) : helperMissing.call(depth0, "translate", "Edit", options)))
    + "</button>\n        </li>\n    ";
  return buffer;
  }

function program14(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        <li>\n            <button class=\"btn copy\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Copy", options) : helperMissing.call(depth0, "translate", "Copy", options)))
    + "</button>\n        </li>\n    ";
  return buffer;
  }

function program16(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n        <li>\n            <div class=\"well well-small option-controller\">\n                <label>\n                    <input class=\"toggle-visibility\" type=\"checkbox\"";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.hidden), {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += ">\n                    ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Hide this content", options) : helperMissing.call(depth0, "translate", "Hide this content", options)))
    + "\n                </label>\n            </div>\n        </li>\n    ";
  return buffer;
  }
function program17(depth0,data) {
  
  
  return " checked";
  }

  buffer += "<div class=\"nodestructure-actions\">\n    <div class=\"heading\">\n        ";
  stack1 = helpers['if'].call(depth0, depth0.hasThumb, {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n\n        <h3>"
    + escapeExpression(((stack1 = ((stack1 = depth0['class']),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</h3>\n\n        <h2>«"
    + escapeExpression(((stack1 = ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "»</h2>\n\n        <div class=\"meta\">\n            <ul>\n                ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.object), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n                ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.node),stack1 == null || stack1 === false ? stack1 : stack1.status), {hash:{},inverse:self.noop,fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            </ul>\n        </div>\n    </div>\n</div>\n\n<ul class=\"action-buttons clearfix\">\n\n    ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.actions),stack1 == null || stack1 === false ? stack1 : stack1.read), {hash:{},inverse:self.noop,fn:self.program(10, program10, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.actions),stack1 == null || stack1 === false ? stack1 : stack1.edit), {hash:{},inverse:self.noop,fn:self.program(12, program12, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.actions),stack1 == null || stack1 === false ? stack1 : stack1.copy), {hash:{},inverse:self.noop,fn:self.program(14, program14, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n    ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.actions),stack1 == null || stack1 === false ? stack1 : stack1.hide), {hash:{},inverse:self.noop,fn:self.program(16, program16, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n</ul>";
  return buffer;
  })

});