define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function";


  buffer += "Deleting ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.name, options) : helperMissing.call(depth0, "translate", depth0.name, options)))
    + " will also remove all sub-locations. <span class=\"danger\">";
  if (stack2 = helpers.nodeCount) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.nodeCount; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " sub-locations will also be deleted.</span>";
  return buffer;
  })

});