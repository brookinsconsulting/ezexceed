define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  buffer += "<div class=\"input-append\">\n    <input class=\"span2 search\" type=\"text\" placeholder=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Search", options) : helperMissing.call(depth0, "translate", "Search", options)))
    + "...\" />\n    <span class=\"add-on\"><i class=\"icon-search\"></i></span>\n</div>\n<ul class=\"nodes\"></ul>";
  return buffer;
  })

});