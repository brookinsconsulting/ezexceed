define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  buffer += "<li class=\"notice\">\n    <img src=\"/extension/ezexceed/design/ezexceed/images/kp/16x16/Info.png\" class=\"icon-16\">\n    <span>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "No results", options) : helperMissing.call(depth0, "translate", "No results", options)))
    + "</span>\n</li>";
  return buffer;
  })

});