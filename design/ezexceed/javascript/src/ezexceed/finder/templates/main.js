define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  


  return "<div class=\"finder\">\n    <div class=\"breadcrumbs\"><p></p></div>\n    <div style=\"clear:both;\"></div>\n    <div class=\"kp-finder-content\">\n        <div class=\"kp-columns-wrapper\">\n            <div class=\"kp-columns\"></div>\n        </div>\n        <div class=\"actions\"></div>\n    </div>\n</div>\n";
  })

});