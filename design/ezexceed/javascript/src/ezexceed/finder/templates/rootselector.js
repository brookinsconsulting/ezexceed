define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, self=this, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n	<li";
  stack1 = helpers['if'].call(depth0, depth0.selected, {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += ">\n	    <a data-node-id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.cap),stack1 ? stack1.call(depth0, depth0.name, false, options) : helperMissing.call(depth0, "cap", depth0.name, false, options)))
    + "</a>\n	</li>\n    ";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return " class=\"active\"";
  }

  buffer += "<a data-toggle=\"dropdown\" class=\"btn btn-mini dropdown-toggle\">\n    ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.cap),stack1 ? stack1.call(depth0, ((stack1 = depth0.selectedNode),stack1 == null || stack1 === false ? stack1 : stack1.name), false, options) : helperMissing.call(depth0, "cap", ((stack1 = depth0.selectedNode),stack1 == null || stack1 === false ? stack1 : stack1.name), false, options)))
    + "\n    <span class=\"caret\"></span>\n</a>\n\n<ul class=\"dropdown-menu\">\n    ";
  stack2 = helpers.each.call(depth0, depth0.rootNodes, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</ul>";
  return buffer;
  })

});