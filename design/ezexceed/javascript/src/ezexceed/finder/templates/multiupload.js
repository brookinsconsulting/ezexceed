define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;


  buffer += "<button id=\"";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n    <img src=\"";
  options = {hash:{
    'size': (16)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.icon),stack1 ? stack1.call(depth0, "Add", options) : helperMissing.call(depth0, "icon", "Add", options)))
    + "\" class=\"icon-16\"/>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Upload files", options) : helperMissing.call(depth0, "translate", "Upload files", options)))
    + "\n</button>";
  return buffer;
  })

});