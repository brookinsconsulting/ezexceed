/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
 
 define(['backbone', 'funky'], function(Backbone, Funky)
 {
    /** @class Finder.Collection */
    var Collection = Backbone.Collection.extend(
    {
        /**
         * Retrives the active column.
         *
         * @return {Backbone.Model}
         */
        getActive : function(property)
        {
            var activeColumn = this.find(Funky.truthyProp('active', 'get'));

            if (!activeColumn) {
                return;
            }

            return property ? activeColumn.get(property) : activeColumn;
        },

        /**
         * Retrieves a column if it contains the given node.
         *
         * @param node
         * @return {Backbone.Model}
         */
        byChildId : function(node)
        {
            return this.find(function(model)
            {
                return model
                    .get('view')
                    .model
                    .get('children')
                    .contains(node);
            });
        },

        /**
         * Retrieves a column-index by model-id.
         *
         * @param nodeId
         * @return {number}
         */
        indexOfId : function(nodeId)
        {
            var column = this.find(function(model)
            {
                return model.get('view').model.id === nodeId;
            });

            return column ? column.get('view').index : -1;
        }
    });

    return {
        collection : Collection
    };
 });