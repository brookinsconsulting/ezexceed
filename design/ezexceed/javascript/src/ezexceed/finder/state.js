/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone'], function(Backbone)
{
    /** @class Finder.State */
    return Backbone.Model.extend({
        objects : null,
        frozen : null,

        initialize : function()
        {
            _.bindAll(this);
            this.objects = {
                marked : null,
                clipboard : null
            };
        },

        freeze : function()
        {
            if (this.frozen) {
                return;
            }

            this.objects.marked.off('fetched', this.onFetched, this);

            this.frozen = true;
        },

        unFreeze : function()
        {
            if (!this.frozen) {
                return;
            }

            this.objects.marked.on('fetched', this.onFetched, this);

            this.frozen = false;
        },

        mark : function(node, options)
        {
            options = (options || {});

            if (this.objects.marked) {
                this.objects.marked.off('fetched', this.onFetched, this);
            }

            this.objects.marked = node;
            this.objects.marked.on('fetched', this.onFetched, this);

            if (!options.hasOwnProperty('silent') || !options.silent) {
                this.trigger('mark', this.objects.marked);
                this.objects.marked.load();
            }
        },

        onFetched : function(node)
        {
            this.trigger('update', node);
        },

        clipboard : function(node, type)
        {
            if (node) {
                this.objects.clipboard = {node : node, type : type};
                this.trigger('clipboard', node);
            }
            else {
                return this.objects.clipboard;
            }
        },

        emptyClipboard : function()
        {
            this.objects.clipboard = null;
            this.trigger('clipboard');
        },

        setError : function(msg, node)
        {
            this.trigger('error', {msg: msg, node: node});
        }
    });
});
