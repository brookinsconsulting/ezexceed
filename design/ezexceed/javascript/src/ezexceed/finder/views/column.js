/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/containerview', './node', 'ezexceed/scrollbarwidth', './multiupload', 'ezexceed/finder/templates/column', 'ezexceed/finder/templates/columnempty'],
    function(View, NodeView, ScrollBarWidth, MultiUpload, ColumnTemplate, ColumnEmptyTemplate)
{
    /**
     * @class Finder.Column
     */
    return View.extend({
        DND_THRESHOLD : 100,

        bindKeysScoped : true,
        index : 0,
        activeIndex : -1,
        height : 0,
        views : null,
        openNodeId : false,
        /** @type Finder.MultiUpload */
        multiUpload : null,
        searchString : '',

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['state', 'openNodeId']));

            this.collection = this.model.get('children');

            this.views = {};

            this.listenTo(this, {'column:domready' : this.renderMultiUpload});

            this.filter = _.debounce(this.filter, 250);

            this.init();
        },

        remove : function()
        {
            _(this.views).invoke('remove');

            if (this.multiUpload) {
                this.multiUpload.remove();
            }

            View.prototype.remove.apply(this, arguments);
        },

        freeze : function()
        {
            if (this.multiUpload) {
                this.multiUpload.remove();
            }

            this.stopListening(this.model);
        },

        unFreeze : function()
        {
            this.listenTo(this.model, {'fetched' : this.renderChildren});

            if (this.multiUpload) {
                this.renderMultiUpload();
            }
        },

        nodeRemoved : function(node)
        {
            var focusNode;
            var parent = node.parent();

            /** > 1 => The view not yet been removed. */
            if (_(this.views).size() > 1) {
                var keys = _(this.views).keys();
                var removedNodeIndex = _(keys).indexOf(node.id.toString());

                var focusId;

                /** No nodes further down, select previous one. */
                if (removedNodeIndex + 1 >= keys.length) {
                    focusId = keys[removedNodeIndex - 1];
                }
                /** Select next node. */
                else {
                    focusId = keys[removedNodeIndex + 1];
                }

                focusNode = this.views[focusId].model;

                this.views[node.id].remove();
                delete this.views[node.id];

                if (this.activeIndex > 0) {
                    this.activeIndex--;
                }
            }
            /** No more children, select parent. */
            else {
                this.model.set('hasChildren', false);
                focusNode = this.model;
            }

            /** parent is in reality this.model, but they have become duplicated somehow. */
            this.model.removeChild(node);
            parent.removeChild(node);

            this.renderChildren(this.model);

            /** Might be the last child. */
            if (this.views.hasOwnProperty(focusNode.id)) {
                this.expand(this.activeIndex);
            }
            else {
                this.state.mark(focusNode);
                /** Select previous colunmn. */
                this.trigger('columnSelect', this.model, this.index);
                /** Resize column-container. */
                this.trigger('rendered');
            }
        },

        events : {
            'click a:not(.disabled)' : 'expand',
            'click' : 'select',
            'mark' : 'mark',
            'keyup input.search' : 'filter'
        },

        select : function(e)
        {
            if (e.target === this.el && !this.$el.hasClass('active')) {
                /**
                 * Then it's the column that is clicked
                 * Trigger the parents model
                 */
                this.activeIndex = -1;
                this.trigger('columnSelect', this.model, this.index + 1);
                this.$('.active').removeClass('active');
                this.$el.addClass('active');

                this.trigger('rendered', this);
            }
        },

        mark : function(e, options)
        {
            options = (options || {});
            var target = this.$(e.target);
            var node;
            if (target[0] !== this.$el[0]) {
                node = this.collection.get(target.data('id'));
            }
            else {
                node = this.model;
            }

            // Node is in reality a Backbone.View
            if (options.hasOwnProperty('parent')) {
                node = node.parent();
            }

            if (options.hasOwnProperty('clipboard')) {
                if (options.clipboard.move) {
                    this.state.clipboard(node, 'move');
                }
            }
            else {
                this.state.mark(node, options);
            }
        },

        filter : function(e)
        {
            /** Do not trigger search when navigating with keys. */
            if (_.contains(['up', 'left', 'right', 'down'], this.keyName(e.which))) {
                return;
            }

            this.searchString = this.$('input.search').val();
            this.model.searchString = this.searchString;
            this.model.load({clearCache:true});
        },

        expand : function(e)
        {
            var model = this.collection.at(this.activeIndex);

            if (_(e).isNumber()) {
                model = this.collection.at(e);
                this.activeIndex = e;
            }
            else if (_(e).isObject()) {
                var target = this.$(e.currentTarget);
                model = this.collection.get(target.data('id'));
                this.activeIndex = this.collection.indexById(model.id);
            }

            /**
             * Clear cache if there was a search
             */
            if (model.searchString) {
                model.searchString = '';

                var cached = model.nodeCache.get(model.id);
                if (cached) {
                    model.nodeCache.remove(cached);
                }
            }

            this.state.mark(model);
            this.$('.active').removeClass('active');
            if (model && this.views.hasOwnProperty(model.id)) {
                var nodeView = this.views[model.id];
                nodeView.trigger('select');

                /**
                 * Just trigger an expand event
                 * and hopefully eZExceed.Views.Columns will handle it
                 * and render it into a new model
                 */
                var newIndex = this.index + 1;
                this.trigger('expand', model, newIndex);

                // Scroll column when keyboard is navigating
                var nodeHeight = 25;
                var scrollTop = this.$el.scrollTop();
                var offset = nodeView.$el.position().top;

                if ((offset - nodeHeight) < 0) {
                    // If node is above column viewport, scroll
                    this.$el.scrollTop(scrollTop + offset - nodeHeight);
                }
                else if ((offset + nodeHeight) > this.height) {
                    // If node is below column viewport, scroll
                    this.$el.scrollTop((offset + nodeHeight) - this.height + scrollTop);
                }
            }
        },

        /**
         * Detect if this column is expandable given its current active model
         */
        hasChildren : function()
        {
            return (this.model.has('hasChildren') && this.model.get('hasChildren'));
        },

        render : function()
        {
            this.activeIndex = -1;
            this.$el.html(ColumnTemplate({}))
                .data('model', this.model);

            if (ScrollBarWidth === 0) {
                this.$el.css({
                    overflow : 'auto',
                    overflowX : 'scroll'
                });
            }

            if (this.model.get('hasChildren')) {
                this.renderChildren(this.model);
            }
            return this;
        },

        renderMultiUpload : function()
        {
            /** Multiupload is for containers only. */
            if (!this.model.get('isContainer')) {
                return;
            }

            var uploadOptions = {
                model : this.model,
                state : this.state
            };

            /** Must be attached to dom before render is called. */
            this.multiUpload = new MultiUpload(uploadOptions);

            this.$el.append(this.multiUpload.$el);
            this.multiUpload.render();

            this.$('ul').css('padding-bottom', this.multiUpload.$el.height());
        },

        renderChildren : function(model)
        {
            if (this.frozen === true) {
                return;
            }

            this.collection = model.get('children');
            var nodeList;

            if (this.collection.length === 0) {
                if (this.model.searchString) {
                    nodeList = ColumnEmptyTemplate({});
                }
                else {
                    return;
                }
            }
            else {
                /** If this is an update, we must find the active node to make sure it get the active status. */
                var activeId = this.$('.node.active a').data('id');

                nodeList = this.collection.map(function(child, index)
                {
                    var nodeView = this.nodeView(child, index < this.DND_THRESHOLD);
                    if (activeId == child.id) {
                        nodeView.select();
                    }
                    return nodeView.el;
                }, this);
            }

            this.$('ul').html(nodeList);

            this.trigger('rendered', this);

            this.height = this.$el.height();

            if (this.openNodeId) {
                this.goToContext(this.openNodeId);
                this.openNodeId = false;
            }
            if (this.searchString) {
                if (this.collection.length) {
                    this.expand(0);
                }
                else {
                    /**
                     * Make the column active and the parent node as the selected one
                     * Must reset the searchString, or else there will be an eternal loop
                     */
                    this.searchString = '';
                    this.activeIndex = -1;
                    this.trigger('columnSelect', this.model, this.index + 1);
                    this.$el.addClass('active');
                    this.trigger('rendered', this);
                }
            }
            return this;
        },

        // This expands the sitemap all the way down to
        // a specified model
        goToContext : function(nodeId)
        {
            var el = this.$('#node-' + nodeId);
            if (el.length > 0) {
                this.expand(el.parent().index());
            }
        },

        nodeView : function(model, dnd)
        {
            if (this.views.hasOwnProperty(model.id)) {
                this.views[model.id].remove();
            }

            var view = new NodeView({
                model : model,
                state : this.state,
                dnd : dnd
            });

            /** This method is run every time a column is rendered. This prevents duplicate-events from forming. */
            model.off('removed');
            model.on('removed', this.nodeRemoved, this);
            model.view = view;
            this.views[model.id] = view;

            return view.render();
        },

        prev : function()
        {
            if (this.activeIndex > 0) {
                this.expand(this.activeIndex - 1);
            }
        },

        next : function()
        {
            if (this.model.has('hasChildren') && this.model.get('hasChildren')) {
                if (this.collection.length - 1 > this.activeIndex) {
                    this.expand(this.activeIndex + 1);
                }
            }
        }
    });
});
