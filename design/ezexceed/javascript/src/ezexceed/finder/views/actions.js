/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['backbone', 'shared/containerview', 'shared/views/action', './create', './multiupload', 'module', 'jquery-safe', 'ezexceed/finder/templates/actions', 'ezexceed/finder/templates/delete'],
    function(Backbone, View, Action, AddContent, MultiUpload, Module, $, ActionsTemplate, DeleteTemplate)
{
    /**
     * @class Finder.Actions
     */
    return View.extend(
    {
        /** The nodestructure state object which holds the current active node. */
        state : null,
        /** What actions to support. */
        actions : {
            read : false,
            copy : false,
            remove : false,
            edit : false,
            hide : false
        },
        customActions : null,
        dimensions : null,
        $actionContainer : null,
        trashEnabled : null,
        /** @type Finder.MultiUpload */
        multiUpload : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['actions', 'state', 'dimensions', 'customActions']));

            this.trashEnabled = Module.config().trashEnabled;

            this.init();
        },

        freeze : function()
        {
            this.stopListening(this.state);

            if (this.views.multiUpload) {
                this.views.multiUpload.remove();
            }
        },

        unFreeze : function()
        {
            /** Listen for changes in the active collection. ReRender the view if a new node is active. */
            this.listenTo(this.state, {
                'update' : this.render,
                'paste' : this.paste
            });

            this.renderMultiUpload();
        },

        events : {
            'click .copy' : 'copy',
            'click .edit' : 'edit',
            'click .show' : 'view',
            'click .toggle-visibility' : 'toggleVisibility'
        },

        render : function()
        {
            this.views = {};

            var active = this._getActiveModel();

            this._mapActionsAccess(active);

            var contentClass = active.get('class');

            var hasThumb = false;

            if (active && active.get('object')) {
                hasThumb = active.get('object').thumb.length > 0;
            }

            var data = {
                actions : this.actions,
                node : active ? active.toJSON() : false,
                'class' : contentClass ? contentClass.toJSON() : false,
                hasThumb : hasThumb
            };

            this.$el.html(ActionsTemplate(data));

            this.$actionContainer = this.$('ul.action-buttons');

            if (this.customActions === null) {
                this.customActions = this.defaultActions();
                this.customActions.addcontent = new AddContent();
            }

            _(this.views).extend(this.customActions);

            _(this.customActions).each(function(customAction)
            {
                customAction.model = active;
                customAction.state = this.state;

                if (this.actions.hide) {
                    this.$('.toggle-visibility').closest('li').before(customAction.$el);
                }
                else {
                    this.$actionContainer.append(customAction.$el);
                }
                customAction.render();
            }, this);

            this.$el.css('height', this.dimensions.height);

            this.renderMultiUpload();

            return this;
        },

        renderMultiUpload : function()
        {
            var active = this._getActiveModel();

            if (!active) {
                return;
            }

            /** The purpose of this view here is only to allow uploading of files to nodes that have no children. */
            if (!active.get('isContainer') || active.get('hasChildren')) {
                return;
            }

            /** Must be attached to dom before render is called. */
            this.views.multiUpload = new MultiUpload({model : active, state : this.state});
            this.$el.append(this.views.multiUpload.$el);
            this.views.multiUpload.render();
        },

        defaultActions : function()
        {
            if (!this.actions.remove) {
                return {};
            }

            var statusTextFn = function(model)
            {
                if (model.get('allChildrenCount')) {
                    var context = {
                        name : {
                            text : model.get('name'),
                            quotes : true
                        },
                        nodeCount : model.get('allChildrenCount')
                    };

                    return {
                        text : DeleteTemplate(context)
                    };
                }

                if (model.get('object').assignedNodesCount <= 1) {

                    var removeText = '';

                    if (this.trashEnabled) {
                        removeText = 'Removing it will also move the content to Trash';
                    }
                    else {
                        removeText = 'Removing it will permanently delete the content';
                    }

                    return [
                        'This is the only location for',
                        {
                            text : model.get('name'),
                            quotes : true
                        },
                        {
                            text : '. '
                        },
                        removeText
                    ];
                }

                return '';
            };

            var removeOptions = {
                actionText : function(model)
                {
                    var buttonText = this.trashEnabled ? 'Trash' : 'Delete';

                    return (model.get('object').assignedNodesCount <= 1 || model.get('allChildrenCount')) ? buttonText : 'Remove';
                },
                statusText : statusTextFn,
                validate : function(model)
                {
                    return model.get('access').remove;
                },
                confirm : function(model)
                {
                    return (model.get('object').assignedNodesCount <= 1 || model.get('allChildrenCount'));
                },
                primary : false
            };

            return {
                remove : new Action(removeOptions).on('action', this.removeNode)
            };
        },

        removeNode : function(e, model)
        {
            var data = {ContentNodeID : model.id};
            var state = this.state;
            model.url = '/NodeStructure/delete';

            var success = function(response)
            {
                if (response.success) {
                    model.trigger('removed', model);
                    eZExceed.trigger('finder:visibility', {
                        id: model.id
                    });
                    model.set('assignedNodesCount', model.get('assignedNodesCount') - 1);
                    /**
                     * If this is current node, mark it as removed
                     */
                    if (model.id == eZExceed.config.currentNodeId) {
                        eZExceed.currentNodeChanges.removed = true;
                    }
                }
                else if (response.error) {
                    state.setError(response.msg, model);
                }
            };

            Backbone.sync('create', model, {data : data}).done(success);
        },

        view : function(e)
        {
            var target = this.$(e.currentTarget);

            if (target.attr('href')) {
                window.location.href = target.attr('href');
            }
        },

        /** Perform edit action. */
        edit : function()
        {
            this.$('.edit').addClass('edit');
            this.state.trigger('marked:edit');
        },

        toggleVisibility : function()
        {
            var active = this._getActiveModel();
            var url = '/NodeStructure/toggleVisibility/' + active.id;

            Backbone.sync('create', {url : url}, {data : {}}).done(function()
            {
                eZExceed.trigger('finder:visibility', {
                    id: active.id
                });
                active.load({clearCache : true});
            });
        },

        /** Perform copy action. */
        copy : function()
        {
            var active = this._getActiveModel();
            var parent = active.parent();

            if (parent) {
                /** Make a copy in same parent node. */
                this.state.clipboard(active, 'copy');
                this.state.mark(parent, {silent : true});
                parent.addChild(active.clone().set({id : false}));
                parent.load();
                this.paste();
                eZExceed.trigger('finder:copy', {
                    id: active.id
                });
            }
        },

        /** Paste the node in the clipboard state. */
        paste : function()
        {
            var active = this._getActiveModel();
            var errorMsg;
            if (this.canPaste()) {
                var data = {}, url = '/NodeStructure/', state = this.state;
                var clipboard = state.clipboard();
                var parent = clipboard.node.parent();

                /** Refresh the active node(the node that is pasted to) from server. */
                if (clipboard.type === 'copy') {
                    data = {
                        SelectedNodeId : active.id,
                        ObjectId : clipboard.node.get('objectId')
                    };
                    url += 'copy';
                }

                if (clipboard.type === 'move') {
                    if (!clipboard.node.canMoveTo(active)) {
                        errorMsg = eZExceed.translate([
                            "Can't move to ",
                            {text : active.get('name'), quotes : true}
                        ]);
                        state.setError(errorMsg, active);
                        return false;
                    }

                    data = {
                        NewParentNode : active.id,
                        NodeId : clipboard.node.id
                    };

                    url += 'move';

                    if (parent.id === active.id) {
                        return false;
                    }
                }

                active.url = url;
                var _this = this;

                var success = function(response)
                {
                    if (response.error) {
                        state.setError(response.msg, active);
                    }

                    if (!parent) {
                        parent = active.parent();
                    }

                    var options = {clearCache : true};
                    $.when(parent.load(options), active.load(options)).then(function()
                    {
                        _this.state.trigger('setNodePath', response.node.pathString);
                    });
                };

                Backbone.sync('create', active, {data : data}).done(success);
            }
            else {
                errorMsg = eZExceed.translate([
                    {text : active.get('name'), quotes : true},
                    "does not support subcontent"
                ]);
                this.state.setError(errorMsg, active);
                this.state.mark(active);
                return false;
            }
        },

        canPaste : function()
        {
            var clipboard = this.state.clipboard();

            if (clipboard) {
                if (clipboard.type === 'copy') {
                    return true;
                }

                var active = this._getActiveModel();

                if (clipboard.node.canMoveTo(active)) {
                    return true;
                }
            }
            return false;
        },

        /** Get the actually active model from the active collection. */
        _getActiveModel : function()
        {
            if (this.state.objects.marked) {
                return this.state.objects.marked;
            }

            return false;
        },

        _mapActionsAccess : function(node)
        {
            var nodeAccess = node.get('access');
            var actions = this.actions;

            _(nodeAccess).each(function(access, key)
            {
                if (actions.hasOwnProperty(key) && actions.key) {
                    actions[key] = access;
                }
            });
        }
    });
});
