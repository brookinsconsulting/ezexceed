/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/containerview', 'funky', 'ezexceed/finder/templates/node'], function(View, Funky, Template)
{
    return View.extend({
        tagName : 'li',
        className : 'node',
        childrenEl : null,
        childrenList : null,
        state : null,
        dnd : false,
        mutable : true,

        $loaderIcon : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['state', 'dnd']));

            this.$loaderIcon = this._loader({size:16, className: 'icon-16'});

            this.init();
        },

        events : function() {
            var toggleMutable = Funky.toggleProp('mutable');

            return {
                'eze:immutable' : toggleMutable,
                'eze:mutable' : toggleMutable
            };
        },

        remove : function()
        {
            if (this.mutable) {
                View.prototype.remove.apply(this, arguments);
            }
        },

        freeze : function()
        {
            this.stopListening();
        },

        unFreeze : function()
        {
            this.listenTo(this, {
                'select' : this.select
            });

            this.listenTo(this.model, {
                'fetch' : this.showLoader,
                'change fetched' : this.render
            });

            this.listenTo(this.state, {
                'clipboard' : this.clipboard
            });
        },

        showLoader : function()
        {
            this.$('img').replaceWith(this.$loaderIcon);
        },

        render : function()
        {
            var name = this.model.get('name');

            var data = {
                id : this.model.id,
                classId : this.model.get('classId'),
                name : name,
                hasName : name.length > 0,
                isContainer : this.model.get('isContainer')
            };

            var droppable = this.dnd && this.model.get('isContainer');

            this.$el.html(Template(data))
                .data('model', this.model)
                .addClass(droppable ? 'drop' : 'no-drop')
                .toggleClass('hidden-object', this.model.get('invisible') || this.model.get('hidden'))
                .toggleClass('empty-node', !this.model.get('hasChildren'));

            this.clipboard();
            this.show();

            return this;
        },

        select : function()
        {
            this.$el.addClass('active');
            return this;
        },

        clipboard : function()
        {
            if (this.state)
            {
                var clipboard = this.state.clipboard();

                if (clipboard && clipboard.node === this.model) {
                    this.$el.addClass('clipboard');
                }
                else {
                    this.$el.removeClass('clipboard');
                }
            }
            return this;
        }
    });
});
