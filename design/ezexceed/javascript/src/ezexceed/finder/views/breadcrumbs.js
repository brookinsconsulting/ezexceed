/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/containerview', './rootselector'], function(View, RootSelector)
{
    return View.extend(
    {
        state : null,
        node : null,
        path : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['state', 'node']));

            var selectorOptions = {
                state : this.state,
                rootNodeId : this.node.id
            };

            this.views = {rootSelector : new RootSelector(selectorOptions)};

            this.$el.prepend(this.views.rootSelector.render().$el);

            this.init();
        },
        
        events : {
            'click p a' : function(e)
            {
                var nodeId = this.$(e.target).data('nodeId');
                this.state.trigger('navigateBreadcrumbs', nodeId);
            }
        },

        freeze : function()
        {
            this.node.off(null, null, this);
            this.stopListening(this.state);
            this.stopListening(this.node);
        },

        unFreeze : function()
        {
            this.listenTo(this.state, {'mark' : this.update});
            this.listenTo(this.node, {'fetched' : this.renderLast});
        },

        /**
         * Triggered when the eZExceed.State object receives a
         * marked Node model.
         * (Re)renders the breadcrumb view
         */
        update : function(node)
        {
            this.node = node;
            this.path = [];
            var runs = 0;
            if (this.node) {
                while (node !== false && runs < 10) {
                    runs++;
                    this.path.push({
                        name: node.get('name'),
                        id: node.get('id')
                    });
                    node = node.parent();
                }
            }

            return this.render();
        },

        render : function()
        {
            var pathLinks = _.map(this.path.reverse(), function(nodeData)
            {
                return '<a data-node-id="' + nodeData.id + '">' + nodeData.name + '</a>';
            });
            
            this.$('p').html(pathLinks.join(' › '));
            return this;
        },

        renderLast : function()
        {
            this.path[0] = {
                name: this.node.get('name'),
                id: this.node.get('id')
            };
            this.render();
        }
    });
});