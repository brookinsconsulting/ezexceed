/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['backbone' , 'shared/containerview', 'funky', 'shared/buttondropdown', 'module', 'ezexceed/finder/templates/rootselector'],
    function(Backbone, View, Funky, ButtonDropdown, Module, Template)
{
    /**
     * @class Finder.RootSelector
     */
    return View.extend(
    {
        className : 'btn-group',
        state : null,
        rootNodeId : null,
        buttonDropdown : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['state', 'rootNodeId']));

            this.collection = new Backbone.Collection();
            /**
            * Sanitize model names as sometimes a root node
            * has an empty name even though it should not happen
            * In this case we default it for the root selector
            * specifically
            */
            this.listenTo(this.collection, {
                add : function(model)
                {
                    if (!model.get('name')) {
                        model.set('name', 'NodeId: ' + model.id);
                    }
                }
            });
            this.createRootNodes();

            return this;
        },

        events : {
            'click .dropdown-menu a' : 'selectRoot'
        },

        updateCollection : function(nodeId)
        {
            var node = this.collection.get(nodeId);
            if (!node) {
                return false;
            }

            this.selectedNode().set('selected', false);

            return node.set('selected', true);
        },

        selectRoot : function(e)
        {
            e.stopPropagation();

            var rootNodeId = this.$(e.target).data('nodeId');

            this.buttonDropdown.close();

            if (rootNodeId === this.selectedNode().id) {
                return;
            }

            var selected = this.updateCollection(rootNodeId);

            this.state.trigger('rootChanged', selected.id);
        },

        createRootNodes : function()
        {
            var rootNodes = Module.config().rootNodes;

            if (rootNodes.sitemapRootNode) {
                this.collection.add(rootNodes.sitemapRootNode);
                this.collection.get(rootNodes.sitemapRootNode.id).set('selected', true);
            }

            if (rootNodes.mediaRootNode) {
                this.collection.add(rootNodes.mediaRootNode);
                this.collection.get(rootNodes.mediaRootNode.id).set('selected', false);
            }

            if (rootNodes.classRootNodes && _.isObject(rootNodes.classRootNodes)) {
                this.collection.add(_.map(rootNodes.classRootNodes, function(root)
                {
                    return {
                        name : root.name,
                        id : parseInt(root.id, 10),
                        selected : false
                    };
                }));
            }

            this.updateCollection(this.rootNodeId);
        },

        selectedNode : function()
        {
            return this.collection.find(Funky.truthyProp('selected', 'get'));
        },

        render : function()
        {
            var context = {
                rootNodes : this.collection.toJSON(),
                selectedNode : this.selectedNode().toJSON()
            };

            this.$el.html(Template(context));

            this.buttonDropdown = new ButtonDropdown({el : this.$el});

            if (this.collection.length === 1) {
                this.hide();
            }

            return this;
        }
    });
});
