/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 *
 */

define(['shared/containerview', 'ezexceed/finder/model', 'funky', './dnd', './column', 'ezexceed/scrollbarwidth'],
    function(View, Model, Funky, DnD, Column, ScrollBarWidth)
{
    /**
     * @class Finder.Columns
     * @property {Finder.Collection} collection
     */
    return View.extend(
    {
        /** @type Finder.State */
        state : null,
        finderOptions : null,
        /** @type Finder.DnD */
        DnD : null,
        views : null,
        dNdavailable : true,
        dimensions : null,
        nodeIdPath : false,

        rendered : false,

        initialize : function(options)
        {
            _.bindAll(this);

            this.collection = new Model.collection();
            this.columnWidth = 213 + ScrollBarWidth;

            _.extend(this, _.pick(options, ['state', 'finderOptions', 'rootNodeId']));

            // Normalize node id path so it starts at the node we got as root node
            if (this.finderOptions && this.finderOptions.hasOwnProperty('nodeIdPath')) {
                this.setNodeIdPath(this.finderOptions.nodeIdPath);
            }

            this.state.on('setNodePath', this.expandNodeIdPath);
            
            this.listenTo(this.state, {
                'navigateBreadcrumbs' : this.navigateBreadcrumbs
            });

            this.listenTo(this.collection, {
                'add' : this.renderAt
            });

            if (options.hasOwnProperty('dNd') && !options.dNd) {
                this.dNdavailable = false;
            }

            this.init();
        },

        remove : function()
        {
            this.collection.each(function(model)
            {
                model.get('view').remove();
            });

            View.prototype.remove.apply(this, arguments);
        },

        expandNodeIdPath : function(nodeIdPath)
        {
            this.setNodeIdPath(nodeIdPath);
            var node = this.state.objects.marked;

            var index = this.collection.indexOfId(node.id);
            if (index < 0) {
                index = this.collection.length;
            }

            this.insertNode(node, index, true);
        },

        // Take a path string (1/2/3) and filter anything above the rootNodeId away
        // and keep its tail: What is needed to perform automagic expanding to the current
        // node
        setNodeIdPath : function(path)
        {
            if (path.length === 0) {
                return;
            }

            path = path.replace(/^\//, '').replace(/\/$/, '').split('/');

            path = _(path).map(function(nodeId){
                return parseInt(nodeId, 10);
            });

            if (path) {
                this.nodeIdPath = _.tail(path, _(path).indexOf(this.rootNodeId) + 1);
            }

            return this;
        },

        setDimensions : function(dimensions)
        {
            this.dimensions = dimensions;
        },

        freeze : function()
        {
            this.collection.each(function(column)
            {
                this.unBindColumnEvents(column);
                column.get('view').trigger('freeze');
            }, this);
        },

        unFreeze : function()
        {
            this.collection.each(function(column)
            {
                this.bindColumnEvents(column);
                column.get('view').trigger('unfreeze');
            }, this);
        },

        bindColumnEvents : function(column)
        {
            this.listenTo(column, {
                'expand' : this.insertNode,
                'rendered' : this.refreshScrollPosition,
                'columnSelect' : this.selectColumn
            });

            this.listenTo(column, {
                'rendered' : this.enableDnD
            });
        },

        unBindColumnEvents : function(column)
        {
            this.stopListening(column);
        },

        enableDnD : function()
        {
            // Add DnD to all elements in column
            if (this.DnD) {
                this.DnD.render();
            }
        },

        render : function()
        {
            if (this.dNdavailable) {
                this.DnD = new DnD({
                    state : this.state,
                    views : this.views,
                    el : this.$el
                }).render();
            }

            return this;
        },

        /**
         * Actually insert a Column at a given index
         * Any existing column at that index and beyond is removed first
         */
        insertAt : function(col, index)
        {
            col.index = index;
            this._removeColumns(index);

            this.collection.invoke('set', 'active', false);

            this.collection.add({
                view : col,
                active : true
            });
        },

        renderAt : function(model)
        {
            var view = model.get('view');
            this.bindColumnEvents(view);

            this.$el.append(view.el);
            view.render().trigger('column:domready');
        },

        selectColumn : function(node, index)
        {
            this._removeColumns(index);
            this.state.mark(node);
            this.$('.column.active').removeClass('active');
        },

        _removeColumns : function(index)
        {
            index = index || (0);
            while (this.collection.length >= 0 && index < this.collection.length) {
                this.collection.pop().get('view').remove();
            }
        },

        refreshScrollPosition : function()
        {
            // Calculate total with of all columns
            // Apply the width to the container
            var width = this.collection.length * this.columnWidth;
            this.$el.css('width', width);

            // Set height on each column
            if (this.dimensions) {
                this.$('.column').css('height', this.dimensions.height);
            }

            // Left-scroll align
            var container = this.$el.parent();
            var cWidth = container.width();
            container.scrollLeft(width - cWidth);

            return this;
        },

        /**
         * Insert a new nodes children as a column into the column list
         * Used as an event bound trigger for `Column:expand`
         */
        insertNode : function(node, index, removeColumns)
        {
            var hasChildren = node.get('hasChildren');

            if (!hasChildren && !removeColumns) {
                this._removeColumns(index);
                this.refreshScrollPosition(this.collection.last());
            }
            else {
                this.$('.column.active').removeClass('active');

                var openNodeId = this.getNodeIdForIndex(index);
                this.insertAt(this.columnView(node, openNodeId), index);
            }
        },

        getNodeIdForIndex : function(index)
        {
            if (!this.nodeIdPath) {
                return false;
            }

            if (this.nodeIdPath.hasOwnProperty(index)) {
                var nodeId = this.nodeIdPath[index];
                this.nodeIdPath[index] = false;
                return nodeId;
            }

            return false;
        },

        keys : {
            'up down right left' : 'navigate',
            'enter' : 'visit'
        },

        visit : function()
        {
            var node = this.state.objects.marked;
            if (node) {
                eZExceed.navigateTo(node.get('fullUrl'));
            }
        },

        navigate : function(e, key)
        {
            e.preventDefault();

            /** Prefer marked. */
            var activeNode = this.state.objects.marked;

            var column = this.collection.byChildId(activeNode), view;

            if (column) {
                view = column.get('view');
            }
            else {
                view = this.collection.at(0).get('view');
            }

            switch (key) {
                case 'up':
                    view.prev();
                    break;
                case 'down':
                    view.next();
                    break;
                case 'left':
                    this.prev();
                    break;
                case 'right':
                    this.next();
                    break;
                default:
                    return true;
            }

            return false;
        },

        prev : function()
        {
            var column = this.collection.byChildId(this.state.objects.marked);

            if (!column) {
                return;
            }

            var selectedColumnIndex = this.collection.indexOf(column);

            if (selectedColumnIndex > 0) {
                column.set('active', false);
                this.collection.at(selectedColumnIndex - 1).set('active', true);

                this.collection.getActive('view').expand();
            }
            else {
                var view = column.get('view');
                //Trigger the column select if root node
                view.$el.trigger('click');
                // Reset the activeIndex.
                view.activeIndex = -1;
            }
        },

        next : function()
        {
            var children = this.state.objects.marked.get('children');

            if (children && children.length) {
                // Move focus to the first node in the new column
                var active = this.collection.getActive() || this.collection.at(0);

                active.get('view').expand(0);
            }
        },

        /**
         * Helper method for creating the Column view
         */
        columnView : function(node, openNodeId)
        {
            return new Column({
                model : node,
                tag : 'div',
                openNodeId : openNodeId,
                className : 'column',
                state : this.state
            });
        },
        
        navigateBreadcrumbs : function(nodeId)
        {
            var $nodeLink = this.$('#node-' + nodeId);
            
            if ($nodeLink.length) {
                $nodeLink.trigger('click');
            }
            else {
                this.collection.at(0).get('view').$el.trigger('click');
            }
        }
    });
});
