/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['jquery-safe', 'shared/containerview', './node', 'jqueryui/draggable', 'jqueryui/droppable', 'jquery-ui.touch'],
    function($, View, NodeView)
{
    /**
     * @class Finder.DnD
     */
    return View.extend(
    {
        state : null,
        timeoutId : null,
        replaceDragged : null,
        dragTarget : null,
        copyTarget : null,

        initialize : function(options)
        {
            /** jQuery DnD does not provide the target. This necessitates usage of $(this). In other words, please leave the bindAll-call as is. */
            _.bindAll(this, 'render', 'onStart', 'handleDrop', 'handleTimeout', 'onStop', 'makeDraggable');

            this.state = options.state;
            this.timeoutId = [];
        },

        events : {
            'node:drop div.column' : 'handleDrop',
            'node:dragOver li.node.drop' : 'handleTimeout',
            'mouseenter .node' : 'makeDraggable'
        },

        makeDraggable : function(e)
        {
            var target = this.$(e.currentTarget);
            if (!target.hasClass('draggable')) {
                target.draggable({
                    start : this.onStart,
                    stop : this.onStop,
                    helper : 'clone',
                    appendTo : this.$el,
                    revert : 'invalid',
                    stack : 'li.node',
                    scroll : true,
                    distance : 10,
                    refreshPositions : true
                });
            }
        },

        render : function()
        {
            this.$('.column').droppable({
                accept : 'li.node',
                drop : this.onDrop
            });

            this.$('li.node.drop').droppable({
                drop : this.onDrop,
                over : this.onDragOver,
                accept : 'li.node',
                greedy : true,
                out : this.onDragOut,
                hoverClass : 'dragover',
                tolerance : 'pointer'
            });

            return this;
        },

        selectStart : function()
        {
            return false;
        },

        onStart : function(e)
        {
            var target = $(e.target);
            /** Prevents text-selection in IE8. */
            target
                .bind('selectstart', this.selectStart)
                .trigger('eze:immutable');

            this.$el.addClass('dnd');

            /**
             * Move the dragged node into the #kp-columns and hide it
             * Must be done because column might be removed from DOM when showing other columns.
             * Make a copy of the dragged node and insert it at the column so it doesn't seem like it's disappeared
             */
            target.find('a').trigger('mark', {silent : true, clipboard : {move : true}});
            this.copyTarget = target.clone();
            this.hide(target);
            target.after(this.copyTarget);
            this.$el.append(target);
            this.dragTarget = target;
        },

        handleTimeout : function(e, clear)
        {
            var aTag = $(e.currentTarget).children('a');
            var id = aTag.data('id');

            if (clear) {
                if (this.timeoutId[id]) {
                    clearTimeout(this.timeoutId[id]);
                }
            }
            else {
                this.timeoutId[id] = setTimeout(function()
                {
                    aTag.click();
                }, 500);
            }
        },

        onDragOver : function(e, ui)
        {
            if (ui.draggable.find('a').data('id') !== $(this).find('a').data('id')) {
                $(this).trigger('node:dragOver', false);
            }
        },

        onDragOut : function()
        {
            $(this).trigger('node:dragOver', true);
        },

        handleDrop : function(e, data)
        {
            var options = {clipboard : {move : true}};
            var target = data.target;
            var values = target.data();

            $(data.ui.draggable).find('a').trigger('mark', options);

            var nodeModel = values.model || target.closest('li').data('model');
            if (!nodeModel) {
                return false;
            }

            this.state.mark(nodeModel, {silent : true});

            var dummyView = null;

            if (target.hasClass('column')) {
                dummyView = new NodeView({
                    model : this.state.objects.clipboard.node.clone().set({id : null}),
                    state : this.state
                });
                target.children('ul').append(dummyView.render().$el);
            }

            this.state.trigger('paste');

            if (dummyView !== null) {
                dummyView.remove();
            }
        },

        onDrop : function(e, ui)
        {
            $(this).trigger('node:drop', {ui : ui, target : $(this)});
        },

        onStop : function()
        {
            /** IE8 text-select on drag-fix. */
            this.dragTarget.unbind('selectstart');

            this.$el.removeClass('dnd');

            if (this.copyTarget.parent().length) {
                this.copyTarget.replaceWith(this.show(this.dragTarget));
            }
            else {
                this.dragTarget
                    .trigger('eze:mutable')
                    .remove();
            }

            $('.kp-columns li.node').removeClass('dragover');

            this.state.emptyClipboard();
        }
    });
});
