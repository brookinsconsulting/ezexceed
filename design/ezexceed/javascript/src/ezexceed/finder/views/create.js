define(['shared/containerview', 'ezexceed/addcontent/widget'], function(View, Widget)
{
    return View.extend({

        tagName : 'li',
        pathString : null,
        done : false,
        /** @type Finder.State */
        state : null,

        initialize : function()
        {
            this.views = {};
            _.bindAll(this).init();
        },

        freeze : function() {},

        unFreeze : function()
        {
            if (this.done) {
                this.expand();
            }
        },

        render : function()
        {
            if (this.views.widget) {
                this.views.widget.off('edit:done', this.add);
            }

            var options = {
                config : {
                    direction : 'top'
                }
            };

            this.views.widget = new Widget(options).render();
            this.views.widget.nodeId = this.model.id;
            this.views.widget.on('edit:done', this.add);
            this.$el.html(this.views.widget.$el);
            if (this.model.get('isContainer')) {
                this.show();
            }
            else {
                this.hide();
            }

            return this;
        },

        add : function(contentObject)
        {
            if (!contentObject) {
                return;
            }

            var locations = contentObject.get('locations');
            var first = locations.at(0);

            if (first) {
                this.pathString = first.get('pathString');
            }

            this.model.load({clearCache : true}).done(this.addDone);
        },

        addDone : function()
        {
            this.done = true;

            if (!this.frozen) {
                this.expand();
            }
        },

        expand : function()
        {
            this.done = false;
            this.state.trigger('setNodePath', this.pathString);
        }
    });
});
