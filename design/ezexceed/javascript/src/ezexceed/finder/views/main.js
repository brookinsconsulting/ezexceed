/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/containerview', 'shared/node', '../state', './breadcrumbs', './actions', './columns', 'funky', 'module', 'ezexceed/finder/templates/main'],
    function(View, Node, State, Breadcrumbs, Actions, Columns, Funky, Module, Template)
{
    /**
     * @class Finder.Main
     */
    return View.extend(
    {
        root : null,
        /** @type Finder.State */
        state : null,
        views : null,
        options : null,
        height : 0,
        dNd : true,
        fetch : false,
        nodeCache : null,
        rootNodeId : null,
        dimensions : null,
        rendered : false,
        containersOnly : false,

        name : "sitemap",

        initialize : function(options)
        {
            _.bindAll(this);

            this.state = new State();

            this.state.on('rootChanged', this.rootChanged);

            _.extend(this, _.pick(options, ['height', 'rootNodeId', 'root', 'options', 'containersOnly']));

            this.views = {};

            if (this.nodeCache === null) {
                this.nodeCache = new Node.collection();
            }

            if (this.rootNodeId === null) {
                this.rootNodeId = Module.config().rootNodeId;
                this.options.rootNodeId = this.rootNodeId;
            }

            if (this.root === null) {
                this.root = this.getRootNode();
            }

            this.root.on('fetched', this.triggerLoadedAndRemove, this);

            this.$el.html(this._loader({size : 32, className :'icon-32'}));

            this.on('destruct', this.remove);

            this.init();
        },

        // This triggers an event that propagates back to the Toolbar
        // that can disable its pulsing indicating
        // that the Sitemap is still being loaded
        triggerLoadedAndRemove : function()
        {
            this.trigger('loaded');
            this.root.off('fetched', this.triggerLoadedAndRemove, this);
        },

        resize : function(dimensions)
        {
            this.dimensions = dimensions;

            if (this.rendered) {
                this.$el.css(dimensions);

                var columnDimensions = {
                    width : dimensions.width - this.views.actions.$el.outerWidth(true),
                    height : dimensions.height - this.views.active.$el.outerHeight(true)
                };

                this.views.columns.setDimensions(columnDimensions);
            }
        },

        unFreeze : function()
        {
            this.listenTo(this, {
                'resize' : this.resize,
                'stacked': this.render
            });

            this.listenTo(this.state, {
                'error' : this.renderError,
                'marked:edit' : this.editObject
            });

            this.state.unFreeze();
        },

        freeze : function()
        {
            this.stopListening();
            this.state.freeze();
        },

        editObject : function()
        {
            this.trigger('freeze');
            var model = this.state.objects.marked;

            var options = {id : model.get('objectId')};


            var context = {
                heading : {
                    text : model.get('name'),
                    quotes : true
                },
                classId : model.get('classId')
            };

            eZExceed.trigger('finder:edit', {
                id: options.id,
                name : context.heading.text
            });

            var onDestruct = this.editCompleted;

            require(['ezexceed/edit'], function(Edit)
            {
                eZExceed.stack.push(Edit.view, options, context).on('destruct', onDestruct);
            });
        },

        editCompleted : function(model, closeAll)
        {
            /** No need to update sitemap if it is going to be destroyed shortly after. */
            if (closeAll) {
                return;
            }

            var editNode = this.state.objects.marked;
            var locations = model.get('locations');

            locations.each(function(location)
            {
                if (location.get('childrenCount') < 100) {
                    this.nodeCache.remove(location);
                }

                if (location.get('parent').childrenCount < 100) {
                    this.nodeCache.remove(location.get('parentId'));
                }

            }, this);

            if (!locations.get(editNode.id)) {
                editNode = locations.find(Funky.truthyProp('isMain', 'get'));

                if (editNode) {
                    this.nodeCache.reset();
                }
            }

            if (editNode) {
                this.options.nodeIdPath = editNode.get('pathString');
                this.rootChanged(this.root.id);
            }
        },

        getRootNode : function()
        {
            var node = this.nodeCache.get(this.rootNodeId);

            if (!node) {
                node = new Node.model({
                    id : this.rootNodeId,
                    nodeCache : this.nodeCache,
                    containersOnly : this.containersOnly,
                    /** A root-node should always be a container. */
                    isContainer : true
                });
            }

            return node;
        },

        rootChanged : function(rootNodeId)
        {
            this.options.rootNodeId = rootNodeId;

            _(this.views).invoke('remove');
            this.root = null;
            this.frozen = true;
            this.initialize(this.options);

            this.render();
            this.views.columns.refreshScrollPosition();
        },

        remove : function()
        {
            View.prototype.remove.apply(this, arguments);
            this.nodeCache.reset();
        },

        render : function()
        {
            this.$el
                .css('overflow', 'hidden')
                .html(Template({}));

            this.views.active = new Breadcrumbs({
                el : this.$('.breadcrumbs'),
                state : this.state,
                node : this.root
            });

            // Action view
            this.views.actions = this._actionsView();

            this.$('.kp-columns-wrapper').css({
                'max-width' : this.$el.outerWidth() - this.views.actions.$el.outerWidth()
            });

            this.views.columns = new Columns({
                state : this.state,
                el : this.$('.kp-columns'),
                dNd : this.dNd,
                finderOptions : this.options,
                rootNodeId : this.rootNodeId
            });

            if (this.root) {
                this.state.mark(this.root);
                this.views.columns.insertNode(this.root, 0, false);
            }

            this.renderColumns();

            return this;
        },

        renderColumns : function()
        {
            this.views.columns.render();
            this.rendered = true;
            this.resize(this.dimensions);
            this.views.columns.refreshScrollPosition();
        },

        renderError : function(error)
        {
            if (error.msg) {
                this.$('.kp-finder-content').before('<div class="error">' + error.msg + '</div>');
                _.delay(this.clearErrors, 1500);
            }
        },

        clearErrors : function()
        {
            var errors = this.$('.error');
            errors.fadeOut(250, errors.remove);
        },

        _actionsView : function()
        {
            var options = {
                el : this.$('.actions'),
                state : this.state,
                dimensions : this.dimensions
            };

            _.extend(options, _.pick(this.options, ['actions', 'customActions']));

            return new Actions(options);
        }
    });
});
