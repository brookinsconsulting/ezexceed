/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['shared/view', 'module', 'jquery-safe', 'plupload/plupload', 'ezexceed/finder/templates/multiupload'],
    function(View, Module, $, Plupload, Template)
    {
    /**
     * @class Finder.MultiUpload
     */
    return View.extend(
    {
        className : 'upload',
        button : 'multiupload',
        uploader : null,
        uploadContainer : 'upload',
        uploadUrl : '/device/imageUpload/',
        state : null,

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['state']));

            this.button += this.model.id;
            this.uploadContainer += this.model.id;
        },

        remove : function()
        {
            if (this.uploader) {
                this.uploader.destroy();
                this.uploader = null;
            }

            View.prototype.remove.apply(this, arguments);
        },

        enableUpload : function()
        {
            /** View is detached and plupload would crash if initialized. */
            if (!$.contains(document.documentElement, this.el)) {
                return;
            }

            var uploadOptions = {
                browse_button : this.button,
                container : this.uploadContainer,
                file_data_name : 'image',
                url : this.uploadUrl,
                multipart_params : {
                    parentNodeId : this.model.id
                }
            };

            _.defaults(uploadOptions, Module.config().plupload);

            /** Ugly hack to go with ezformtoken. */
            if (eZExceed.formtoken) {
                uploadOptions.multipart_params.ezxform_token = eZExceed.formtoken;
            }

            this.uploader = new Plupload.Uploader(uploadOptions);

            this.uploader.init();
            this.uploader.bind('FileUploaded', this.fileUploaded);
            this.uploader.bind('UploadComplete', this.uploadComplete);
            this.uploader.bind('FilesAdded', this.filesAdded);
        },

        filesAdded : function()
        {
            var statusContext = {
                size : 16,
                className : 'icon-16',
                statusText : ['Uploading', {text : '…'}]
            };

            this.$('button').addClass('uploading').html(this._loader(statusContext));

            this.uploader.start();
        },

        fileUploaded : function(up, file, info)
        {
            /** Plupload doesn't care about response-format. */
            var response = $.parseJSON(info.response);

            if (!response.ok) {
                this.state.setError(response.message, this.model);
            }
        },

        uploadComplete : function()
        {
            this.model.on('fetched', this.expandPath);
            this.model.load({clearCache : true});
            this.render();
        },

        expandPath : function()
        {
            this.model.off('fetched', this.expandPath);
            this.state.mark(this.model);
            this.state.trigger('setNodePath', this.model.get('pathString'), true);
        },

        render : function()
        {
            this.$el.html(Template({id : this.button}));
            this.$el.attr('id', this.uploadContainer);

            this.enableUpload();

            return this;
        }
    });
});
