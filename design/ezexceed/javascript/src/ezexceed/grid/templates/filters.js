define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n            <option value=\"";
  if (stack1 = helpers.key) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.key; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.value, options) : helperMissing.call(depth0, "translate", depth0.value, options)))
    + "</option>\n            ";
  return buffer;
  }

  buffer += "<form class=\"form-search\">\n    <div class=\"input-prepend\">\n        <span class=\"add-on\"><i class=\"icon-search\"></i></span>\n        <input name=\"q\" type=\"text\" placeholder=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Type to search", options) : helperMissing.call(depth0, "translate", "Type to search", options)))
    + "\" class=\"input-medium\">\n    </div>\n\n    <div class=\"eze-filters\">\n        <p class=\"description\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Last edit date", options) : helperMissing.call(depth0, "translate", "Last edit date", options)))
    + "</p>\n        <select class=\"chzn-select date-facet\" name=\"date\">\n            <option value=\"\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "All time", options) : helperMissing.call(depth0, "translate", "All time", options)))
    + "</option>\n            ";
  stack2 = helpers.each.call(depth0, depth0.periods, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n        </select>\n\n        <p class=\"description\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Content type", options) : helperMissing.call(depth0, "translate", "Content type", options)))
    + "</p>\n        <select class=\"type-facet\" name=\"type\"></select>\n\n        <div class=\"eze-facet-author\">\n            <p class=\"description\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "By author", options) : helperMissing.call(depth0, "translate", "By author", options)))
    + "</p>\n            <select class=\"author-facet\" name=\"author\" disabled=\"disabled\">\n                <option value=\"\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "All authors", options) : helperMissing.call(depth0, "translate", "All authors", options)))
    + "</option>\n            </select>\n        </div>\n    </div>\n</form>\n";
  return buffer;
  })

});