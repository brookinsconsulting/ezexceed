define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n<div class=\"thumb\" style=\"background:url("
    + escapeExpression(((stack1 = ((stack1 = depth0.images),stack1 == null || stack1 === false ? stack1 : stack1.thumb)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + ") center center no-repeat;background-size:cover\">\n";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n<div class=\"thumb\">\n    <img class=\"class-icon icon-48\" src=\"";
  options = {hash:{
    'id': (depth0.classId),
    'size': (48)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\">\n";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n        <a href=\"#";
  if (stack1 = helpers.id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + ":";
  if (stack1 = helpers.version) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.version; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" class=\"edit\">\n    ";
  return buffer;
  }

function program7(depth0,data) {
  
  
  return "\n        <span class=\"edit\">\n    ";
  }

function program9(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n        ";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n    ";
  return buffer;
  }

function program11(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "No name", options) : helperMissing.call(depth0, "translate", "No name", options)))
    + "\n    ";
  return buffer;
  }

function program13(depth0,data) {
  
  
  return "\n        </a>\n    ";
  }

function program15(depth0,data) {
  
  
  return "\n        </span>\n    ";
  }

function program17(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n    ";
  stack1 = helpers['if'].call(depth0, depth0.published, {hash:{},inverse:self.program(21, program21, data),fn:self.program(18, program18, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n    <br>\n";
  return buffer;
  }
function program18(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n        <span class=\"label label-success\" title=\"#";
  if (stack1 = helpers.version) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.version; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Published", options) : helperMissing.call(depth0, "translate", "Published", options)))
    + "</span>\n        ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.dateFormat),stack1 ? stack1.call(depth0, depth0.published, options) : helperMissing.call(depth0, "dateFormat", depth0.published, options)))
    + " ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "by", options) : helperMissing.call(depth0, "translate", "by", options)))
    + " "
    + escapeExpression(((stack1 = ((stack1 = depth0.publishedBy),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\n        ";
  stack2 = helpers['if'].call(depth0, depth0.changed, {hash:{},inverse:self.noop,fn:self.program(19, program19, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    ";
  return buffer;
  }
function program19(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n            <span class=\"label label-info\" title=\"#";
  if (stack1 = helpers.changedVersion) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.changedVersion; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "I changed", options) : helperMissing.call(depth0, "translate", "I changed", options)))
    + "</span>\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.dateFormat),stack1 ? stack1.call(depth0, depth0.changed, options) : helperMissing.call(depth0, "dateFormat", depth0.changed, options)))
    + "\n        ";
  return buffer;
  }

function program21(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n        <span class=\"label label-important\" title=\"#";
  if (stack1 = helpers.version) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.version; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Never published", options) : helperMissing.call(depth0, "translate", "Never published", options)))
    + "</span>\n    ";
  return buffer;
  }

function program23(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += ", ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "published on", options) : helperMissing.call(depth0, "translate", "published on", options)))
    + " <a href=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.loc),stack1 == null || stack1 === false ? stack1 : stack1.url)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" title=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Go to", options) : helperMissing.call(depth0, "translate", "Go to", options)))
    + " &laquo;"
    + escapeExpression(((stack1 = ((stack1 = depth0.loc),stack1 == null || stack1 === false ? stack1 : stack1.pathFormatted)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "&raquo;\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.loc),stack1 == null || stack1 === false ? stack1 : stack1.name)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</a>\n        ";
  stack2 = helpers['if'].call(depth0, depth0.multipleLocations, {hash:{},inverse:self.noop,fn:self.program(24, program24, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    ";
  return buffer;
  }
function program24(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n        ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "and", options) : helperMissing.call(depth0, "translate", "and", options)))
    + " ";
  if (stack2 = helpers.multipleLocations) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.multipleLocations; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + " ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, depth0.otherLocationsString, options) : helperMissing.call(depth0, "translate", depth0.otherLocationsString, options)))
    + "</span>\n        ";
  return buffer;
  }

  buffer += "<label class=\"check\"><input type=\"checkbox\" class=\"select\"></label>\n";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.images),stack1 == null || stack1 === false ? stack1 : stack1.thumb), {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</div>\n\n<h2>\n    ";
  stack2 = helpers['if'].call(depth0, depth0.canEdit, {hash:{},inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n    ";
  stack2 = helpers['if'].call(depth0, depth0.hasName, {hash:{},inverse:self.program(11, program11, data),fn:self.program(9, program9, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n    ";
  stack2 = helpers['if'].call(depth0, depth0.canEdit, {hash:{},inverse:self.program(15, program15, data),fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n</h2>\n<p>\n\n";
  stack2 = helpers.unless.call(depth0, depth0.deleted, {hash:{},inverse:self.noop,fn:self.program(17, program17, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n    <img src=\"";
  options = {hash:{
    'id': (depth0.classId),
    'size': (16)
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.classIcon),stack1 ? stack1.call(depth0, options) : helperMissing.call(depth0, "classIcon", options)))
    + "\" alt=\"";
  if (stack2 = helpers.type) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.type; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"icon-16\">\n    ";
  if (stack2 = helpers.type) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.type; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2);
  stack2 = helpers['if'].call(depth0, depth0.loc, {hash:{},inverse:self.noop,fn:self.program(23, program23, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n</p>\n";
  return buffer;
  })

});