define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n            <a class=\"restore\">\n                ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Restore selected", options) : helperMissing.call(depth0, "translate", "Restore selected", options)))
    + "\n            </a>\n        ";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n            <a class=\"publish\">\n                ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Publish selected", options) : helperMissing.call(depth0, "translate", "Publish selected", options)))
    + "\n            </a>\n        ";
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                    ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Permanently remove selected", options) : helperMissing.call(depth0, "translate", "Permanently remove selected", options)))
    + "\n                ";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", stack1;
  buffer += "\n                    ";
  stack1 = helpers['if'].call(depth0, depth0.trashEnabled, {hash:{},inverse:self.program(10, program10, data),fn:self.program(8, program8, data),data:data});
  if(stack1 || stack1 === 0) { buffer += stack1; }
  buffer += "\n                ";
  return buffer;
  }
function program8(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                    ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Trash selected", options) : helperMissing.call(depth0, "translate", "Trash selected", options)))
    + "\n                    ";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n                    ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Permanently remove selected", options) : helperMissing.call(depth0, "translate", "Permanently remove selected", options)))
    + "\n                    ";
  return buffer;
  }

  buffer += "<div class=\"btn-group pull-left\">\n    <a class=\"btn dropdown-toggle disabled\" data-toggle=\"dropdown\">\n        ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Batch operations", options) : helperMissing.call(depth0, "translate", "Batch operations", options)))
    + "\n        <span class=\"caret\"></span>\n    </a>\n    <ul class=\"dropdown-menu\">\n        <li>\n        ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.toggles),stack1 == null || stack1 === false ? stack1 : stack1.trash), {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n        </li>\n\n        <li>\n            <a class=\"remove\">\n                ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.toggles),stack1 == null || stack1 === false ? stack1 : stack1.trash), {hash:{},inverse:self.program(7, program7, data),fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n            </a>\n        </li>\n    </ul>\n</div>\n";
  return buffer;
  })

});