define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, stack2, options, self=this, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;

function program1(depth0,data) {
  
  
  return "active";
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n        <button type=\"button\"\n            class=\"btn ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.toggles),stack1 == null || stack1 === false ? stack1 : stack1.trash), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"\n            data-trash=1\n            data-mine=0>\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Trash", options) : helperMissing.call(depth0, "translate", "Trash", options)))
    + "\n        </button>\n        ";
  return buffer;
  }

  buffer += "<div class=\"sort-by\">\n    <h2>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Sort by", options) : helperMissing.call(depth0, "translate", "Sort by", options)))
    + "</h2>\n    <div class=\"btn-group\">\n        <button data-sort=\"date\"\n            class=\"btn ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.sortby),stack1 == null || stack1 === false ? stack1 : stack1.date), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"\n            type=\"button\">\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Date", options) : helperMissing.call(depth0, "translate", "Date", options)))
    + "\n        </button>\n\n        <button data-sort=\"author\"\n            class=\"btn ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.sortby),stack1 == null || stack1 === false ? stack1 : stack1.author), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"\n            type=\"button\">\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Author", options) : helperMissing.call(depth0, "translate", "Author", options)))
    + "\n        </button>\n\n        <button data-sort=\"type\"\n            class=\"btn ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.sortby),stack1 == null || stack1 === false ? stack1 : stack1.type), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"\n            type=\"button\">\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Content type", options) : helperMissing.call(depth0, "translate", "Content type", options)))
    + "\n        </button>\n    </div>\n</div>\n\n<div class=\"show-content-toggle\">\n    <h2>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Show", options) : helperMissing.call(depth0, "translate", "Show", options)))
    + "</h2>\n    <div class=\"btn-group show\" data-toggle=\"buttons-radio\">\n        <button type=\"button\"\n            class=\"btn ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.toggles),stack1 == null || stack1 === false ? stack1 : stack1.published), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"\n            data-mine=0>\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "Published", options) : helperMissing.call(depth0, "translate", "Published", options)))
    + "\n        </button>\n\n        <button type=\"button\"\n            class=\"btn ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.toggles),stack1 == null || stack1 === false ? stack1 : stack1.mine), {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\"\n            data-mine=1>\n            ";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "My drafts", options) : helperMissing.call(depth0, "translate", "My drafts", options)))
    + "\n        </button>\n\n        ";
  stack2 = helpers['if'].call(depth0, depth0.trashEnabled, {hash:{},inverse:self.noop,fn:self.program(3, program3, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n    </div>\n</div>\n";
  return buffer;
  })

});