define(['handlebars'], function(Handlebars) {

return Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [2,'>= 1.0.0-rc.3'];
helpers = helpers || Handlebars.helpers; data = data || {};
  var buffer = "", stack1, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;


  buffer += "<section class=\"eze-table\">\n    <h1>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.translate),stack1 ? stack1.call(depth0, "All content", options) : helperMissing.call(depth0, "translate", "All content", options)))
    + " </h1>\n\n    <section class=\"eze-table-actions\">\n        <input type=\"checkbox\" class=\"check-all\">\n        <div class=\"batch\"></div>\n        <div class=\"table-toggles\"></div>\n    </section>\n\n    <section class=\"eze-filters-wrap\"></section>\n\n    <section class=\"eze-table-content\">\n    </section>\n</section>\n";
  return buffer;
  })

});