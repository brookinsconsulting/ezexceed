/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['module', 'shared/modal', 'shared/contentobject', 'shared/view', './list',
    './filter', './actions', './toggles', 'ezexceed/addcontent/widget', 'ezexceed/grid/templates/main',
    'jquerypp/form_params'],
    function(Module, Modal, Content, View, ListView, FilterView, ActionsView, TogglesView, CreateWidget, Template)
{
    return View.extend({
        facets : null,
        classConstraints : null,

        options : null,
        actions : null,

        name : 'find',

        createWidget : null,

        initialize : function(options)
        {
            _.bindAll(this);

            this.config = Module.config();
            this.classes = this.config.classes;
            this.nodeId = this.config.rootNodeId;

            if (options.hasOwnProperty('nodeId')) {
                this.nodeId = options.nodeId;
            }

            _.defaults(this.options, {
                actions : [ActionsView],
                list : {}
            });

            _.extend(this, _.pick(options, ['classConstraints']));

            this.collection = new Content.collection();
            this.collection.url = '/Object/find';
            this.listenTo(this.collection, {
                destroy: this.objectRemoved
            });

            this.$el.html(this._loader({wrap: true, className:'icon-32',size:32}));

            this.filters = new FilterView({
                collection : this.collection,
                constrain : this.classConstraints,
                classes : this.classes
            });

            this.on({
                'freeze' : this.freeze,
                'unfreeze' : this.unfreeze,
                'destruct': this.remove
            }).unfreeze();

            return this;
        },

        unfreeze : function()
        {
            this.on('stacked', this.renderContent, this);
            return this.triggerOnViews('unfreeze');
        },

        freeze : function()
        {
            this.off('stacked', null, this);
            return this.triggerOnViews('freeze');
        },

        triggerOnViews : function(trigger)
        {
            var views = ['filters', 'createWidget', 'list'];
            _.each(views, function(view)
            {
                if (this.hasOwnProperty(view) && this[view]) {
                    this[view].trigger(trigger);
                }
            }, this);
            return this;
        },

        events : {
            'click .check-all' : 'toggleAll'
        },

        toggleAll : function(e)
        {
            this.collection.invoke('set', 'selected', e.currentTarget.checked);
        },

        render : function()
        {
            this.$el.html(Template({}));

            this.renderList()
                .renderActions()
                .renderToggles()
                .renderFilters()
                .renderContent()
                .renderCreateNew();

            this.$('.form-search input[name="q"]').focus();

            this.trigger('loaded');

            return this;
        },

        renderCreateNew : function()
        {
            this.createWidget = new CreateWidget({
                nodeId : this.nodeId
            }).render();

            // Once model is created and edit view rendered,
            // close the add content popover, add the model to our collection
            // and render the item in the background
            this.listenTo(this.createWidget, {
                'edit:done' : this.onObjectCreated
            });

            this.$('h1').append(this.createWidget.$el);
            return this;
        },

        renderFilters : function()
        {
            this.filters
                .setElement(this.$('.eze-filters-wrap'))
                .render()
                .search();

            return this;
        },

        renderToggles : function()
        {
            if (!this.toggles) {
                this.toggles = new TogglesView({
                    options : this.options.list,
                    collection : this.collection,
                    classes : this.classes,
                    el : this.$('.table-toggles')
                });
            }
            this.toggles.render();
            return this;
        },

        renderActions : function()
        {
            if (!this.actions) {
                this.actions = _.map(this.options.actions, function(actionView)
                {
                    return new actionView({
                        filters : this.filters,
                        collection : this.collection,
                        classes : this.classes
                    }).render();
                }, this);
                this.$('.eze-table-actions .batch').append(_.pluck(this.actions, 'el'));
            }
            else {
                _.invoke(this.actions, 'render');
            }
            return this;
        },

        renderList : function()
        {
            if (!this.list) {
                this.list = new ListView({
                    options : this.options.list,
                    collection : this.collection,
                    classes : this.classes,
                    el : this.$('.eze-table-content')
                });
            }
            this.list.render();
            return this;
        },

        renderContent : function()
        {
            if (this.list) {
                this.list.unfreeze();
            }
            return this;
        },

        // Callback after the create-new-widget detects Edit is closed
        // and we can render the object into the list
        onObjectCreated : function(model)
        {
            if (this.filters.mine) {
                this.collection.add(model, {at: 0});
            }
            this.collection.clearCache();
        },

        /**
         * After an object is removed, this is triggered
         * @param model
         */
        objectRemoved : function(model)
        {
            if (model.id == eZExceed.config.currentObjectId) {
                eZExceed.currentNodeChanges.removed = true;
            }
        },

        remove : function()
        {
            if (this.createWidget) {
                this.createWidget.remove();
            }

            this.stopListening();
            View.prototype.remove.apply(this);
        }
    });
});
