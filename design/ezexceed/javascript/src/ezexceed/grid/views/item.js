/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['shared/view', 'ezexceed/grid/templates/item'], function(View, Template)
{
    return View.extend({
        tagName : 'li',
        className : 'item clearfix',

        initialize : function(options)
        {
            _.bindAll(this);

            _.extend(this, _.pick(options, ['tpl']));

            this.listenTo(this.model, {
                'change:selected' : this.toggleChecked,
                'destroy' : this.removing,
                'publish' : this.publishing,
                'restore' : this.restoring,
                'restored' : this.remove,
                'remove' : this.removing,
                'change:name change:published change:modified published' : this.render
            });
        },

        events : {
            'click a.edit' : function(e)
            {
                e.preventDefault();
                this.$('h2').append(
                    this._loader({
                        size : 16,
                        className : 'icon-16'
                    })
                );
                require(['ezexceed/edit'], this.edit);
            },
            'click .thumb' : 'toggleState',
            'click .select' : 'toggleState'
        },

        toggleState : function()
        {
            this.model.set('selected', !this.model.get('selected'));
        },

        toggleChecked : function(model, state)
        {
            this.$('.select').attr('checked', state);
        },

        publishing : function()
        {
            this.$el.addClass('publishing');
        },

        restoring : function()
        {
            this.$el.addClass('restoring');
        },

        removing : function()
        {
            this.$el.addClass('removing');
            this.model.set('selected', false);
            _.delay(this.removeFromDOM, this.animationTime);
        },

        // wrapping `remove` so its possible to check if my group
        // is empty
        removeFromDOM : function()
        {
            var group = this.$el.parent();
            this.remove();
            if (group.children().length === 0)
                group.parent().remove();
        },

        render : function()
        {
            this.$el.removeClass('publishing removing');

            this.model.set('selected', false);

            var contentClass = this.model.get('class');
            var type = contentClass ? contentClass.get('name') : 'Unknown';
            var classId = contentClass ? contentClass.id : false;

            var locations = this.model.get('locationsummary');

            var loc = false;
            var multipleLocations = false;
            var otherLocationsString = '';

            if (locations && locations.length > 0) {
                loc = locations[0];
                multipleLocations = loc.total > 1 ? loc.total - 1 : false;
                otherLocationsString = 'other location';
                if (multipleLocations > 1) otherLocationsString += 's';
            }

            var changed = this.model.get('changed');
            var changedVersion = false;
            var changedVersions = this.model.get('versionsChanged');

            if (changedVersions.length) {
                _(changedVersions).each(function(version){
                    if (version.modified > changed) {
                        changedVersion = version.version;
                    }
                });
            }

            var name = this.model.get('name');
            var context = _.defaults({
                name : name,
                hasName : name.length > 0,
                classId : classId,
                loc : loc,
                multipleLocations : multipleLocations,
                otherLocationsString : otherLocationsString,
                type : type,
                changed : changed,
                changedVersion : changedVersion
            }, this.model.toJSON());

            this.$el.html(Template(context));
            return this;
        },

        edit : function(Edit)
        {
            var context = {
                heading : {
                    text : this.model.get('name'),
                    quotes : true
                },
                classId : this.model.get('class').id
            };

            eZExceed.trigger('find:edit', {
                id: this.model.id,
                name : this.model.get('name')
            });

            eZExceed.stack.push(
                Edit.view,
                {id : this.model.id},
                context
            ).once('destruct', this.update);
        },

        update : function()
        {
            this.model._url = false;
            this.model._url = this.model.url('get');

            this.model.once('fetched', this.render).fetch();
        }
    });
});
