/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'ezexceed/grid/templates/toggles'], function(View, Template)
{
    return View.extend({
        classes : null,
        $buttons : null,

        sort : 'date',

        toggles : {
            trash : false,
            mine : false,
            published : true
        },

        initialize : function()
        {
            _.bindAll(this);
        },

        events : {
            'click .sort-by button' : 'onSort',
            'click .show button' : 'toggleShow'
        },

        toggleShow : function(e)
        {
            this.$('.show .active').removeClass('active');

            this.toggles = this.$(e.currentTarget).addClass('active').data();

            eZExceed.trigger('find:show', {
                toggles: this.toggles
            });
        },

        onSort : function(e)
        {
            this.$('.sort-by .active').removeClass('active');
            this.sort = this.$(e.currentTarget).addClass('active').data('sort');
            eZExceed.trigger('find:sort', {
                sort: this.sort
            });
        },

        render : function()
        {
            var active = this.getActive();
            var sortby = this.getSortBy();
            var context = {
                trashEnabled : eZExceed.config.trashEnabled,
                sortby : {
                    date : sortby === 'date',
                    author : sortby === 'author',
                    type : sortby === 'type'
                },
                toggles : {
                    published : active === 'published',
                    trash : active === 'trash',
                    mine : active === 'mine'
                }
            };
            this.$el.html(Template(context));
            this.$buttons = this.$('button');
            return this;
        },

        getActive : function()
        {
            if (this.toggles.trash) {
                return 'trash';
            }
            if (this.toggles.mine) {
                return 'mine';
            }
            return 'published';
        },

        getSortBy : function()
        {
            return this.sort;
        }
    });
});
