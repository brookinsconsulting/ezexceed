/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
define(['module', 'shared/view', './item', 'moment', 'jquery-safe', 'ezexceed/grid/templates/group', 'ezexceed/grid/templates/empty', 'ezexceed/grid/templates/loading', 'shared/templates/loader'],
    function(Module, View, ItemView, moment, $, GroupTemplate, EmptyTemplate, Loader, StandardLoader)
{
    return View.extend({
        itemsRendered : 0,

        options : null,

        frozen : true,

        xhr : null,

        initialize : function(options)
        {
            _.bindAll(this);
            _.defaults(this.options, options.options, {
                view : ItemView,
                item : {},
                selected : []
            });
            this.onAdd = _.debounce(this.onAdd, 2);

            return this.on({
                'freeze': this.freeze,
                'unfreeze': this.unfreeze
            });
        },

        unfreeze : function()
        {
            if (!this.frozen) {
                return;
            }

            this.listenTo(this.collection, {
                'reset': this.renderItems,
                'loading': this.renderLoader,
                'search': this.resetScrollTop
            });
            if (this.collection.length > 0)
                this.renderItems();

            this.frozen = false;

            return this;
        },

        freeze : function()
        {
            this.frozen = true;
            this.stopListening(this.collection);
            return this;
        },

        events : {
            'scroll' : 'pager'
        },

        pager : function()
        {
            if (this.collection.length < this.collection.total) {
                var height = this.$el.height();
                var distance = this.$('.eze-inner').height() - (height + this.$el.scrollTop());
                if (distance < 100) {
                    this.renderLoader({tpl: Loader});

                    if (this.xhr === null) {
                        this.xhr = this.collection.more().done(this.onAdd);
                    }
                }
            }
        },

        resetScrollTop : function()
        {
            this.$el.scrollTop(0);
        },

        onAdd : function()
        {
            this.xhr = null;
            var html = false;
            if (this.collection.length > 0) {
                // Group collection entries
                var groups = {};
                this.collection.chain()
                    .rest(this.itemsRendered)
                    .each(function(model)
                    {
                        var group = this.getGroup(model);
                        var item = this.renderItem(model);

                        if (group in groups) groups[group].push(item);
                        else groups[group] = [item];
                    }, this);

                html = _.map(groups, function(items, heading)
                {
                    var group = $(GroupTemplate({
                        heading : heading
                    }));
                    group.find('.items').html(items);
                    return group[0];
                }, this);

                this.itemsRendered = this.collection.length;
                this.$('.eze-loader').remove();
            }

            if (html) {
                this.$inner.append(html);
            }
        },

        render : function()
        {
            this.$el.html('<div class="eze-inner"></div>');

            this.$inner = this.$('.eze-inner');
            this.renderLoader();

            if (this.collection.length > 0)
                this.renderItems();
            return this;
        },

        renderItems : function()
        {
            this.$('.eze-loader').remove();
            this.itemsRendered = 0;
            this.$inner.empty();
            if (this.collection.length === 0)
                this.$inner.html(EmptyTemplate({}));
            else
                this.onAdd();
        },

        renderItem : function(model)
        {
            var options = _.extend(this.options.item, {
                selected : _.contains(this.options.selected, model.id),
                collection : this.collection,
                model : model
            });
            var view = new this.options.view(options);
            return view.render().el;
        },

        renderLoader : function(config)
        {
            config = config || ({});
            _.defaults(config, {
                tpl: StandardLoader,
                wrap: true,
                size: 32,
                className : 'icon-32'
            });
            var template = config.tpl;
            delete config.tpl;
            var loader = this.$('.eze-loader');
            if (loader.length === 0) {
                this.$el.append(template(config));
            }
        },

        getGroup : function(model)
        {
            var group;
            if (this.collection.sortBy === 'date') {
                var field = this.collection.toggles.mine ? 'changed' : 'modified';
                return moment(model.get(field) * 1000).fromNow();
            }
            else if (this.collection.sortBy === 'type') {
                group = model.get('class').get('name');
            }
            else if (this.collection.sortBy === 'author') {
                group = model.get('createdBy');
            }
            return group;
        }
    });
});
