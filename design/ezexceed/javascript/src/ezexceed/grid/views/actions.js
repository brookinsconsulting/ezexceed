/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'shared/buttondropdown', 'ezexceed/grid/templates/actions'], function(View, ButtonDropdown, Template)
{
    return View.extend({
        classes : null,
        filters : null,

        toggles : {
            trash : false,
            mine : false,
            published : true
        },

        initialize : function(options)
        {
            _.bindAll(this);
            _.extend(this, _.pick(options, ['classes', 'filters']));
            eZExceed.on('find:show', this.update);
            this.collection.on({
                'change:selected' : this.toggleChecked,
                'reset' : this.render
            });
        },

        events : {
            'click .publish' : 'onPublish',
            'click .restore' : 'onRestore',
            'click .remove' : 'onRemove'
        },

        update : function(data)
        {
            this.toggles = _.extend(this.toggles, data.toggles);
            this.render();
        },

        onPublish : function()
        {
            var selection = this.collection.where({selected:true});
            _.invoke(selection, 'publish');
            eZExceed.trigger('find:publish', {
                count : selection.length
            });
            this.collection.clearCache();
        },

        onRestore : function()
        {
            var selection = this.collection.where({selected:true});
            _.invoke(selection, 'restore');
            eZExceed.trigger('find:restore', {
                count : selection.length
            });
            this.collection.clearCache();
        },

        onRemove : function()
        {
            var selection = this.collection.where({selected:true});
            _.invoke(selection, 'remove', this.toggles.trash);
            eZExceed.trigger('find:remove', {
                count : selection.length,
                permanent : this.toggles.trash
            });
            this.collection.clearCache();
        },

        render : function()
        {
            var active = this.getActive();
            var context = {
                trashEnabled : eZExceed.config.trashEnabled,
                toggles : {
                    published : active === 'published',
                    trash : active === 'trash',
                    mine : active === 'mine'
                }
            };
            this.$el.html(Template(context));
            this.dropdown = new ButtonDropdown({el : this.$('.btn-group')});

            return this;
        },

        toggleChecked : function()
        {
            if (this.collection.where({selected:true}).length > 0) {
                this.dropdown.enable();
            }
            else {
                this.dropdown.disable();
            }
            return this;
        },

        getActive : function()
        {
            if (this.toggles.trash) {
                return 'trash';
            }
            if (this.toggles.mine) {
                return 'mine';
            }
            return 'published';
        }
    });
});
