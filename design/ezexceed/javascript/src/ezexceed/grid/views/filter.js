/**
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

define(['shared/view', 'backbone', 'selectable', 'ezexceed/grid/templates/author', 'ezexceed/grid/templates/classes', 'ezexceed/grid/templates/filters'],
    function(View, Backbone, selectable, AuthorTemplate, ClassesTemplate, FiltersTemplate)
{
    var Authors = Backbone.Collection.extend(
    {
        url : '/Object/authors',

        parse : function(data)
        {
            return data.authors;
        }
    });

    var Statistics = Backbone.Collection.extend(
    {
        url : 'Object/statistics'
    });

    return View.extend({
        sort : 'date',
        mine : false,
        trash : false,
        xhr : null,
        lastSearch : null,

        authors : null,
        statistics : null,
        $authors: null,
        $types : null,
        deferredSearch : null,

        initialize : function(options)
        {
            _.bindAll(this);
            _.extend(this, _.pick(options, ['classes', 'constrain']));

            this.authors = new Authors();
            this.listenTo(this.authors, {
                reset: this.renderAuthors
            });
            this.authors.fetch().done(this.renderAuthors);

            this.statistics = new Statistics();
            this.listenTo(this.statistics, {
                reset: this.renderClasses
            });
            this.statistics.fetch().done(this.renderClasses);

            this.deferredSearch = _.debounce(this.search, 250);

            this.listenTo(this.collection, {
                'published' : this.onPublished
            });

            eZExceed.on({
                'find:show': this.onToggle,
                'find:sort': this.onSort
            });
        },

        events : {
            'submit .form-search' : 'search',
            'change .form-search select' : 'search',
            'keyup .form-search input' : 'deferredSearch'
        },

        onPublished : function(model)
        {
            if (this.mine) {
                this.collection.remove(model);
            }
        },

        onToggle : function(data)
        {
            var toggles = data.toggles;
            _.defaults(toggles, {
                mine: false,
                trash: false 
            });
            this.mine = toggles.mine;
            this.trash = toggles.trash;

            if (toggles.mine) {
                this.hide(this.$authors);
            }
            else {
                this.show(this.$authors);
            }
            this.search();
            return this;
        },

        onSort : function(data)
        {
            this.sort = data.sort;
            this.collection.sortBy = this.sort;
            this.search();
        },

        search : function()
        {
            var data = this.$('.form-search').formParams();

            _.each(data, function(value, key)
            {
                if (value === '' || value === null) {
                    delete data[key];
                }
                else {
                    data[key] = value.replace(/^\s+|\s+$/g, '');
                }
            });

            // If no specific type is set, set every allowed type
            if (!(data.hasOwnProperty('type'))) {
                data.type = _.pluck(this.allowedTypes, 'key');
            }

            // Relay filter values from self to xhr data
            var modes = _.pick(this, ['sort', 'mine', 'trash']);
            _.extend(data, modes);

            if (!_.isEqual(data, this.lastSearch)) {
                this.collection.trigger('loading');
                this.collection.toggles = {
                    mine: this.mine,
                    trash: this.trash
                };

                /** Might need to hide object-count. */
                if (this.lastSearch !== null && (this.lastSearch.mine != data.mine || this.lastSearch.trash != data.trash)) {
                    this.renderClasses();
                }

                this.lastSearch = data;
                if (this.xhr) {
                    this.xhr.abort();
                }
                this.collection.sortBy = this.sort;

                eZExceed.trigger('find:search', _.clone(data));

                this.xhr = this.collection.search(data);
            }
        },

        renderAuthors : function()
        {
            this.$('.author-facet').append(this.authors.map(function(author)
            {
                return AuthorTemplate(author.toJSON());
            }, this)).attr('disabled', null);
            return this;
        },

        renderClasses : function()
        {
            var classes = _(this.allowedTypes).map(function(cls)
            {
                var stats = this.statistics.get(cls.key);
                cls.objectCount = stats ? stats.get('count') : 0;
                cls.hideCount =  Boolean(this.mine || this.trash);
                return cls;
            }, this);

            this.$types.html(ClassesTemplate(classes));
            return this;
        },

        render : function()
        {
            this.$el.html(FiltersTemplate({periods : this.periodList()}));

            this.$types = this.$('.type-facet');
            this.$authors = this.$('.eze-facet-author');
            return this.update();
        },

        update : function()
        {
            this.allowedTypes = this.classList(this.constrain);
            this.renderAuthors()
                .renderClasses();
            selectable(this.$('select'));
            return this;
        },

        classList : function(allowed)
        {
            allowed = allowed || ([]);
            var el = this.$types;
            var selected = false;
            if (allowed.length === 1) {
                selected = allowed[0];
            }
            else if (el.length > 0) {
                selected = parseInt(el.val(), 10);
            }
            return this.classes.chain()
                .filter(function(cls)
                {
                    return allowed.length < 1 || _.include(allowed, cls.id);
                })
                .map(function(cls)
                {
                    return {
                        key : cls.id,
                        selected : cls.id === selected,
                        value : cls.get('name')
                    };
                }, this).value();
        },

        periodList : function()
        {
            return [
                {key : '7 days ago', value : 'Last week'},
                {key : '1 month ago', value : 'Last month'}
            ];
        }
    });
});
