/**
 *
 * Causes all js-dependencies to always be reloaded.
 *
 * @param context
 * @param map
 */

var readyStateCheckInterval = setInterval(function() {
    if (document.readyState === "complete" && !/*@cc_on!@*/0) {
        if (typeof require === 'undefined') require = {};

        require.onResourceLoad = function(context, map)
        {
            if (map.name.indexOf('jqueryui') !== -1) {
                return;
            }

            require.undef(map.name);
            require.config({
                urlArgs : "bust=" +  (new Date()).getTime()
            });
        };

        clearInterval(readyStateCheckInterval);
    }
}, 10);