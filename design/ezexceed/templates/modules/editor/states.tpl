{if gt( $object.allowed_assign_state_list|count, 0)}
<section class="eze-container states last">
    <h1>{"Settings"|i18n("eze")}</h1>
    {foreach $object.allowed_assign_state_list as $allowed_assign_state_info}
        {if gt($allowed_assign_state_info.states|count, 0)}
            <h2>{$allowed_assign_state_info.group.current_translation.name|wash}</h2>
            <p class="description">{$allowed_assign_state_info.group.current_translation.description|wash}</p>

            <select class="select-state" data-groupid="{$allowed_assign_state_info.group.id}" name="SelectedStateIDList[]" {if $allowed_assign_state_info.states|count|eq(1)}disabled="disabled"{/if}>
                {foreach $allowed_assign_state_info.states as $state}
                    <option value="{$state.id}"
                            {if $object.state_id_array|contains($state.id)}selected="selected"{/if}>
                        {$state.current_translation.name|wash}
                    </option>
                {/foreach}
            </select>
        {/if}
    {/foreach}
</section>
{/if}
