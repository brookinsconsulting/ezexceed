{def
    $language = fetch('content', 'locale', hash('locale_code', $attribute.language_code))
    $attr_class = $attribute.contentclass_attribute
    $attr_name = first_set($attr_class.nameList[$content_language], $attr_class.name)|wash|trim
    $attr_desc = first_set($attr_class.descriptionList[$content_language], $attr_class.description)|wash|trim
}

<div class="language hide">
    <img class="flag" src="{$attribute.language_code|flag_icon}" /> {$language.intl_language_name}
</div>

{if and( $attribute.can_translate|not, ne($object.initial_language_code, $attribute.language_code))}
    <span class="label label-required">{'not translatable'|i18n('eze')}</span>

    <label class="heading">{$attr_name}</label>

    {if $attr_class.description}
    <span class="help-block description">{$attr_desc}</span>
    {/if}

    <div class="original">
        {attribute_view_gui attribute_base=$attribute_base attribute=$attribute}
    </div>
{else}
    {if $attribute.is_required}
    <span class="label label-required">{'required'|i18n('eze')}</span>
    {/if}

    {if $attribute.is_information_collector}
    <span class="label label-information">{'collector'|i18n('eze')}</span>
    {/if}

    <label class="heading">{$attr_name}</label>

    {if $attr_class.description}
    <span class="help-block description">{$attr_desc}</span>
    {/if}

    <div class="attribute-content">{attribute_edit_gui attribute=$attribute attribute_base=$attribute_base}</div>

    <input type="hidden" name="ContentObjectAttribute_id[]" value="{$attribute.id}" class="eze-attribute-id" data-attribute-id="{$attribute.id}" />
{/if}
