{def
    $view_parameters = array()
    $attribute_categories = ezini( 'ClassAttributeSettings', 'CategoryList', 'content.ini' )
    $attribute_default_category = ezini( 'ClassAttributeSettings', 'DefaultCategory', 'content.ini' )
    $contentobject_attributes = array()
    $categoryHeading = ''
    $last = 'full-width'
    $width = 'fullwidth'
}
{if is_set($attribute_base)|not}
    {def $attribute_base = "ContentObjectAttribute"}
{/if}

{* Group attributes by attribute-category. *}
{def
    $hasCategory = false()
    $visible = false()
    $wrap = false()
    $a = false()
    $open = false()
}
{foreach $grouped_attributes as $groupName => $attributes}
    {set
        $hasCategory = and($attributes|count, $groupName|isdefaultcategory()|not)
        $visible = contentgroupisvisible($class.identifier, $groupName)
        $categoryHeading = $groupName|upfirst()
    }

    {if $hasCategory}
        {if and(is_set($attribute_categories[$groupName]), $attribute_categories[$groupName]|compare('')|not)}
            {set $categoryHeading = $attribute_categories[$groupName]|upfirst}
        {/if}
        {if $visible}
        <div class="attr-group open" target="{$groupName}">
            <i class="icon-caret-down"></i>
        {else}
        <div class="attr-group closed" target="{$groupName}">
            <i class="icon-caret-right"></i>
        {/if}

            {$categoryHeading|wash|trim}
        </div>
        <div id="eze-attribute-group-{$groupName|trim}"{if $visible|not} class="hide"{/if}>
        {* Force a new attribute wrapper*}
        {set $last = 'full-width'}
    {/if}

    {foreach $attributes as $attribute}

        {* Wrap every fullwidth by itself, and every two following halfwidt *}
        {if or( $wrap, or( eq($attribute.width, 'full-width'), eq($last, 'full-width') ) )}
            {if $open}</div>{/if}
            <div class="eze-object-attribute-wrap">
            {set $open = true()}
        {else}
            {if and( $wrap, eq( $last, 'half-width' ) )}
                {if $open}</div>{/if}
            <div class="eze-object-attribute-wrap">
            {set $open = true()}
            {/if}
        {/if}

        {if or( eq($attribute.width, 'full-width'), and( eq($last, 'half-width'), $wrap|not ) )}
            {set $wrap = true()}
        {else}
            {set $wrap = false()}
        {/if}

        {foreach $attribute.by_language as $a}

            <section class="eze-object-attribute attribute {$a.data_type_string} {$attribute.width}{if $wrap} last{/if}
                language-{$a.language_code}{if ne($current_language, $a.language_code)} hide {/if}"
                data-attribute-base="{$attribute_base}"
                data-id="{$a.id}"
                data-datatype="{$a.data_type_string}"
                data-version="{$a.version}"
                data-language="{$a.language_code}">

                {include uri="design:modules/editor/attribute.tpl"
                    attribute=$a
                    attribute_base=$attribute_base}

            </section>
        {/foreach}

        {if $wrap}
            {set $open = false()}
            </div>
        {/if}

        {set $last = $attribute.width}

    {/foreach}
    {if $open}
    </div>
        {set $open = false()}
    {/if}
    {if $hasCategory}
        </div>
    {/if}
{/foreach}
