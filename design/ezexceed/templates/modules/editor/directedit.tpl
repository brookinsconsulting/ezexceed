{if is_set($attribute_base)|not}
    {def $attribute_base = "ContentObjectAttribute"}
{/if}

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

    <h3>{'Edit'|i18n('eze')} </h3>

    {undef $locale $attributeClass $attributeName}

</div>
<div class="modal-body">
    {include uri="design:modules/editor/attribute.tpl" attribute_base=$attribute_base}
</div>
<div class="modal-footer">
    <a class="btn discard">{'Discard'|i18n('eze')}</a>
    <a class="btn btn-primary publish">{'Publish'|i18n('eze')}</a>
</div>

{undef $attribute_base}