{if gt($versions|count, 1)}
<section class="eze-object-translations">
    <ul>
        <li><h1>{'Show translations'|i18n('eze')}</h1></li>
    {foreach $versions as $version}
        <li class="eze-language">
            <label
            ><input id="eze-edit-language-{$version.initial_language.locale}"
                    type="checkbox"
                    {if eq($current_language, $version.initial_language.locale)}checked="checked"{/if}
                    data-language="{$version.initial_language.locale}"
                    data-version="{$version.version}"
                >{$version.initial_language.name|wash|trim}</label>
        </li>
    {/foreach}
    </ul>
</section>
{/if}
