{if is_set($node)}
    {set $object = $node.object}
{/if}

{if is_set($edit_language)|not}
    {def $edit_language = false()}
{/if}

{if is_set($current_language)|not}
    {def $current_language = $object.current_language}
{/if}

{def $class = $object.content_class}

<form id="eze-edit-form"
    autocomplete="off"
    enctype="multipart/form-data"
    method="post"
    onsubmit="javascript:return false;"
    action="/Object/save/{$object.id}">

{if is_set($include)}
    {foreach $include as $file}
        {include uri=$file}
    {/foreach}
{else}
    {* Left column *}
    <section class="eze-object-attributes">
        {include uri="design:modules/editor/languages.tpl"}
        {include uri="design:modules/editor/attributes.tpl"}
    </section>

    {* Right column *}
    <section class="eze-object-sidebar">
        <section class="eze-container eze-publish"></section>

        {if fetch('content', 'access', hash( 'access', 'manage_locations', 'contentobject', $object))}
        <section class="eze-container eze-locations">
            <h1>{'Pages'|i18n('eze')}</h1>
            <p class="description">{'Show content on these pages'|i18n('eze')}</p>
            <ul class="eze-items"></ul>
            {if eq($object.main_parent_node_id, 1)|not}
                <button type="button" class="btn add">{'Add to page'|i18n('eze')}…</button>
            {/if}

            <div class="remove-location"></div>
        </section>
        {/if}

        <section class="eze-container eze-activity"></section>

        {if $class.is_container}
        <section class="eze-container subtree"></section>
        {/if}

        {if fetch('user', 'has_access_to', hash('module', 'state', 'function', 'assign'))}
            {include uri="design:modules/editor/states.tpl"}
        {/if}
    </section>

{/if}

</form>
