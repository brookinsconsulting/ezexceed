{def
    $is_dynamic = false()
    $is_custom = false()
    $fetch_params = array()
    $action = $block.action
    $addManualItems = eq(ezini($block.type, 'ManualAddingOfItems', 'block.ini'), 'enabled')
}

{if and($addManualItems|not, ezini_hasvariable($block.type, 'FetchClass', 'block.ini'))}
    {set $is_dynamic = true()}
{elseif and($addManualItems|not, ezini_hasvariable($block.type, 'FetchClass', 'block.ini' )|not)}
    {set $is_custom = true()}
{/if}

{if is_set( $block.fetch_params )}
    {set $fetch_params = unserialize($block.fetch_params)}
{/if}

{* Items. *}
{if $is_custom|not}
    {include uri='design:block/edit/standard.tpl'}
    {* Settings. *}
    {include uri='design:block/edit/_rotation.tpl'}
{/if}

{include uri='design:block/edit/dynamic.tpl'}

{* Custom attributes. *}
<div class="customAttributes">
    {include uri='design:block/edit/_attributes.tpl'}
</div>