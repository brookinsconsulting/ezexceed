<form class="form-inline well well-small block-settings" onsubmit="javascript:return false;">
	<p>
        {'Rotate active content every'|i18n('eze')}
        <input
                class="rotationAttribute"
                name="value"
                value="{$block.rotation.value}"
                type="number"
                min="0"
                max="60"
                step="1"
        />
        <select class="rotationAttribute" name="unit">
            <option value="2" {if eq($block.rotation.unit, 2)}selected{/if}>{'minute'|i18n('eze')}</option>
            <option value="3" {if eq($block.rotation.unit, 3)}selected{/if}>{'hour'|i18n('eze')}</option>
            <option value="4" {if eq($block.rotation.unit, 4)}selected{/if}>{'day'|i18n('eze')}</option>
        </select>
        <br />
		<label class="checkbox">
            <input
                    class="rotationCheckboxAttribute"
                    type="checkbox"
                    value="2"
                    {if eq($block.rotation.type, 2)}checked="checked"{/if}
                    name="shuffle"
            />
            {'Shuffle content when rotating'|i18n('eze')}
        </label>
	</p>
</form>
