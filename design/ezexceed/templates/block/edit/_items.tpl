<h2>{$heading|i18n('eze')}</h2>
<ul class="eze-items" data-item-type="{$itemType}">
{if $items|count}
    {foreach $items as $item}
        {include uri='design:block/edit/_item.tpl' item=$item type=$itemType}
    {/foreach}
{/if}
</ul>

{if and( $items|count|not, $empty) }
    {$empty}
{/if}
