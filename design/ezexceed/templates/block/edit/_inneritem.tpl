{if $activeContent}
    {def $locale = ezini('RegionalSettings', 'Locale')
        $dateformat = 'iso8601'}
    {if $locale|contains('@')}
        {set $locale = $locale|explode('@')[0]}
    {/if}

    {if $locale|compare( 'nor-NO' )}
        {set $dateformat = 'no_common'}
    {/if}

    <span>{'Activate at'|i18n('eze')}</span>
    <input type="text" class="publish-date activate-date datepicker" name="publishDate" value="{$item.ts_publication|datetime($dateformat)}">
    <input type="text" class="activate-time time-picker" name="publishTime" value="{$item.ts_publication|datetime('custom', '%H:%i')}" autocomplete="OFF">

    {undef $locale $dateformat}
{/if}
