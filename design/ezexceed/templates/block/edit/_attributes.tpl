{def $custom_attributes = array()
    $custom_attribute_types = array()
    $custom_attribute_names = array()
    $loop_count = 0
    $selection = false()
    $value = ''
}

{if ezini_hasvariable($block.type, 'CustomAttributes', 'block.ini')}
    {set $custom_attributes = ezini($block.type, 'CustomAttributes', 'block.ini')}
{/if}
{if ezini_hasvariable($block.type, 'CustomAttributeTypes', 'block.ini')}
    {set $custom_attribute_types = ezini($block.type, 'CustomAttributeTypes', 'block.ini')}
{/if}
{if ezini_hasvariable($block.type, 'CustomAttributeNames', 'block.ini')}
    {set $custom_attribute_names = ezini($block.type, 'CustomAttributeNames', 'block.ini')}
{/if}

{if $custom_attributes|count}
    <h2>{'Custom attributes'|i18n('eze')}</h2>
{/if}

{foreach $custom_attributes as $custom_attrib}
    {def $use_browse_mode = array()}
    {if ezini_hasvariable($block.type, 'UseBrowseMode', 'block.ini')}
        {set $use_browse_mode = ezini($block.type, 'UseBrowseMode', 'block.ini')}
    {/if}

    {if eq( $use_browse_mode[$custom_attrib], 'true')}
        <button id="block-choose-source" class="btn btn-primary block-control" data-name="{$custom_attrib}">{'Choose source'|i18n('eze')}</button>

        <span class="source">
            {'Current source:'|i18n('eze')}
            {if is_set($block.custom_attributes)}
            {foreach $block.custom_attributes as $custom_attrib => $value}
            {if eq($use_browse_mode[$custom_attrib], 'true')}
                {fetch('content', 'node', hash('node_id', $value)).name|wash()}
            {/if}
            {/foreach}
        {/if}
        </span>
    {else}
        <label>
            <span>{if is_set($custom_attribute_names[$custom_attrib] )}{$custom_attribute_names[$custom_attrib]}{else}{$custom_attrib}{/if}:</span>

            {set $value = $block.custom_attributes[$custom_attrib]}

            {if is_set($custom_attribute_types[$custom_attrib])}

             {switch match = $custom_attribute_types[$custom_attrib]}
                 {case match = 'text'}
                     <textarea id="block-custom_attribute-{$block_id}-{$loop_count}"
                               class="customAttribute"
                               name="{$custom_attrib}"
                               rows="7">{$value|wash()}</textarea>
                 {/case}
                 {case match = 'checkbox'}
                     <input id="block-custom_attribute-{$block_id}-{$loop_count}-b" class="customAttribute"
                            type="checkbox"
                            name="{$custom_attrib}"
                            {if eq($value, '1')}checked="checked"{/if}
                            value="1"
                     />

                 {/case}
                 {case match = 'string'}
                     <input id="block-custom_attribute-{$block_id}-{$loop_count}" class="customAttribute"
                            type="text"
                            name="{$custom_attrib}"
                            value="{$value}"/>
                 {/case}
                 {case match = 'select'}
                     {set $selection = ezini($block.type, 'CustomAttributeSelection_'|concat($custom_attrib), 'block.ini')}
                     <select name="{$custom_attrib}" class="customAttribute">
                         {foreach $selection as $key => $name}
                            <option value="{$key}"{if eq($value, $key)} selected="selected"{/if}>{$name}</option>
                         {/foreach}
                     </select>
                 {/case}
                 {case}
                     <input id="block-custom_attribute-{$block_id}-{$loop_count}" class="customAttribute"
                            type="text"
                            name="{$custom_attrib}"
                            value="{$value}"/>
                 {/case}
             {/switch}
         {else}
             <input id="block-custom_attribute-{$block_id}-{$loop_count}" class="customAttribute"
                    type="text"
                    name="{$custom_attrib}"
                    value="{$value}"/>
         {/if}
         </label>
    {/if}
    {undef $use_browse_mode}
    {set $loop_count=inc( $loop_count )}
{/foreach}