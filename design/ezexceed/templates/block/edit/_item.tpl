{def $object = fetch('content', 'object', hash('object_id', $item.object_id))}

{* Object is deleted and no longer exists. *}
{if $object|not}
    {set $object = hash('id', $item.object_id)}
{/if}

{include uri='design:global/objectlistitem.tpl' sortable=$sortable innerTpl='design:block/edit/_inneritem.tpl' object=$object}

{undef $object}