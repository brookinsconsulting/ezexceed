{if $is_dynamic}
{foreach ezini($block.type, 'FetchParameters', 'block.ini') as $fetch_parameter => $value}
    {if eq( $fetch_parameter, 'Source' )}
        <span class="source">
            {'Current source:'|i18n('eze')}
            {if is_set( $fetch_params['Source'] )}
                {if is_array( $fetch_params['Source'] )}
                    {foreach $fetch_params['Source'] as $source}
                        {def $source_node = fetch( 'content', 'node', hash( 'node_id', $source ) )}
                        <a href="{$source_node.url_alias|ezurl('no')}" target="_blank" title="{$source_node.name|wash()}[{$source_node.object.content_class.name|wash}]">{$source_node.name|wash}</a>{delimiter}, {/delimiter}
                        {undef $source_node}
                    {/foreach}
                    {else}
                    {def $source_node = fetch( 'content', 'node', hash( 'node_id', $fetch_params['Source'] ) )}
                    <a href="{$source_node.url_alias|ezurl('no')}" target="_blank" title="{$source_node.name|wash()}[{$source_node.object.content_class.name|wash()}]">{$source_node.name|wash}</a>
                    {undef $source_node}
                {/if}
            {/if}
        </span>
        <button id="block-choose-dynamic-source" class="fetchAttribute btn btn-primary">{'Choose source'|i18n('eze')}</button>
        {else}
        <label for="fetchClasses">{$fetch_parameter}:</label><input id="fetchClasses-{$fetch_parameter}" class="fetchAttribute" type="text" name="{$fetch_parameter}" value="{$fetch_params[$fetch_parameter]}"/>
    {/if}
    {/foreach}
{/if}

{if $addManualItems}
    <button class="btn add-item btn-primary">{'Select content to show in this block'|i18n('eze')}…</button>
{/if}
