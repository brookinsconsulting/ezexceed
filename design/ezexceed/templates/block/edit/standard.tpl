<div class="itemsContainer">
    {* Valid items. *}
    {include uri='design:block/edit/_items.tpl'
        items=$block.valid
        itemType='valid'
        heading='Active content'|i18n('eze')
        empty='No content currently active'|i18n('eze')
        activeContent=false()
        sortable=true()}

    {* Queued items. *}
    {include
        uri='design:block/edit/_items.tpl'
        items=$block.waiting
        itemType='waiting'
        heading='Queued up content'|i18n('eze')
        empty='No content queued up'|i18n('eze')
        activeContent=true()}
</div>
