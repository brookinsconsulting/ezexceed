{default attribute_base=ContentObjectAttribute}
{if ne( $attribute_base, 'ContentObjectAttribute' )}
    {def $id_base = concat( 'ezcoa-', $attribute_base, '-', $attribute.contentclassattribute_id, '_', $attribute.contentclass_attribute_identifier )}
{else}
    {def $id_base = concat( 'ezcoa-', $attribute.contentclassattribute_id, '_', $attribute.contentclass_attribute_identifier )}
{/if}
{def $locale = ezini('RegionalSettings', 'Locale')
     $dateformat = 'iso8601'}
{if $locale|contains('@')}
    {set $locale = $locale|explode('@')[0]}
{/if}

{if $locale|compare( 'nor-NO' )}
    {set $dateformat = 'no_common'}
{/if}



<div class="attribute-base" data-attribute-base="{$attribute_base}" data-id="{$attribute.id}">
    <div class="input-wrap">
        <span class="kp-icon16 date-icon"></span>
        <input
                type="text"
                id="{$id_base}_date_{$attribute.language_code}"
                placeholder="{'Date'|i18n( 'eze' )}"
                class="date"
                name="{$attribute_base}_datetime_date_{$attribute.id}"
                {if $attribute.content.timestamp}data-value="{$attribute.content.timestamp|datetime('iso8601')}"{/if}
                value="{if $attribute.content.is_valid}{$attribute.content.timestamp|datetime($dateformat)}{/if}" />
    </div>
    <div class="input-wrap">
        <span class="kp-icon16 time-icon-alt"></span>
        <input
               type="text"
               id="{$id_base}_time_{$attribute.language_code}"
               placeholder="{'Time'|i18n( 'eze' )}"
               class="time"
               name="{$attribute_base}_datetime_time_{$attribute.id}"
               value="{if $attribute.content.is_valid}{if $attribute.content.hour|lt(10)}0{/if}{$attribute.content.hour}:{$attribute.content.minute}{/if}" />
    </div>
    <button type="button" class="btn btn-inverse btn-small clear">
        <i class="icon-remove icon-white"></i> {'Clear'|i18n('eze')}
    </button>
</div>

{undef $locale $dateformat}
{/default}
