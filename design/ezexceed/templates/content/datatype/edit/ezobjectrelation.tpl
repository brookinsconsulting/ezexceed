<ul class="sortable eze-items">
{if $attribute.content}
    {include uri='design:global/objectlistitem.tpl'
        sortable=false()
        object=fetch( content, object, hash( object_id, $attribute.content.id ) )}
{/if}
</ul>
<p>
    <button type="button"
        class="btn add"
        data-node-id="{$attribute.object.main_node_id}">
    {'Select content'|i18n('eze')}</button>
</p>
