{def $multiple = eq( $attribute.content.enum_ismultiple, 1 )
    $selected = array()
}

{foreach $attribute.content.enumobject_list as $item}
    {set $selected = $selected|append($item.enumelement)}
<input type="hidden" class="enum-id" value="{$item.id}" />
<input type="hidden" class="enum-value" value="{$item.enumvalue|wash}" />
<input type="hidden" class="enum-element" value="{$item.enumelement|wash}" />
{/foreach}

{foreach $attribute.content.enum_list as $item}
{/foreach}

<select class="chzn" name="{$attribute_base}_select_data_enumelement_{$attribute.id}"{if $multiple} multiple{/if}>
{foreach $attribute.content.enum_list as $item}
    <option value="{$item.enumelement|wash}"
        data-id="{$item.id}"
        data-value="{$item.enumvalue|wash}"
        {if $selected|contains($item.enumelement)}selected{/if}>
        {$item.enumelement}
    </option>
{/foreach}
</select>
