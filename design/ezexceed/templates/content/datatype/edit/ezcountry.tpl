{def $countries=fetch( 'content', 'country_list' )
    $class=$attribute.class_content
    $alpha_2 = ''
    $multiple=$class.multiple_choice
    $selected = array()
    $country=$attribute.content.value}

<select name="{$attribute_base}_country_{$attribute.id}" {if $multiple}multiple{/if}>
 {if $class_content.multiple_choice|not}
    <option value="">{'Not specified'|i18n( 'eze' )}</option>
 {/if}
{foreach $countries as $key => $current_country}
     {set $alpha_2 = $current_country.Alpha2}
     {if $country|ne( '' )}
        {if $country|is_array|not}
            {* Backwards compatability *}
            <option {if $country|eq( $current_country.Name )}selected{/if} value="{$alpha_2}">{$current_country.Name}</option>
        {else}
            <option {if is_set( $country.$alpha_2 )}selected{/if} value="{$alpha_2}">{$current_country.Name}</option>
        {/if}
     {else}
            <option {if is_set( $class.default_countries.$alpha_2 )}selected{/if} value="{$alpha_2}">{$current_country.Name}</option>
     {/if}
{/foreach}

</select>
