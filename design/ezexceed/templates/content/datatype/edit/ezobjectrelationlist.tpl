{def $class = $attribute.class_content
    $nodeId = 2
}

{if is_set( $class.default_placement.node_id )}
    {set $nodeId = $class.default_placement.node_id}
{else}
    {set $nodeId = $attribute.object.main_node_id}
{/if}

<ul class="sortable eze-items">
{if $attribute.content.relation_list}
    {foreach $attribute.content.relation_list as $item}
        {include uri='design:global/objectlistitem.tpl'
            sortable=true()
            object=fetch( content, object, hash( object_id, $item.contentobject_id ) )}
    {/foreach}
{/if}
</ul>
<p>
    <button type="button"
        class="btn add"
        data-node-id="{$nodeId}">
    {'Select content'|i18n('eze')}
    </button>
</p>


{if is_set( $attribute.class_content.class_constraint_list[0] )}
    <input
        type="hidden"
        class="constraints"
        name="{$attribute_base}_browse_for_object_class_constraint_list[{$attribute.id}]"
        value="{$attribute.class_content.class_constraint_list|implode(',')}"
    >
{/if}
