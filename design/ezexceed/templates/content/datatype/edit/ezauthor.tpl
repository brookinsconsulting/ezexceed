<ul class="eze-items sortable">
    {foreach $attribute.content.author_list as $index => $author}
    <li data-name="{$author.name|wash}"
        data-email="{$author.email|wash}">
        <div class="wrap">
            <img src="{"kp/16x16/User.png"|ezimage("no")}" class="icon-16">
            <input type="text" value="{$author.name|wash}" class="eze-name" placeholder="{'Name'|i18n('eze')}" />
            <input type="email" value="{$author.email|wash}" class="eze-email" placeholder="{'E-mail'|i18n('eze')}" />
            <div class="right-align">
                <button type="button" class="btn btn-mini remove remove-btn">x</button>
            </div>
        </div>
    </li>
    {/foreach}
</ul>
<p>
    <button type="button" class="btn add">{"Add new author"|i18n("eze")}</button>
</p>
