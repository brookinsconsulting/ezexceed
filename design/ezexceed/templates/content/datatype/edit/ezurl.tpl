{* URL. *}
<input type="url"
    placeholder="{'Web address'|i18n( 'eze' )}"
    name="{$attribute_base}_ezurl_url_{$attribute.id}"
    autocorrect="off"
    autocapitalize="off"
    autocomplete="off"
    spellcheck="off"
    value="{$attribute.content|wash( xhtml )}" />

{* Text. *}
<input type="text"
    placeholder="{'Title'|i18n( 'eze' )}"
    name="{$attribute_base}_ezurl_text_{$attribute.id}"
    value="{$attribute.data_text|wash( xhtml )}" />
