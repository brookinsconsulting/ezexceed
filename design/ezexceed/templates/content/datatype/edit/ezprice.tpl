<div class="input-append">
    <input class="eze-save"
        type="text"
        name="{$attribute_base}_data_price_{$attribute.id}" 
        value="{$attribute.content.price|l10n( clean_currency )}" />

    {if $attribute.class_content.is_vat_included|not}
    <span class="add-on">+ {$attribute.content.selected_vat_type.name}, {$attribute.content.selected_vat_type.percentage}%</span>
    {/if}
</div>

<div class="control-group">

    <label for="ezprice-{$attribute_base}-{$attribute.id}-vat">
        {'VAT'|i18n( 'design/standard/class/datatype' )}
    </label>

    <select class="eze-save" id="ezprice-{$attribute_base}-{$attribute.id}-vat" name="{$attribute_base}_ezprice_inc_ex_vat_{$attribute.id}">
        <option value="1" {if eq( $attribute.content.is_vat_included, true() )}selected{/if}>{'Price inc. VAT'|i18n( 'design/standard/class/datatype' )}</option>
        <option value="2" {if eq( $attribute.content.is_vat_included, false() )}selected{/if}>{'Price ex. VAT'|i18n( 'design/standard/class/datatype' )}</option>
    </select>

{if gt( $attribute.content.vat_type|count, 1 )}
    {def $current_val=$attribute.content.selected_vat_type.id}
    <label for="ezprice-{$attribute_base}-{$attribute.id}-vattype">{'VAT type'|i18n( 'design/standard/class/datatype' )}</label>
    <select class="eze-save" name="{$attribute_base}_ezprice_vat_id_{$attribute.id}" id="ezprice-{$attribute_base}-{$attribute.id}-vattype">
    {foreach $attribute.content.vat_type as $vat_type}
        <option value="{$vat_type.id}" {if eq( $vat_type.id, $current_val )}selected{/if}>
        {$vat_type.name|wash}{if $vat_type.is_dynamic|not}, {$vat_type.percentage}%{/if}
        </option>
    {/foreach}
    </select>
{else}
    {foreach $attribute.content.vat_type as $vat_type}
    <input name="{$attribute_base}_ezprice_vat_id_{$attribute.id}" value="{$vat_type.id}" type="hidden" class="eze-save" />
    {/foreach}
{/if}

</div>
