{def $locale = ezini('RegionalSettings', 'Locale')
    $dateformat = 'iso8601'}
{if $locale|contains('@')}
    {set $locale = $locale|explode('@')[0]}
{/if}

{if $locale|compare( 'nor-NO' )}
    {set $dateformat = 'no_common'}
{/if}

<div class="input-wrap">
    <input type="text"
        name="{$attribute_base}_date_date_{$attribute.id}"
        placeholder="{'Date'|i18n( 'eze' )}"
        {if $attribute.content.timestamp}data-value="{$attribute.content.timestamp|datetime('iso8601')}"{/if}
        value="{if $attribute.content.is_valid}{$attribute.content.timestamp|datetime($dateformat)}{/if}" />
</div>

<button type="button" class="btn btn-inverse btn-small clear">
    <i class="icon-remove icon-white"></i> {'Clear'|i18n('eze')}
</button>

{undef $locale $dateformat}
