{* Make sure to normalize floats from db  *}
{def $latitude  = $attribute.content.latitude|explode(',')|implode('.')
     $longitude = $attribute.content.longitude|explode(',')|implode('.')
}

<section class="actions"
    data-sensor="{ezini('GMapSettings', 'UseSensor', 'ezgmaplocation.ini')}">

    <div class="input-append pull-left">
        <input type="text"
            class="address input-medium"
            name="{$attribute_base}_data_gmaplocation_address_{$attribute.id}"
            value="{$attribute.content.address}"
            data-autosave="off"
            data-previous-address="{$attribute.content.address}">
        <button type="button"
            title="{'Restores location and address values to what it was on page load.'|i18n('extension/ezgmaplocation/datatype')}"
            class="btn clear-address">x</button>
        <button type="button" class="find-address btn">
            {'Find address'|i18n('extension/ezgmaplocation/datatype')}
        </button>
        <img src="{'images/loader.gif'|ezdesign('no')}" class="eze-loader find-address-loader icon-16" />
    </div>
    <div class="get-my-current-position pull-right">
        <button type="button" class="current-pos btn"
            title="{'Gets your current position if your browser support GeoLocation and you grant this website access to it'|i18n('extension/ezgmaplocation/datatype')}">
            {'Get my current location'|i18n('eze')}
        </button>
    </div>
</section>

<div class="map" id="ezgml-map-{$attribute.id}" style="width: 500px; height: 300px;"></div>

<input class="lat" type="hidden" readonly
    name="{$attribute_base}_data_gmaplocation_latitude_{$attribute.id}"
    value="{$latitude}" />

<input class="lng" type="hidden" readonly
    name="{$attribute_base}_data_gmaplocation_longitude_{$attribute.id}"
    value="{$longitude}" />
