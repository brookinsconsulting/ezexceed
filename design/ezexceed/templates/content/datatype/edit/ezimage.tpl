{def $content = $attribute.content
    $hasImage = $content.original.is_valid
    $extensions = ezini('ExtensionSettings', 'ActiveAccessExtensions', 'site.ini')|merge(ezini( 'ExtensionSettings', 'ActiveExtensions', 'site.ini'))
}

<div class="thumbnail{if $hasImage|not} hide{/if}">
    {if $hasImage}
        {def $image_content = $attribute.content
             $image_class=ezini( 'ImageSettings', 'DefaultEditAlias', 'content.ini' )
        }

        {if $image_content.is_valid}
            {def $image = $image_content[$image_class]}
            <img src={$image.url|ezroot} width="{$image.width}" height="{$image.height}" />
            {undef $image}
        {/if}
        {undef $image_content $image_class}

        <button class="close remove" type="button"
            name="CustomActionButton[{$attribute.id}_delete_image]">x</button>

        {if $extensions|contains('ezie')}
        <div class="ezie-container">
            <button type="button"
                class="btn btn-inverse edit-image ezieEdit ezieEditButton"
                name="ezieEdit[{array( "ezie/prepare", $attribute.contentobject_id, $attribute.language_code, $attribute.id, $attribute.version )|implode( '/' )|ezurl( no )}]"
                id="ezieEdit_{$attribute.id}_{$attribute.version}_{$attribute.contentobject_id}"
                value="{'Edit image'|i18n( 'eze' )}">
            {"Edit image"|i18n('eze')}
            </button>
        </div>
        {/if}
    {/if}
</div>

{if $hasImage}
    <input name="{$attribute_base}_data_imagealttext_{$attribute.id}"
            type="text"
            placeholder="{'Add image description'|i18n( 'eze' )}"
            value="{$content.alternative_text|wash(xhtml)}"
            class="image-description"
    >

{/if}

{if $hasImage|not}
    <div class="upload" id="eze-upload-new-image-{$attribute.id}">
        <input class="ezimage-max-file-size" type="hidden" name="MAX_FILE_SIZE" value="{$attribute.contentclass_attribute.data_int1|mul( 1024, 1024 )}"/>
        <button type="button" class="btn"
            id="eze-image-browser-{$attribute.id}">
        {"Upload new image"|i18n('eze')}
        </button>
    </div>
{/if}

{undef $hasImage $content $extensions}