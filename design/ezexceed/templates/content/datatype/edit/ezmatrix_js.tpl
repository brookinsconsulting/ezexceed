{literal}
<script type="x-jquery-tmpl" id="tmpl-ezmatrix-cell">
    <td>
        <input class="box" type="text" name="${base}_ezmatrix_cell_${attributeId}[]" value="" />
    </td>
</script>
{/literal}

{literal}
<script type="x-jquery-tmpl" id="tmpl-ezmatrix-checkbox">
<td>
    <input class="rowSelect" type="checkbox" name="${base}_data_matrix_remove_${attributeId}[]" value="${rowIndex}" title="{/literal}{'Select row for removal.'|i18n('design/standard/content/datatype' )}{literal}"/>
</td>
</script>
{/literal}