{def $filename=''
     $mimetype=''
     $size=''
}
{if $attribute.content}
    {set $filename=$attribute.content.original_filename
         $mimetype=$attribute.content.mime_type
         $size=$attribute.content.filesize|si( byte )
    }
{/if}

<div class="wrap">
    {* Current file. *}
    <p class="file{if $attribute.content|not} hide{/if}">{$filename} <small class="mimetype">{$size}</small></p>

    <input class="hidden-delete"
        type="hidden"
        name="CustomActionButton[{$attribute.id}_delete_binary]"
        value="{'Remove'|i18n( 'eze' )}"
        disabled>

    <button type="button"
        class="remove btn{if $attribute.content|not} hide{/if}"
        name="CustomActionButton[{$attribute.id}_delete_binary]">
        {'Remove this file'|i18n( 'eze' )}
    </button>

    {* New file
        <div id="kp-upload-new-binary-{$attribute.id}" class="upload-new-binary">
            <input type="hidden" name="MAX_FILE_SIZE" value="{$attribute.contentclass_attribute.data_int1}000000"/>
            <button type="button" id="kp-browse-upload-binary-{$attribute.id}" class="upload">{'Click to add from disk'|i18n( 'design/standard/content/datatype' )}{if $attribute.content}{/if}</button>
        </div>
        *}

    <div class="upload" id="eze-upload-new-file-{$attribute.id}">
        <input class="eze-max-file-size"
            type="hidden"
            name="MAX_FILE_SIZE"
            value="{$attribute.contentclass_attribute.data_int1|mul( 1024, 1024 )}"/>
        <button type="button" class="btn"
            id="eze-file-browser-{$attribute.id}">
            {"Upload new file"|i18n('eze')}
        </button>
    </div>
</div>
