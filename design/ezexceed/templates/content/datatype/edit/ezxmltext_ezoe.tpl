{def
    $input_handler=$attribute.content.input
    $layout_settings = $input_handler.editor_layout_settings
    $editorRow=10
    $locale = ezini('RegionalSettings', 'Locale', 'site.ini')
    $spell_languages = '+English=en'
    $cur_locale = fetch( 'content', 'locale' )
}

{if gt($attribute.contentclass_attribute.data_int1,1)}
    {set editorRow=$attribute.contentclass_attribute.data_int1}
{/if}

{if $attribute.language_code|eq( $locale )}
    {set $spell_languages = concat( '+', $cur_locale.intl_language_name, '=', $cur_locale.http_locale_code|explode('-')[0])}
{else}
    {def $atr_locale = fetch( 'content', 'locale', hash('locale_code', $attribute.language_code)) }

    {set $spell_languages = concat('+', $atr_locale.intl_language_name, '=', $atr_locale.http_locale_code|explode('-')[0])}
    {set $spell_languages = concat( $spell_languages, ',', $cur_locale.intl_language_name, '=', $cur_locale.http_locale_code|explode('-')[0])}
    {undef $atr_locale}
{/if}

{def $languagePack = '-'|concat($locale)}

<p class="hide">
    <button type="button" class="btn enableEditor">Enable editor</button>
</p>

<div class="oe-window" data-language-pack="{$languagePack}">

    <input type="hidden"
            class="hiddenData"
            data-attributeid="{$attribute.id}"
            data-contentobjectid="{$attribute.contentobject_id}"
            data-version="{$attribute.object.current_version}"
            data-buttons="{$layout_settings['buttons']|implode(',')}"
            data-pathlocation="{$layout_settings['path_location']}"
            data-toolbarlocation="{$layout_settings['toolbar_location']}"
            data-spelllanguages = "{$spell_languages}" />

    <textarea
            id="{$attribute_base}_data_text_{$attribute.id}"
            name="{$attribute_base}_data_text_{$attribute.id}"
            rows="{$editorRow}"
            class="box">{$input_handler.input_xml}</textarea>
</div>
{undef $locale $cur_locale $languagePack}
