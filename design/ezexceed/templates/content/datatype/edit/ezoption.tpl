{def $prefix = concat( $attribute_base, '-', $attribute.id )}

<span class="bootstrap" data-information-collection="{$attribute.is_information_collector}"></span>

<input type="text"
    class="eze-save name"
    placeholder="{'Name'|i18n( 'design/standard/content/datatype' )}"
    name="{$attribute_base}_data_option_name_{$attribute.id}"
    value="{$attribute.content.name|wash()}" />

<span class="help-inline">{"Heading label to display to users"|i18n("eze")}</span>

<table class="table">
    <thead>
        <tr>
            <th>{'Option'|i18n( 'design/standard/content/datatype' )}</th>
            {if $attribute.is_information_collector|not}
            <th>{'Additional price'|i18n( 'design/standard/content/datatype' )}</th>
            {/if}
            <th></th>
        </tr>
    </thead>

    <tbody>
    {foreach $attribute.content.option_list as $index => $item}
    <tr>

        {* Option. *}
        <td>
            <input type="text"
                class="eze-save option input-medium"
                name="{$attribute_base}_data_option_value_{$attribute.id}"
                value="{$item.value|wash()}" />
        </td>

    {if $attribute.is_information_collector|not}
        {* Price. *}
        <td>
            <input type="text"
                class="eze-save price input-small"
                name="{$attribute_base}_data_option_additional_price_{$attribute.id}"
                value="{$item.additional_price}" />
        </td>
    {/if}

    {* Remove. *}
        <td>
            <button type="button" class="btn btn-danger btn-mini remove" data-id="{$item.id}">
                {'Remove'|i18n('eze')}
            </button>

            <input type="hidden"
                class="eze-save id"
                name="{$attribute_base}_data_option_id_{$attribute.id}"
                value="{$item.id}" />
        </td>

    </tr>
    {/foreach}
    </tbody>

</table>

<button type="button" class="btn btn-primary add"
    data-name="CustomActionButton[{$attribute.id}_new_option]">
    {'Add option'|i18n('design/standard/content/datatype')}
</button>
