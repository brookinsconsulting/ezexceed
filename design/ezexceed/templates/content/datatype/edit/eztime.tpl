<input id="{$attribute_base}_time_{$attribute.language_code}"
    type="text"
    name="{$attribute_base}_time_{$attribute.id}"
    placeholder="{'Time'|i18n( 'design/standard/content/datatype' )}"
    value="{if $attribute.content.is_valid}{if $attribute.content.hour|lt(10)}0{/if}{$attribute.content.hour}:{if $attribute.content.minute|lt(10)}0{/if}{$attribute.content.minute}{if $attribute.contentclass_attribute.data_int2|eq(1)}:{if $attribute.content.second|lt(10)}0{/if}{$attribute.content.second}{/if}{/if}" />
