{def $attribute_base=ContentObjectAttribute
     $selected_id_array=$attribute.content
}

{if $attribute.class_content.is_multiselect}
    {section var=Options loop=$attribute.class_content.options}
    <label><input type="checkbox" value="{$Options.item.id}"
                    {if $selected_id_array|contains( $Options.item.id )}checked="checked"{/if} />{$Options.item.name|wash( xhtml )}</label>
    {/section}
{else}
<select name="{$attribute_base}_ezselect_selected_array_{$attribute.id}">
    <option value=""> - </option>
    {section var=Options loop=$attribute.class_content.options}
        <option value="{$Options.item.id}"
            {if $selected_id_array|contains( $Options.item.id )}selected="selected"{/if}>{$Options.item.name|wash( xhtml )}</option>
    {/section}
</select>
{/if}

{undef $attribute_base $selected_id_array}
