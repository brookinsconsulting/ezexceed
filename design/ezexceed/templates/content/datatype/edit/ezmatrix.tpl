{default attribute_base=ContentObjectAttribute}
    {let matrix=$attribute.content}

<div class="attribute-base" data-attribute-base="{$attribute_base}" data-id="{$attribute.id}" data-column-count="{$matrix.columnCount}" data-row-count="{$matrix.rowCount}">
    {* Matrix. *}
    <table class="table-matrix table table-bordered table-striped table-condensed" cellspacing="0">
        <thead>
        <tr>
            <th class="tight">&nbsp;</th>
                {section var=ColumnNames loop=$matrix.columns.sequential}
                <th>{$ColumnNames.item.name}</th>{/section}
        </tr>
        </thead>

        {if $matrix.rows.sequential|not}
            <tr class="dummy">
                <td>
                    <input class="rowSelect" type="checkbox"
                           name="{$attribute_base}_data_matrix_remove_{$attribute.id}[]" value="0"
                           title="{'Select row for removal'|i18n( 'eze' )}"/>
                </td>
                {section var=ColumnNames loop=$matrix.columns.sequential}
                    <td>
                        <input type="text" name="{$attribute_base}_ezmatrix_cell_{$attribute.id}[]"
                               value="" placeholder="..."/>
                    </td>
                {/section}
            </tr>
        {else}
            {section var=Rows loop=$matrix.rows.sequential}
                <tr>

                {* Remove. *}
                    <td>
                        <input class="rowSelect" type="checkbox" name="{$attribute_base}_data_matrix_remove_{$attribute.id}[]" value="{$Rows.index}" title="{'Select row for removal'|i18n( 'eze' )}"/>
                    </td>
                {* Custom columns. *}
                    {section var=Columns loop=$Rows.item.columns}
                        <td>
                            <input type="text" name="{$attribute_base}_ezmatrix_cell_{$attribute.id}[]" value="{$Columns.item|wash(xhtml)}" placeholder="..."/>
                        </td>
                    {/section}

                </tr>
            {/section}
        {/if}
    </table>

    {* Buttons. *}

    <button class="btn removeSelectedRows"
           type="submit" name="CustomActionButton[{$attribute.id}_remove_selected]"
           title="{'Remove selected rows from the matrix.'|i18n('design/standard/content/datatype')}">
        {'Remove selected'|i18n('design/standard/content/datatype')}</button>

        &nbsp;&nbsp;
        {let row_count=sub( 40, count( $matrix.rows.sequential ) ) index_var=0}
            {if $row_count|lt(1)}
                {set row_count=0}
            {/if}
            <p>
            <select name="{$attribute_base}_data_matrix_add_count_{$attribute.id}" title="{'Number of rows to add.'|i18n( 'design/standard/content/datatype' )}" class="chzn-select numMatrixRows">
                <option value="1">1</option>
                {section loop=$row_count}
                    {set index_var=$index_var|inc}
                    {delimiter modulo=5}
                        <option value="{$index_var}">{$index_var}</option>
                    {/delimiter}
                {/section}
            </select>

                <button class="btn addMatrixRows" type="submit" name="CustomActionButton[{$attribute.id}_new_row]"
                       title="{'Add new rows to the matrix.'|i18n('design/standard/content/datatype')}">
                    {'Add rows'|i18n('design/standard/content/datatype')}</button>
            </p>
        {/let}
    </div>
    {/let}
{/default}
