<input type="text"
    spellcheck="off"
    autocapitalize="off"
    autocorrect="off"
    name="{$attribute_base}_data_text_{$attribute.id}"
    placeholder="{'E-mail'|i18n( 'eze' )}"
    value="{$attribute.data_text|wash(xhtml)}"/>
