<div class="eze-update-on-edit" data-id="{$attribute.id}">
    <div class="eze-inner">
        {$attribute.content.output.output_text}
    </div>
    {$attribute|exceed_editable}
</div>