{if eq( http-header('X-Exceed-RenderToolbar'), 'no' )|not}
    {def $locale = ezini('RegionalSettings', 'ContentObjectLocale') }
    {if $locale|contains('@')}
        {set $locale = $locale|explode('@')[0]}
    {/if}
    {if and( is_set($model.layoutAttribute), $model.layoutAttribute.object.can_edit )}
        {def $canEdit = true()}
    {else}
        {def $canEdit = false()}
    {/if}

    {if or($canEdit, $model.entities)}
    <div class="eze">
        <div class="eze-pencil eze-block-pencil kp-edit-block invisible">
            <div class="kp-edit-block-box">
                <ul class="kp-edit-block-elements">

                    {if $canEdit}
                        {if $model.block}
                            {if eq(ezini($model.block.type, 'ManualAddingOfItems', 'block.ini'), 'enabled')}
                        <li class="add">
                            <a class="item"
                                data-object-id="{$model.layoutAttribute.contentobject_id}"
                                data-zone-index="{$model.zoneIndex}"
                                data-block-id="{$model.block.id}"
                                data-language="{$locale}"
                                data-block-name="{$model.blockName}"
                            >
                                <img src="{'kp/16x16/white/Add.png'|ezimage('no')}" class="icon-16" alt/>
                                <strong>{'Add'|i18n('eze')}</strong> {'content to'|i18n('eze')}  «{$model.blockName}»
                            </a>
                        </li>
                            {/if}
                        {else}
                        <li>
                            <span>{$model.title}</span>
                        </li>
                        {/if}
                    {/if}

                    {foreach $model.entities as $entity}
                        {if is_set($entity.separator)}
                        <li class="separator">{$entity.title}</li>
                        {else}
                        <li data-id="{$entity.id}" class="eze-node">
                            <a class="kp-edit-node item"
                                data-object-id="{$entity.id}"
                                data-language="{$locale}"
                                data-class-identifier="{$entity.classIdentifier}"
                            ><img src="{'kp/16x16/white/Edit.png'|ezimage('no')}" class="icon-16" alt/>
                                <strong>{"Edit"|i18n('eze')}</strong> «{$entity.name}»
                            </a>
                        </li>
                        {/if}
                    {/foreach}
                    {if $canEdit}
                        <li class="edit">
                            <a class="item"
                                data-object-id="{$model.layoutAttribute.contentobject_id}"
                                data-zone-index="{$model.zoneIndex}"
                                data-block-id="{$model.block.id}"
                                data-language="{$locale}"
                                data-block-name="{$model.blockName}"
                            ><img src="{'kp/16x16/white/Settings.png'|ezimage('no')}" class="icon-16" alt/>
                            <strong>{'Settings'|i18n('eze')}</strong> {'for'|i18n('eze')} «{$model.blockName}»</a>
                        </li>
                    {/if}
                    {undef $locale}
                </ul>
            </div>
        </div>
    </div>
    {/if}
{/if}
