{if eq( http-header('X-Exceed-RenderToolbar'), 'no')|not}
<div class="eze-pencil attribute-direct-edit invisible"
    type="button"
    data-attribute="{$model.name}"
    data-object-id="{$model.objectId}"
    data-type="{$model.class}"
    data-language="{$model.lang}"
    data-version="{$model.version}"
    title="{'Edit'|i18n('eze')} {$model.name}"
><span></span></div>
{/if}
