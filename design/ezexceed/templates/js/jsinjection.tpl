<script type="text/javascript">

function _eZExceedJSInjectionBootstrap()
{ldelim}

{literal}
    var bootstrapper = {};

    bootstrapper.Translations = {};
    bootstrapper.rootNodes = {};
    bootstrapper.rootNodes.classRootNodes = {};

{/literal}

    bootstrapper.trashEnabled = Boolean({trashenabled()});

    /** Global. */
    bootstrapper.Translations['and'] = '{'and'|i18n('eze')}';
    bootstrapper.Translations['in'] = '{'in'|i18n('eze')}';
    bootstrapper.Translations['Published'] = '{'Published'|i18n('eze')}';
    bootstrapper.Translations['by'] = '{'by'|i18n('eze')}';
    bootstrapper.Translations['Saving'] = '{"Saving"|i18n('eze')}';
    bootstrapper.Translations['View'] = '{"View"|i18n('eze')}';
    bootstrapper.Translations['Remove'] = '{"Remove"|i18n('eze')}';
    bootstrapper.Translations['Copy'] = '{"Copy"|i18n('eze')}';
    bootstrapper.Translations['Edit'] = '{"Edit"|i18n('eze')}';
    bootstrapper.Translations['Keep'] = '{"Keep"|i18n('eze')}';
    bootstrapper.Translations['Remove'] = '{"Remove"|i18n('eze')}';
    bootstrapper.Translations['Delete'] = '{"Delete"|i18n('eze')}';
    bootstrapper.Translations['Modified'] = '{"Modified"|i18n('eze')}';
    bootstrapper.Translations['Main'] = '{"Main"|i18n('eze')}';
    bootstrapper.Translations['You have been logged out'] = '{"You have been logged out"|i18n('eze')}';
    bootstrapper.Translations['Status'] = '{"Status"|i18n('eze')}';
    bootstrapper.Translations['Name'] = '{"Navn"|i18n('eze')}';
    bootstrapper.Translations['E-mail'] = '{"E-post"|i18n('eze')}';
    bootstrapper.Translations['New'] = '{"New"|i18n('eze')}';
    bootstrapper.Translations['Deleting'] = '{"Deleting"|i18n('eze')}';
    bootstrapper.Translations['Username'] = '{"Username"|i18n('eze')}';
    bootstrapper.Translations['Password'] = '{"Password"|i18n('eze')}';
    bootstrapper.Translations['No name'] = '{"No name"|i18n('eze')}';
    bootstrapper.Translations['Settings for'] = '{"Settings for"|i18n('eze')}';

    /** Preview. */
    bootstrapper.Translations['Preview'] = '{"Preview"|i18n('eze')}';
    bootstrapper.Translations['Preview this page on an URL'] = '{"Preview this page on an URL"|i18n('eze')}';
    bootstrapper.Translations['Send preview to devices'] = '{"Send preview to devices"|i18n('eze')}';
    bootstrapper.Translations['You have no connected devices. Download the iPhone or iPad app to use this new and powerful way of working across devices'] =
        '{"You have no connected devices. Download the iPhone or iPad app to use this new and powerful way of working across devices"|i18n('eze')}';
    bootstrapper.Translations['Send preview'] = '{'Send preview'|i18n('eze')}';
    bootstrapper.Translations['Preview sent'] = '{'Preview sent'|i18n('eze')}';
    bootstrapper.Translations['Mirror browsing'] = '{'Mirror browsing'|i18n('eze')}';
    bootstrapper.Translations['Get the app from the App Store'] = '{'Get the app from the App Store'|i18n('eze')}';
    bootstrapper.Translations['Loading future'] = '{'Loading future'|i18n('eze')}';
    bootstrapper.Translations['Preview the future with Timeline'] = '{'Preview the future with Timeline'|i18n('eze')}';
    bootstrapper.Translations['Turn on timeline'] = '{'Turn on timeline'|i18n('eze')}';
    bootstrapper.Translations['Turn off timeline'] = '{'Turn off timeline'|i18n('eze')}';

    /** Activity. */
    bootstrapper.Translations['Activity'] = '{"Activity"|i18n('eze')}';
    bootstrapper.Translations['This page'] = '{"This page"|i18n('eze')}';
    bootstrapper.Translations['All activity'] = '{"All activity"|i18n('eze')}';
    bootstrapper.Translations['me'] = '{"me"|i18n('eze')}';
    bootstrapper.Translations['You'] = '{"You"|i18n('eze')}';
    bootstrapper.Translations['Pinned'] = '{"Pinned"|i18n('eze')}';
    bootstrapper.Translations['Write message'] = '{"Write message"|i18n('eze')}';
    bootstrapper.Translations['Share'] = '{"Share"|i18n('eze')}';
    bootstrapper.Translations['Comment'] = '{"Comment"|i18n('eze')}';
    bootstrapper.Translations['Like'] = '{"Like"|i18n('eze')}';
    bootstrapper.Translations['Unlike'] = '{"Unlike"|i18n('eze')}';
    bootstrapper.Translations['like this'] = '{"like this"|i18n('eze')}';
    bootstrapper.Translations['Show more'] = '{"Show more"|i18n('eze')}';
    bootstrapper.Translations['Load more'] = '{"Load more"|i18n('eze')}';
    bootstrapper.Translations['people'] = '{"people"|i18n('eze')}';
    bootstrapper.Translations['Comments on this content'] = '{"Comments on this content"|i18n('eze')}';

    /** Stack. */
    bootstrapper.Translations['Close all'] = '{'Close all'|i18n('eze')}';
    bootstrapper.Translations['Close'] = '{'Close'|i18n('eze')}';
    bootstrapper.Translations['Go back'] = '{'Go back'|i18n('eze')}';

    /** Content. */
    bootstrapper.Translations['Create new'] = '{'Create new'|i18n('eze')}';
    bootstrapper.Translations['Create new content'] = '{'Create new content'|i18n('eze')}';
    bootstrapper.Translations['Publish now'] = '{"Publish now"|i18n('eze')}';
    bootstrapper.Translations['Input required'] = '{"Input required"|i18n('eze')}';
    bootstrapper.Translations['All changes saved'] = '{"All changes saved"|i18n('eze')}';
    bootstrapper.Translations['You have deleted the page you are viewing'] = '{"You have deleted the page you are viewing"|i18n('eze')}';
    bootstrapper.Translations['The parent page must be refreshed in order to update the view'] = '{"The parent page must be refreshed in order to update the view"|i18n('eze')}';
    bootstrapper.Translations['You have changed the page you are viewing'] = '{"You have changed the page you are viewing"|i18n('eze')}';
    bootstrapper.Translations['The page must be refreshed in order to update the view'] = '{"The page must be refreshed in order to update the view"|i18n('eze')}';
    bootstrapper.Translations['Select row for removal'] = '{"Select row for removal"|i18n('eze')}';
    bootstrapper.Translations['This location has subcontent! Removing it will also remove all subcontent. Are you sure you want to remove'] = '{"This location has subcontent! Removing it will also remove all subcontent. Are you sure you want to remove"|i18n('eze')}';

    /** Edit. */
    bootstrapper.Translations['Add relations to'] = '{"Add relations to"|i18n('eze')}';
    bootstrapper.Translations['Revert all changes'] = '{"Revert all changes"|i18n('eze')}';
    bootstrapper.Translations['Uploading…'] = '{"Uploading…"|i18n('eze')}';
    bootstrapper.Translations['Browse sitemap'] = '{"Browse sitemap"|i18n('eze')}';
    bootstrapper.Translations['Insert link'] = '{"Insert link"|i18n('eze')}';
    bootstrapper.Translations['Please enter an address'] = '{"Please enter an address"|i18n('eze')}';
    bootstrapper.Translations['Please enter a valid address'] = '{"Please enter a valid address"|i18n('eze')}';
    bootstrapper.Translations['Geolocation failed'] = '{"Geolocation failed"|i18n('eze')}';

    /** Subtree. */
    bootstrapper.Translations['subcontent below'] = '{"subcontent below"|i18n('eze')}';
    bootstrapper.Translations['Content under'] = '{"Content under"|i18n('eze')}';
    bootstrapper.Translations['Edit and sort content under this page'] = '{"Edit and sort content under this page"|i18n('eze')}';
    bootstrapper.Translations['priority'] = '{"priority"|i18n('eze')}';
    bootstrapper.Translations['alphabetically'] = '{"alphabetically"|i18n('eze')}';

    /** Finder. */
    bootstrapper.Translations['Are you sure you want to remove'] = '{"Are you sure you want to remove"|i18n('eze')}';
    bootstrapper.Translations['Place the content on this page'] = '{"Place the content on this page"|i18n('eze')}';
    bootstrapper.Translations['The content is placed on this page'] = '{"The content is placed on this page"|i18n('eze')}';
    bootstrapper.Translations['Yes, remove it'] = '{"Yes, remove it"|i18n('eze')}';
    bootstrapper.Translations['No, keep it'] = '{"No, keep it"|i18n('eze')}';
    bootstrapper.Translations['Remove from this page'] = '{"Remove from this page"|i18n('eze')}';
    bootstrapper.Translations['Hide this content'] = '{"Hide this content"|i18n('eze')}';
    bootstrapper.Translations['Remove'] = '{"Remove"|i18n('eze')}';
    bootstrapper.Translations['Sitemap'] = '{"Sitemap"|i18n('eze')}';
    bootstrapper.Translations['This is the only location for'] = '{"This is the only location for"|i18n('eze')}';
    bootstrapper.Translations['Removing it will also move the content to Trash'] = '{"Removing it will also move the content to Trash"|i18n('eze')}';
    bootstrapper.Translations['Removing it will permanently delete the content'] = '{"Removing it will permanently delete the content"|i18n('eze')}';
    bootstrapper.Translations['does not support subcontent'] = '{"does not support subcontent"|i18n('eze')}';
    bootstrapper.Translations["Can't move to"] = "{"Can't move to"|i18n('eze')}";
    bootstrapper.Translations["will also remove all sub-content."] = "{"will also remove all sub-content."|i18n('eze')}";
    bootstrapper.Translations["sub-content will also be deleted."] = "{"sub-content will also be deleted."|i18n('eze')}";
    bootstrapper.Translations["Upload files"] = "{"Upload files"|i18n('eze')}";
    bootstrapper.Translations["Uploading"] = "{"Uploading"|i18n('eze')}";
    bootstrapper.Translations["Search"] = "{"Search"|i18n('eze')}";
    bootstrapper.Translations["No results"] = "{"No results"|i18n('eze')}";

    /** Find. */
    bootstrapper.Translations['other locations'] = '{'other locations'|i18n('eze')}';
    bootstrapper.Translations['other location'] = '{'other location'|i18n('eze')}';
    bootstrapper.Translations['published on'] = '{'published on'|i18n('eze')}';
    bootstrapper.Translations['Find content'] = '{'Find content'|i18n('eze')}';
    bootstrapper.Translations['All content'] = '{'All content'|i18n('eze')}';
    bootstrapper.Translations['Batch operations'] = '{'Batch operations'|i18n('eze')}';
    bootstrapper.Translations['Publish selected'] = '{'Publish selected'|i18n('eze')}';
    bootstrapper.Translations['Delete selected'] = '{'Delete selected'|i18n('eze')}';
    bootstrapper.Translations['Sort by'] = '{'Sort by'|i18n('eze')}';
    bootstrapper.Translations['Date'] = '{'Date'|i18n('eze')}';
    bootstrapper.Translations['Author'] = '{'Author'|i18n('eze')}';
    bootstrapper.Translations['Content type'] = '{'Content type'|i18n('eze')}';
    bootstrapper.Translations['Type to search'] = '{'Type to search'|i18n('eze')}';
    bootstrapper.Translations['Last edit date'] = '{'Last edit date'|i18n('eze')}';
    bootstrapper.Translations['All time'] = '{'All time'|i18n('eze')}';
    bootstrapper.Translations['Last week'] = '{'Last week'|i18n('eze')}';
    bootstrapper.Translations['Last month'] = '{'Last month'|i18n('eze')}';
    bootstrapper.Translations['All content types'] = '{'All content types'|i18n('eze')}';
    bootstrapper.Translations['By author'] = '{'By author'|i18n('eze')}';
    bootstrapper.Translations['Sort by author'] = '{'Sort by author'|i18n('eze')}';
    bootstrapper.Translations['Sort by date published'] = '{'Sort by date published'|i18n('eze')}';
    bootstrapper.Translations['Sort by content type'] = '{'Sort by content type'|i18n('eze')}';
    bootstrapper.Translations['All authors'] = '{'All authors'|i18n('eze')}';
    bootstrapper.Translations['I changed'] = '{'I changed'|i18n('eze')}';
    bootstrapper.Translations['Loading more'] = '{'Loading more'|i18n('eze')}';
    bootstrapper.Translations['Never published'] = '{'Never published'|i18n('eze')}';
    bootstrapper.Translations['Set relation for'] = '{'Set relation for'|i18n('eze')}';
    bootstrapper.Translations['Select this content'] = '{'Select this content'|i18n('eze')}';
    bootstrapper.Translations['Go to'] = '{'Go to'|i18n('eze')}';
    bootstrapper.Translations['Go to primary page'] = '{'Go to primary page'|i18n('eze')}';
    bootstrapper.Translations['locations'] = '{'locations'|i18n('eze')}';
    bootstrapper.Translations['My drafts'] = '{'My drafts'|i18n('eze')}';
    bootstrapper.Translations['Show'] = '{'Show'|i18n('eze')}';
    bootstrapper.Translations['Sorry, no content found. Please try another search criteria.'] = '{'Sorry, no content found. Please try another search criteria.'|i18n('eze')}';
    bootstrapper.Translations['Trash'] = '{'Trash'|i18n('eze')}';
    bootstrapper.Translations['Trash selected'] = '{'Trash selected'|i18n('eze')}';
    bootstrapper.Translations['Permanently remove selected'] = '{'Permanently remove selected'|i18n('eze')}';
    bootstrapper.Translations['Restore selected'] = '{'Restore selected'|i18n('eze')}';

    /** Publish. */
    bootstrapper.Translations['Publish this content'] = '{"Publish this content"|i18n('eze')}';
    bootstrapper.Translations['Last published'] = '{"Last published"|i18n('eze')}';
    bootstrapper.Translations['Publish pages'] = '{"Publish pages"|i18n('eze')}';
    bootstrapper.Translations['Publish all related pages'] = '{"Publish all related pages"|i18n('eze')}';
    bootstrapper.Translations['It has never been published'] = '{"It has never been published"|i18n('eze')}';
    bootstrapper.Translations['Content was successfully published'] = '{"Content was successfully published"|i18n('eze')}';
    bootstrapper.Translations['Publishing'] = '{"Publishing"|i18n('eze')}';
    bootstrapper.Translations['changed'] = '{"changed"|i18n('eze')}';
    bootstrapper.Translations['published'] = '{"published"|i18n('eze')}';
    bootstrapper.Translations['new'] = '{"new"|i18n('eze')}';
    bootstrapper.Translations['This content is published'] = '{"This content is published"|i18n('eze')}';
    bootstrapper.Translations['The content is being published'] = '{"The content is being published"|i18n('eze')}';

    /** Locations. */
    bootstrapper.Translations['Add location to'] = '{"Add location to"|i18n('eze')}';
    bootstrapper.Translations['Show all'] = '{"Show all"|i18n('eze')}';
    bootstrapper.Translations['Hide'] = '{"Hide"|i18n('eze')}';

    /** Settings. */
    bootstrapper.Translations['Error'] = '{"Error"|i18n('eze')}';
    bootstrapper.Translations['Error!'] = '{"Error!"|i18n('eze')}';
    bootstrapper.Translations['Error saving!'] = '{"Error saving!"|i18n('eze')}';

    /** Layout. */
    bootstrapper.Translations['Layout for'] = '{"Layout for"|i18n('eze')}';

    /** Block. */
    bootstrapper.Translations['Add content to the block'] = '{"Add content to the block"|i18n('eze')}';
    bootstrapper.Translations['Choose this node as source'] = '{"Choose source"|i18n('eze')}';
    bootstrapper.Translations['The content is already added'] = '{"The content is already added"|i18n('eze')}';
    bootstrapper.Translations['Remove from the block'] = '{"Remove from the block"|i18n('eze')}';
    bootstrapper.Translations['Add item to'] = '{"Add item to"|i18n('eze')}';
    bootstrapper.Translations['Select source'] = '{"Select source"|i18n('eze')}';
    bootstrapper.Translations['Activate at'] = '{"Activate at"|i18n('eze')}';

    /** Log back in */
    bootstrapper.Translations["You've been logged out"] = "{"You've been logged out"|i18n('eze')}";
    bootstrapper.Translations["Login and resume"] = '{"Login and resume"|i18n('eze')}';

    /** Keymedia */
    bootstrapper.Translations["Select crops"] = "{"Select crops"|i18n('eze')}";
    bootstrapper.Translations["Select crop"] = "{"Select crop"|i18n('eze')}";
    bootstrapper.Translations["Select media"] = "{"Select media"|i18n('eze')}";
    bootstrapper.Translations["Alternate text"] = "{"Alternate text"|i18n('eze')}";
    bootstrapper.Translations["Class"] = "{"Class"|i18n('eze')}";
    bootstrapper.Translations["Image is to small for this version"] = "{"Image is to small for this version"|i18n('eze')}";
    bootstrapper.Translations["Search for media"] = "{"Search for media"|i18n('eze')}";
    bootstrapper.Translations["Upload new media"] = "{"Upload new media"|i18n('eze')}";
    bootstrapper.Translations["Shared"] = "{"Shared"|i18n('eze')}";

    /** eZWriterFlow */
    bootstrapper.Translations["Awaiting approval"] = "{"Awaiting approval"|i18n('eze')}";
    bootstrapper.Translations["Content sent for approval to"] = "{"Content sent for approval to"|i18n('eze')}";
    bootstrapper.Translations["group"] = "{"group"|i18n('eze')}";
    bootstrapper.Translations["Send content for approval"] = "{"Send content for approval"|i18n('eze')}";
    bootstrapper.Translations["Approve content"] = "{"Approve content"|i18n('eze')}";
    bootstrapper.Translations["Content was sent for approval"] = "{"Content was sent for approval"|i18n('eze')}";
    bootstrapper.Translations["Reject content changes"] = "{"Reject content changes"|i18n('eze')}";
    bootstrapper.Translations["Send to"] = "{"Send to"|i18n('eze')}";

    /** Error-handling */
    bootstrapper.Translations["An internal server error happened, this has been reported"] = '{"An internal server error happened, this has been reported"|i18n('eze')}';
    bootstrapper.Translations["Dismiss"] = '{"Dismiss"|i18n('eze')}';

    bootstrapper.currentUserId = '{$current_user.contentobject_id}';

    /** Add siteaccess language. */
    {def $locale = ezini('RegionalSettings', 'ContentObjectLocale')}
    {if $locale|contains('@')}
        {set $locale = $locale|explode('@')[0]}
    {/if}
    bootstrapper.currentLanguage = '{$locale}';

    /** Set the locale language. This is commonly used for the admin language */
    {set $locale = ezini('RegionalSettings', 'Locale')}
    {if $locale|contains('@')}
        {set $locale = $locale|explode('@')[0]}
    {/if}
    bootstrapper.locale = '{$locale}';

    /** Current siteaccess */
    bootstrapper.siteaccess = '{$access_type.name}';

    /** Current nodeId and objectId */
    bootstrapper.currentNodeId = {$current_node.node_id};
    bootstrapper.currentObjectId = {$current_node.contentobject_id};

    {def $rootNodeId = ezexceedsitemaprootnode()}
    {def $rootNode = fetch('content', 'node', hash('node_id', $rootNodeId))}

    {if $rootNode}
        bootstrapper.rootNodes.sitemapRootNode = {ldelim}id : {$rootNode.node_id}, name : '{$rootNode.name}'{rdelim};
    {/if}

    {set $rootNodeId = ezini('NodeSettings', 'MediaRootNode', 'content.ini')}
    {set $rootNode = fetch('content', 'node', hash('node_id', $rootNodeId))}

    {if $rootNode}
        bootstrapper.rootNodes.mediaRootNode = {ldelim}id : {$rootNode.node_id}, name : '{$rootNode.name}'{rdelim};
    {/if}

    {def $classRootNodes = ezini('eZExceedSiteMap', 'ClassRoot', 'ezexceed.ini')}

    {foreach $classRootNodes as $identifier => $rootNodeId}
        {set $rootNode = fetch('content', 'node', hash('node_id', $rootNodeId))}
        {if $rootNode}
            bootstrapper.rootNodes.classRootNodes[{$rootNodeId}] = {ldelim}
                id : {$rootNode.node_id},
                name : '{$rootNode.name}',
                identifier : '{$identifier}'
            {rdelim};
        {/if}
    {/foreach}

    {undef $rootNodeId $rootNode $classRootNodes}

    {def $dateFormats = ezini('ClassSettings', 'Formats', 'datetime.ini')}

    bootstrapper.dateFormat = [];
    bootstrapper.dateFormat['iso8601'] = '{$dateFormats['js_iso8601']}';
    bootstrapper.dateFormat['no_common'] = '{$dateFormats['js_no_common']}';
    {if $locale|compare( 'nor-NO' )}
        bootstrapper.dateFormatCurrent = 'no_common';
    {else}
        bootstrapper.dateFormatCurrent = 'iso8601';
    {/if}

    {undef $locale}
    {undef $dateFormats}

    {def $canCreateClasses = globalclasslist()}
    bootstrapper.canCreateClasses = {ldelim}{rdelim};

    {foreach $canCreateClasses as $objectClass}
        bootstrapper.canCreateClasses[{$objectClass.id}] = '{$objectClass.name}';
    {/foreach}

    {def $classes = fetch('class', 'list', hash('sort_by', array( 'name', true() )))}
    bootstrapper._contentClasses = [];

    {foreach $classes as $class}
        bootstrapper._contentClasses.push({ldelim}
            id : {$class.id},
            name : '{$class.name}',
            identifier : '{$class.identifier}',
            iconFile : '{$class.identifier|class_icon("normal", "", true())}',
            canCreate : {if $canCreateClasses|contains($class.id)}true{else}false{/if},
            isContainer : {$class.is_container}
        {rdelim});
    {/foreach}

    {undef $classes $canCreateClasses}

    {def $sections = fetch('section', 'list', hash('sort by', 'id'))}

    bootstrapper.Sections = {ldelim}{rdelim};

    {foreach $sections as $section}
        bootstrapper.Sections[{$section.id}] = '{$section.name}';
    {/foreach}

    {undef $sections}

    {literal}
    bootstrapper.defaultColumns =
    [
        {
            name : 'Name',
            selected : true,
            key : 'name',
            className : 'kp-table-primary'
        }
    ];

    bootstrapper.images = {x16 : {}, x24 : {}, x32 : {}, x48 : {}, x128 : {}};
    bootstrapper.plupload = {};

    {/literal}
    bootstrapper.iconBaseUrl = "{'images/kp'|ezdesign('no')}";

    bootstrapper.images.x128.Documents = {'kp/128x128/Documents.png'|ezimage};
    bootstrapper.images.x128.Grid = {'kp/128x128/Search.png'|ezimage};
    bootstrapper.images.x128.Settings = {'kp/128x128/Settings.png'|ezimage};
    bootstrapper.images.x128.Layout = {'kp/128x128/Layout.png'|ezimage};
    bootstrapper.images.x128.Loader = {'loader.gif'|ezimage};
    bootstrapper.images.x128.LoaderStack = {'stack-loader.gif'|ezimage};
    bootstrapper.images.x128.DefaultUser = {'default-user.jpg'|ezimage};
    bootstrapper.images.x128.User = {'kp/128x128/User.png'|ezimage};
    bootstrapper.images.x128.Sitemap = {'kp/128x128/Network.png'|ezimage};
    bootstrapper.images.x128['Alert'] = {'kp/128x128/Alert.png'|ezimage};
    bootstrapper.images.x128.StackClose = {'icons/stack-close.png'|ezimage};
    bootstrapper.images.x128.StackBack = {'icons/stack-go-back.png'|ezimage};

    bootstrapper.images.x48.Documents = {'kp/48x48/Documents.png'|ezimage};
    bootstrapper.images.x48.Grid = {'kp/48x48/Search.png'|ezimage};
    bootstrapper.images.x48.Settings = {'kp/48x48/Settings.png'|ezimage};
    bootstrapper.images.x48.Layout = {'kp/48x48/Layout.png'|ezimage};
    bootstrapper.images.x48.Loader = {'loader.gif'|ezimage};
    bootstrapper.images.x48.LoaderStack = {'stack-loader.gif'|ezimage};
    bootstrapper.images.x48.DefaultUser = {'default-user.jpg'|ezimage};
    bootstrapper.images.x48.User = {'kp/48x48/User.png'|ezimage};
    bootstrapper.images.x48.Sitemap = {'kp/48x48/Network.png'|ezimage};
    bootstrapper.images.x48['Alert'] = {'kp/48x48/Alert.png'|ezimage};
    bootstrapper.images.x48.StackClose = {'icons/stack-close.png'|ezimage};
    bootstrapper.images.x48.StackBack = {'icons/stack-go-back.png'|ezimage};

    bootstrapper.images.x32.Documents = {'kp/32x32/Documents.png'|ezimage};
    bootstrapper.images.x32.Grid = {'kp/32x32/Search.png'|ezimage};
    bootstrapper.images.x32.Settings = {'kp/32x32/Settings.png'|ezimage};
    bootstrapper.images.x32.Layout = {'kp/32x32/Layout.png'|ezimage};
    bootstrapper.images.x32.Loader = {'loader.gif'|ezimage};
    bootstrapper.images.x32.LoaderStack = {'stack-loader.gif'|ezimage};
    bootstrapper.images.x32.DefaultUser = {'default-user.jpg'|ezimage};
    bootstrapper.images.x32.User = {'kp/32x32/User.png'|ezimage};
    bootstrapper.images.x32.Sitemap = {'kp/32x32/Network.png'|ezimage};
    bootstrapper.images.x32['Alert'] = {'kp/32x32/Alert.png'|ezimage};
    bootstrapper.images.x32.StackClose = {'icons/stack-close.png'|ezimage};
    bootstrapper.images.x32.StackBack = {'icons/stack-go-back.png'|ezimage};
    bootstrapper.images.x32.Add = {'kp/32x32/Add.png'|ezimage};

    bootstrapper.images.x16.Documents = {'kp/16x16/Documents.png'|ezimage};
    bootstrapper.images.x16.Grid = {'kp/16x16/Search.png'|ezimage};
    bootstrapper.images.x16.Settings = {'kp/16x16/Settings.png'|ezimage};
    bootstrapper.images.x16.Layout = {'kp/16x16/Layout.png'|ezimage};
    bootstrapper.images.x16.Loader = {'loader.gif'|ezimage};
    bootstrapper.images.x16.LoaderStack = {'stack-loader.gif'|ezimage};
    bootstrapper.images.x16.DefaultUser = {'default-user.jpg'|ezimage};
    bootstrapper.images.x16.User = {'kp/16x16/User.png'|ezimage};
    bootstrapper.images.x16.Sitemap = {'kp/16x16/Network.png'|ezimage};
    bootstrapper.images.x16['Alert'] = {'kp/16x16/Alert.png'|ezimage};
    bootstrapper.images.x16.StackClose = {'icons/stack-close.png'|ezimage};
    bootstrapper.images.x16.StackBack = {'icons/stack-go-back.png'|ezimage};
    bootstrapper.images.x16.Add = {'kp/16x16/Add.png'|ezimage};

    bootstrapper.images.x24.List = {'kp/24x24/Ordered-listb.png'|ezimage};
    bootstrapper.images.x24.Thumbnails = {'kp/24x24/Thumbnails.png'|ezimage};
    bootstrapper.images.x24.Sitemap = {'kp/24x24/Network.png'|ezimage};

    bootstrapper.plupload.flash_swf_url = '{'javascript/libs/plupload/Moxie.swf'|ezdesign('no')}';
    bootstrapper.plupload.runtimes = 'html5,flash,html4';

    {if $extensions|contains('ezoe')}
        {def
            $content_css_list_temp = ezini('StylesheetSettings', 'EditorCSSFileList', 'design.ini')
            $content_css_list = array()
            $skin = ezini('EditorSettings', 'Skin', 'ezoe.ini')
            $editor_css_list = array(concat('skins/', $skin, '/ui.css'))
        }

        {foreach $content_css_list_temp as $css}
            {set $content_css_list = $content_css_list|append($css|explode('<skin>')|implode($skin))}
        {/foreach}

        /** EzOE. */
        bootstrapper.eZOeGlobalSettings = {fetch('ezoe', 'get')};

        bootstrapper.eZOeGlobalSettings.theme_ez_editor_css = '{ezcssfiles($editor_css_list, 3, true())|implode(',')}';
        bootstrapper.eZOeGlobalSettings.theme_ez_content_css = '{ezcssfiles($content_css_list, 3, true())|implode(',')}';
        bootstrapper.eZOeGlobalSettings.popup_css = '{concat("stylesheets/skins/", $skin, "/dialog.css")|ezdesign('no')}';

        bootstrapper.eZOeGlobalSettings.ez_root_url = '{'/'|ezroot('no')}';
        bootstrapper.eZOeGlobalSettings.ez_extension_url = '{'/ezoe'|ezroot('no')}';
        bootstrapper.eZOeGlobalSettings.ez_js_url = '{'/extension/ezoe/design/standard/javascript/'|ezroot('no')}';
        bootstrapper.eZOeGlobalSettings.spellchecker_rpc_url = '{'/ezoe/spellcheck_rpc'|ezurl('no')}';
        bootstrapper.eZOeGlobalSettings.atd_rpc_url = '{'/ezoe/atd_rpc?url='|ezurl('no')}';
        bootstrapper.eZOeGlobalSettings.atd_css_url = '{'javascript/plugins/AtD/css/content.css'|ezdesign('no')}';
        bootstrapper.eZOeGlobalSettings.ez_tinymce_url = '{'javascript/tiny_mce.js'|ezdesign('no')}';
    {/if}

    return bootstrapper;

{rdelim}

</script>
