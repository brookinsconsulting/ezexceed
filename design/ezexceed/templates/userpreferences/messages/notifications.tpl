<h2>{'Notifications'|i18n('eze')}</h2>
<label class="checkbox">
	<input
            id="combinedDigest"
            type="checkbox"
            value=""
            name="combinedDigest"
            {if $settings.receive_digest}checked="checked"{/if}
    >
	{'Recieve all notifications in one email'|i18n('eze')}
</label>

<h2>{'Wrap-up email'|i18n('eze')}</h2>

<p>
    <label class="radio">{'Daily, at'|i18n('eze')}</label>

    <input
            type="radio"
            value="3"
            id="digestType3"
            name="digestType"
            {if eq($settings.digest_type, 3)}checked="checked"{/if}
    >
    <select name="dailyTime">
        {foreach $handler.available_hours as $hour}
            <option value="{$hour}" {if eq($hour, $settings.time)}selected="selected"{/if}>{$hour}</option>
        {/foreach}
    </select>
</p>

<p>
    <label class="radio">{'Weekly, on'|i18n('eze')}</label>
    <input
            type="radio"
            value="1"
            id="digestType1"
            name="digestType"
            {if eq($settings.digest_type, 1)}checked="checked"{/if}
    >
    <select name="weekday">
        {foreach $handler.all_week_days as $weekday}
            <option value="{$weekday}" {if eq($weekday, $settings.day)}selected="selected"{/if}>{$weekday}</option>
        {/foreach}
    </select>
</p>

<p>
    <label class="radio">{'Monthly, on day'|i18n('eze')}</label>
    <input
            type="radio"
            value="2"
            id="digestType2"
            name="digestType"
            {if eq($settings.digest_type, 2)}checked="checked"{/if}
    >

    <select name="dayOfMonth">
        {foreach $handler.all_month_days as $dayOfMonth}
            <option value="{$dayOfMonth}" {if eq($dayOfMonth, $settings.day)}selected="selected"{/if}>{$dayOfMonth}</option>
        {/foreach}
    </select>
</p>