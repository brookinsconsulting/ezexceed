{def $collaboration = $handler.collaboration_handlers[0]}
{def $selection = $handler.collaboration_selections}

<h3>{"Collaboration notification"|i18n("design/notification")}</h3>
<p>{"Choose which collaboration items you want to get notifications for:"|i18n("design/notification")}</p>

<ul>
    <li>
        <label for="collaboration">{$collaboration.info.type-name|wash}</label>
        <input id="collaboration" type="checkbox" name="collaboration" value="{$collaboration.info.type-identifier}" {if $selection|contains($collaboration.info.type-identifier)}checked="checked"{/if} />
    </li>
</ul>

{undef $handlers}
{undef $selection}