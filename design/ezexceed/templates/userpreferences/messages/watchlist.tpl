<div class="contentheader">
    <h2>{"Node notification"|i18n("design/standard/notification")}</h2>
</div>

<table class="watchList" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th width="69%">
        {"Name"|i18n("design/standard/notification")}
        </th>
        <th width="30%">
        {"Class"|i18n("design/standard/notification")}
        </th>
        <th width="30%">
        {"Section"|i18n("design/standard/notification")}
        </th>
        <th width="1%">
        {"Select"|i18n("design/standard/notification")}
        </th>
    </tr>

{section name=Rules loop=$handler.rules sequence=array(bgdark,bglight)}
    <tr>
        <td class="{$Rules:sequence}">
            <a href={concat("/content/view/full/",$Rules:item.node.node_id,"/")|ezurl}>
                {$Rules:item.node.name|wash}
            </a>
        </td>

        <td class="{$Rules:sequence}">
            {$Rules:item.node.object.content_class.name|wash}
        </td>

        <td class="{$Rules:sequence}">
            {fetch(section,object,hash(section_id,$Rules:item.node.object.section_id)).name|wash}
        </td>

        <td class="{$Rules:sequence}">
            <button class="remove">x</button>
        </td>
    </tr>
{/section}
</table>
<div class="buttonblock">
    <input class="button" type="submit" name="NewRule_{$handler.id_string}" value="{'New'|i18n('design/standard/notification')}"/>
</div>