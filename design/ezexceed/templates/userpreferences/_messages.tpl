{def $handlerList = fetch('notification','handler_list')}

<form
        class="auto"
        name="notifications"
        method="post"
        action='/UserPreferences/saveMessages'
>
    {def $handler = $handlerList.ezgeneraldigest}
    {def $settings = $handler.settings}
        {include uri='design:userpreferences/messages/notifications.tpl'}
    {undef $settings}
    {undef $handler}

    {*{def $handler = $handlerList.ezcollaborationnotification}*}
        {*{include uri='design:userpreferences/messages/collaboration.tpl'}*}
    {*{undef $handler}*}
</form>

{undef $handlerList}