<div class="eze-autosave">
    <img src="{'images/loader.gif'|ezdesign('no')}" class="icon-16 saving hide" />
    <span class="saved hide">√ {'All changes saved'|i18n('eze')}</span>
    <span class="saving hide">{'Saving'|i18n('eze')}…</span>
</div>

{*<ul class="nav nav-tabs">*}
	{*<li class="active">*}
        {*<a data-toggle="tab" href="#tab1">{'Your profile'|i18n('eze')}</a>*}
    {*</li>*}
	{*<li>*}
        {*<a data-toggle="tab" href="#tab2">{'Notifications'|i18n('eze')}</a>*}
    {*</li>*}
{*</ul>*}

<div class="tab-content">
    <div id="tab1" class="tab-pane active">
        {include uri='design:userpreferences/_properties.tpl'}
    </div>
    {*<div id="tab2" class="tab-pane">*}
        {*{include uri='design:userpreferences/_messages.tpl'}*}
    {*</div>*}
    {if ezini('SiteSettings', 'AdditionalLoginFormActionURL')}
        {def $text = 'Login to eZ Publish Administration Interface'|i18n('eze')}
    <a href="{ezini('SiteSettings', 'AdditionalLoginFormActionURL')}"
        class="btn pull-left"
        title="{$text}">{$text}</a>
    {/if}
    <a class="btn pull-right" href={'/user/logout'|ezurl} title="{'Logout'|i18n('eze')}">{'Logout'|i18n('eze')}</a>
</div>
