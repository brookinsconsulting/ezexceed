{def
    $firstName = $attributes.first_name
    $lastName = $attributes.last_name
    $signature = $attributes.signature
    $auth = $attributes.user_account.content
}

<form class="row-fluid auto"
    method="post"
    action="/UserPreferences/properties"
    name="properties"
>

    <input type="hidden" name="fields[objectId]" value="{$user.contentobject_id}" />

    {if or(is_object($firstName), is_object($signature))}
    <div class="span5">
	{if is_object($firstName)}
        <label for="{$firstName.contentclass_attribute.identifier}">{$firstName.contentclass_attribute.name}</label>
        <input
                type="text"
                name="fields[attributes][{$firstName.contentclass_attribute_identifier}]"
                value="{$firstName.content}"
                tabindex="1"
                placeholder="{'Type something'|i18n('eze')}…"
        />
	{/if}

	{if is_object($signature)}
        <label for="{$signature.contentclass_attribute.identifier}">{$signature.contentclass_attribute.name}</label>
        <input type="text"
            name="fields[attributes][{$signature.contentclass_attribute_identifier}]"
            value="{$signature.content}"
            tabindex="3"
            placeholder="{'Type something'|i18n('eze')}…"
        />
	{/if}
    </div>
    {/if}

    <div class="span5">
	{if is_object($lastName)}
        <label for="{$lastName.contentclass_attribute.identifier}">{$lastName.contentclass_attribute.name}</label>
        <input
                type="text"
                name="fields[attributes][{$lastName.contentclass_attribute_identifier}]"
                value="{$lastName.content}"
                tabindex="2"
                placeholder="{'Type something'|i18n('eze')}…"
        />
	{/if}
    </div>

</form>

<div class="well well-small">
    <form
            id="user-auth"
            method="post"
            action="/UserPreferences/auth/{$user.contentobject_id}"
            name="auth"
            class="row-fluid"
    >
        <fieldset>
        <legend>{'Update login information'|i18n('eze')}</legend>

        <div class="span5">
            <label>{'Username'|i18n('eze')}</label>
            <input id="ez_username"
                    name="username"
                    spellcheck="off"
                    autocorrect="off"
                    type="text"
                    placeholder="{'Type something…'|i18n('eze')}"
                    tabindex="3"
                    value="{$auth.login}"
            />

            <label>{'Password'|i18n('eze')}</label>
            <input
                    type="password"
                    placeholder="{'Type something…'|i18n('eze')}"
                    id="ez_password"
                    autocomplete="off"
                    tabindex="5"
                    name="password"

            />
            <button class="btn save" type="submit" tabindex="7">{'Update login information'|i18n('eze')}</button>
        </div>

        <div class="span5">
            <label>{'Email address'|i18n('eze')}</label>
            <input
                    type="email"
                    placeholder="{'Type something…'|i18n('eze')}"
                    spellcheck="off"
                    autocorrect="off"
                    id="ez_email"
                    name="email"
                    tabindex="4"
                    value="{$auth.email}"
            />

            <label>{'Repeat password'|i18n('eze')}</label>
            <input
                    type="password"
                    placeholder="{'Type something…'|i18n('eze')}"
                    id="ez_repeatpassword"
                    autocomplete="off"
                    tabindex="6"
                    name="repeatpassword"
            />
        </div>
        </fieldset>
    </form>
</div>

{undef $firstName $lastName $signature $auth}
