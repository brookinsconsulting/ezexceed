{def $is_custom = false()}

{if and(eq(ezini($block.type, 'ManualAddingOfItems', 'block.ini'), 'disabled'), ezini_hasvariable($block.type, 'FetchClass', 'block.ini')|not)}
    {set $is_custom = true()}
{/if}

<li data-block-id="{$block.id}">
    <div class="wrap">
        <span class="reorder"></span>
        <input type="text" class="name" placeholder="{'Type a name'|i18n('eze')}" name="name" value="{$block.name|wash}" maxlength="26">
        <span title="{ezini($block.type, 'Name', 'block.ini')|wash}">{ezini($block.type, 'Name', 'block.ini')|wash}</span>


        {def $view_name = ezini($block.type, 'ViewName', 'block.ini')}
        {def $views = ezini($block.type, 'ViewList', 'block.ini')}

        <div class="right-align">
            {if gt($views|count, 1)}
            <select class="chzn-select" name="view">
                {foreach $views as $view}
                    {if eq($views|count, 1)}
                        <option value="">{'Select view'|i18n('design/standard/block/edit')}</option>
                    {/if}
                    <option value="{$view}" {if eq($block.view, $view)}selected="selected"{/if}>{$view_name[$view]}</option>
                {/foreach}
            </select>
            {/if}
            {undef $views}

            {if $is_custom|not}
                <select class="chzn-select blockOverflow" name="overflow_id">
                    <option value="">{'Set overflow'|i18n('eze')}</option>
                    {foreach $zone.blocks as $index => $overflow_block}
                        {if eq($overflow_block.id, $block.id)}
                            {continue}
                        {/if}

                        <option value="{$overflow_block.id}" {if eq($overflow_block.id, $block.overflow_id)}selected="selected"{/if}>{$overflow_block.name|wash}</option>
                    {/foreach}
                </select>
            {/if}

            <a class="btn btn-mini edit-btn" href="">{'Edit'|i18n('eze')}</a>
            <a class="btn btn-mini remove-btn" href="">x</a>
        </div>
    </div>
</li>