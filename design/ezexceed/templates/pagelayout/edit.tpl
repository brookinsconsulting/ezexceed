<section class="eze-layout">
    <h1>{'Choose layout'|i18n('eze')}</h1>

    {if $model.allowedZones|count|gt(0)}
        {foreach $model.allowedZones as $allowedZone}
            {if $allowedZone['classes']|contains($model.objectClassname)}
                <img alt="{$allowedZone.name|wash}"
                    src="{concat( "ezpage/thumbnails/", $allowedZone.thumbnail )|ezimage('no')}"
                    class="zone-type-selector{if eq($model.currentZoneLayout, $allowedZone.type)} active-layout{/if}"
                    data-value="{$allowedZone.type}">
            {/if}
        {/foreach}
    {/if}

    <h2>{'Zones with blocks of content'|i18n('eze')}</h2>
    <p>{'Arrange, add or remove blocks of content inside zones'|i18n('eze')}</p>

    {def $currentZoneNames = ezini($model.currentZoneLayout, 'ZoneName', 'zone.ini')}
    {def $zonenames = array()}

    <div data-toggle="buttons-radio" class="nav btn-group">
        {foreach $model.pageModel.zones as $index => $zone}
            {if $zonenames|contains($zone.zone_identifier)|not}
            {if is_set($currentZoneNames[$zone.zone_identifier])}
                {def $navigationName = $currentZoneNames[$zone.zone_identifier]}
            {else}
                {def $navigationName = $zone.zone_identifier}
            {/if}
            <a data-toggle="tab" data-id="{$index}" href="#tab{sum($index, 1)}" class="btn{if eq($index, 0)} active{/if}">{$navigationName}</a>
            {/if}
            {set $zonenames = $zonenames|append($zone.zone_identifier)}
         {/foreach}
    </div>

    <div class="tab-content">
        {foreach $model.pageModel.zones as $index => $zone}
        <div id="tab{sum($index, 1)}" class="tab-pane{if eq($index, 0)} active{/if}" data-zone-index="{$index}">
            <ul class="eze-items with-dropdown">
                {foreach $zone.blocks as $block}
                    {include uri='design:pagelayout/block.tpl' zone=$zone block=$block objectId=$model.pageModel.objectId}
                {/foreach}
            </ul>

            <p class="no-content{if gt(count($zone.blocks), 0)} hide{/if}">{'No content is added'|i18n('eze')}</p>

            <form class="add-new" action="#">
                <select class="chzn-select" name="addBlock">
                    {foreach $model.blockNames as $type => $name}
                        <option value="{$type}">{$name}</option>
                    {/foreach}
                </select>

                <button class="btn" type="submit">{'Add new block'|i18n('eze')}</button>
            </form>

        </div>
        {/foreach}
    </div>
</section>
