{ezcss_load(array(
    'lib/bootstrap.min.css',
    'eze.css',
    'lib/font-awesome.css',
    'select2/select2.css',
    'lib/custom-theme/jquery-ui-1.8.24.custom.css',
    'lib/jquery.timepicker.css'
))}

{ezcss_load(ezini('Stylesheets', 'Global', 'ezexceed.ini'))}

<!--[if lte IE 9]>
{ezcss_require(array('ie-legacy.css'))}
<![endif]-->

{def
    $renderToolbar = http-header('X-Exceed-RenderToolbar')
    $extensions = ezini('ExtensionSettings', 'ActiveAccessExtensions', 'site.ini')
    $locale = ezini('RegionalSettings', 'ContentObjectLocale')
}

{set $extensions = $extensions|merge(ezini( 'ExtensionSettings', 'ActiveExtensions', 'site.ini'))}

{if $extensions|contains('ezie')}
    {include uri='design:ezie/jscss.tpl'}
    {include uri='design:ezie/gui.tpl'}
{/if}

<div class="eze eze-main">

{if eq($renderToolbar, 'no')|not}

    {if $locale|contains('@')}
        {set $locale = $locale|explode('@')[0]}
    {/if}

    <nav id="eze-toolbar" class="hide"
        data-name="{$content_object|objectname|wash}"
        data-class-id="{$content_object.contentclass_id}"
        data-language="{$locale}"
        data-site-root="{"/"|ezroot('no')}"
        data-object-id="{$content_object.id}"
        data-node-id="{$current_node.node_id}"
        data-node-id-path="{$current_node.path_string}"
    >
        <ul>
            {if fetch('user', 'has_access_to', hash('module', 'user', 'function', 'selfedit'))}
            <li class="eze-user-preferences" data-user-id="{$current_user.contentobject.id}">
                {def $avatar = $current_user|avatar}
                {if $avatar}
                <a><img src="{$avatar}" class="avatar">{$current_user.login}</a>
                {else}
                <a><b class="eze-settings"></b>{$current_user.login}</a>
                {/if}
            </li>
            {/if}

            {if and( $current_node.is_container, $current_node.object.can_create_class_list )}
            <li class="create">
                <a><b class="eze-create"></b>{'Create'|i18n('eze')}</a>
            </li>
            {/if}

            {if fetch('content', 'access', hash('access', 'edit', 'contentobject', $content_object))}
            <li class="edit">
                <a>
                    <b class="eze-document"></b>{'Edit'|i18n('eze')}
                </a>
            </li>
            {/if}

            <li class="find">
                <a>
                    <b class="eze-search"></b>{'Find'|i18n('eze')}
                </a>
            </li>

            <li class="publish">
                <a>
                    <b class="eze-publish"></b>
                    <span class="status-icon"></span>{'Publish'|i18n('eze')}
                </a>
            </li>

            <li class="activity">
                <a>
                    <b class="eze-activity"></b>{'Activity'|i18n('eze')}
                </a>
            </li>

            {* Require ezflow installed, and access to edit of current object in order to show layout button. *}
            {def $exceedlayout = false()
                 $timeline = false()}
            {if $extensions|contains('ezflow')}
            {* Only layout for relevant objects. *}
                {foreach $content_object.data_map as $attribute}
                    {if eq($attribute.data_type_string,'ezpage')}
                        {set $exceedlayout = true()}
                        {set $timeline = true()}
                        {break}
                    {/if}
                {/foreach}
                {if and($exceedlayout, fetch('content', 'access', hash('access', 'edit', 'contentobject', $content_object))|not)}
                    {set $exceedlayout = false()}
                {/if}
            {/if}

            <li class="preview"{if $timeline} data-timeline="1"{/if}>
                <a>
                    <b class="eze-preview"></b>{'Preview'|i18n('eze')}
                </a>
            </li>
            {undef $timeline}

            {if has_access_to_limitation('ezjscore', 'call', hash('FunctionList', array('NodeStructure_get')))}
            <li class="sitemap" data-rootnode="{ezexceedsitemaprootnode()}">
                <a><b class="eze-sitemap"></b>{'Sitemap'|i18n('eze')}</a>
            </li>
            {/if}

            {if $exceedlayout}
            <li class="layout">
                <a>
                    <b class="eze-layout"></b>{'Zones'|i18n('eze')}
                </a>
            </li>
            {/if}

            {undef $exceedlayout}

            {* Custom buttons defined by other extensions *}
            {def $buttons = ezini('Toolbar', 'Buttons', 'ezexceed.ini') }
            {foreach $buttons as $button}
                {include uri="design:global/button.tpl" button=$button}
            {/foreach}
            {undef $buttons}
        </ul>
    </nav>
    {undef $locale}

    <div id="exceed-jui-container" class="exceed-jui"></div>

    {include uri="design:global/bootstrap.tpl" extension=$extensions}

    {undef $extensions}
{else}
{literal}
<style>body{margin-left: 0;}</style>
{/literal}
{/if}

    <span id="exceed-native-exchange"
        data-objectid="{$content_object.id}"
        data-nodeid="{$current_node.node_id}"
        data-version="{$current_node.contentobject_version}"
        data-nodeidpath="{$current_node.path_string}">
    </span>
</div>
