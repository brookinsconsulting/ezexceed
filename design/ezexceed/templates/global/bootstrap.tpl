{* Needs to load yui2 for ezdatetime attribute to work *}
{ezscript(array('ezjsc::yui2'))}

{def
    $js = ezini('Javascripts', 'Initializer', 'ezexceed.ini')
    $ns = ezini('AMD', 'NS', 'ezexceed.ini')
}

{if $js}
{ezscript($js)}
{/if}

<script>
    var compiled = "{ezini('eZJSCore', 'Packer', 'ezjscore.ini')}" === "enabled";
{literal}
    var user = {
{/literal}
        username: "{$current_user.login}",
        email: "{$current_user.email}",
        name: "{$current_user.contentobject.name}"
{literal}
    };
    var moduleFolder = 'src/';
    if (compiled) moduleFolder = 'compiled/' + moduleFolder;

    if (typeof require === 'undefined') var require = {};

    require.baseUrl = '/extension/ezexceed/design/ezexceed/javascript/' + moduleFolder;

    require.paths = {
        'stack' : 'shared/stack',
        'modal' : 'shared/modal',
        'selectable' : 'shared/selectable',
        'timepicker' : 'shared/timepicker',
        'funky' : 'shared/funky',

        'jquery' : '../libs/jquery-1.9.1',
        'modernizr' : '../libs/modernizr',
        'moment' : '../libs/moment',
        'jquerypp' : '../libs/jquerypp',
        'jquery-safe' : '../libs/jquery.noconflict',
        'jqueryui' : '../libs/jqueryui',
        'jquery-ui.touch' : '../libs/jquery.ui.touch-punch.min',
        'select2' : '../libs/select2/select2',
        'underscore' : '../libs/lodash',
        'plupload' : '../libs/plupload',
        'backbone.touch' : '../libs/backbone.touch',
        'backbone.keys' : '../libs/backbone.keys',
        'backbone' : '../libs/backbone',
        'handlebars' : '../libs/handlebars.runtime',
        'jquery.timepicker' : '../libs/jquery.timepicker',
        'jquery.placeholder' : '../libs/jquery.placeholder.min'
    };
    {/literal}
    {* Extensions providing other namespaces for AMD *}
    {foreach $ns as $key => $path}
    require.paths['{$key}'] = '{$path}';
    {/foreach}
    {literal}

    require.map = {
        '*' : {
            'edit/datatypes/base' : 'shared/datatype'
        },
        'jquerypp/form_params' : {
            'jquery' : 'jquery-safe'
        },
        'select2' : {
            'jquery' : 'jquery-safe'
        },
        'jquery.placeholder' : {
            'jquery' : 'jquery-safe'
        },
        'jquery.timepicker' : {
            'jquery' : 'jquery-safe'
        }
    };

    require.shim = {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps: ['underscore', 'jquery-safe'],
            exports: 'Backbone'
        },
        'backbone.touch': {
            deps: ['backbone'],
            exports: 'Backbone'
        },
        'backbone.keys': {
            deps: ['backbone'],
            exports: 'Backbone'
        },
        'handlebars': {
            exports: 'Handlebars'
        },
        'plupload/moxie' : {
            exports: 'mOxie'
        },
        'plupload/plupload': {
            deps: ['plupload/moxie'],
            exports: 'plupload'
        },
        'jqueryui' : ['jquery'],
        'jquery-ui.touch' : {
            deps: ['jqueryui/core']
        },
        'jquery.timepicker': {
            exports: 'jQuery.fn.timepicker'
        },
        'jquery.placeholder' : {
            exports : 'jQuery.fn.placeholder'
        }
    };

    require.callback = function()
    {
        require(['jquery-safe', 'ezexceed/main'], function($, App)
        {
            if (typeof window.onexceed === 'function') {
                var cb = window.onexceed;
            }

            var ezFormToken = $('#ezxform_token_js').attr('title');
            window.eZExceed = App;
            window.eZExceed.formtoken = ezFormToken;
            window.eZExceed.user = user;
            window.eZExceed.init.call(App, {
                $doc : $(document),
                root : $('#eze-toolbar').data('siteRoot')
            });
            window.eZExceed.showExceed();

            window.eZExceed.override = function(paths) {
                require.config({
                    paths: paths
                });
            }
            if (window._eze) {
                var key;
                for (key in window._eze) {
                    eZExceed.override(window._eze[key]);
                }
            }
            window._eze = {
                push: eZExceed.override
            };
            if (cb) cb(eZExceed);
        });
    };

    if (!compiled) {
        require.urlArgs = "bust=" +  (new Date()).getTime();
        require.deps = ['handlebars'];
    }
    else {
        require.urlArgs = "v2.0.8";
    }
{/literal}
</script>
<script src={'javascript/libs/require.js'|ezdesign}></script>

{* EzOE. *}
{if $extensions|contains('ezoe')}
    {def $dependency_js_list = array('tiny_mce_jquery.js')
        $plugins = ezini('EditorSettings', 'Plugins', 'ezoe.ini')
    }
    {foreach $plugins as $plugin}
        {set $dependency_js_list = $dependency_js_list|append(concat('plugins/', $plugin|trim, '/editor_plugin.js'))}
    {/foreach}

    <span id="ezexceed-ezoe" class="hide">{foreach ezscriptfiles($dependency_js_list, 2, true()) as $js}{$js},{/foreach}</span>
    {undef $dependency_js_list $plugins }
{/if}

{* Javascript - Injections. *}
{include uri='design:js/jsinjection.tpl'}

<script>
{literal}
    // Quickly pad and make room for toolbar to avoid flicking once
    // javascript loading is done
    if (!document.getElementById('eze-toolbar')) {
        document.body.style.marginLeft = 0;
    }
    else {
        var margin = document.body.style.marginLeft;

        margin = margin !== '' ? parseInt(margin, 10) : 0;
        if (margin !== 63) {
            document.body.setAttribute('data-margin-left', margin);
            margin = margin + 63;

            document.body.style.marginLeft = margin;
            document.getElementById('eze-toolbar').setAttribute('class', '');
        }
    }
{/literal}
</script>
