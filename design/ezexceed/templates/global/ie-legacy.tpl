{literal}
<!--[if IE]>
    <style type="text/css">
        /* Remove unsupported default checkbox style */
        .kp input[type="checkbox"] { background: none; }

        /* Add colors and png, no support for rgba */
        .kp .button, .kp button { color: #000; }
        .kp #kp-page-actions .kp-toolbar-actions li button { color: #fff; }
        .kp ul.kp-nav2 .kp-user-menu { border-left: 1px solid #444; }
        .kp ul.kp-nav2 .chatter { border-right: 1px solid #000; border-left: 1px solid #444; }
        .kp ul.kp-nav2 .kp-toolbar-sitemap { border-right: 1px solid #000; }
        .kp .sidebar-edit .container.publish-container, .kp .sidebar-edit .container .state-group, .kp .sidebar-edit .container .locations li { border-bottom: 1px solid rgb(220,220,220);}
        .kp .sidebar-edit .container .locations { border-top: 1px solid rgb(220,220,220); }
        .kp .kp-toolbar-top, .kp#kp-toolbar .kp-toolbar-bottom { border-bottom: 1px solid rgb(91,91,91);}
        ul.kp-nav li.active a { border-right: 1px solid rgb(91,91,91); border-left: 1px solid rgb(91,91,91); }
        ul.kp-nav li.active a { background: #242424; }
        
        
        .kp .attribute.ezimage .image-container .button, 
        .kp .attribute.ezimage .image-container button, 
        .kp .attribute.keymedia .image-container .button, 
        .kp .attribute.keymedia .image-container button {
            background: #333;
            border: 1px solid #000;
        }
        

        /* Adding opacity filter hacks */
        .kp .classlist li .layoutContainer { -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; }

        /* Bootstrap runover */
        .kp header { filter: none; }

        /* Misc. fixes */
        .kp ul.kp-nav2 .kp-user-menu ul { top: 44px; }
        .kp .primary-edit { border-right: 1px solid #999; }
        .kp .sidebar-edit { overflow-y: scroll; overflow-x: hidden;}

        /* IE6-8 doesnt support last child */
        .kp .segments { border-right: 1px solid #ccc }

        /* IE doesnt support shadows */
        .inline-edit.kp-modal, #kp-modal-backdrop.regular #kp-modal, #kp-modal-backdrop.regular #kp-modal, .kp .small.kp-modal, #kp-modal-backdrop .close-modal, .kp .autosave .save-status {border:1px solid #666}

        /* IE doesnt know all selectors, durr */
        .kp #kp-page-actions .kp-toolbar-actions li .kp-modal button { color: #333 }


    </style>
<![endif]-->
{/literal}
