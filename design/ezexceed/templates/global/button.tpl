{def 
    $lookup = concat('Toolbar_', $button)
    $name = ezini($lookup, 'Name', 'ezexceed.ini')
    $icon = ezini($lookup, 'Icon', 'ezexceed.ini')
    $url = ezini($lookup, 'URL', 'ezexceed.ini')
    $type = ezini($lookup, 'Type', 'ezexceed.ini')
    $module = ezini($lookup, 'Module', 'ezexceed.ini')
}
<li class="{$button} eze-{$type} extension">

    <a {if $url}href="{$url}"{/if}
        {if $module}data-module="{$module}"{/if}
        data-type="{$type}"
        data-name="{$name}"
        data-icon="{$icon}"
        data-class="{$button}">
        <img src={concat('kp/128x128/white/', $icon, '.png')|ezimage} alt="{$name}">{$name|i18n}
    </a>
</li>
