<li data-id="{$object.id}" class="object-by-id-{$object.id}">
    <div class="wrap">

        {if $sortable}
        <span class="reorder"></span>
        {/if}
        <img src="{$object.class_identifier|class_icon('small', '', true)}" class="icon-16" />
        <small>{$object.class_name|wash}</small>
        <span class="object-name">{$object|objectname|wash}</span>

        <div class="right-align">
            {if $innerTpl}
                {include uri=$innerTpl}
            {/if}

            <a class="btn btn-mini edit-btn"
                data-id="{$object.id}"
                data-type="{$object.class_name|wash}"
                data-class-identifier="{$object.class_identifier}"
                data-class-id="{$object.contentclass_id}"
                data-name="{$object|objectname|wash}"
            >
                {'Edit'|i18n('eze')}
            </a>
            <a class="btn btn-mini remove-btn remove" data-object-id="{$object.id}">x</a>
        </div>
    </div>
</li>
