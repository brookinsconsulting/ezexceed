{if $jsFiles|contains('https://www.google.com/jsapi')|not}
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
{/if}

{* Load JavaScript dependencys + JavaScriptList *}

{ezscript_load(ezini('JavaScriptSettings', 'HeaderJavaScriptList', 'design.ini'))}

<!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

{* IE-legacy code. Fix bug in IE-css-loading for tinyMCE. Then remove. *}
<!--[if IE]>
{def $extensions = ezini( 'ExtensionSettings', 'ActiveAccessExtensions', 'site.ini')}
{set $extensions = $extensions|merge(ezini( 'ExtensionSettings', 'ActiveExtensions', 'site.ini'))}
{if $extensions|contains('ezoe')}
    {def
        $skin = ezini('EditorSettings', 'Skin', 'ezoe.ini')
        $css_files = array(concat('skins/', $skin, '/ui.css'))
    }

    {set $css_files = $css_files|append(concat('skins/', $skin, '/dialog.css'))}

    {foreach ezini('StylesheetSettings', 'EditorCSSFileList', 'design.ini') as $css}
        {set $css_files = $css_files|append($css|explode('<skin>')|implode($skin))}
    {/foreach}

    {ezcss_require($css_files)}

    {undef $css_files}
{/if}
{undef $extensions}
<![endif]-->

<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0, user-scalable=0" />
