eZExceed
========

A fresh and modern CMS interface built on top of eZ Publish

## Developer documentation
Check out the [eZExceed guide](http://keyteqlabs.github.io/ezexceed-guide) for details
on what you should do to make editing in eZ Exceed / eZ as great an experience as possible.

## Dependencies

* eZ 4.6 or higher
* ezwebin extension
* ezjscore extension
* PHP 5.3
* Mysql 5
* Web server of your liking

## Installation

See ./INSTALL.md

## Known issues

* Keeping your install outside of root (/) can cause errors inside `ezoe`
* If the ezdemo design has higher priority than `ezexceed` design it will overwrite, and break, the datetime attribute type
