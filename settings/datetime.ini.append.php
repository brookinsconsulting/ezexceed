<?php /**

[ClassSettings]
Formats[js_iso8601]=yy-mm-dd
Formats[iso8601]=%Y-%m-%d
Formats[no_common]=%d.%m.%Y
Formats[js_no_common]=dd.mm.yy
