<?php

/**

[eZJSCore]

LoadFromCDN=disabled

[ezjscServer]

FunctionList[]=EndpointGenerator_writeEndpoints
FunctionList[]=Router_handle
FunctionList[]=Router_handleEZXFormToken
FunctionList[]=Router_getEzxFormToken
FunctionList[]=Router_handleModule
FunctionList[]=SearchHandler_search
FunctionList[]=SearchHandler_history
FunctionList[]=SearchHandler_clearHistory
FunctionList[]=Activity_getGlobalCount
FunctionList[]=Activity_activityEndpoint
FunctionList[]=Activity_getStickies
FunctionList[]=Activity_mentions
FunctionList[]=Activity_setLastRead
FunctionList[]=Activity_getUnreadCount
FunctionList[]=Activity_getUnreadCountNumber
FunctionList[]=Activity_lookUpUser
FunctionList[]=Activity_handlePushQueue
FunctionList[]=Activity_submitNewActivity
FunctionList[]=Activity_addSticky
FunctionList[]=Activity_removeSticky
FunctionList[]=Activity_unlikeActivity
FunctionList[]=Activity_likeActivity
FunctionList[]=Activity_deleteActivity
FunctionList[]=AddContent_get
FunctionList[]=Block_edit
FunctionList[]=Block_addItem
FunctionList[]=Block_renderPencil
FunctionList[]=Block_removeItem
FunctionList[]=Block_saveRotationAttribute
FunctionList[]=Block_saveFetchAttribute
FunctionList[]=Block_saveCustomAttribute
FunctionList[]=Block_sortItem
FunctionList[]=Block_setPublicationTimestamp
FunctionList[]=Device_register
FunctionList[]=Device_imageUpload
FunctionList[]=Device_unregister
FunctionList[]=Device_notifyLike
FunctionList[]=Device_notifyReply
FunctionList[]=Device_notifyMention
FunctionList[]=Device_ready
FunctionList[]=Device_isLoggedIn
FunctionList[]=Device_find
FunctionList[]=Device_preview
FunctionList[]=Device_follow
FunctionList[]=Device_logout
FunctionList[]=Device_login
FunctionList[]=ObjectEdit_get
FunctionList[]=ObjectEdit_getAttribute
FunctionList[]=ObjectEdit_getSubtree
FunctionList[]=ObjectEdit_tinyMceLanguagePack
FunctionList[]=Pagelayout_initialize
FunctionList[]=Pagelayout_edit
FunctionList[]=Pagelayout_moveBlock
FunctionList[]=Pagelayout_removeBlock
FunctionList[]=Pagelayout_addBlock
FunctionList[]=Pagelayout_autosaveBlockAttribute
FunctionList[]=Pagelayout_autosaveLayout
FunctionList[]=Publish_run
FunctionList[]=Publish_status
FunctionList[]=UserPreferences_get
FunctionList[]=UserPreferences_properties
FunctionList[]=UserPreferences_auth
FunctionList[]=UserPreferences_saveMessages
FunctionList[]=NodeStructure_get
FunctionList[]=NodeStructure_toggleVisibility
FunctionList[]=NodeStructure_copy
FunctionList[]=NodeStructure_delete
FunctionList[]=NodeStructure_setSort
FunctionList[]=NodeStructure_move
FunctionList[]=Object_save
FunctionList[]=Object_get
FunctionList[]=Object_find
FunctionList[]=Object_authors
FunctionList[]=Object_statistics
FunctionList[]=Object_remove
FunctionList[]=Object_removeUnused
FunctionList[]=Object_removeInternalDrafts
FunctionList[]=Object_removeDrafts
FunctionList[]=Object_publish
FunctionList[]=Object_create
FunctionList[]=Object_findKeywords
FunctionList[]=Object_preferences
FunctionList[]=Object_restore
FunctionList[]=Preview_url

[ezjscServer_EndpointGenerator]
Class=ezexceed\classes\ote\EndpointGenerator

Functions[]=EndpointGenerator_writeEndpoints

[ezjscServer_Router]
Class=ezexceed\classes\ote\Router

Functions[]=Router_handle

[ezjscServer_Router]
Class=ezexceed\classes\ote\Router

Functions[]=Router_handleEZXFormToken

[ezjscServer_Router]
Class=ezexceed\classes\ote\Router

Functions[]=Router_getEzxFormToken

[ezjscServer_Router]
Class=ezexceed\classes\ote\Router

Functions[]=Router_handleModule

[ezjscServer_SearchHandler_search]
Class=ezexceed\modules\SearchHandler\SearchHandler

Functions[]=SearchHandler_search

[ezjscServer_SearchHandler_history]
Class=ezexceed\modules\SearchHandler\SearchHandler

Functions[]=SearchHandler_history

[ezjscServer_SearchHandler_clearHistory]
Class=ezexceed\modules\SearchHandler\SearchHandler

Functions[]=SearchHandler_clearHistory

[ezjscServer_Activity_getGlobalCount]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_getGlobalCount

[ezjscServer_Activity_activityEndpoint]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_activityEndpoint

[ezjscServer_Activity_getStickies]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_getStickies

[ezjscServer_Activity_mentions]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_mentions

[ezjscServer_Activity_setLastRead]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_setLastRead

[ezjscServer_Activity_getUnreadCount]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_getUnreadCount

[ezjscServer_Activity_getUnreadCountNumber]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_getUnreadCountNumber

[ezjscServer_Activity_lookUpUser]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_lookUpUser

[ezjscServer_Activity_handlePushQueue]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_handlePushQueue

[ezjscServer_Activity_submitNewActivity]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_submitNewActivity

[ezjscServer_Activity_addSticky]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_addSticky

[ezjscServer_Activity_removeSticky]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_removeSticky

[ezjscServer_Activity_unlikeActivity]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_unlikeActivity

[ezjscServer_Activity_likeActivity]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_likeActivity

[ezjscServer_Activity_deleteActivity]
Class=ezexceed\modules\activity\Activity

Functions[]=Activity_deleteActivity

[ezjscServer_AddContent_get]
Class=ezexceed\modules\add_content\AddContent

Functions[]=AddContent_get

[ezjscServer_Block_edit]
Class=ezexceed\modules\block\Block

Functions[]=Block_edit

[ezjscServer_Block_addItem]
Class=ezexceed\modules\block\Block

Functions[]=Block_addItem

[ezjscServer_Block_renderPencil]
Class=ezexceed\modules\block\Block

Functions[]=Block_renderPencil

[ezjscServer_Block_removeItem]
Class=ezexceed\modules\block\Block

Functions[]=Block_removeItem

[ezjscServer_Block_saveRotationAttribute]
Class=ezexceed\modules\block\Block

Functions[]=Block_saveRotationAttribute

[ezjscServer_Block_saveFetchAttribute]
Class=ezexceed\modules\block\Block

Functions[]=Block_saveFetchAttribute

[ezjscServer_Block_saveCustomAttribute]
Class=ezexceed\modules\block\Block

Functions[]=Block_saveCustomAttribute

[ezjscServer_Block_sortItem]
Class=ezexceed\modules\block\Block

Functions[]=Block_sortItem

[ezjscServer_Block_setPublicationTimestamp]
Class=ezexceed\modules\block\Block

Functions[]=Block_setPublicationTimestamp

[ezjscServer_Device_register]
Class=ezexceed\modules\device\Device

Functions[]=Device_register

[ezjscServer_Device_imageUpload]
Class=ezexceed\modules\device\Device

Functions[]=Device_imageUpload

[ezjscServer_Device_unregister]
Class=ezexceed\modules\device\Device

Functions[]=Device_unregister

[ezjscServer_Device_notifyLike]
Class=ezexceed\modules\device\Device

Functions[]=Device_notifyLike

[ezjscServer_Device_notifyReply]
Class=ezexceed\modules\device\Device

Functions[]=Device_notifyReply

[ezjscServer_Device_notifyMention]
Class=ezexceed\modules\device\Device

Functions[]=Device_notifyMention

[ezjscServer_Device_ready]
Class=ezexceed\modules\device\Device

Functions[]=Device_ready

[ezjscServer_Device_isLoggedIn]
Class=ezexceed\modules\device\Device

Functions[]=Device_isLoggedIn

[ezjscServer_Device_find]
Class=ezexceed\modules\device\Device

Functions[]=Device_find

[ezjscServer_Device_preview]
Class=ezexceed\modules\device\Device

Functions[]=Device_preview

[ezjscServer_Device_follow]
Class=ezexceed\modules\device\Device

Functions[]=Device_follow

[ezjscServer_Device_logout]
Class=ezexceed\modules\device\Device

Functions[]=Device_logout

[ezjscServer_Device_login]
Class=ezexceed\modules\device\Device

Functions[]=Device_login

[ezjscServer_ObjectEdit_get]
Class=ezexceed\modules\objectedit\ObjectEdit

Functions[]=ObjectEdit_get

[ezjscServer_ObjectEdit_getAttribute]
Class=ezexceed\modules\objectedit\ObjectEdit

Functions[]=ObjectEdit_getAttribute

[ezjscServer_ObjectEdit_getSubtree]
Class=ezexceed\modules\objectedit\ObjectEdit

Functions[]=ObjectEdit_getSubtree

[ezjscServer_ObjectEdit_tinyMceLanguagePack]
Class=ezexceed\modules\objectedit\ObjectEdit

Functions[]=ObjectEdit_tinyMceLanguagePack

[ezjscServer_Pagelayout_initialize]
Class=ezexceed\modules\pagelayout\Pagelayout

Functions[]=Pagelayout_initialize

[ezjscServer_Pagelayout_edit]
Class=ezexceed\modules\pagelayout\Pagelayout

Functions[]=Pagelayout_edit

[ezjscServer_Pagelayout_moveBlock]
Class=ezexceed\modules\pagelayout\Pagelayout

Functions[]=Pagelayout_moveBlock

[ezjscServer_Pagelayout_removeBlock]
Class=ezexceed\modules\pagelayout\Pagelayout

Functions[]=Pagelayout_removeBlock

[ezjscServer_Pagelayout_addBlock]
Class=ezexceed\modules\pagelayout\Pagelayout

Functions[]=Pagelayout_addBlock

[ezjscServer_Pagelayout_autosaveBlockAttribute]
Class=ezexceed\modules\pagelayout\Pagelayout

Functions[]=Pagelayout_autosaveBlockAttribute

[ezjscServer_Pagelayout_autosaveLayout]
Class=ezexceed\modules\pagelayout\Pagelayout

Functions[]=Pagelayout_autosaveLayout

[ezjscServer_Publish_run]
Class=ezexceed\modules\publish\Publish

Functions[]=Publish_run

[ezjscServer_Publish_status]
Class=ezexceed\modules\publish\Publish

Functions[]=Publish_status

[ezjscServer_UserPreferences_get]
Class=ezexceed\modules\user_preferences\UserPreferences

Functions[]=UserPreferences_get

[ezjscServer_UserPreferences_properties]
Class=ezexceed\modules\user_preferences\UserPreferences

Functions[]=UserPreferences_properties

[ezjscServer_UserPreferences_auth]
Class=ezexceed\modules\user_preferences\UserPreferences

Functions[]=UserPreferences_auth

[ezjscServer_UserPreferences_saveMessages]
Class=ezexceed\modules\user_preferences\UserPreferences

Functions[]=UserPreferences_saveMessages

[ezjscServer_NodeStructure_get]
Class=ezexceed\modules\nodestructure\NodeStructure

Functions[]=NodeStructure_get

[ezjscServer_NodeStructure_toggleVisibility]
Class=ezexceed\modules\nodestructure\NodeStructure

Functions[]=NodeStructure_toggleVisibility

[ezjscServer_NodeStructure_copy]
Class=ezexceed\modules\nodestructure\NodeStructure

Functions[]=NodeStructure_copy

[ezjscServer_NodeStructure_delete]
Class=ezexceed\modules\nodestructure\NodeStructure

Functions[]=NodeStructure_delete

[ezjscServer_NodeStructure_setSort]
Class=ezexceed\modules\nodestructure\NodeStructure

Functions[]=NodeStructure_setSort

[ezjscServer_NodeStructure_move]
Class=ezexceed\modules\nodestructure\NodeStructure

Functions[]=NodeStructure_move

[ezjscServer_Object_save]
Class=ezexceed\modules\content\Object

Functions[]=Object_save

[ezjscServer_Object_get]
Class=ezexceed\modules\content\Object

Functions[]=Object_get

[ezjscServer_Object_find]
Class=ezexceed\modules\content\Object

Functions[]=Object_find

[ezjscServer_Object_authors]
Class=ezexceed\modules\content\Object

Functions[]=Object_authors

[ezjscServer_Object_statistics]
Class=ezexceed\modules\content\Object

Functions[]=Object_statistics

[ezjscServer_Object_remove]
Class=ezexceed\modules\content\Object

Functions[]=Object_remove

[ezjscServer_Object_removeUnused]
Class=ezexceed\modules\content\Object

Functions[]=Object_removeUnused

[ezjscServer_Object_removeInternalDrafts]
Class=ezexceed\modules\content\Object

Functions[]=Object_removeInternalDrafts

[ezjscServer_Object_removeDrafts]
Class=ezexceed\modules\content\Object

Functions[]=Object_removeDrafts

[ezjscServer_Object_publish]
Class=ezexceed\modules\content\Object

Functions[]=Object_publish

[ezjscServer_Object_create]
Class=ezexceed\modules\content\Object

Functions[]=Object_create

[ezjscServer_Object_findKeywords]
Class=ezexceed\modules\content\Object

Functions[]=Object_findKeywords

[ezjscServer_Object_preferences]
Class=ezexceed\modules\content\Object

Functions[]=Object_preferences

[ezjscServer_Object_restore]
Class=ezexceed\modules\content\Object

Functions[]=Object_restore

[ezjscServer_Preview_url]
Class=ezexceed\modules\preview\Preview

Functions[]=Preview_url

*/
