<?php

/**

[SiteSettings]
# Site index public available. For changes to take effect, the search
# index must be updated by running bin/php/updatesearchindex.php
IndexPubliclyAvailable=enabled
# Search other installations
SearchOtherInstallations=enabled

[LanguageSearch]
SearchMainLanguageOnly=disabled

*/

?>