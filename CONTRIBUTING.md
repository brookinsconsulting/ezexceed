Submitting an issue
===================

Your feedback is much appreciated, we just ask you go through this little checklist first:

1. Make sure your issue is reproduced in a supported server environment and browser version
2. Read through the [eZ Exceed guide](https://github.com/KeyteqLabs/ezexceed-guide/blob/master/en/README.md)
3. Search through old issues and see that you are not re-submitting an old issue
4. Include as much information as possible in the issue:
  * Write a clear and concise description of the issue
  * The server setup
  * Browser version + extensions, OS version, device type
  * Screenshots or video for bonus points

Submitting a Pull Request
=========================

While we appreciate feedback, we absolutely love pull requests!
Follow this checklist:

1. Fork the repo
2. Run the tests
3. Create a new testcase proving the bug / feature
4. Implement the fix / feature
5. Ensure you follow the coding styles and conventions of the project
6. Push to your fork and submit a pull request.
