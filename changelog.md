## 2.0.8

* Makes autosave less sensitive to false positives

## 2.0.7

* Fix popover position when scrolled down on page

## 2.0.6

* Fixes broken insert link for ezxmltext

## 2.0.5

* Better error message for 504 responses due to badly tuned gateways
* Squashes the long lasting "saved information disappearing" bug
* Increases eZPublish5 compliance for badly generated root urls to /_fragment
* Add urls to errorception meta data 

## 2.0.4

* Fixes error when returning from sitemap initiated by "Add to block"

## 2.0.3

* Fixes broken Add to block from pencil

## 2.0.2

* Fixes broken support for some Exceed extensions relying on edit/datatypes/base js module

## 2.0.1

* Fixes file uploads in edit for IE 8-9

## 2.0.0

### Features

* Toolbar applications. Add buttons to toolbar and load custom html/js

### Bugfixes

* Fixes date/time picker for layout blocks
* Replace imagine phar by inlining imagine source code for ez cloud compatability

### Misc

* Pass the same args to stack.popped event as destruct on stackItem
* Move edit/datatypes/base to shared/datatype
* Split into three js modules: libs, shared, ezexceed
* Add support for directly loading helveticons in {{icon name}} handlebars helper
* Removes a bunch of unused old template files
* Made toolbar scrollable for sites with many buttons, or low resolution


## 2.0.0 RC 4

### Bugfixes

* Reordering of block content didnt work
* Fixes Find filters that did not render properly
* Sandboxed Handlebar template usage
* Fixes js error in ezxmltext
* Fixes comment rendering for activity

## 2.0.0 RC 3

### Bugfixes

* window.JST global namespace is not used anymore
* Fixed a JS error when adding a block to a zone
* Compatability fix for thumb generation to overcome an ezpublish 5 bug

## 2.0.0 RC 2

### Bugfixes

* Fixes an issue where some nodes where shown as not accessible to end users

## 2.0.0 RC 1

### Features

* Sitemap breadcrumb is made clickable

### Bugfixes

* Fixes invisible language selector in Edit on some sites
* Fixes inivisble delete confirmation in sitemap
* ezstarrating is now saved properly
* Fixes ezimage upload in IE8
* eztext attribute now gets its missing linebreaks
* Fixes autosave in IE8 on several datatypes
* Give feedback when user updates credentials
* Empty name of root node in sitemap no longer crashes
* Positioning of silver pencil popover is no longer outside viewport
* Autosave errors in Edit are back again
* Show correct KeyMedia thumb based on language
* Remove object from My Drafts after publishing
* ezxmltext now works on touch on all sites

### Misc

* Dont allow changing users username to blank
* Empty password fields after changing passwords for user
* Removes placeholder fallback in IE8
* Better default autosave handling for basic datatypes
* Upgrade to jquery 1.9
* Upgrade to plupload 2.0 alpha

## 2.0.0 beta 12

### Features

* You can now create sub content directly from Edit
* Show object thumb in Sitemap

### Bugfixes

* Fixes, and improves, broken thumb generation in Find
* Show an alert to the user when a server call crashes
* Sorting ezobjectrelationlist data now works for many items

### Misc

* Removing locations in Edit is now faster
* Locations in Edit is scrollabl when more than 9
* Optimisations and improvements to Sitemap destruct
* Added missing translations

## 2.0.0 beta 11

2013-01-24

* Fixes ezimage upload with ezformtoken
* Fixes arrow navigation within text fields in Edit
* Major overhaul of Sitemap fixes a number of edge case bugs
* Adds a loader when posting activity

## 2.0.0 beta 10

2013-01-16

* Added support for ezflow timeline
* Fixes sorting bug when saving ezobjectrelationlist attributes

## 2.0.0 beta 9

2013-01-15

### Features

* Added multiupload in Sitemap
* Adds Trash in Find
* Adds a root selector in Sitemap for browsing Media

### Bugfixes

* Fixes setting publication time on ezflow block items
* Set default half/full width for attributes in Edit

## 2.0.0 beta 8

2013-01-10

* Support eZ Publish 5
* Object relation add dialog now filters for class constraints
* Fixes User preferences rendering non-existant fields

## 2.0.0 beta 7

2013-01-04

* Fixes paging in Find
* Fixes load more in Activity

## 2.0.0 beta 6

2012-12-19

* Notify user of required page load after editing the current page
* Add support for sorting subcontent for all locations in Edit
* Support for "user:" and wildcards in Find search
* Allow the user to log back in without losing any work when being logged out by system
* Correct URL for locations link in Find

## 2.0.0 beta 5

2012-12-13

* Revert changes function in Edit
* Remember closed/opened attribute groups in Edit for user
* Pre-select the changed languages in Edit when going from Find->My drafts
* Only ever show autosave on last stack item
* Reset scroll after applying filter in Find

## 2.0.0 beta 4

2012-11-30

* Added a publish status indicator in toolbar
* Dont set focus to activity when opening Edit
* Fixes IE 8 compability mode breaking Exceed

## 2.0.0 beta 3

2012-11-29

### Bugfixes

* Default VAT-type was not preselected
* Removes exceed favicon
* Edit permissions are now honoured in Find
* Fixes correct attribute wrapping in Edit for half/full width attributes
* Fixes remembering scroll position in Edit when going back in stack
* Remove device from site when the site is removed in-app

### Misc

* Further speed optimisations of Sitemap
* Added loaders in Sitemap
* Optimised Edit loading when object was placed on hundreds of nodes
* Optimise thumbs shown in Find


## 2.0.0 beta 2

2012-11-12

### Features

* Setting main node in Edit
* Retina graphics on retina devices
* Show publish status in sitemap
* Flash "Close all" in stack when someone tries to implicitly close a multi-stack
* Added create new in sitemap
* Show users avatar in toolbar
* Devices can now be removed
* ezxmltext editor can now be disabled

### Optimisations

* Major optimisations of Find and Sitemap
* Non-droppable nodes are greyed out when dragging in Sitemap

### Bugfixes

* Fixed setting object relations on touch devices
* Fixed add to block so it respects allowed classes for that block
* Fixed last published time that was erroneous
* Fixed matrix datatype
* Fixed so edit does not create too many versions
* Fixed sitemap auto scroll when using keyboard navigation
* Delete newly created draft after edit if it has no changes
* Close stack when clickin again in toolbar

### Misc

* Increase number of activity items shown
* White edit popovers are now moveable
* Improved usability for block pencils
* Removes spellchecking on email, url and user attributes
* Show a warning when trying to delete a subtree from sitemap
* Better go-back buttons in stack
* Add a iOS app download link

## 2.0.0 beta 1

2012-10-19

eZ Exceed 2.0.0 is a complete re-architecting of the code base as well as markup.
Its now rewritten to be based on top of Twitter Bootstrap, require.js and a completely sandboxed
environment when it comes to javascript code.

### Features

* Completely new left aligned toolbar
* New search functionality through Find

## 1.1

### Upgrading

1. Update the code
2. Run the `sql/mysql/1.1.sql`.
3. Clear caches

### Features

* Trash can with restore function
* Single attribute edit / Semi direct manipulation of ezxmltext
* Improved permissions check for buttons in toolbar
* Improved permissions check for pencil and pencil content
* Adds control of child nodes ordering / priority from objects edit
* Recursive publishing
* Device preview and mirrored browsing for iOS apps
* Edit-button added to every listing of objects (ezobjectrelation / block content)
* Proper support for ezgmapslocation

### Bugfixes

* Removed all hashbang urls from View
* Style issues with bootstrapp css
* Calculations of finder dimensions
* Attributes on unpublished ezflow block was impossible to save
* Search did not work
* Small modals close on click outside
* Cluster setups failed
* Activity unread count displaying wrong count
* Fixes open/close of attribute groups
* Checkbox custom attribute for ezflow blocks was impossible to toggle off
* Support custom attribute of type select for ezflow blocks.
* Fixes bug with dual publish status lights and new objects
* Fixes language issues with tinymce
* Dont show addcontent button when node isnt a container
* Update name of objects when going back in stack from edit -> finder/grid
* Dont show site select when theres only one siteaccess
* Disable scroll behind modals when modal is open

### Misc

* Dont enable view latest draft + view cache tweaks for users without access to toolbar
* Open user menu on click, not hover
* New design for tinymce editor
* Use touch events for touch devices
* Support drag and drop on touch
* Enable toolbar extensions
* Use 32px, crisper, icons for object edit heading
* Use the user-friendly attribute group name if it exists.
* New default icons for classes
* Change wording for adding content into block -> Add to block vs. Add your content here
* Added queued ezflow block objects to pencil for that block
* Norwegian translation
