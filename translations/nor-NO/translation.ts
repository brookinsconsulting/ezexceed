<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
    <context>
        <name>eze</name>
        <message>
            <source>Yes</source>
            <translation>Ja</translation>
        </message>
        <message>
            <source>No</source>
            <translation>Nei</translation>
        </message>
        <message>
            <source>Name</source>
            <translation>Navn</translation>
        </message>
        <message>
            <source>E-mail</source>
            <translation>E-post</translation>
        </message>
        <message>
            <source>Delete</source>
            <translation>Slett</translation>
        </message>
        <message>
            <source>and</source>
            <translation>og</translation>
        </message>
        <message>
            <source>in</source>
            <translation>i</translation>
        </message>
        <message>
            <source>by</source>
            <translation>av</translation>
        </message>
        <message>
            <source>Sort Ascending</source>
            <translation>Sorter A-Å</translation>
        </message>
        <message>
            <source>Sort Descending</source>
            <translation>Sorter Å-A</translation>
        </message>
        <message>
            <source>New</source>
            <translation>Ny</translation>
        </message>
        <message>
            <source>Add</source>
            <translation>Legg til</translation>
        </message>
        <message>
            <source>Edit</source>
            <translation>Redigèr</translation>
        </message>
        <message>
            <source>Saving</source>
            <translation>Lagrer</translation>
        </message>
        <message>
            <source>View</source>
            <translation>Se</translation>
        </message>
        <message>
            <source>Remove</source>
            <translation>Fjern</translation>
        </message>
        <message>
            <source>Discard</source>
            <translation>Forkast</translation>
        </message>
        <message>
            <source>Copy</source>
            <translation>Kopier</translation>
        </message>
        <message>
            <source>Modified</source>
            <translation>Endret</translation>
        </message>
        <message>
            <source>Error!</source>
            <translation>Feil!</translation>
        </message>
        <message>
            <source>Close</source>
            <translation>Lukk</translation>
        </message>
        <message>
            <source>Close all</source>
            <translation>Lukk alle</translation>
        </message>
        <message>
            <source>Go back</source>
            <translation>Gå tilbake</translation>
        </message>
        <message>
            <source>Create</source>
            <translation>Opprett</translation>
        </message>
        <message>
            <source>Create new</source>
            <translation>Lag ny</translation>
        </message>
        <message>
            <source>Create new content</source>
            <translation>Opprett nytt innhold</translation>
        </message>
        <message>
            <source>Publish</source>
            <translation>Publisér</translation>
        </message>
        <message>
            <source>Publish now</source>
            <translation>Publisèr nå</translation>
        </message>
        <message>
            <source>Input required</source>
            <translation>Påkrevd</translation>
        </message>
        <message>
            <source>Are you sure you want to remove</source>
            <translation>Er du sikker på at du vil fjerne</translation>
        </message>
        <message>
            <source>Add your content here</source>
            <translation>Legg til innhold her</translation>
        </message>
        <message>
            <source>Keep</source>
            <translation>Behold</translation>
        </message>
        <message>
            <source>Yes, remove it</source>
            <translation>Ja jeg vil fjerne</translation>
        </message>
        <message>
            <source>No, keep it</source>
            <translation>Nei, behold denne</translation>
        </message>
        <message>
            <source>Remove</source>
            <translation>Fjern</translation>
        </message>
        <message>
            <source>Show translations</source>
            <translation>Vis oversettelser</translation>
        </message>
        <message>
            <source>Show content on these pages</source>
            <translation>Vis innholdet på disse sidene</translation>
        </message>
        <message>
            <source>Pages</source>
            <translation>Sider</translation>
        </message>
        <message>
            <source>Add to page…</source>
            <translation>Legg til side…</translation>
        </message>
        <message>
            <source>Add to page</source>
            <translation>Legg til på side</translation>
        </message>
        <message>
            <source>Current locations.</source>
            <translation>Nåværende sider</translation>
        </message>
        <message>
            <source>Go back</source>
            <translation>Gå tilbake</translation>
        </message>
        <message>
            <source>Saving…</source>
            <translation>Lagrer…</translation>
        </message>
        <message>
            <source>All changes saved!</source>
            <translation>Alle endringer lagret!</translation>
        </message>
        <message>
            <source>All changes saved</source>
            <translation>Alle endringer lagret</translation>
        </message>
        <message>
            <source>Error saving!</source>
            <translation>Lagring feilet!</translation>
        </message>
        <message>
            <source>Retry</source>
            <translation>Prøv igjen</translation>
        </message>
        <message>
            <source>This content is published</source>
            <translation>Dette innholdet er publisert</translation>
        </message>
        <message>
            <source>It was published</source>
            <translation>Publisert</translation>
        </message>
        <message>
            <source>Last published</source>
            <translation>Sist publisert</translation>
        </message>
        <message>
            <source>It has never been published.</source>
            <translation>Har ikke vært publisert</translation>
        </message>
        <message>
            <source>It has never been published</source>
            <translation>Har ikke vært publisert</translation>
        </message>
        <message>
            <source>Publishing…</source>
            <translation>Publiserer…</translation>
        </message>
        <message>
            <source>Publish now</source>
            <translation>Publisèr nå</translation>
        </message>
        <message>
            <source>Current locations.</source>
            <translation>Nåværende sider</translation>
        </message>
        <message>
            <source>other locations</source>
            <translation>andre sider</translation>
        </message>
        <message>
            <source>other location</source>
            <translation>annen side</translation>
        </message>
        <message>
            <source>published on</source>
            <translation>publisert på</translation>
        </message>
        <message>
            <source>Find</source>
            <translation>Søk</translation>
        </message>
        <message>
            <source>Find content</source>
            <translation>Finn innhold</translation>
        </message>
        <message>
            <source>All content</source>
            <translation>Alt innhold</translation>
        </message>
        <message>
            <source>Batch operations</source>
            <translation>Operasjoner</translation>
        </message>
        <message>
            <source>Publish selected</source>
            <translation>Publiser valgte</translation>
        </message>
        <message>
            <source>Delete selected</source>
            <translation>Slett valgte</translation>
        </message>
        <message>
            <source>Current locations.</source>
            <translation>Slett valgte</translation>
        </message>
        <message>
            <source>Sort by</source>
            <translation>Sorter på</translation>
        </message>
        <message>
            <source>Published</source>
            <translation>Publisert</translation>
        </message>
        <message>
            <source>Never published</source>
            <translation>Aldri publisert</translation>
        </message>
        <message>
            <source>Author</source>
            <translation>Forfatter</translation>
        </message>
        <message>
            <source>Date</source>
            <translation>Dato</translation>
        </message>
        <message>
            <source>Content type</source>
            <translation>Innholdstype</translation>
        </message>
        <message>
            <source>Type to search</source>
            <translation>Skriv inn søkeord</translation>
        </message>
        <message>
            <source>Last edit date</source>
            <translation>Sist redigert</translation>
        </message>
        <message>
            <source>All time</source>
            <translation>Alle</translation>
        </message>
        <message>
            <source>Last week</source>
            <translation>Sist uke</translation>
        </message>
        <message>
            <source>Last month</source>
            <translation>Siste måned</translation>
        </message>
        <message>
            <source>All content types</source>
            <translation>Alle innholdstyper</translation>
        </message>
        <message>
            <source>By author</source>
            <translation>Etter forfatter</translation>
        </message>
        <message>
            <source>Sort by author</source>
            <translation>Sortert etter forfatter</translation>
        </message>
        <message>
            <source>Sort by date published</source>
            <translation>Sortert etter publisert-tidspunkt</translation>
        </message>
        <message>
            <source>Sort by content type</source>
            <translation>Sortert etter innholdstype</translation>
        </message>
        <message>
            <source>All authors</source>
            <translation>Alle forfattere</translation>
        </message>
        <message>
            <source>I changed</source>
            <translation>Jeg endret</translation>
        </message>
        <message>
            <source>Select content</source>
            <translation>Velg innhold</translation>
        </message>
        <message>
            <source>Set relation for</source>
            <translation>Relater innhold til</translation>
        </message>
        <message>
            <source>Select this content</source>
            <translation>Velg innhold</translation>
        </message>
        <message>
            <source>Publish this content</source>
            <translation>Publisèr innholdet</translation>
        </message>
        <message>
            <source>Publish all related pages</source>
            <translation>Publisér alle relaterte sider</translation>
        </message>
        <message>
            <source>changed</source>
            <translation>endret</translation>
        </message>
        <message>
            <source>published</source>
            <translation>publisert</translation>
        </message>
        <message>
            <source>new</source>
            <translation>ny</translation>
        </message>
        <message>
            <source>Error</source>
            <translation>Feil</translation>
        </message>
        <message>
            <source>Layout for</source>
            <translation>Sone-styring for</translation>
        </message>
        <message>
            <source>Add to block</source>
            <translation>Legg til i blokk</translation>
        </message>
        <message>
            <source>Hide this content</source>
            <translation>Skjul dette innholdet</translation>
        </message>
        <message>
            <source>Go to</source>
            <translation>Gå til</translation>
        </message>
        <message>
            <source>Go to primary page</source>
            <translation>Gå til primær-siden</translation>
        </message>
        <message>
            <source>locations</source>
            <translation>lokasjoner</translation>
        </message>
        <message>
            <source>You have been logged out</source>
            <translation>Du har blitt logget ut</translation>
        </message>
        <message>
            <source>Send preview</source>
            <translation>Forhåndsvis</translation>
        </message>
        <message>
            <source>Preview sent</source>
            <translation>Sendt</translation>
        </message>
        <message>
            <source>Mirror browsing</source>
            <translation>Speil</translation>
        </message>
        <message>
            <source>My drafts</source>
            <translation>Mine kladder</translation>
        </message>
        <message>
            <source>Trash</source>
            <translation>Papirkurv</translation>
	    </message>
        <message>
            <source>Trash selected</source>
            <translation>Kast i papirkurven</translation>
        </message>
        <message>
            <source>Permanently remove selected</source>
            <translation>Slett for alltid</translation>
        </message>
        <message>
            <source>Restore selected</source>
            <translation>Gjenopprett</translation>
        </message>
        <message>
            <source>Show</source>
            <translation>Vis</translation>
        </message>
        <message>
            <source>Main</source>
            <translation>Primær</translation>
        </message>
        <message>
            <source>This is the only location for</source>
            <translation>Dette er siste sidetilhørighet for</translation>
        </message>
        <message>
            <source>Removing it will also move the content to Trash</source>
            <translation>Fjerning av denne vil flytte innholdet til papirkurven</translation>
        </message>
        <message>
            <source>Separate keywords with comma «,»</source>
            <translation>Skill nøkkelord med komma «,»</translation>
        </message>
        <message>
            <source>Login to eZ Publish Administration Interface</source>
            <translation>Logg inn i eZ Publish administrasjonsgrensesnitt</translation>
        </message>
        <message>
            <source>Type something…</source>
            <translation>Fyll inn…</translation>
        </message>
        <message>
            <source>Update login information</source>
            <translation>Oppdatèr innloggingsinformasjon</translation>
        </message>
        <message>
            <source>Username</source>
            <translation>Brukernavn</translation>
        </message>
        <message>
            <source>Password</source>
            <translation>Passord</translation>
        </message>
        <message>
            <source>Email address</source>
            <translation>E-post adresse</translation>
        </message>
        <message>
            <source>Repeat password</source>
            <translation>Gjenta passord</translation>
        </message>
        <message>
            <source>Repeated password does not match password</source>
            <translation>Passordene er ikke like</translation>
        </message>
        <message>
            <source>Invalid email</source>
            <translation>Ugyldig e-post</translation>
        </message>
        <message>
            <source>Password is too short</source>
            <translation>Passordet er ikke langt nok</translation>
        </message>
        <message>
            <source>Sitemap</source>
            <translation>Sidekart</translation>
        </message>
        <message>
            <source>Preview</source>
            <translation>Forhåndsvis</translation>
        </message>
        <message>
            <source>Activity</source>
            <translation>Meldinger</translation>
        </message>
        <message>
            <source>Zones</source>
            <translation>Soner</translation>
        </message>
        <message>
            <source>Publish pages</source>
            <translation>Publisér sidene</translation>
        </message>
        <message>
            <source>This page</source>
            <translation>Denne siden</translation>
        </message>
        <message>
            <source>All activity</source>
            <translation>Alle</translation>
        </message>
        <message>
            <source>me</source>
            <translation>meg</translation>
        </message>
        <message>
            <source>Write message</source>
            <translation>Skriv melding</translation>
        </message>
        <message>
            <source>Share</source>
            <translation>Del</translation>
        </message>
        <message>
            <source>Show more</source>
            <translation>Vis flere</translation>
        </message>
        <message>
            <source>Load more</source>
            <translation>Last flere</translation>
        </message>
        <message>
            <source>Pinned</source>
            <translation>Bokmerke</translation>
        </message>
        <message>
            <source>Comment</source>
            <translation>Kommentèr</translation>
        </message>
        <message>
            <source>Like</source>
            <translation>Lik</translation>
        </message>
        <message>
            <source>Unlike</source>
            <translation>Liker ikke</translation>
        </message>
        <message>
            <source>like this</source>
            <translation>liker dette</translation>
        </message>
        <message>
            <source>Just now</source>
            <translation>Akkurat nå</translation>
        </message>
        <message>
            <source>Less than 5 minutes ago</source>
            <translation>Mindre enn 5 minutter siden</translation>
        </message>
        <message>
            <source>About count minutes ago</source>
            <translation>Omlag count minutter siden</translation>
        </message>
        <message>
            <source>About count hours ago</source>
            <translation>Omlag count timer siden</translation>
        </message>
        <message>
            <source>Today</source>
            <translation>I dag</translation>
        </message>
        <message>
            <source>Yesterday</source>
            <translation>I går</translation>
        </message>
        <message>
            <source>count days ago</source>
            <translation>count dager siden</translation>
        </message>
        <message>
            <source>Send preview to devices</source>
            <translation>Send forhåndsvisning til enheter</translation>
        </message>
        <message>
            <source>Preview this page on an URL</source>
            <translation>Url til forhåndsvisning</translation>
        </message>
        <message>
            <source>You have no connected devices. Download the iPhone or iPad app to use this new and powerful way of
                working across devices
            </source>
            <translation>Du har ingen enheter tilkoblet. Last ned iPhone eller iPad appen for å bruke denne nye og
                kraftige måten å jobbe på mellom ulike enheter
            </translation>
        </message>
        <message>
            <source>Get the app from the App Store</source>
            <translation>Last ned appen fra App Store</translation>
        </message>
        <message>
            <source>Choose layout</source>
            <translation>Velg layout</translation>
        </message>
        <message>
            <source>Zones with blocks of content</source>
            <translation>Soner med innholdsblokker</translation>
        </message>
        <message>
            <source>Arrange, add or remove blocks of content inside zones</source>
            <translation>Organiser, legg til eller fjern innholdsblokker i sonene</translation>
        </message>
        <message>
            <source>No content is added</source>
            <translation>Innhold er ikke lagt til</translation>
        </message>
        <message>
            <source>Add new block</source>
            <translation>Legg til blokk</translation>
        </message>
        <message>
            <source>Type a name</source>
            <translation>Fyll inn navn</translation>
        </message>
        <message>
            <source>Select view</source>
            <translation>Velg visning</translation>
        </message>
        <message>
            <source>Set overflow</source>
            <translation>Velg flyt</translation>
        </message>
        <message>
            <source>Active content</source>
            <translation>Aktivt innhold</translation>
        </message>
        <message>
            <source>No content currently active</source>
            <translation>Ingen innhold er aktivt</translation>
        </message>
        <message>
            <source>Queued up content</source>
            <translation>Innhold i kø</translation>
        </message>
        <message>
            <source>No content queued up</source>
            <translation>Ingen innhold i kø</translation>
        </message>
        <message>
            <source>Current source:</source>
            <translation>Nåværende kilde</translation>
        </message>
        <message>
            <source>Choose source</source>
            <translation>Velg kilde</translation>
        </message>
        <message>
            <source>Select content to show in this block</source>
            <translation>Velg innhold for visning i denne blokken</translation>
        </message>
        <message>
            <source>Custom attributes</source>
            <translation>Egendefinerte verdier</translation>
        </message>
        <message>
            <source>Rotate active content every</source>
            <translation>Rotèr aktivt innhold hver</translation>
        </message>
        <message>
            <source>Shuffle content when rotating</source>
            <translation>Bland innhold ved rotering</translation>
        </message>
        <message>
            <source>minute</source>
            <translation>minutt</translation>
        </message>
        <message>
            <source>hour</source>
            <translation>time</translation>
        </message>
        <message>
            <source>day</source>
            <translation>dag</translation>
        </message>
        <message>
            <source>Add item to</source>
            <translation>Legg innhold til</translation>
        </message>
        <message>
            <source>Add content to the block</source>
            <translation>Legg til i blokken</translation>
        </message>
        <message>
            <source>The content is already added</source>
            <translation>Innholdet er allerede lagt til</translation>
        </message>
        <message>
            <source>Remove from the block</source>
            <translation>Fjern fra blokken</translation>
        </message>
        <message>
            <source>Activate at</source>
            <translation>Aktivèringstidspunkt</translation>
        </message>
        <message>
            <source>Settings</source>
            <translation>Innstillinger</translation>
        </message>
        <message>
            <source>not translatable</source>
            <translation>ikke oversettbart</translation>
        </message>
        <message>
            <source>required</source>
            <translation>påkrevd</translation>
        </message>
        <message>
            <source>Add new author</source>
            <translation>Legg til ny forfatter</translation>
        </message>
        <message>
            <source>Remove this file</source>
            <translation>Fjern filen</translation>
        </message>
        <message>
            <source>Upload new file</source>
            <translation>Last opp ny fil</translation>
        </message>
        <message>
            <source>Not specified</source>
            <translation>Ikke spesifisert</translation>
        </message>
        <message>
            <source>Time</source>
            <translation>Tidspunkt</translation>
        </message>
        <message>
            <source>Edit image</source>
            <translation>Redigèr bilde</translation>
        </message>
        <message>
            <source>Add image description</source>
            <translation>Legg til bildebeskrivelse</translation>
        </message>
        <message>
            <source>Upload new image</source>
            <translation>Last opp nytt bilde</translation>
        </message>
        <message>
            <source>Web address</source>
            <translation>Webadresse</translation>
        </message>
        <message>
            <source>Title</source>
            <translation>Tittel</translation>
        </message>
        <message>
            <source>Get my current location</source>
            <translation>Hent min nåværende lokasjon</translation>
        </message>
        <message>
            <source>Place the content on this page</source>
            <translation>Plassèr innholdet på denne siden</translation>
        </message>
        <message>
            <source>The content is placed on this page</source>
            <translation>Innholdet er plassert på denne siden</translation>
        </message>
        <message>
            <source>Add relations to</source>
            <translation>Legg til relasjoner på</translation>
        </message>
        <message>
            <source>Add location to</source>
            <translation>Legg til side på</translation>
        </message>
        <message>
            <source>Remove from this page</source>
            <translation>Fjern fra denne siden</translation>
        </message>
        <message>
            <source>You have deleted the page you are viewing</source>
            <translation>Du har slettet siden du står på</translation>
        </message>
        <message>
            <source>The parent page must be refreshed in order to update the view</source>
            <translation>Siden på nivået over må lastes på nytt for å oppdatere visningen</translation>
        </message>
        <message>
            <source>You have changed the page you are viewing</source>
            <translation>Du har endret siden du står på</translation>
        </message>
        <message>
            <source>The page must be refreshed in order to update the view</source>
            <translation>Siden må lastes på nytt for å oppdatere visningen</translation>
        </message>
        <message>
            <source>Logout</source>
            <translation>Logg ut</translation>
        </message>
        <message>
            <source>Select row for removal</source>
            <translation>Velg raden som skal fjernes</translation>
        </message>
        <message>
            <source>does not support subcontent</source>
            <translation>støtter ikke undersider</translation>
        </message>
        <message>
            <source>Can't move to</source>
            <translation>Kan ikke flytte til</translation>
        </message>
        <message>
            <source>Comments on this content</source>
            <translation>Kommentarer til dette innholdet</translation>
        </message>
        <message>
            <source>Browse media library</source>
            <translation>Søk etter media</translation>
        </message>
        <message>
            <source>Upload new image</source>
            <translation>Last opp nytt</translation>
        </message>
        <message>
            <source>Scale variants</source>
            <translation>Skalér</translation>
        </message>
        <message>
            <source>Select crops</source>
            <translation>Skalér</translation>
        </message>
        <message>
            <source>Select crop</source>
            <translation>Velg utsnitt</translation>
        </message>
        <message>
            <source>Alternate text</source>
            <translation>Alternativ tekst</translation>
        </message>
        <message>
            <source>Class</source>
            <translation>Klasse</translation>
        </message>
        <message>
            <source>Image is to small for this version</source>
            <translation>Bildet er for lite for dette utsnittet</translation>
        </message>
        <message>
            <source>Select media</source>
            <translation>Velg media</translation>
        </message>
        <message>
            <source>Search for media</source>
            <translation>Søk etter media</translation>
        </message>
        <message>
            <source>Upload new media</source>
            <translation>Last opp nytt media</translation>
        </message>
        <message>
            <source>Upload new</source>
            <translation>Last opp</translation>
        </message>
        <message>
            <source>Shared</source>
            <translation>Delt</translation>
        </message>
        <message>
            <source>This location has subcontent! Removing it will also remove all subcontent. Are you sure you want to
                remove
            </source>
            <translation>Denne siden har undersider! Fjerning av denne siden fjerner også alle undersider. Er du sikker
                på at du vil fjerne
            </translation>
        </message>
        <message>
            <source>Deleting</source>
            <translation>Sletting av</translation>
        </message>
        <message>
            <source>will also remove all sub-locations.</source>
            <translation>sletter også alt underinnhold.</translation>
        </message>
        <message>
            <source>sub-locations will also be deleted.</source>
            <translation>undersider blir også slettet.</translation>
        </message>
        <message>
            <source>subcontent below</source>
            <translation>undersider</translation>
        </message>
        <message>
            <source>Content under</source>
            <translation>Innhold under</translation>
        </message>
        <message>
            <source>Edit and sort content under this page</source>
            <translation>Rediger og sorter innhold under denne siden</translation>
        </message>
        <message>
            <source>priority</source>
            <translation>prioritet</translation>
        </message>
        <message>
            <source>alphabetically</source>
            <translation>alfabetisk</translation>
        </message>
        <message>
            <source>Username</source>
            <translation>Brukernavn</translation>
        </message>
        <message>
            <source>Password</source>
            <translation>Passord</translation>
        </message>
        <message>
            <source>You've been logged out</source>
            <translation>Du har blitt logget ut</translation>
        </message>
        <message>
            <source>Login and resume</source>
            <translation>Logg på og fortsett</translation>
        </message>
        <message>
            <source>You</source>
            <translation>Du</translation>
        </message>
        <message>
            <source>Settings for</source>
            <translation>Innstillinger for</translation>
        </message>
        <message>
            <source>Upload files</source>
            <translation>Last opp filer</translation>
        </message>
        <message>
            <source>Uploading</source>
            <translation>Laster opp</translation>
        </message>
        <message>
            <source>Loading future</source>
            <translation>Henter fremtiden</translation>
        </message>
        <message>
            <source>Preview the future with Timeline</source>
            <translation>Forhåndsvis siden i fremtiden</translation>
        </message>
        <message>
            <source>Turn on timeline</source>
            <translation>Aktivér tidslinje</translation>
        </message>
        <message>
            <source>Turn off timeline</source>
            <translation>Deaktivér tidslinje</translation>
        </message>
        <message>
            <source>Uploading…</source>
            <translation>Laster opp…</translation>
        </message>
        <message>
            <source>Revert all changes</source>
            <translation>Tilbakestill endringer</translation>
        </message>
        <message>
            <source>Select source</source>
            <translation>Velg kilde</translation>
        </message>
        <message>
            <source>Publishing</source>
            <translation>Publiserer</translation>
        </message>
        <message>
            <source>Content was successfully published</source>
            <translation>Innholdet er nå publisert</translation>
        </message>
        <message>
            <source>You have no connected devices. Download the iPhone or iPad app to use this new and powerful way of working across devices</source>
            <translation>Ingen enheter er tilknyttet. Last ned appen til din iPhone eller iPdag for å kunne jobbe på tvers av enheter</translation>
        </message>
        <message>
            <source>This location has subcontent! Removing it will also remove all subcontent. Are you sure you want to remove</source>
            <translation>Denne lokasjonen har innhold under seg. Dersom den slettes, vil innholdet under også slettes. Er du sikkert på at du vil fjerne</translation>
        </message>
        <message>
            <source>will also remove all sub-content.</source>
            <translation>vil også fjerne alt under-innhold.</translation>
        </message>
        <message>
            <source>sub-content will also be deleted.</source>
            <translation>under-innhold vil også bli slettet.</translation>
        </message>
        <message>
            <source>Loading more</source>
            <translation>Laster mer</translation>
        </message>
        <message>
            <source>Sorry, no content found. Please try another search criteria.</source>
            <translation>Beklager, ingen treff. Vennligst forsøk et annet søkekriterie.</translation>
        </message>
        <message>
            <source>people</source>
            <translation>personer</translation>
        </message>
        <message>
            <source>Removing it will permanently delete the content</source>
            <translation>Innholdet vil bli slettet dersom denne tas bort.</translation>
        </message>
        <message>
            <source>An internal server error happened, this has been reported</source>
            <translation>En intern serverfeil har oppstått, feilen har blitt innrapportert</translation>
        </message>
        <message>
            <source>Dismiss</source>
            <translation>Lukk</translation>
        </message>
        <message>
            <source>No name</source>
            <translation>Uten navn</translation>
        </message>
        <message>
            <source>Search</source>
            <translation>Søk</translation>
        </message>
        <message>
            <source>No results</source>
            <translation>Ingen treff</translation>
        </message>
        <message>
            <source>Show all</source>
            <translation>Vis alle</translation>
        </message>
        <message>
            <source>Hide</source>
            <translation>Skjul</translation>
        </message>
        <message>
            <source>Insert link</source>
            <translation>Sett inn lenke</translation>
        </message>
        <message>
            <source>Browse sitemap</source>
            <translation>Hent fra sidekart</translation>
        </message>
        <message>
            <source>The content is being published</source>
            <translation>Publiserer innhold</translation>
        </message>
        <message>
            <source>Awaiting approval</source>
            <translation>Venter på godkjenning</translation>
        </message>
        <message>
            <source>Content sent for approval to</source>
            <translation>Innholdet ble sendt til</translation>
        </message>
        <message>
            <source>group</source>
            <translation>gruppen</translation>
        </message>
        <message>
            <source>Send content for approval</source>
            <translation>Send til godkjenning</translation>
        </message>
        <message>
            <source>Approve content</source>
            <translation>Godkjenn innholdet</translation>
        </message>
        <message>
            <source>Content was sent for approval</source>
            <translation>Dette innholdet ble lagt til godkjenning</translation>
        </message>
        <message>
            <source>Reject content changes</source>
            <translation>Forkast foreslåtte endringer</translation>
        </message>
        <message>
            <source>Send to</source>
            <translation>Send til</translation>
        </message>
        <message>
            <source>Username is empty</source>
            <translation>Brukernavn er tomt</translation>
        </message>
        <message>
            <source>Please enter an address</source>
            <translation>Vennligst fyll inn en adresse</translation>
        </message>
        <message>
            <source>Please enter a valid address</source>
            <translation>Vennligst fyll inn en gyldig adresse</translation>
        </message>
        <message>
            <source>Geolocation failed</source>
            <translation>Geolokalisering feilet</translation>
        </message>
    </context>
</TS>
