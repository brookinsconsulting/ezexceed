<?php
/**
 * File containing eZExceedInfo class
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 *
 */

class eZExceedInfo
{
    static function info()
    {
        return array(
            'Name' => 'eZExceed',
            'Version' => '//autogen//',
            'Copyright' => '//autogen//',
            'License' => '//autogen//'
        );
    }
}

?>
