<?php

/**
 * Utility-functions for preview
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\classes\utilities;

/**
 *
 * Description
 *
 * Utility-functions for preview
 *
 * @author Bjørnar Helland / bh@keyteq.no
 * @since 08.10.2012
 *
 */

class PreviewUtilities
{
    public static function createKey($domain, $objectId, $versionId, $languageCode = false, $placementId = false)
    {
        $keyString = $domain . (string) $objectId . (string) $versionId;

        if ($languageCode)
            $keyString .= (string) $languageCode;
        if ($placementId)
            $keyString .= (string)$placementId;

        $eZExceedINI = \eZINI::instance('ezexceed.ini');
        $salt = IniUtilities::checkGetSetting($eZExceedINI, 'eZExceedPushNotification', 'SignKey', false);

        return sha1($salt . sha1($keyString));
    }
}
