<?php

/**
 * Utility-functions for retrieving ini-settings
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\classes\utilities;

/**
 *
 * Description
 *
 * Utility-functions for retrieving ini-settings.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 24.04.2012
 *
 */

class IniUtilities
{
    /**
     *
     * Adds support for default-value when retrieving an ini-setting.
     *
     * @param \eZINI $ini
     * @param string $block
     * @param string $variable
     * @param mixed $fallback
     *
     * @return string|bool
     *
     */
    public static function checkGetSetting($ini, $block, $variable, $fallback = false)
    {
        if ($ini->hasVariable($block, $variable))
            return $ini->variable($block, $variable);
        elseif (is_callable($fallback))
            return $fallback();

        return $fallback;
    }
}
