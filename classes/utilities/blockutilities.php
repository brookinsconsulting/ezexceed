<?php

/**
 * Utilitiy methods for eZFlow-blocks
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\classes\utilities;

/**
 *
 * Description
 *
 * Utilitiy methods for eZFlow-blocks.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 29.03.2012
 *
 */

class BlockMethods
{
    /**
     *
     * Gets the available views if any.
     *
     * @param \eZPageBlock $block
     *
     * @return array
     *
     */
    public static function retrieveAvailableViews($block)
    {
        $eZINI = self::getIni();

        $blockName = $block->attribute('type');

        $viewTypes = $eZINI->variable($blockName, 'ViewList');
        $viewNames = $eZINI->variable($blockName, 'ViewName');

        return array_combine($viewTypes, $viewNames);
    }

    /**
     *
     * Retrieves the first available view for a block.
     *
     * @param \eZPageBlock $block
     *
     * @return string
     *
     */
    public static function getDefaultView($block)
    {
        $eZINI = self::getIni();

        $viewTypes = $eZINI->variable($block->attribute('type'), 'ViewList');

        return array_shift($viewTypes);
    }

    /**
     *
     * Retrieves eZ ini-handler.
     *
     * @param string $file
     *
     * @return \eZINI
     *
     */
    protected static function getIni($file = 'block.ini')
    {
       return \eZINI::instance($file);
    }

    /**
     *
     * Retrieves the best description available.
     *
     * @param \eZPageBlock $block
     *
     * @return string
     *
     */
    public static function retrieveBlockDescription($block)
    {
        $name = $block->attribute('name');

        if (strlen($name) === 0)
            $name = self::getBlockNameByType($block->attribute('type'));

        if (strlen($name) === 0)
            $name = $block->attribute('type');

        return $name;
    }

    /**
     *
     * Retrieves block-typename.
     *
     * @param $type
     * @param null|\eZINI $eZINI
     *
     * @return string
     *
     */
    public static function getBlockNameByType($type, $eZINI = null)
    {
        if ($eZINI === null)
            $eZINI = self::getIni();

        return $eZINI->variable($type, 'Name');
    }
}
