<?php

/**
 * Utility methods for objects
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\classes\utilities;

/**
 *
 * Description
 *
 * Utility methods for objects.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 23.03.2012
 *
 */

class Objects
{
    const MAX_NODES_REMOVE_SUBTRE = 100;

    /**
     *
     * Checks if deleted objects should be moved to the trash.
     *
     * @return bool
     *
     */
    public static function movetoTrash()
    {
        $contentINI = \eZINI::instance('content.ini');

        if (!$removeAction = $contentINI->variable('RemoveSettings', 'DefaultRemoveAction'))
            $removeAction = 'trash';

        if ($removeAction !== 'trash' && $removeAction !== 'delete')
            $removeAction = 'trash';

        $moveToTrash = ($removeAction === 'trash') ? true : false;

        return $moveToTrash;
    }

    /**
     *
     * Retrieves the first attribute used for url-creation in eZ.
     *
     * @param \eZContentObject $object
     *
     * @return \eZContentObjectAttribute|null
     */
    public static function getNameAttribute($object)
    {
        $contentClass = $object->contentClass();

        /** Courtesy of eZContentClass/urlAliasName. */
        $pattern = $contentClass->attribute('url_alias_name') ?: $contentClass->attribute('contentobject_name');

        $identifiers = self::getIdentifiers($pattern);

        $attributes = $object->fetchAttributesByIdentifier($identifiers);

        if (is_array($attributes))
            return array_shift($attributes);
        else return null;
    }

    /**
     * Method extracted from eZNamePatternResolver.
     *
     * @param string $patternString
     *
     * @return array
     */
    protected static function getIdentifiers($patternString)
    {
        $allTokens = '#<(.*)>#U';
        $identifiers = '#\W#';

        $tmpArray = array();
        preg_match_all($allTokens, $patternString, $matches);

        foreach ($matches[1] as $match)
        {
            $tmpArray[] = preg_split($identifiers, $match, -1, PREG_SPLIT_NO_EMPTY);
        }

        $retArray = array();
        foreach ($tmpArray as $matchGroup)
        {
            if (is_array($matchGroup))
            {
                foreach ($matchGroup as $item)
                {
                    $retArray[] = $item;
                }
            }
            else
            {
                $retArray[] = $matchGroup;
            }
        }
        return $retArray;
    }
}
