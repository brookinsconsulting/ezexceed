<?php

/**
 * Publish model
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models;

use \ezexceed\models\content\Object;
use ezexceed\models\content\object\Formatter;

/**
 * Description
 *
 * Model for handling publishing.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 15.11.2012
 */
class Publish
{
    /**
     * Retrieves status for an object.
     *
     * @param \eZContentObject $object
     * @param null $languages
     *
     * @return array
     */
    public function status($object, $languages = null)
    {
        $languages = $languages ? (array) $languages : array();

        $response = array(
            'ok' => true,
            'published' => $object->attribute('published'),
            'modified' => $object->attribute('modified'),
            'objects' => array()
        );

        $relatedObjects = Object::getRelatedObjects($object);
        /** Adds the main object to the beginning of the array. */
        array_unshift($relatedObjects, $object);

        /** @var $relatedObjects \eZContentObject[] */
        foreach ($relatedObjects as $relatedObject) {
            /** If an object has been deleted, the cron-job might not have cleaned the page-attribute yet. */
            if (!is_object($relatedObject)) {
                continue;
            }

            $publishStatus = Object::changedSincePublished($relatedObject, $languages);

            if ($publishStatus['status'] !== Object::PUBLISHEDSTATUS_PUBLISHED) {
                $response['objects'][] = self::formatObject($relatedObject) + $publishStatus;
            }
        }

        return $response;
    }

    /**
     * Publishes one or more object-versions.
     *
     * @param $objects
     *
     * @return array
     */
    public static function publishObjects($objects)
    {
        $published = array();
        $ok = true;

        foreach ($objects as $object) {
            $objectId = !empty($object['id']) ? $object['id'] : false;
            $versions = !empty($object['versions']) ? $object['versions'] : false;

            if (!$objectId || !$versions) {
                continue;
            }

            $contentObject = \eZContentObject::fetch($objectId);

            if (!$contentObject) {
                continue;
            }

            $result = Object::publish($contentObject, $versions);

            if ($result['ok']) {
                $contentObject = \eZContentObject::fetch($objectId);

                $publishData = array(
                    'value' => $contentObject->attribute('published'),
                    'info' => Object::changedSincePublished($contentObject),
                    'creator' => Formatter::getUserName($contentObject->attribute('owner_id'))
                );

                $published[$objectId] = $publishData;
            }
            else {
                $ok = false;
            }
        }

        $response = compact('published');
        $response['ok'] = $ok;

        return $response;
    }

    /**
     * Retrieves relevant fields for an object.
     *
     * @param \eZContentObject $object
     *
     * @return array
     */
    protected static function formatObject($object)
    {
        return array(
            'id' => $object->ID,
            'name' => Object::name($object),
            'classId' => $object->contentClass()->ID,
        );
    }
}