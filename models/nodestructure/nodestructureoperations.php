<?php
/**
 * Contains data for the necessary node-operations
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\nodestructure;

use \ezexceed\classes\utilities\Objects;

/**
 *
 * Description
 *
 * Contains data for the necessary node-operations.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 22.03.2012
 *
 */

class NodeStructureOperations
{
    /** Local data. */

    /** @var \eZHTTPTool */
    protected $http;


    /** Copy data */

    /** @var \eZContentObject */
    public $object;
    /** @var \eZContentObjectTreeNode */
    public $node;


    /** Delete data */

    /** @var array */
    public $deleteIDArray;


    /** Move data. */

    /** @var string */
    public $nodeId;
    /** @var string */
    public $newParentNodeId;
    /** @var string */
    public $objectId;

    /**
     *
     * Initializes the class.
     *
     */
    public function __construct()
    {
        $this->http = \eZHTTPTool::instance();
    }

    /**
     *
     * Fetches and validates the data necessary for a copy-action.
     *
     * @return string
     *
     */
    public function fetchCopyData()
    {
        if (!$this->http->hasPostVariable('ObjectId'))
            return $this->messageResult('ObjectId is missing.');

        if (!$this->http->hasPostVariable('SelectedNodeId'))
            return $this->messageResult('SelectedNodeId is missing.');

        $objectId = $this->http->postVariable('ObjectId');
        $newParentNodeId = $this->http->postVariable('SelectedNodeId');

        if (!$this->object = \eZContentObject::fetch($objectId))
            return $this->messageResult('Object with ID ' . $objectId . ' does not exist.');

        if (!$this->object->attribute('can_read'))
            return $this->messageResult('Access denied. Cannot copy object.');

        if (!$this->node = \eZContentObjectTreeNode::fetch($newParentNodeId))
            return $this->messageResult('Access denied. Cannot create node under the specified selected node.');

        $classId = $this->object->attribute('contentclass_id');

        if (!$this->node->checkAccess('create', $classId))
            return $this->messageResult('Cannot copy object ' . $objectId . 'to node ' . $newParentNodeId . ' ,the current user does not have create permission for class ID: ' . $classId);

        return $this->messageResult();
    }

    /**
     *
     * Standarized response when something bad happens.
     *
     * @param $message
     *
     * @return array
     */
    protected function messageResult($message = '')
    {
        if (strlen($message) > 0)
            return array ('error' => true, 'msg' => $message);
        else
            return null;
    }

    /**
     *
     * Fetches data necessary for a delete-operation.
     *
     * @return array
     *
     */
    public function fetchDeleteData()
    {
        if (!$this->http->hasPostVariable('ContentNodeID') || !is_numeric($this->http->postVariable('ContentNodeID')))
            return $this->messageResult('ContentNodeID is missing or is not numeric');

        $this->deleteIDArray = array($this->http->postVariable('ContentNodeID'));

        $contentINI = \eZINI::instance('content.ini');

        $info = \eZContentObjectTreeNode::subtreeRemovalInformation($this->deleteIDArray);

        $deleteResult = $info['delete_list'];
        $deleteNodeIdArray = array();

        /** Check if number of nodes being removed not more then MaxNodesRemoveSubtree setting. */
        if (!$maxNodesRemoveSubtree = $contentINI->variable('RemoveSettings', 'MaxNodesRemoveSubtree'))
            $maxNodesRemoveSubtree = Objects::MAX_NODES_REMOVE_SUBTRE;

        foreach (array_keys($deleteResult) as $removeItemKey)
        {
            $removeItem = $deleteResult[$removeItemKey];
            /** @var $node \eZContentObjectTreeNode */
            $node = $removeItem['node'];

            $deleteNodeIdArray[] = $node->attribute('node_id');

            if ($removeItem['child_count'] > $maxNodesRemoveSubtree)
                return $this->messageResult('Number of nodes being removed is more than MaxNodesRemoveSubtree setting');
        }

        return $this->messageResult();
    }

    /**
     *
     * Fetches data necessary for a move-operation.
     *
     * @return array
     *
     */
    public function fetchMoveData()
    {
        if (!$this->http->hasPostVariable('NodeId'))
            return $this->messageResult('NodeId is missing');

        if (!$this->http->hasPostVariable('NewParentNode'))
            return $this->messageResult('NewParentNode is missing');

        $nodeId = $this->http->postVariable('NodeId');
        $newParentNodeId = $this->http->postVariable('NewParentNode');

        if (!$newParentNode = \eZContentObjectTreeNode::fetch($newParentNodeId))
            return $this->messageResult('Content node with ID ' . $newParentNodeId . ' does not exist. Cannot use that as parent node for node: ' . $nodeId);

        /** Check that all user has access to move all selected nodes. */
        if (!$node = \eZContentObjectTreeNode::fetch($nodeId))
            return $this->messageResult('Cannot find node for id: ' . $nodeId);

        if (!$node->canMoveFrom())
            return $this->messageResult('Access denied. Cannot move node');

        if (!$object = $node->object())
            return $this->messageResult('Object for node not available');

        $nodeToMoveList[] = array('node_id' => $nodeId, 'object_id' => $object->attribute('id'));

        $class = $object->contentClass();
        $classId = $class->attribute('id');

        /** check if the object can be placed under the selected node. */
        if (!$newParentNode->canMoveTo($classId))
            return $this->messageResult('Cannot move node ' . $nodeId . 'as child of parent node ' . $newParentNodeId . ', the current user does not have create permission for class ID: ' . $classId);

        /** Check if we try to move the node as child of itself or one of its children. */
        if (in_array($node->attribute('node_id'), $newParentNode->pathArray()))
            return $this->messageResult('Cannot move node ' . $nodeId . 'as child of itself or one of its own children (node ' . $newParentNodeId . ').');

        $this->nodeId = $nodeId;
        $this->newParentNodeId = $newParentNodeId;
        $this->objectId = $object->attribute('id');

        return $this->messageResult();
    }
}