<?php
/**
 * Common operations performed on nodes
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\nodestructure;

/**
 *
 * Description
 *
 * Common operations performed on nodes.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 07.03.2012
 *
 */

use ezexceed\classes\utilities\Objects;

class NodeOperations
{
    /**
     *
     * Assigns a new main-node.
     *
     * @param \eZContentObject $object
     * @param \eZContentObjectTreeNode $newMainNode
     *
     * @return bool
     */
    public static function assignNewMainNode($object, $newMainNode)
    {
        $nodeId = $newMainNode->attribute('node_id');

        $parentNodeId = (int) $newMainNode->attribute('parent_node_id');
        $version = (int) $object->currentVersion(false);
        $objectID = (int) $object->attribute('id');

        $db = \eZDB::instance();
        $db->begin();
        $db->query("UPDATE ezcontentobject_tree SET main_node_id=$nodeId WHERE contentobject_id=$objectID");
        $db->query("UPDATE eznode_assignment SET is_main=1 WHERE contentobject_id=$objectID AND contentobject_version=$version AND parent_node=$parentNodeId");
        $db->query("UPDATE eznode_assignment SET is_main=0 WHERE contentobject_id=$objectID AND contentobject_version=$version AND parent_node!=$parentNodeId");
        $db->commit();

        return true;
    }

    /**
     *
     * Checks if object has a hidden default-node.
     *
     * @param \eZContentObject $object
     *
     * @return \eZContentObjectTreeNode
     *
     */
    protected static function checkRemoveHiddenNodes($object)
    {
        /** @var $node \eZContentObjectTreeNode */
        foreach($object->assignedNodes() as $node)
        {
            if ($node->childrenCount() > 0)
                continue;

            if ($node->attribute('is_hidden') == 1 && $node->attribute('is_invisible') == 1)
                self::removeNodeFromObject($node->attribute('node_id'));
        }
    }

    /**
     *
     * Retrieves the content-root-node.
     *
     * @return bool
     *
     */
    public static function getContentRootNodeId()
    {
        $contentRootNodeId = \eZINI::instance('content.ini')->variable('NodeSettings', 'RootNode');

        return $contentRootNodeId;
    }

    /**
     *
     * Adds a node and/or replaces the default-node.
     *
     * @param \eZContentObject $object
     * @param string $nodeId
     *
     * @param bool $main
     * @param bool $hidden
     *
     * @return \eZContentObjectTreeNode
     *
     */
    public static function addNode($object, $nodeId, $main = false, $hidden = false)
    {
        $db = \eZDB::instance();
        $db->begin();
        $node = $object->addLocation($nodeId, true);
        $db->commit();

        $db->begin();
        $node->updateSubTreePath();
        $db->commit();

        self::checkRemoveHiddenNodes($object);

        if ($main === true)
            self::assignNewMainNode($object, $node);
        else
            self::assignNewMainNode($object, $object->attribute('main_node'));

        if ($hidden === true)
            \eZContentObjectTreeNode::hideSubTree($node);

        return $node;
    }

    /**
     *
     * Retrieves the first node where user can assign content.
     *
     * @return string|null
     *
     */
    public static function getAssignableNodeId()
    {
        $nodes = \eZContentObjectTreeNode::fetchList();

        /** @var $node \eZContentObjectTreeNode */
        foreach($nodes as $node)
        {
            $objectId = $node->attribute('contentobject_id');

            if (is_numeric($objectId) && $objectId > 0)
            {
                if (count($node->canCreateClassList()) > 0)
                    return $node->attribute('node_id');
            }
        }

        return null;
    }

    /**
     *
     * Removes a node from an object.
     *
     * @param string $nodeId
     *
     * @return bool
     *
     */
    public static function removeNodeFromObject($nodeId)
    {
        \eZContentObjectTreeNode::removeSubtrees(array($nodeId), Objects::movetoTrash());

        return true;
    }
}
