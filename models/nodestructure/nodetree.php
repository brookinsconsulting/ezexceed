<?php
/**
 * Nodetree-model
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\nodestructure;

use ezexceed\models\content\Object;
use \eZLocale;
use \eZURI;
use \eZWordToImageOperator;
use \eZTemplate;
use \eZINI;
use \eZContentObjectTreeNode;
use \eZContentObject;

/**
 *
 * Description
 *
 * Nodetree-model.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 30.09.2011
 *
 */
class NodeTree
{
    /**
     * @var int The current node id.
     */
    protected $nodeId;
    /** @var \eZContentObjectTreeNode */
    protected $node;
    /** @var array */
    protected $response;
    /** @var bool */
    protected $containersOnly;

    /**
     * @param int $nodeId Retrieves the rootnode.
     * @param \eZContentObjectTreeNode $node
     * @param $containersOnly
     */
    public function init($nodeId, $node = null, $containersOnly = false)
    {
        $this->nodeId = $nodeId;
        $this->node = $node;
        $this->containersOnly = $containersOnly;

        /** Get the root node. */
        if (!$this->nodeId) {
            $ini = eZINI::instance('contentstructuremenu.ini');
            $this->nodeId = $ini->variable('TreeMenu', 'RootNodeID');
        }
    }

    /**
     *
     * Validates the node.
     *
     * @return bool
     */
    protected function validate()
    {
        if (!$this->node) {
            $this->response = array('error' => true, 'msg' => 'Node not found.');
            return false;
        }

        /** @var $contentObject \eZContentObject */
        $contentObject = $this->node->attribute('object');
        if (!$contentObject) {
            $this->response = array('error' => true, 'msg' => 'Object on node not available.');
            return false;
        }

        return true;
    }

    public function buildLocationSummaryTree($object = false)
    {
        if ($this->node === null)
            $this->node = eZContentObjectTreeNode::fetch($this->nodeId);

        if ($this->validate())
            return $this->summaryNode($this->node, $object);
        else
            return $this->response;
    }

    /**
     * Minimal node-representation used for locations.
     *
     * @return array
     */
    public function buildMinimalTree()
    {
        if ($this->node === null)
            $this->node = eZContentObjectTreeNode::fetch($this->nodeId);

        if (!$this->validate()) {
            return $this->response;
        }

        $node = $this->node;

        $data = $this->minimalNode($node);
        $data['parent'] = $this->minimalNode($node->attribute('parent'));

        return $data;
    }

    /**
     * Retrieves summary.
     *
     * @param $location
     * @param bool|\eZContentObject $object
     *
     * @return array|bool
     */
    protected function summaryNode($location, $object = false)
    {
        if (!is_object($location)) return false;

        $node = $object ? $object->attribute('main_node') : $location;

        if (!is_object($node)) return false;

        $url = $node->attribute('url');
        \eZURI::transformURI($url);

        $pathFormatted = $url;

        $allDrafts = self::getDraftsInfo();
        $classIdentifier = $node->ClassIdentifier;
        $classInfo = self::getContentClassInfo($classIdentifier);

        $response = array(
            'id' => (int)$location->NodeID,
            'objectId' => (int)$location->ContentObjectID,
            'name' => $location->Name,
            'url' => $url,
            'pathString' => $node->PathString,
            'classId' => $classInfo['id'],
            'isMain' => $node->isMain(),
            'pathFormatted' => $pathFormatted
        );

        if (isset($allDrafts[$node->ContentObjectID])) {
            $object = $location->object();
            $newName = Object::name($object);
            if ($newName != $response['name']) {
                $response['oldName'] = $node->Name;
                $response['name'] = $newName;
            }
        }

        return $response;
    }

    protected function getDraftsInfo()
    {
        static $allDrafts;
        if (!isset($allDrafts))
            $allDrafts = Object::getUsersDraftsInfo(\eZUser::currentUserID());
        return $allDrafts;
    }

    protected function formatPath($node)
    {
        return $node->PathString;
    }

    /**
     * Build a minimal representation of a node object
     *
     * @param \eZContentObjectTreeNode $node
     * @return array
     */
    protected function minimalNode($node)
    {
        if (!is_object($node)) return false;

        $url = $node->url();
        \eZURI::transformURI($url);

        $classIdentifier = $node->ClassIdentifier;
        $classInfo = self::getContentClassInfo($classIdentifier);

        $response = array(
            'id' => (int)$node->NodeID,
            'objectId' => (int)$node->ContentObjectID,
            'url' => $url,
            'name' => $node->Name,
            'pathString' => $node->PathString,
            'classId' => $classInfo['id'],
            'childrenCount' => self::fastSubtreeCount($node->NodeID),
            'parentId' => (int)$node->ParentNodeID,
            'isMain' => $node->NodeID == $node->MainNodeID,
            'pathFormatted' => $url
        );

        $allDrafts = self::getDraftsInfo();

        if (isset($allDrafts[$node->ContentObjectID])) {
            $object = $node->object();
            $newName = Object::name($object);
            if ($newName != $response['name']) {
                $response['oldName'] = $node->Name;
                $response['name'] = $newName;
            }
        }

        return $response;
    }

    /**
     *
     * Builds an array containing the root node and it's children.
     *
     * @param bool $getChildren
     *
     * @return array
     *
     */
    public function buildTreeArray($getChildren = true, $q = false, $offset = 0)
    {
        if ($this->node === null)
            $this->node = eZContentObjectTreeNode::fetch($this->nodeId);

        if (!$this->validate()) {
            return $this->response;
        }

        $this->response = array(
            'node' => $this->nodeToArray($this->node)
        );

        $limit = 100;

        $sort = $this->node->sortArray();
        $params = array(
            'Depth' => 1,
            'DepthOperator' => 'eq',
            'SortBy' => $sort,
            'Limit' => $limit,
            'IgnoreVisibility' => true,
            'AsObject' => false
        );

        $params = $this->addFilters($params);

        if ($getChildren) {
            $sort = $this->node->sortArray();
            if (!empty($q)) {
                $db = \eZDB::instance();
                $parsedquery = '%' . $db->escapeString($q) . '%';
                $sql = "SELECT t.node_id FROM ezcontentobject_tree t where t.parent_node_id = {$this->nodeId} and exists (select * from ezcontentobject_name n where n.name like '{$parsedquery}' and n.contentobject_id = t.contentobject_id and n.content_version = t.contentobject_version) limit {$offset},{$limit}";
                $rows = $db->arrayQuery($sql);
                $nodeids = array();

                foreach ($rows as $idrow) {
                    $nodeids[] = $idrow['node_id'];
                }

                if (count($nodeids) > 0) {
                    $children = eZContentObjectTreeNode::fetch($nodeids, false, false);

                    // if just one hit, the fetch doesn't wrap it in an array, which this class expects. wrap it
                    if (isset($children['id'])) {
                        $children = array($children);
                    }
                }
                else {
                    $children = array();
                }
            }
            else {
                /** @var $children eZContentObjectTreeNode[] */
                $children = $this->node->subTree($params);
            }

            $containers = array();
            $noncontainers = array();

            $sortField = $sort[0][0];
            if ($sortField !== 'priority') {
                $this->response['node']['children'] = array();
                /** @var $child \eZContentObjectTreeNode */
                foreach ($children as $child) {
                    $this->response['node']['children'][] = $this->nodeArrayToResponseArray($child, false);
                }
            } else {
                /** @var $child \eZContentObjectTreeNode */
                foreach ($children as $child) {
                    $node = $this->nodeArrayToResponseArray($child, false);
                    if ($node['isContainer'])
                        $containers[] = $node;
                    else
                        $noncontainers[] = $node;
                }

                $this->response['node']['children'] = array_merge($containers, $noncontainers);
            }
        }

        return $this->response;
    }

    /**
     * Filters out non containers if requested.
     *
     * @param array $params
     *
     * @return array
     */
    protected function addFilters($params)
    {
        if ($this->containersOnly) {
            $params['ExtendedAttributeFilter'] = array(
                'id' => 'eZExceed_node_filter',
                'params' => $this->containersOnly
            );
        }

        return $params;
    }

    /**
     * Retrieves the icon imageUrl for a given contentclass.
     *
     * @static
     *
     * @param $classIdentifier
     * @param $iconType
     *
     * @return string
     *
     */
    public static function getIconUrl($classIdentifier, $iconType)
    {
        $iconUrl = $classIdentifier;

        $tpl = eZTemplate::instance();
        $operator = new eZWordToImageOperator();
        $operatorParameters = array(array(array(1, $iconType)), 2 => array(array(true, true)));

        $operator->modify($tpl, 'class_icon', $operatorParameters, '', '', $iconUrl, array(), null);

        return $iconUrl;
    }

    /**
     *
     * Builds the iconidentifier used to build image-path.
     *
     * @param bool $large
     * @param bool $whiteIcons
     *
     * @return string
     *
     */
    public static function getIconTypeIdentifier($large = false, $whiteIcons = false)
    {
        $size = $large ? 'large' : 'small';
        $prefix = $whiteIcons ? 'white' : false;

        if ($prefix !== false)
            return $prefix . ucfirst($size);
        else
            return $size;
    }

    protected static function getContentClassInfo($classIdentifier)
    {
        static $classIds;
        if (!isset($classIds))
            $classIds = array();

        if (!isset($classIds[$classIdentifier])) {
            $class = \eZContentClass::fetchByIdentifier($classIdentifier);
            if (is_object($class)) {
                $classIds[$classIdentifier]['id'] = (int)$class->ID;
                $classIds[$classIdentifier]['is_container'] = (bool)$class->IsContainer;
            } else {
                $classIds[$classIdentifier]['id'] = 0;
                $classIds[$classIdentifier]['is_container'] = false;
            }
        }

        return $classIds[$classIdentifier];
    }

    /**
     * Converts a node array to a response array (same format at nodeToArray, but faster when served with arrays straight from db)
     *
     * @param \eZContentObjectTreeNode $node
     *
     * @param bool $fetchAllMetaInfo
     *
     * @return array
     */
    public function nodeArrayToResponseArray($node, $fetchAllMetaInfo = true)
    {
        $classIdentifier = $node['class_identifier'];
        $classInfo = self::getContentClassInfo($classIdentifier);

        $invisible = (bool)$node['is_invisible'];

        $id = $node['node_id'];

        static $contentRootNode;
        if (!isset($contentRootNode))
            $contentRootNode = NodeOperations::getContentRootNodeId();

        $allDrafts = self::getDraftsInfo();

        $nodeArray = array(
            'id' => (int)$id,
            'name' => $node['name'],
            'parentId' => (int)$node['parent_node_id'],
            'isMain' => $id == $node['main_node_id'],
            'object' => false,
            //'objectId' => (int)$node['contentobject_id'],
            'classId' => (int)$classInfo['id'],
            'pathString' => $node['path_string'],
            'hidden' => (bool)$node['is_hidden'],
            'invisible' => $invisible,
            'status' => false,
            'isContainer' => $classInfo['is_container'],
        );

        if ($fetchAllMetaInfo) {
            if (!isset($object))
                $object = \eZContentObject::fetch($node['contentobject_id']);

            $node = \eZContentObjectTreeNode::fetch($id);
            $url = $node->url();
            eZURI::transformURI($url);
            $fullUrl = $node->attribute('url_alias');

            $nodeArray['url'] = $url;
            $nodeArray['fullUrl'] = $fullUrl;

            \eZURI::transformURI($fullUrl, true, 'full');

            $subTreeCountParameters = array(
                'Depth' => 1,
                'DepthOperator' => 'eq',
                'IgnoreVisibility' => true
            );

            $subTreeCountParameters = $this->addFilters($subTreeCountParameters);
            $locale = eZLocale::instance();

            $db = \eZDB::instance();

            $sql = 'SELECT count(*) as c FROM ezcontentobject_tree WHERE contentobject_id = ' . $nodeArray['objectId'] . ' LIMIT 1';

            $rows = $db->arrayQuery($sql);
            $locationCount = $rows[0]['c'];

            $subTreeCount = (int)$node->subTreeCount($subTreeCountParameters);
            $nodeArray['objectId'] = $object->ID;
            $nodeArray['status'] = Object::changedSincePublished($object);
            $nodeArray['childrenCount'] = $subTreeCount;
            $nodeArray['hasChildren'] = $subTreeCount > 0;
            $nodeArray['parent'] = $this->minimalNode($node->attribute('parent'));

            $nodeArray['object'] = array(
                'modified' => $locale->formatShortDateTime($object->attribute('modified')),
                'author' => $this->getAuthor($object->owner()),
                'assignedNodesCount' => (int)$locationCount
            );

            $nodeArray['access'] = array(
                'read' => $node->canRead() && !$invisible,
                'edit' => $node->canEdit(),
                'remove' => $node->canRemove(),
                'move' => $node->canMoveFrom(),
                'copy' => $id != $contentRootNode,
                'hide' => $node->canHide() && $id != $contentRootNode
            );
        }
        else {
            $db = \eZDB::instance();

            if ($this->containersOnly) {
                $sql = 'SELECT
                            node_id
                        FROM
                            ezcontentobject_tree et,
                            ezcontentobject eo,
                            ezcontentclass ec
                        WHERE
                            eo.id = et.contentobject_id AND
                            ec.id = eo.contentclass_id AND
                            ec.is_container = 1 AND
                            parent_node_id =' . $id . ' LIMIT 1';
            }
            else {
                $sql = 'SELECT node_id FROM ezcontentobject_tree WHERE parent_node_id = ' . $id . ' LIMIT 1';
            }

            $rows = $db->arrayQuery($sql);
            $nodeArray['hasChildren'] = count($rows) > 0 ;
        }

        if (isset($nodeArray['objectId']) && isset($allDrafts[$nodeArray['objectId']])) {
            if (!isset($object))
                $object = \eZContentObject::fetch($node['contentobject_id']);

            $newName = Object::name($object);
            if ($newName !== $node['name']) {
                $nodeArray['oldName'] = $node['name'];
                $nodeArray['name'] = $newName;
            }
        }

        return $nodeArray;
    }

    /**
     * Converts a node object to an array
     *
     * @param \eZContentObjectTreeNode $node
     *
     * @param bool $fetchAllMetaInfo
     *
     * @return array
     */
    public function nodeToArray($node, $fetchAllMetaInfo = true)
    {
        $classIdentifier = $node->ClassIdentifier;
        $classInfo = self::getContentClassInfo($classIdentifier);

        $invisible = (bool)$node->attribute('is_invisible');

        $id = $node->attribute('node_id');

        static $contentRootNode;
        if (!isset($contentRootNode))
            $contentRootNode = NodeOperations::getContentRootNodeId();

        $allDrafts = self::getDraftsInfo();

        $nodeArray = array(
            'id' => (int)$id,
            'parentId' => (int)$node->attribute('parent_node_id'),
            'isMain' => $node->attribute('node_id') == $node->attribute('main_node_id'),
            'objectId' => (int)$node->attribute('contentobject_id'),
            'classId' => (int)$classInfo['id'],
            'pathString' => $node->attribute('path_string'),
            'hidden' => (bool)$node->attribute('is_hidden'),
            'invisible' => $invisible
        );

        $nodeArray['isContainer'] = $classInfo['is_container'];
        $nodeArray['name'] = $node->Name;

        if (isset($allDrafts[$node->attribute('contentobject_id')])) {
            $object = $node->object();
            $newName = Object::name($object);
            if ($newName != $nodeArray['name']) {
                $nodeArray['oldName'] = $node->Name;
                $nodeArray['name'] = $newName;
            }
        }

        if ($fetchAllMetaInfo) {
            if (!isset($object))
                $object = $node->object();
            $url = $node->url();
            eZURI::transformURI($url);
            $fullUrl = $node->attribute('url_alias');

            $nodeArray['url'] = $url;
            $nodeArray['fullUrl'] = $fullUrl;

            $subTreeCountParameters = array(
                'Depth' => 1,
                'DepthOperator' => 'eq',
                'IgnoreVisibility' => true
            );

            $db = \eZDB::instance();
            $sql = 'SELECT count(*) as c FROM ezcontentobject_tree WHERE contentobject_id = ' . $nodeArray['objectId'] . ' LIMIT 1';
            $rows = $db->arrayQuery($sql);
            $locationCount = $rows[0]['c'];

            $subTreeCountParameters = $this->addFilters($subTreeCountParameters);
            $locale = eZLocale::instance();

            $subTreeCount = $node->subTreeCount($subTreeCountParameters);
            $completeSubTreeCount = $node->subTreeCount(array('IgnoreVisibility' => true));
            $nodeArray['status'] = Object::changedSincePublished($object);
            $nodeArray['childrenCount'] = (int)$subTreeCount;
            $nodeArray['allChildrenCount'] = (int)$completeSubTreeCount;
            $nodeArray['hasChildren'] = $subTreeCount > 0;
            $nodeArray['parent'] = $this->minimalNode($node->attribute('parent'));

            $images = Object::images($object, array('width' => 336, 'height' => 336));

            $nodeArray['object'] = array(
                'modified' => $locale->formatShortDateTime($object->attribute('modified')),
                'author' => $this->getAuthor($object->owner()),
                'assignedNodesCount' => (int)$locationCount,
                'thumb' => $images['thumb']
            );
            $nodeArray['access'] = array(
                'read' => $node->canRead() && !$invisible,
                'edit' => $node->canEdit(),
                'remove' => $node->canRemove(),
                'move' => $node->canMoveFrom(),
                'copy' => $id != $contentRootNode,
                'hide' => $node->canHide() && $id != $contentRootNode
            );
        }

        return $nodeArray;
    }

    protected function getAuthor($author)
    {
        return array('id' => (int)$author->ID, 'name' => $author->Name);
    }

    /**
     *
     * Retrieves a subtree for a nodeId
     *
     * @param $nodeId
     * @param array $params
     *
     * @return array
     * @throws \ezcBaseFunctionalityNotSupportedException
     *
     */
    public static function getSubTree($nodeId, $params = array())
    {
        $limit = isset($params['limit']) ? (int)$params['limit'] : 25;
        $offset = isset($params['offset']) ? (int)$params['offset'] : 0;
        $sort = isset($params['sort']) ? $params['sort'] : 'priority';
        $order = isset($params['order']) ? $params['order'] : false;
        $objectNameFilter = isset($params['objectNameFilter']) ? $params['objectNameFilter'] : '';

        $node = \eZContentObjectTreeNode::fetch($nodeId);
        if (!$node instanceOf \eZContentObjectTreeNode) {
            throw new \ezcBaseFunctionalityNotSupportedException('Fetch node list', "Parent node '$nodeId' is not valid");
        }

        $params = array(
            'Depth' => 1,
            'Limit' => $limit,
            'Offset' => $offset,
            'SortBy' => array(array($sort, $order)),
            'DepthOperator' => 'eq',
            'ObjectNameFilter' => $objectNameFilter,
            'IgnoreVisibility' => true
        );

        $languages = \eZContentObject::translationStringList();
        \eZContentLanguage::setPrioritizedLanguages($languages);

        // fetch nodes and total node count
        if ($objectNameFilter=='') {
            $count = self::fastSubtreeCount($node->NodeID);
        }
        else {
            $count = $node->subTreeCount($params);
        }

        if ($count && $count > 1000 && $sort == 'priority' && $order) {
            $list = self::fastSubTreeByPriority($node, $offset, $limit);
        }
        else {
            $nodeArray = $count ? $node->subTree($params) : false;
            \eZContentLanguage::clearPrioritizedLanguages();
    
            // generate json response from node list
            $list = array();
            if ($nodeArray) {
                $list = static::formatSubtreeList($nodeArray);
            }
        }

        $sortMap = self::getSortMap();

        $sortKey = $node->attribute('sort_field');
        if (!isset($sortMap[$sortKey]))
            $sortKey = 9;

        return array(
            'parentNodeId' => (int)$nodeId,
            'count' => is_array($list) ? count($list) : 0,
            'total' => (int)$count,
            'list' => $list,
            'limit' => $limit,
            'offset' => $offset,
            'sort' => $sortMap[$sortKey],
            'order' => $node->attribute('sort_order'),
            'sortMap' => self::getSortMap()
        );
    }

    public static function fastSubTreeByPriority($node, $offset, $limit)
    {
        $db = \eZDB::instance();

        $sql = 'SELECT * 
        FROM ezcontentobject_tree
        WHERE ezcontentobject_tree.parent_node_id = '.$node->NodeID.'
        ORDER BY ezcontentobject_tree.priority ASC limit '.$offset.', '.$limit;

        $rows = $db->arrayQuery($sql);
        
        $response = array();

        /** @var $object \eZContentObjectTreeNode */
        foreach ($rows as $row) {
            $object = \eZContentObject::fetch($row['contentobject_id']);
            $classInfo = self::getContentClassInfo($object->ClassIdentifier);
            $resp = array(
                'id' => $row['node_id'],
                'nodeId' => $row['node_id'],
                'version' => $object->CurrentVersion,
                'canEdit' => $object->canEdit(),
                'name' => $object->Name,
                'objectId' => $row['contentobject_id'],
                'classId' => $classInfo['id'],
                'isContainer' => $classInfo['is_container'],
            );

            $response[] = $resp;
        }

        return $response;
    }

    protected static function fastSubtreeCount($nodeId)
    {
        $db = \eZDB::instance();

        $sql = 'SELECT count(*) as c
        FROM ezcontentobject_tree
        WHERE ezcontentobject_tree.parent_node_id = '.$nodeId;
        
        $rows = $db->arrayQuery($sql);
        return $rows[0]['c'];
    }

    /**
     * Retrieves the supported sort-alternatives.
     *
     * @return array
     */
    protected static function getSortMap()
    {
        $sortMap = array(
            8 => 'priority',
            9 => 'alphabetically',
            2 => 'published'
        );

        return $sortMap;
    }

    /**
     * Format and skip most of the data from a list of subcontent
     *
     * @param array $nodes
     * @return array
     */
    protected static function formatSubtreeList($nodes)
    {
        $response = array();

        /** @var $object \eZContentObjectTreeNode */
        foreach ($nodes as $object) {
            $classInfo = self::getContentClassInfo($object->ClassIdentifier);
            $resp = array(
                'id' => $object->attribute('node_id'),
                'nodeId' => $object->attribute('node_id'),
                'version' => $object->attribute('contentobject_version'),
                'canEdit' => $object->canEdit(),
                'name' => $object->Name,
                'objectId' => $object->attribute('contentobject_id'),
                'classId' => $classInfo['id'],
                'isContainer' => $classInfo['is_container'],
            );

            $response[] = $resp;
        }

        return $response;
    }


    /**
     * Sort the child nodes -> hasChildren -> alphabetical
     * @param array $nodes
     * @return void
     */
    protected function sortArray(array &$nodes)
    {
        usort($nodes, function($a, $b)
        {
            if ($a['isContainer']) {
                if ($b['isContainer']) {
                    $result = strcmp(strtolower($a['name']), strtolower($b['name']));
                    if ($result > 0) {
                        return 1;
                    }
                    elseif ($result == 0) {
                        return 0;
                    }
                    return -1;
                }
                return -1;
            }
            elseif ($b['isContainer']) {
                return 1;
            }
            else {
                $result = strcmp(strtolower($a['name']), strtolower($b['name']));
                if ($result > 0) {
                    return 1;
                }
                elseif ($result == 0) {
                    return 0;
                }
                return -1;
            }
        });
    }
}
