<?php

namespace ezexceed\models\activity;

use \ezexceed\models\activity\Activity;

/**
 * Like
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
class Like extends \eZPersistentObject
{
    const ERROR_ACTIVITYNOTFOUND = 'Activity not found';
    const ERROR_EMPTYACTIVITYID = 'Activity not set';
    const ERROR_ALREADYLIKED = 'Already liked';
    const ERROR_INVALIDUSER = 'Invalid user';

    protected $errorMsg;

    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    protected static $definition = array(
        'fields' => array(
            'activity_id' => array('name' => 'activity_id', 'datatype' => 'integer', 'required' => true),
            'user_id' => array('name' => 'user_id', 'datatype' => 'integer', 'required' => true),
            'time' => array('name' => 'time', 'datatype' => 'integer', 'required' => true)
        ),
        'keys' => array('activity_id','user_id'),
        'class_name' => '\\ezexceed\\models\\activity\\Like',
        'name' => 'ezexceed_activity_like'
    );

    public static function definition()
    {
        return self::$definition;
    }

    public static function exists($activityId, $userId)
    {
        return \eZPersistentObject::fetchObject(self::definition(), null, array( 'activity_id' => $activityId, 'user_id' => $userId))!==null;
    }

    public static function fetchForActivity($activityId)
    {
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array('activity_id' => $activityId), array('time' => 'desc'));
    }

    public function remove( $conditions = null, $extraConditions = null )
    {
        if (empty($this->activity_id))
        {
            $this->errorMsg = self::ERROR_EMPTYACTIVITYID;
            return false;
        }

        $activity = Activity::fetch($this->activity_id);

        if (!is_object($activity))
        {
            $this->errorMsg = self::ERROR_ACTIVITYNOTFOUND;
            return false;
        }

        $userObj = self::getUser($this->user_id);

        // check if submitting user exists
        if ($userObj == null)
        {
            $this->errorMsg = self::ERROR_INVALIDUSER;
            return false;
        }

        parent::remove($conditions, $extraConditions);

        // set has_likes in activity to 0 if this was the only like it had
        $likes = Like::fetchForActivity($this->activity_id);
        if (count($likes)==0)
        {
            \eZPersistentObject::updateObjectList(array('definition' => Activity::definition(), 'update_fields' => array('has_likes' => 0), 'conditions' => array('id' => $this->activity_id)));
        }

        return true;
    }

    protected static function getUser($id)
    {
        $userObject = \eZContentObject::fetch($id);

        if ($userObject!==null&&is_object($userObject)&&($classId = $userObject->ClassID) > 0)
        {
            $list = \eZContentClassAttribute::fetchFilteredList(array('contentclass_id' => $classId, 'data_type_string' => 'ezuser'));
            if (!is_array($list)||count($list)==0)
            {
                return null;
            }
            return $userObject;
        }
        return null;
    }

    public function store($fieldFilters = null)
    {
        $this->time = time();

        if (empty($this->activity_id))
        {
            $this->errorMsg = self::ERROR_EMPTYACTIVITYID;
            return false;
        }

        $userObj = self::getUser($this->user_id);

        // check if submitting user exists
        if ($userObj == null)
        {
            $this->errorMsg = self::ERROR_INVALIDUSER;
            return false;
        }

        $activity = Activity::fetch($this->activity_id);

        if (!is_object($activity))
        {
            $this->errorMsg = self::ERROR_ACTIVITYNOTFOUND;
            return false;
        }

        if (self::exists($this->activity_id, $this->user_id))
        {
            $this->errorMsg = self::ERROR_ALREADYLIKED;
            return false;
        }

        parent::store($fieldFilters);
        if ($activity->user_id!=$this->user_id)
        {
            // do not send notifications to yourself
            \ezexceed\modules\device\Device::notifyLike($activity->user_id, $userObj, $this->activity_id, $activity->contentobject_id);
        }

        if ($activity->has_likes == 0)
        {
            \eZPersistentObject::updateObjectList(array('definition' => Activity::definition(), 'update_fields' => array('has_likes' => 1), 'conditions' => array('id' => $this->activity_id)));
        }

        return true;
    }

    public static function create(array $data = array())
    {
        $device = new static($data);
        return $device;
    }
}
