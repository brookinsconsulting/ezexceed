<?php

/**
 * Mention
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\activity;

class Mention extends \eZPersistentObject
{
    protected static $definition = array(
        'fields' => array(
            'activity_id' => array('name' => 'activity_id', 'datatype' => 'integer', 'required' => true),
            'mentioned_user_id' => array('name' => 'mentioned_user_id', 'datatype' => 'integer', 'required' => true)
        ),
        'keys' => array('activity_id','user_id'),
        'class_name' => '\\ezexceed\\models\\Mention',
        'name' => 'ezexceed_activity_mention'
    );

    public static function definition()
    {
        return self::$definition;
    }

    public static function create(array $data = array())
    {
        $device = new static($data);
        return $device;
    }
}
