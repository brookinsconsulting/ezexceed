<?php

/**
 * Tag
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\activity;

class Tag extends \eZPersistentObject
{
    protected static $definition = array(
        'fields' => array(
            'activity_id' => array('name' => 'activity_id', 'datatype' => 'integer', 'required' => true),
            'tag' => array('name' => 'tag', 'datatype' => 'string', 'required' => true)
        ),
        'keys' => array('activity_id', 'tag'),
        'class_name' => '\\ezexceed\\models\\Tag',
        'name' => 'ezexceed_activity_tag'
    );

    public static function definition()
    {
        return self::$definition;
    }

    public static function create(array $data = array())
    {
        $device = new static($data);
        return $device;
    }
}
