<?php

namespace ezexceed\models\activity;

use \ezexceed\models\activity\Activity;

/**
 * Sticky
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */
class Sticky extends \eZPersistentObject
{
    const ERROR_ACTIVITYNOTFOUND = 'Activity not found';
    const ERROR_EMPTYACTIVITYID = 'Activity not set';
    const ERROR_ALREADYSTICKY = 'Activity already sticky';

    protected $errorMsg;

    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    protected static $definition = array(
        'fields' => array(
            'activity_id' => array('name' => 'activity_id', 'datatype' => 'integer', 'required' => true),
            'user_id' => array('name' => 'user_id', 'datatype' => 'integer', 'required' => true),
            'time' => array('name' => 'time', 'datatype' => 'integer', 'required' => true)
        ),
        'keys' => array('activity_id','user_id'),
        'class_name' => '\\ezexceed\\models\\activity\\Sticky',
        'name' => 'ezexceed_activity_sticky'
    );

    public static function definition()
    {
        return self::$definition;
    }

    public function fetch($activityId, $userId)
    {
        return \eZPersistentObject::fetchObject(self::definition(), null, array( 'activity_id' => $activityId, 'user_id' => $userId));
    }

    public static function exists($activityId, $userId)
    {
        return \eZPersistentObject::fetchObject(self::definition(), null, array( 'activity_id' => $activityId, 'user_id' => $userId))!==null;
    }

    public static function fetchForUserId($userId)
    {
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array('user_id' => $userId), array('time' => 'desc'));
    }

    public function store($fieldFilters = null)
    {
        $this->time = time();

        if (empty($this->activity_id))
        {
            $this->errorMsg = self::ERROR_EMPTYACTIVITYID;
            return false;
        }

        $activity = Activity::fetch($this->activity_id);

        if (!is_object($activity))
        {
            $this->errorMsg = self::ERROR_ACTIVITYNOTFOUND;
            return false;
        }

        if (self::exists($this->activity_id, $this->user_id))
        {
            $this->errorMsg = self::ERROR_ALREADYSTICKY;
            return false;
        }

        parent::store($fieldFilters);

        return true;
    }

    public static function create(array $data = array())
    {
        $device = new static($data);
        return $device;
    }
}
