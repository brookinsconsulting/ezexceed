<?php

/**
 * Activity
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\activity;

class Activity extends \eZPersistentObject
{
    const ERROR_EMPTYACTIVITY = 'Activity not set';
    const ERROR_STOREFAILED = 'Store failed';
    const ERROR_MENTIONNOTUSER = 'Mention not a valid user';
    const ERROR_INVALIDUSER = 'Invalid user';
    const ERROR_ACTIVITYTOOLONG = 'Activity too long';
    const ERROR_REPLIEDTOACTIVITYNOTFOUND = 'Activity replied to not found';
    const ERROR_CONTENTOBJECTNOTFOUND = 'Content object not found';
    const ERROR_INVALIDREPLYOBJECT = 'Reply cannot be connected to this object';

    protected $errorMsg;

    protected static $definition = array(
        'fields' => array(
            'id' => array('name' => 'id', 'datatype' => 'integer', 'required' => true),
            'activitystring' => array('name' => 'activitystring', 'datatype' => 'string', 'required' => true),
            'time' => array('name' => 'time', 'datatype' => 'integer', 'required' => true),
            'user_id' => array('name' => 'user_id', 'datatype' => 'integer', 'required' => true),
            'addressedto_id' => array('name' => 'addressedto_id', 'datatype' => 'integer', 'required' => true),
            'replyto_id' => array('name' => 'replyto_id', 'datatype' => 'integer', 'required' => true),
            'contentobject_id' => array('name' => 'contentobject_id', 'datatype' => 'integer', 'required' => true),
            'has_replies' => array('name' => 'has_replies', 'datatype' => 'integer', 'required' => true),
            'has_likes' => array('name' => 'has_likes', 'datatype' => 'integer', 'required' => true),
        ),
        'keys' => array('id'),
        "increment_key" => 'id',
        'class_name' => '\\ezexceed\\models\\activity\\Activity',
        'name' => 'ezexceed_activity'
    );

    public static function definition()
    {
        return self::$definition;
    }

    public function getErrorMsg()
    {
        return $this->errorMsg;
    }

    public static function create(array $data = array())
    {
        $device = new static($data);
        return $device;
    }

    public static function exists($id)
    {
        return self::fetch($id)!==null;
    }

    public static function fetch($id)
    {
        return \eZPersistentObject::fetchObject(self::definition(), null, array( 'id' => $id));
    }

    /*
    public static function fetchForContentObjectCount($contentObjectId)
    {
        return \eZPersistentObject::count(self::definition(), self::contentObjectConditions($contentObjectId));
    }
    */

    public static function fetchForContentObject($contentObjectId, $limit = 10, $offset = 0)
    {
        return \eZPersistentObject::fetchObjectList(self::definition(), null, self::contentObjectConditions($contentObjectId), array('id' => 'desc'), array('limit' => $limit, 'offset' => $offset));
    }

    protected static function contentObjectConditions($contentObjectId)
    {
        return array('contentobject_id' => $contentObjectId, 'replyto_id' => 0);
    }

    public static function unreadCount($lastActivityRead, $userid)
    {
        $customFields = array( array( 'operation' => 'COUNT(*)', 'name' => 'row_count' ) );
        $rows = \eZPersistentObject::fetchObjectList(self::definition(), array(), array('id' => array( '>', $lastActivityRead )), array(), null, false, false, $customFields, null, ' and (addressedto_id IN (0,'.(int)$userid.') or activitystring like \'%@{'.$userid.'}%\')');
        return $rows[0]['row_count'];
    }

    public static function fetchGlobal($userid, $limit = 10, $offset = 0)
    {
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array('replyto_id' => 0), array('id' => 'desc'), array('limit' => $limit, 'offset' => $offset), true, false, null, null, ' and (addressedto_id IN (0,'.(int)$userid.') or activitystring like \'%@{'.$userid.'}%\')');
    }

    public static function fetchReplies($id, $limit = 10, $offset = 0)
    {
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array('replyto_id' => $id), array('id' => 'asc'), array('limit' => $limit, 'offset' => $offset));
    }

    public static function fetchByHashtag($tag, $limit = 10, $offset = 0)
    {
        $db = \eZDB::instance();
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array(), array('ezexceed_activity.id' => 'desc'), array('limit' => $limit, 'offset' => $offset),
            true,
            false,
            $custom_fields = null,
            array('ezexceed_activity_tag'),
            ' WHERE ezexceed_activity.id = ezexceed_activity_tag.activity_id and ezexceed_activity_tag.tag = \''.$db->escapeString($tag).'\''
        );
    }

    public static function fetchBySticky($userid, $limit = 10, $offset = 0)
    {
        $db = \eZDB::instance();
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array(), array('id' => 'desc'), array('limit' => $limit, 'offset' => $offset),
            true,
            false,
            $custom_fields = null,
            null,
            ' where ezexceed_activity.id IN (select activity_id from ezexceed_activity_sticky where user_id = \''.$db->escapeString($userid).'\') '
        );
    }

    public static function fetchMentions($userid, $limit = 10, $offset = 0)
    {
        $db = \eZDB::instance();
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array(), array('ezexceed_activity.id' => 'desc'), array('limit' => $limit, 'offset' => $offset),
            true,
            false,
            $custom_fields = null,
            array('ezexceed_activity_mention'),
            ' where ezexceed_activity.id = ezexceed_activity_mention.activity_id and ezexceed_activity_mention.mentioned_user_id = \''.$db->escapeString($userid).'\''
        );
    }

    protected static function dayDiff($time1, $time2)
    {
            $s = abs($time2 - $time1);

            $m = intval($s/60);
            $s = $s % 60;
            $h = intval($m/60);
            $m = $m % 60;
            $d = intval($h/24);
            $h = $h % 24;

            return $d;
    }

    protected static function formatTime($time)
    {
        $now = time();
        $todaystart = mktime(0, 0, 0, date('m', $now), date('d', $now), date('Y', $now));
        $activitydaystart = mktime(0, 0, 0, date('m', $time), date('d', $time), date('Y', $time));
        $daydiff = self::dayDiff($activitydaystart, $todaystart);

        $diff = $now-$time;

        if ($diff<60)
        {
            // less than a minute
            $text = \ezpI18n::tr('eze', 'Just now');
        }
        elseif ($diff<(60*5))
        {
            $text = \ezpI18n::tr('eze', 'Less than 5 minutes ago');
        }
        elseif ($diff<3600)
        {
            // less than an hour, round to nearest 5 minutes
            $text = \ezpI18n::tr('eze', 'About count minutes ago', null, array('count' => (round($diff / 60 / 5) * 5)));
        }
        elseif ($diff<21600)
        {
            // less than 6 hours, round to nearest
            $text = \ezpI18n::tr('eze', 'About count hours ago', null, array('count' => floor($diff / (60 * 60))));
        }
        elseif ($daydiff == 0)
        {
            $text = \ezpI18n::tr('eze', 'Today');
        }
        elseif ($daydiff == 1)
        {
            $text = \ezpI18n::tr('eze', 'Yesterday');
        }
        else
        {
            $text = \ezpI18n::tr('eze', 'count days ago', null, array('count' => $daydiff));
        }
        return $text;
    }

    public function getData()
    {
        // add properties
        $data = array(
            'id' => (string)$this->id,
            'activitystring' => $this->activitystring,
            'timeformatted' => self::formatTime($this->time),
            'time' => (string)$this->time,
            'user_id' => (string)$this->user_id,
            'user_fullname' => self::getUserNameForUserId($this->user_id),
            'replyto_id' => (string)$this->replyto_id,
            'contentobject_id' => (string)$this->contentobject_id
        );

        if ($this->has_likes > 0)
        {
            $data['likes'] = array();
            $likes = Like::fetchForActivity($this->id);

            foreach ($likes as $like)
            {
                $data['likes'][(string)$like->user_id] = self::getUserNameForUserId($like->user_id);
            }
        }

        // add hash tags, reparse instead of db queries
        if (self::stringHasHashTags($this->activitystring))
        {
            $data['tags'] = array();
            $tags = self::parseHashTagsInActivityString($this->activitystring);
            foreach ($tags as $tag)
            {
                $data['tags'][] = '#'.$tag;
            }
        }

        // add mentions, reparse instead of db queries

        if (self::stringHasMentions($this->activitystring))
        {
            $data['mentions'] = array();
            $mentions = self::parseMentionsInActivityString($this->activitystring);
            foreach ($mentions as $mention)
            {
                $data['mentions'][$mention] = self::getUserNameForUserId($mention);
            }
        }

        // add replies recursively
        if ($this->has_replies>0)
        {
            $data['replies'] = array();
            $replies = self::fetchReplies($this->id);

            foreach ($replies as $reply) {
                $data['replies'][] = $reply->getData();
            }
        }
        return $data;
    }

    protected static function formatActivityString($activitystring)
    {
        $mentions = self::parseMentionsInActivityString($activitystring);

        foreach ($mentions as $mention)
        {
            $name = self::getUserNameForUserId($mention);
            $activitystring = str_replace('@{'.$mention.'}', '@'.$name, $activitystring);
        }

        return $activitystring;
    }

    protected static function getUserNameForUserId($id)
    {
        static $cache;
        if (!isset($cache))
        {
            $cache = array();
        }
        if (!isset($cache[$id]))
        {
            // get user's name from object db
            $object = \eZContentObject::fetch($id);
            if (is_object($object))
            {
                $cache[$id] = $object->Name;
            }
            else
                $cache[$id] = false;
        }
        return $cache[$id];
    }

    public function store($fieldFilters = null)
    {
        $hashTags = self::parseHashTagsInActivityString($this->activitystring);
        $mentions = self::parseMentionsInActivityString($this->activitystring);
        $adressedTo = self::parseAdressedToInActivityString($this->activitystring);

        // set time to now
        $this->time = time();
        $this->has_replies = 0;
        $this->has_likes = 0;
        $this->addressedto_id = $adressedTo;

        // validate activity string
        if (empty($this->activitystring))
        {
            $this->errorMsg = self::ERROR_EMPTYACTIVITY;
            return false;
        }

        // max length 4000 bytes
        if (strlen($this->activitystring)>4000)
        {
            $this->errorMsg = self::ERROR_ACTIVITYTOOLONG;
            return false;
        }

        // check if all mentions are actual users
        foreach ($mentions as $mention)
        {
            if (!self::isUser($mention))
            {
                $this->errorMsg = self::ERROR_MENTIONNOTUSER;
                return false;
            }
        }

        $userObj = self::getUser($this->user_id);

        // check if submitting user exists
        if ($userObj == null)
        {
            $this->errorMsg = self::ERROR_INVALIDUSER;
            return false;
        }

        // if activity is a reply to another activity, make sure it exists
        if ($this->replyto_id != 0)
        {
            $repliedToObj = self::fetch($this->replyto_id);

            if ($repliedToObj === null)
            {
                $this->errorMsg = self::ERROR_REPLIEDTOACTIVITYNOTFOUND;
                return false;
            }

            // a reply cannot be connected to another object than the activity it is a reply to
            if ($this->contentobject_id != 0)
            {
                if ($repliedToObj->contentobject_id != $this->contentobject_id)
                {
                    $this->errorMsg = self::ERROR_INVALIDREPLYOBJECT;
                    return false;
                }
            }
        }

        // if related to a content object, make sure it exists
        if ($this->contentobject_id != 0&&!self::isContentObject($this->contentobject_id))
        {
            $this->errorMsg = self::ERROR_CONTENTOBJECTNOTFOUND;
            return false;
        }

        // store activity in db
        parent::store($fieldFilters);

        $formattedActivity = self::formatActivityString($this->activitystring);

        if ($this->replyto_id != 0)
        {
            if ($repliedToObj->has_replies == 0)
            {
                // update replied to activity (denormalized), to avoid unnecessary subtree queries in the future
                \eZPersistentObject::updateObjectList(array('definition' => self::definition(), 'update_fields' => array('has_replies' => 1), 'conditions' => array('id' => $this->replyto_id)));
            }

            if ($this->user_id != $repliedToObj->user_id)
            {
                // do not send reply notifications to yourself
                \ezexceed\modules\device\Device::notifyReply($repliedToObj->user_id, $userObj, $this->id, $this->contentobject_id, $formattedActivity);
            }
        }

        // if no id is set by store(), store failed
        if (empty($this->id))
        {
            $this->errorMsg = self::ERROR_STOREFAILED;
            return false;
        }

        // add mentions to db
        foreach ($mentions as $mention)
        {
            $mentionObj = Mention::create(array(
                'activity_id' => $this->id,
                'mentioned_user_id' => $mention
            ));

            $mentionObj->store();

            if ($this->user_id != $mention)
            {
                // do not send mention notifications to yourself
                \ezexceed\modules\device\Device::notifyMention($mention, $userObj, $this->id, $this->contentobject_id, $formattedActivity);
            }
        }

        // add hashtags to db
        foreach ($hashTags as $hashTag)
        {
            $tagObj = Tag::create(array(
                'activity_id' => $this->id,
                'tag' => $hashTag
            ));

            $tagObj->store();
        }

        return true;
    }

    protected static function getUser($id)
    {
        $userObject = \eZContentObject::fetch($id);

        if ($userObject!==null&&is_object($userObject)&&($classId = $userObject->ClassID) > 0)
        {
            $list = \eZContentClassAttribute::fetchFilteredList(array('contentclass_id' => $classId, 'data_type_string' => 'ezuser'));
            if (!is_array($list)||count($list)==0)
            {
                return null;
            }
            return $userObject;
        }
        return null;
    }

    protected static function isUser($id)
    {
        $userObject = \eZContentObject::fetch($id);

        if ($userObject!==null&&is_object($userObject)&&($classId = $userObject->ClassID) > 0)
        {
            $list = \eZContentClassAttribute::fetchFilteredList(array('contentclass_id' => $classId, 'data_type_string' => 'ezuser'));
            if (!is_array($list)||count($list)==0)
            {
                return false;
            }
            return true;
        }
        return false;
    }

    protected static function isContentObject($id)
    {
        if (\eZContentObject::fetch($id)!==null)
        {
            return true;
        }
        return false;
    }

    protected static function stringHasMentions($activityString)
    {
        if (preg_match('/(^|\s|,|.|:)@{([^(})]+)}/i', $activityString))
        {
            return true;
        }
        return false;
    }

    protected static function stringHasHashTags($activityString)
    {
        if (preg_match('/(^|\s)#([^(#|\s|,|!|:|\\.|@)]+)/i', $activityString))
        {
            return true;
        }
        return false;
    }

    protected static function parseAdressedToInActivityString($activityString)
    {
        if (preg_match_all('/^@{([^(})]+)}/', $activityString, $sub)>0)
        {
            if (is_array($sub[1]) && isset($sub[1][0]))
            {
                return $sub[1][0];
            }
        }

        return 0;
    }

    protected static function parseMentionsInActivityString($activityString)
    {
        if (preg_match_all('/(^|\s|,|.|:)@{([^(})]+)}/', $activityString, $sub)>0)
        {
            return $sub[2];
        }
        return array();
    }

    protected static function parseHashTagsInActivityString($activityString)
    {
        if (preg_match_all('/(^|\s)#([^(#|\s|,|!|:|\\.|@)]+)/', $activityString, $sub)>0)
        {
            return $sub[2];
        }
        return array();
    }
}
