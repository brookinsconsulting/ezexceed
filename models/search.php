<?php
/**
 * Search
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

/**
 *
 * Description
 *
 * Klasse for å generere nodetre basert på søk.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 30.09.2011
 *
 */

namespace ezexceed\models;

use \eZSearch;
use \eZINI;

use \ezexceed\models\search\Result;

/**
 *
 * Description
 *
 * Interfaces with the current searchEngine and returns the results as json.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 24.10.2011
 *
 */
class Search
{
    /** @var string The query. */
    protected $q;
    /** @var int The number of hits. */
    protected $total;
    /** @var bool  */
    protected $ezFind;

    /**
     * Standard get/setter.
     *
     * @return int
     */
    public function getSearchCount() { return $this->total; }

    /**
     *
     * Instanciates the class and checks if eZFind is enabled.
     *
     * @param string $query
     */
    public function __construct($query)
    {
        $this->q = $query;
        $this->total = 0;

        $activeExtensions = eZINI::instance('site.ini')->variable('ExtensionSettings', 'ActiveExtensions');

        if (in_array('ezfind', $activeExtensions)) $this->ezFind = true;
        else $this->ezFind = false;
    }

    /**
     *
     * Performs the actual search.
     *
     * @param int $limit
     * @param int $classFilter
     *
     * @return \ezexceed\models\search\Result
     */
    public function performSearch($limit, $classFilter = null)
    {
        $responseModel = new Result($this->q, $limit, $classFilter);

        $ezSearch = new eZSearch();

        $params = array();
        $params['SearchLimit'] = $limit;

        if ($this->ezFind)
        {
            $params['SearchContentClassID'] = array($classFilter);
            $params['facet'][] = array('field' => 'class');
        }

        $result = $ezSearch->search($this->q, $params);

        $responseModel->fromResult($result, $this->ezFind);

        return $responseModel;
    }
}
