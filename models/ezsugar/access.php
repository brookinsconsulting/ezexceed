<?php
/**
 * eZPencil template operator
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\ezsugar;

use eZContentClass;

class Access
{
    /**
     * Creates a list over objects the current user can create.
     *
     * @param \eZUser $user User object to check for
     * @param string $module
     * @parma string $action
     * @return \eZPersistentObject[]
     */
    public static function getGlobalClassList(\eZUser $user, $module, $action)
    {
        $accessResult = $user->hasAccessTo($module, $action);
        $accessWord = $accessResult['accessWord'];

        switch ($accessWord) {
            case 'yes':
                return eZContentClass::fetchList(eZContentClass::VERSION_STATUS_DEFINED, true, false, null, null);
            case 'limited':
                return self::parsePolicies($accessResult['policies']);
            default:
                return array();
        }
    }

    /**
     * Check if user can access a given module/action
     *
     * @param \eZUser $user User object to check for
     * @param string $module
     * @parma string $action
     * @return bool
     */
    public static function userCanAccess(\eZUser $user, $module, $action, $require = 'limited')
    {
        $accessResult = $user->hasAccessTo($module, $action);
        $accessWord = $accessResult['accessWord'];

        switch ($accessWord) {
            case 'yes':
                return true;
            case 'limited':
                if ($require === 'limited') return true;
                // Do detailed check
                return false;
            case 'no':
            default:
                return false;
        }
    }

    /**
     * Parses class-policies if any.
     *
     * @param $policies
     * @return array
     */
    public static function parsePolicies($policies)
    {
        $classes = array();
        $classList = array();

        foreach ($policies as $policy)
        {
            $classPolicy = $policy['Class'];

            if (isset($classPolicy))
            {
                if ($classPolicy =='*')
                    $classList = eZContentClass::fetchList(ezcontentclass::VERSION_STATUS_DEFINED, true, false, null, null);
                else
                    foreach ((array)$classPolicy as $classId)
                        $classes[$classId] = true;
            }
        }

        if (count($classes) > 0)
        {
            $filter = array_keys($classes);
            $classList = eZContentClass::fetchList(ezcontentclass::VERSION_STATUS_DEFINED, true, false, null, null, $filter);
        }

        return $classList;
    }
}
