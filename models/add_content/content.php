<?php

/**
 * Retrieves information regarding user-permissions and adding of content
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\add_content;

use \ezexceed\models\nodestructure\NodeTree;
use ezexceed\classes\utilities\IniUtilities;

/**
 *
 * Description
 *
 * Retrieves information regarding user-permissions and adding of content.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 30.04.2012
 *
 */
class Content
{
    /**
     *
     * Retrieves available classes for the given node for the current user.
     *
     * @param \eZContentObjectTreeNode $nodeId
     *
     * @param array $parsedClasses
     *
     * @return array
     *
     */
    public static function getClassListForNodeId($nodeId, $parsedClasses = array())
    {
        $node = \eZContentObjectTreeNode::fetch($nodeId);
        $classList = $node->canCreateClassList();

        $toolbarINI = \eZINI::instance('websitetoolbar.ini');
        $hiddenClasses = IniUtilities::checkGetSetting($toolbarINI, 'WebsiteToolbarSettings', 'HiddenContentClasses', array());

        foreach ($classList as $class)
        {
            $classId = $class['id'];
            $contentClass = \eZContentClass::fetch($classId);

            if (in_array($contentClass->Identifier, $hiddenClasses))
                continue;

            $class['icon'] = NodeTree::getIconUrl($contentClass->Identifier, 'small');

            $parsedClasses[] = $class;
        }

        return $parsedClasses;
    }
}