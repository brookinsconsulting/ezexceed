<?php
/**
 * Model containing objects for the pencil-operator-view
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\pencil;

use \ezexceed\models\content\Object;
use ezexceed\models\page\Page;
use \ezexceed\classes\utilities\BlockMethods;

/**
 *
 * Description
 *
 * Model containing objects for the pencil-operator-view.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 27.03.2012
 *
 */
class Pencil
{
    /** @var array */
    protected $entities;
    /** @var string */
    protected $title;
    /** @var string */
    protected $blockName;
    /** @var \eZTemplate */
    protected $tpl;
    /** @var int */
    protected $zoneIndex;
    /** @var \eZPageBlock */
    protected $block;
    /** @var \eZContentObjectAttribute */
    protected $layoutAttribute;

    /**
     *
     * Sets the default values.
     *
     * @param \eZTemplate $layoutAttribute
     *
     */
    public function __construct($layoutAttribute)
    {
        $this->entities = array();
        $this->title = '';
        $this->block = null;
        $this->layoutAttribute = $layoutAttribute;
    }

    /**
     *
     * Populates the model based on the given parameters.
     *
     * @param $operatorValue
     *
     */
    public function fill($operatorValue)
    {
        if (is_array($operatorValue))
        {
            foreach ($operatorValue as $key => $value)
            {
                /** Arrays of blocks is not supported. */
                if ($value instanceof \eZPageBlock)
                    return;

                /** Array contains objects or nodes. */
                if (is_numeric($key))
                    $this->addEntity($value);
                else
                    /** Array is a set of Ids. */
                    $this->addIdArray($value, $key);
            }
        }
        else
            $this->addEntity($operatorValue);
    }

    /**
     *
     * Adds an object, node or block to the model.
     *
     * @param $value
     *
     */
    protected function addEntity($value)
    {
        if ($value instanceof \eZPageBlock)
            $this->addBlock($value);
        else if ($value instanceof \eZContentObjectTreeNode)
            $this->addNode($value);
        else if ($value instanceof \eZContentObject)
            $this->addObject($value);
    }

    /**
     * Retrieves the block from the latest user-draft.
     *
     * @param \eZPageBlock $currentBlock
     *
     * @return \eZPageBlock
     */
    protected function fetchLatestUserdraftBlock($currentBlock)
    {
        if ($this->layoutAttribute === null)
            return $currentBlock;

        $objectId = $this->layoutAttribute->attribute('contentobject_id');

        if (!$draft = Object::fetchLatestUserDraft($objectId, \eZUser::currentUserID()))
            return $currentBlock;

        if (!$attribute = Page::fetchEzpageAttribute($draft))
            return $currentBlock;

        /** @var $attributeContent \eZPage */
        $attributeContent = $attribute->content();

        /** @var $zone \eZPageZone */
        foreach($attributeContent->attribute('zones') as $zone) {
            for($i = 0; $i < $zone->getBlockCount(); $i++) {
                $block = $zone->getBlock($i);

                if ($currentBlock->attribute('id') === $block->attribute('id'))
                    return $block;
            }
        }

        return $currentBlock;
    }

    /**
     *
     * Adds the valid nodes from a block.
     *
     * @param \eZPageBlock $block
     *
     */
    protected function addBlock($block)
    {
        $block = $this->fetchLatestUserdraftBlock($block);

        foreach ($block->getValidItemsAsNodes() as $node)
            $this->addNode($node);

        $queue = $block->attribute('waiting');
        if ($queue) {
            $this->addSeparator('Content in queue');

            $map = function($item) {
                /** @var $item \eZPageBlockItem */
                return $item->attribute('object_id');
            };

            $idList = array_map($map, $queue);
            $this->addIdArray($idList, 'objects');
        }

        $this->block = $block;
        $this->blockName = BlockMethods::retrieveBlockDescription($this->block);

        $this->setZoneIndex();
    }

    /**
     *
     * Retrieves the index of a zone/block within a page-attribute.
     *
     */
    protected function setZoneIndex()
    {
        /** @var $attributeContent \eZPage */
        $attributeContent = $this->layoutAttribute->content();
        /** @var $zone \eZPageZone */
        foreach ((array)$attributeContent->attribute('zones') as $zoneIndex => $zone)
        {
            /** @var $block \eZPageBlock */
            foreach ((array)$zone->attribute('blocks') as $block)
            {
                if ($block->attribute('id') === $this->block->attribute('id'))
                {
                    $this->zoneIndex = $zoneIndex;
                    break;
                }
            }
        }
    }

    /**
     *
     * Adds a node.
     *
     * @param \eZContentObjectTreeNode $node
     *
     */
    protected function addNode($node)
    {
        if ($node instanceof \eZContentObjectTreeNode && $node->canEdit()) {
            $this->addObject($node->attribute('object'));
        }
    }

    /**
     *
     * @param \eZContentObject $object
     *
     */
    protected function addObject($object)
    {
        if ($object->canEdit()) {
            $entity = array();

            $entity['id'] = $object->attribute('id');
            $entity['name'] = Object::name($object);
            $entity['classIdentifier'] = $object->contentClassIdentifier();

            $this->entities[] = $entity;
        }
    }

    /**
     * Add a separator in list of entities
     *
     * @param string $title
     */
    protected function addSeparator($title = '')
    {
        $this->entities[] = array(
            'separator' => true,
            'title' => $title
        );
    }

    /**
     *
     * Retrieves and adds an array of entities.
     *
     * @param $values
     * @param $key
     *
     */
    protected function addIdArray($values, $key)
    {
        $definition = null;
        $field = null;

        if ($key === 'objects')
        {
            $field = 'id';
            $definition = \eZContentObject::definition();
        }
        elseif($key === 'nodes')
        {
            $field = 'node_id';
            $definition = \eZContentObjectTreeNode::definition();
        }

        $this->title = \ezpI18n::tr('ezexceed', 'Edit ' . $key);

        if ($definition !== null)
        {
            $conditions = array($field => array($values));
            $entities = \eZPersistentObject::fetchObjectList($definition, null, $conditions);

            foreach($entities as $entity)
                $this->addEntity($entity);
        }

    }

    /**
     *
     * Returns the value of the given attribute.
     *
     * @param $attribute
     *
     * @return mixed
     *
     */
    public function attribute($attribute)
    {
        return $this->$attribute;
    }

    /**
     *
     * Checks if the given property exists.
     *
     * @param $attributeName
     *
     * @return bool
     *
     */
    public function hasAttribute($attributeName)
    {
        if (property_exists(__CLASS__, $attributeName)) return true;
        else return false;
    }
}
