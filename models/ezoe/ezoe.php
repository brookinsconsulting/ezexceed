<?php
/**
 * Retrieves the settings needed for eZOE
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\ezoe;

use \ezexceed\classes\utilities\IniUtilities;

/**
 *
 * Description
 *
 * Retrieves the settings needed for eZOE.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 16.04.2012
 *
 */
class EzOE
{
    /** @var string */
    protected $mode;
    /** @var string */
    protected $theme;
    /** @var string */
    protected $width;
    /** @var string */
    protected $language;
    /** @var bool|string */
    protected $skin;
    /** @var string */
    protected $skin_variant;
    /** @var string */
    protected $plugins;
    /** @var bool|string */
    protected $directionality;
    /** @var string */
    protected $theme_advanced_buttons1;
    /** @var string */
    protected $theme_advanced_buttons2;
    /** @var string */
    protected $theme_advanced_buttons3;
    /** @var string Removes address tag, not suppored by ezxml. */
    protected $theme_advanced_blockformats;
    /** @var bool Ignore, use theme_advanced_statusbar_location. */
    protected $theme_advanced_path_location;
    /** @var string Correct value set by layout code bellow pr attribute. */
    protected $theme_advanced_statusbar_location;
    /** @var string Correct value set by layout code bellow pr attribute. */
    protected $theme_advanced_toolbar_location;
    /** @var bool|string */
    protected $theme_advanced_toolbar_align;
    /** @var bool */
    protected $theme_advanced_toolbar_floating;
    /** @var bool */
    protected $theme_advanced_resize_horizontal;
    /** @var bool */
    protected $theme_advanced_resizing;
    /** @var string */
    protected $valid_elements;
    /** @var string */
    protected $valid_child_elements;
    /** @var string We need to transform nonbreaking white space to encoded form, all other charthers as stored in raw unicode form. */
    protected $entities;
    /** @var bool */
    protected $fix_list_elements;
    /** @var bool */
    protected $fix_table_elements;
    /** @var bool */
    protected $convert_urls;
    /** @var bool */
    protected $inline_styles;
    /** @var string */
    protected $tab_focus;
    /** @var bool|string|array */
    protected $theme_ez_xml_alias_list;
    /** @var string */
    protected $theme_ez_statusbar_open_dialog;
    /** @var bool */
    protected $gecko_spellcheck;
    /** @var bool Disable firefox inline image/table resizing. */
    protected $object_resizing;
    /** @var bool Table edit controlls in gecko. */
    protected $table_inline_editing;
    /** @var bool */
    protected $save_enablewhendirty;
    /** @var string */
    protected $atd_rpc_id;
    /** @var string This list contains the categories of errors we want to show. */
    protected $atd_show_types;
    /** @var string Strings this plugin should ignore. */
    protected $atd_ignore_strings;
    /** @var string "Ignore Always" menu item, uses cookies by default. Set atd_ignore_rpc_url to a URL AtD should send ignore requests to. */
    protected $atd_ignore_enable;

    /**
     * Initializes the variables.
     */
    public function __construct()
    {
        $eZOEIni = \ezini::instance('ezoe.ini');

        $this->mode = 'none';
        $this->theme = 'ez';
        $this->width = '100%';
        $this->language = '-' . \eZLocale::currentLocaleCode();
        $this->skin = $eZOEIni->variable('EditorSettings', 'Skin');
        $this->skin_variant = '';
        $this->plugins = implode(',', IniUtilities::checkGetSetting($eZOEIni, 'EditorSettings', 'Plugins', array()));
        $this->directionality = IniUtilities::checkGetSetting($eZOEIni, 'EditorSettings', 'Directionality', 'ltr');
        $this->theme_advanced_buttons1 = implode(',', IniUtilities::checkGetSetting($eZOEIni, 'EditorLayout', 'Buttons', array()));
        $this->theme_advanced_buttons2 = '';
        $this->theme_advanced_buttons3 = '';
        $this->theme_advanced_blockformats = 'p,pre,h1,h2,h3,h4,h5,h6';
        $this->theme_advanced_path_location = false;
        $this->theme_advanced_statusbar_location = 'bottom';
        $this->theme_advanced_toolbar_location = 'top';
        $this->theme_advanced_toolbar_align = IniUtilities::checkGetSetting($eZOEIni, 'EditorSettings', 'ToolbarAlign', 'left');
        $this->theme_advanced_toolbar_floating = true;
        $this->theme_advanced_resize_horizontal = false;
        $this->theme_advanced_resizing = true;
        $this->valid_elements = '-strong/-b/-bold[class|customattributes],-em/-i/-emphasize[class|customattributes],span[id|type|class|title|customattributes|align|style|view|inline|alt],sub[class|type|customattributes|align],sup[class|type|customattributes|align],u[class|type|customattributes|align],pre[class|title|customattributes],ol[class|customattributes],ul[class|customattributes],li[class|customattributes],a[href|name|target|view|title|class|id|customattributes],p[class|customattributes|align|style],img[id|type|class|title|customattributes|align|style|view|inline|alt|src],table[class|border|width|id|title|customattributes|ezborder|bordercolor|align|style],tr[class|customattributes],th[class|width|rowspan|colspan|customattributes|align|style],td[class|width|rowspan|colspan|customattributes|align|style],div[id|type|class|title|customattributes|align|style|view|inline|alt],h1[class|customattributes|align|style],h2[class|customattributes|align|style],h3[class|customattributes|align|style],h4[class|customattributes|align|style],h5[class|customattributes|align|style],h6[class|customattributes|align|style],br';
        $this->valid_child_elements = 'a[%itrans_na],table[tr],tr[td|th],ol/ul[li],h1/h2/h3/h4/h5/h6/pre/strong/b/p/em/i/u/span/sub/sup/li[%itrans|#text]div/pre/td/th[%btrans|%itrans|#text]';
        $this->entities = '160,nbsp';
        $this->fix_list_elements = true;
        $this->fix_table_elements = true;
        $this->convert_urls = false;
        $this->inline_styles = false;
        $this->tab_focus = ':prev,:next';
        $this->theme_ez_xml_alias_list = IniUtilities::checkGetSetting($eZOEIni, 'EditorSettings', 'XmlTagNameAlias', array());
        $this->theme_ez_statusbar_open_dialog = IniUtilities::checkGetSetting($eZOEIni, 'EditorSettings', 'TagPathOpenDialog') === 'enabled';
        $this->gecko_spellcheck = true;
        $this->object_resizing = false;
        $this->table_inline_editing = true;
        $this->save_enablewhendirty = true;
        $this->atd_rpc_id = 'your API key here';
        $this->atd_show_types = 'Bias Language,Cliches,Complex Expression,Diacritical Marks,Double Negatives,Hidden Verbs,Jargon Language,Passive voice,Phrases to Avoid,Redundant Expression';
        $this->atd_ignore_strings = 'AtD,rsmudge';
        $this->atd_ignore_enable = 'true';
    }

    /**
     *
     * Retrieves settings.
     *
     * @return string
     *
     */
    public function getSettings()
    {
        $settings = array();

        foreach($this as $key => $value)
            $settings[$key] = $value;

        return array('result' => json_encode($settings));
    }
}
