<?php
/**
 * Layout-model
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\layout;

use \ezexceed\models\content\Object;
use \ezexceed\models\page\Page;
use \ezexceed\classes\utilities\BlockMethods;

/**
 *
 * Description
 *
 * Layout-model
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 12.12.2011
 *
 */
class Layout
{
    /** @var array */
    protected $allowedZones;
    /** @var string */
    protected $objectClassname;
    /** @var string */
    protected $currentZoneLayout;
    /** @var array */
    protected $blockNames;
    /** @var Page */
    protected $pageModel;

    /**
     *
     * Creates a new layout-model.
     *
     * @param int $objectId
     * @param $language
     *
     * @return Layout
     *
     */
    public function __construct($objectId, $language)
    {
        $this->pageModel = new Page($objectId, $language);
    }

    /**
     *
     * Returns the value of the given attribute.
     *
     * @param $attribute
     *
     * @return mixed
     *
     */
    public function attribute($attribute)
    {
        return $this->$attribute;
    }

    /**
     * @return \eZPageZone
     */
    public function getZone()
    {
        return $this->pageModel->zone;
    }

    /**
     * @return int
     */
    public function getObjectId()
    {
        return $this->pageModel->objectId;
    }

    /**
     *
     * Checks if the given property exists.
     *
     * @param $attributeName
     *
     * @return bool
     *
     */
    public function hasAttribute($attributeName)
    {
        if (property_exists(__CLASS__, $attributeName)) return true;
        else return false;
    }

    /**
     *
     * Populates the model.
     *
     * @param $zoneIndex
     * @param $blockId
     *
     */
    public function load($zoneIndex, $blockId)
    {
        $this->pageModel->load();
        $this->pageModel->loadLayout($zoneIndex, $blockId);

        $functions = new \eZFlowFunctionCollection();
        $allowedZones = $functions->fetchAllowedZones();

        $eZINI = \eZINI::instance('block.ini');
        $allowedBlockTypes = $eZINI->variable('General', 'AllowedTypes');

        /** @var $allowedBlockTypes array */
        foreach($allowedBlockTypes as $blockType)
            $this->blockNames[$blockType] = BlockMethods::getBlockNameByType($blockType, $eZINI);

        asort($this->blockNames);

        $this->allowedZones = $allowedZones['result'];
        $this->objectClassname = $this->pageModel->contentObject->ClassIdentifier;
        $this->currentZoneLayout = $this->pageModel->attributeContent->attribute('zone_layout');
    }

    /**
     *
     * Gives a block a new position.
     *
     * @param $positions
     *
     */
    public function moveBlock($positions)
    {
        if ($positions > 0) $upwards = true;
        else $upwards = false;

        $positions = abs($positions);
        $blockIndex = $this->pageModel->blockIndex;

        for($i = 0; $i < $positions; $i++)
        {
            if ($upwards)
            {
                $this->pageModel->zone->moveBlockUp($blockIndex);
                $blockIndex--;
            }
            else
            {
                $this->pageModel->zone->moveBlockDown($blockIndex);
                $blockIndex++;
            }
        }
    }

    /**
     *
     * Saves the layout.
     *
     * @param $layout
     *
     */
    public function saveLayout($layout)
    {
        $zoneINI = \eZINI::instance('zone.ini');
        /** @var $allowedZones array */
        $allowedZones = $zoneINI->variable($layout, 'Zones');

        $page = $this->pageModel->attributeContent;

        $page->setAttribute('zone_layout', $layout);

        foreach ($allowedZones as $index => $zoneIdentifier)
        {
            $existingZone = $page->getZone($index);

            if ($existingZone instanceof \eZPageZone)
            {
                $existingZone->setAttribute('action', 'modify');
                $existingZone->setAttribute('zone_identifier', $zoneIdentifier);
            }
            else
            {
                $newZone = $page->addZone(new \eZPageZone());
                $newZone->setAttribute('id', md5(mt_rand() . microtime() . $page->getZoneCount()));
                $newZone->setAttribute('zone_identifier', $zoneIdentifier);
                $newZone->setAttribute('action', 'add');
            }
        }

        $zonediff = $page->getZoneCount() - count($allowedZones);

        if ($zonediff > 0)
        {
            $startIndex = $page->getZoneCount() - $zonediff;
            $firstZone = $page->getZone(0);

            while ($zone = $page->getZone($startIndex))
            {
                $zone->setAttribute('action', 'remove');

                $count = $zone->getBlockCount();

                for($i = 0; $i < $count; $i++)
                {
                    $firstZone->addBlock($zone->getBlock($i));
                }

                $startIndex++;
            }
        }

        $page->sortZones();
    }

    /**
     *
     * Stores the model.
     *
     */
    public function store()
    {
        $this->pageModel->store();
    }

    /**
     *
     * Removes a block.
     *
     */
    public function removeBlock()
    {
        $this->pageModel->zone->removeBlock($this->pageModel->blockIndex);
    }

    /**
     *
     * Adds a block to a zone.
     *
     * @param $blockType
     *
     * @return \eZPageBlock
     *
     */
    public function addBlock($blockType)
    {
        $block = $this->pageModel->zone->addBlock(new \eZPageBlock());

        $block->setAttribute('id', md5(mt_rand() . microtime() . $this->pageModel->zone->getBlockCount()));
        $block->setAttribute('zone_id', $this->pageModel->zone->attribute('id'));
        $block->setAttribute('type', $blockType);
        $block->setAttribute('view', BlockMethods::getDefaultView($block));
        $block->setAttribute( 'action', 'add' );

        return $block;
    }

    /**
     *
     * Saves a block-property.
     *
     * @param $attributeKey
     * @param $attributeValue
     *
     */
    public function saveBlockProperty($attributeKey, $attributeValue)
    {
        if ($this->pageModel && $this->pageModel->block) {
            $this->pageModel->block->setAttribute($attributeKey, $attributeValue);
        }
    }
}
