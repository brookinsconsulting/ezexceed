<?php
/**
 * UserDevice
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\device;

class UserDevice extends \eZPersistentObject
{
    protected static $definition = array(
        'fields' => array(
            'user_id' => array('name' => 'user_id', 'datatype' => 'integer', 'required' => true),
            'device_id' => array('name' => 'device_id', 'datatype' => 'string', 'required' => true),
            'domain' => array('name' => 'domain', 'datatype' => 'string', 'required' => true),
            'type' => array('name' => 'type', 'datatype' => 'string', 'required' => true),
            'name' => array('name' => 'name', 'datatype' => 'string', 'required' => true),
            'os' => array('name' => 'os', 'datatype' => 'string', 'required' => true),
            'os_version' => array('name' => 'os_version', 'datatype' => 'string', 'required' => true),
            'uniquekey' => array('name' => 'uniquekey', 'datatype' => 'string', 'required' => true),
            'siteaccess' => array('name' => 'siteaccess', 'datatype' => 'string', 'required' => true)
        ),
        'keys' => array('user_id', 'device_id', 'domain'),
        'class_name' => '\\ezexceed\\models\\device\UserDevice',
        'name' => 'ezexceed_userdevice'
    );

    public static function fetchByUserId($userId)
    {
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array( 'user_id' => (int) $userId));
    }

    public static function fetchByUserIdDomain($userId, $domain)
    {
        return \eZPersistentObject::fetchObjectList(self::definition(), null, array( 'user_id' => (int) $userId, 'domain' => $domain));
    }

    /**
     * Find a single userDevice based on its uniqueId
     *
     * @param string $uniqueKey
     * @param int $userId
     * @return UserDevice
     */
    public static function fetchByUniqueKey($uniqueKey, $userId = false, $domain = false)
    {
        $criteria = array('uniquekey' => $uniqueKey);
        if (is_numeric($userId) && $userId > 0) {
            $criteria['user_id'] = $userId;
        }
        if ($domain !== false)
            $criteria['domain'] = $domain;
        
        return \eZPersistentObject::fetchObject(self::definition(), null, $criteria);
    }

    /**
     *
     * Retrieves a single userDevice.
     *
     * @param $userId
     * @param $deviceId
     *
     * @return UserDevice
     *
     */
    public static function fetchOne($userId, $deviceId, $domain)
    {
        return \eZPersistentObject::fetchObject(self::definition(), null, array( 'user_id' => (int)$userId, 'device_id' => $deviceId, 'domain' => $domain));
    }

    public static function definition()
    {
        return self::$definition;
    }

    public static function create(array $data = array())
    {
        $device = new static($data);
        return $device;
    }
}
