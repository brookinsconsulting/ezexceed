<?php
/**
 * PushQueue
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\device;

class Preview
{
    public static function previewUrlOnDevice($deviceId, $uniqueKey, $os, $url)
    {
        $alert = 'Page preview';
        $badgeCount = 1;
        $sound = '';

        $customFields = array(
            'p' => $url
        );

        return PushQueue::enqueue($deviceId, $uniqueKey, $os, $alert, $badgeCount, $sound, $customFields);
    }

    public static function previewUrl($userId, $url)
    {
        $text = 'Page preview';

        $customFields = array(
            'p' => $url
        );

        PushQueue::enqueueForUserDevices($userId, $text, $customFields);
    }

    public static function followers($save = false)
    {
        $sessionKey = 'followingDevices';
        if ($save) {
            \eZSession::set($sessionKey, json_encode($save));
        }
        return json_decode(\eZSession::get($sessionKey), true);
    }
}
