<?php
/**
 * PushQueue
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\device;

class PushQueue
{
    const PUSHSERVER_DOMAIN = 'pushserver.keyteq.no';
    
    public static function enqueue($deviceId, $uniqueKey, $os, $alert, $badgeCount, $sound, $customFields = array())
    {
        $eZINI = \eZINI::instance('ezexceed.ini');
        $id = $eZINI->variable('eZExceedPushNotification', 'ID');
        $key = $eZINI->variable('eZExceedPushNotification', 'SignKey');

        if (empty($id)||empty($key)) {
            throw new \Exception("Missing INI settings for [eZExceedPushNotification]ID + SignKey");
        }

        $data = array();
        if (is_array($deviceId))
            $data['deviceIds'] = $deviceId;
        else
            $data['deviceIds'] = array($deviceId);

        $customFields['u'] = $uniqueKey;

        $data['os'] = $os;
        $data['sound'] = $sound;
        $data['alert'] = $alert;
        $data['badge'] = $badgeCount;
        $data['fields'] = $customFields;

        $json = json_encode($data);
        
        self::postToPushServer(array('json' => $json, 'sign' => self::sign($json, $key), 'id' => $id));
        return true;
    }

    protected static function sign($payload, $key)
    {
        return sha1($payload.$key);
    }

    protected static function postToPushServer($params)
    {
        foreach ($params as $key => &$val) {
          if (is_array($val)) $val = implode(',', $val);
            $post_params[] = $key.'='.urlencode($val);
        }

        $post_string = implode('&', $post_params);
    
        $fp = fsockopen('ssl://'.self::PUSHSERVER_DOMAIN,
            443,
            $errno, $errstr, 30);

        $out = "POST /push.php HTTP/1.1\r\n";
        $out.= "Host: ".self::PUSHSERVER_DOMAIN."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($post_string)."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out.= $post_string;
    
        fwrite($fp, $out);
        fclose($fp);
    }

    public static function enqueueForUserDevices($userId, $text, $customFields = array())
    {
        $devices = \ezexceed\models\device\UserDevice::fetchByUserId($userId);

        $sendToDevices = array();
        $host = $_SERVER['HTTP_HOST'];

        foreach ($devices as $device)
        {
            if (isset($sendToDevices[$device->device_id]))
            {
                // ex. if app has http://www.keyteq.no/ setup as a site, and current host is "www.keyteq.no", prefer to send to that site
                if (strpos($device->domain, $host)!==false)
                {
                    $sendToDevices[$device->device_id] = $device;
                }
            }
            else
                $sendToDevices[$device->device_id] = $device;
        }

        foreach ($sendToDevices as $device)
        {
            \ezexceed\models\device\PushQueue::enqueue($device->device_id, $device->uniquekey, $device->os, $text, 1, null, $customFields);
        }
    }
}
