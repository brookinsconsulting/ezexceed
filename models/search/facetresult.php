<?php
/**
 * A model representing a searchFacetResult
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\search;

use \ezexceed\models\nodestructure\NodeTree;

/**
 *
 * Description
 *
 * A model representing a searchFacetResult
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 24.10.2011
 *
 */
class FacetResult
{
    /** @var int ContentClassId */
    public $id;
    /** @var string Name of the content class. */
    public $name;
    /** @var int Number of hits within this facet. */
    public $count;
    /** @var string The icon-url for the content-class. */
    public $icon;

    /**
     *
     * Creates a new instance.
     *
     * @param int $id
     * @param string $name
     * @param int $count
     * @param $icon
     *
     */
    public function __construct($id, $name, $count, $icon)
    {
        $this->id = $id;
        $this->name = $name;
        $this->count = $count;
        $this->icon = $icon;
    }
}
