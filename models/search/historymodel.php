<?php
/**
 * Represents the stored searchistory element
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\search;

/**
 *
 * Description
 *
 * Represents the stored searchistory element.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 24.10.2011
 *
 */
class HistoryModel
{
    /** @var string A previous query. */
    public $q;

    /**
     *
     * Creates a new instance.
     *
     * @param string $q
     */
    public function __construct($q)
    {
        $this->q = $q;
    }
}
