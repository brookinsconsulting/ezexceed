<?php
/**
 * Retrieves the search-history for the current user
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\search;

use \eZPreferences;
use \ezexceed\models\search\HistoryModel;

/**
 *
 * Description
 *
 * Retrieves the search-history for the current user.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 24.10.2011
 *
 */

class HistoryFetcher
{
    const SearchHistory = 'searchHistory';
    const SearchHistoryLimit = 5;

    /**
     *
     * Clears search-history.
     *
     * @static
     *
     * @return array|bool|HistoryModel|null
     */
    public static function clear()
    {
        eZPreferences::setValue(self::SearchHistory, null);

        return self::retrieve(true);
    }

    /**
     *
     * Retrieves the current search-history.
     *
     * @static
     *
     * @return array|bool|HistoryModel|null
     */
    public static function retrieve()
    {
        $searchHistory = eZPreferences::value(self::SearchHistory);

        if ($searchHistory) $searchHistory = unserialize($searchHistory);

        if (!is_array($searchHistory))
        {
            $searchHistory = array();
            $searchHistory['results'] = array();
        }

        return $searchHistory;
    }

    /**
     *
     * Adds a search to history and prunes the previous stored values if necessary.
     *
     * @static
     *
     * @param string $q
     *
     * @return array|bool|HistoryModel|null
     */
    public static function update($q)
    {
        $q = trim($q);

        $searchHistory = self::retrieve();

        if (self::queryExists($q, $searchHistory)) return $searchHistory;

        if (count($searchHistory) >= self::SearchHistoryLimit) array_pop($searchHistory['results']);

        $searchHistoryElement = new HistoryModel($q);

        array_unshift($searchHistory['results'], $searchHistoryElement);

        eZPreferences::setValue(self::SearchHistory, serialize($searchHistory));

        return $searchHistory;
    }

    /**
     *
     * Checks whether the search is already stored in history or not.
     *
     * @param $q
     * @param $searchHistory
     *
     * @return bool
     */
    protected static function queryExists($q, $searchHistory)
    {
        $results = $searchHistory['results'] ?: array();

        /** @var $result HistoryModel */
        foreach ($results as $result)
        {
              if (strcmp($q, $result->q) == 0) return true;
        }

        return false;
    }
}
