<?php
/**
 * Model representing a collection of searchresults with metadata
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\search;

use \ezexceed\models\nodestructure\NodeTree;

/**
 *
 * Description
 *
 * Model representing a collection of searchresults with metadata.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 24.10.2011
 *
 */
class Result
{
    /** @var string The query. */
    public $q;
    /** @var int Limits the number of search results. */
    public $limit;
    /** @var int The number of hits. */
    public $total;
    /** @var  An array containing the resultnodes. */
    public $results;
    /** @var SearchFacetResult[] Contains the facets for the current search. */
    public $facetResults;
    /** @var int Only hits matching this contentclassid is returned. */
    public $classFilter;
    /** @var int[] */
    public $facetIds;

    /**
     *
     * Creates a new instance.
     *
     * @param string $q
     * @param int $limit
     * @param int $classFilter
     */
    public function __construct($q, $limit, $classFilter = null)
    {
        $this->q = $q;
        $this->limit = $limit;
        $this->total = 0;
        // Casts filter to int and defaults to null if value is 0
        $this->classFilter = ((int) $classFilter) ?: null;
        $this->facetIds = array();
    }

    /**
     * Parses the raw result returned from the searchEngine.
     *
     * @param array $result
     * @param bool $ezFind
     * @param bool $whiteIcons
     */
    public function fromResult($result, $ezFind, $whiteIcons = false)
    {
        $treeBuilder = new NodeTree();

        $searchResults = $result['SearchResult'] ?: array();

        if ($searchResults) foreach ($searchResults as $node)
        {
            $this->results[] = $treeBuilder->nodeToArray($node);//, $whiteIcons);
        }

        if (isset($result['SearchCount'])) $this->total = $result['SearchCount'];

        if ($ezFind) $this->parseFacets($result, $whiteIcons);
    }

    /**
     *
     * If eZFind is enabled, this method will return the requested facets.
     *
     * @param array $result
     * @param $whiteIcons
     *
     */
    protected function parseFacets($result, $whiteIcons)
    {
        /** @var $searchExtras \ezfSearchResultInfo */
        $searchExtras = $result['SearchExtras'];

        $facetFields = $searchExtras->attribute('facet_fields') ?: array();

        foreach ($facetFields as $facetField)
        {
            $nameList = $facetField['nameList'];
            $countList = $facetField['countList'];

            foreach($nameList as $classId => $name)
            {
                $icon = NodeTree::getIconUrl(\eZContentClass::classIdentifierByID($classId), NodeTree::getIconTypeIdentifier(false, $whiteIcons));
                $facetResult = new FacetResult($classId, $name, $countList[$classId], $icon);

                $this->facetIds[] = $classId;
                $this->facetResults[$facetField['field']][] = $facetResult;
            }
        }
    }
}
