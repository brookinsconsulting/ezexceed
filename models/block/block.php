<?php
/**
 * Model for managing blocks
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models;

use \ezexceed\models\content\Object;
use ezexceed\classes\utilities\IniUtilities;
use ezexceed\models\nodestructure\NodeTree;
use \ezexceed\models\page\Page;
use \ezexceed\classes\utilities\BlockMethods;

/**
 *
 * Description
 *
 * Model for managing blocks.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 06.01.2012
 *
 */
class BlockModel
{
    /** @var Page */
    public $pageModel;
    /** @var string */
    public $blockName;
    /** @var bool */
    public $itemToBeRemoved;
    /** @var string */
    public $blockType;
    /** @var \eZPageBlock */
    public $block;
    /** @var array */
    public $allowedClasses;

    /**
     *
     * Initializes the object
     *
     * @param $objectId
     * @param $language
     *
     */
    public function __construct($objectId, $language)
    {
        $this->pageModel = new Page($objectId, $language);
        $this->itemToBeRemoved = false;
    }

    /**
     *
     * Updates the rotation-property.
     *
     * @param $attributeKey
     * @param $attributeValue
     *
     */
    public function updateRotation($attributeKey, $attributeValue)
    {
        $rotation = $this->block->attribute('rotation') ?: array();

        $defaults = array('interval' => 0, 'type' => 0, 'value' => 0, 'unit' => 2);

        $rotation += $defaults;

        switch ($attributeKey)
        {
            case 'value' :
                if ($attributeValue === '')
                    $rotation = $defaults;
                else
                {
                    $rotation['value'] = $attributeValue;

                    if (!isset($rotation['unit']) || !is_numeric($rotation['unit']))
                        $rotation['unit'] = 2;

                    $rotationUnit = $rotation['unit'];
                    $rotation['interval'] = pow(60, $rotationUnit - 1) * $attributeValue;
                }
            break;

            case 'unit' :
                $rotation['unit'] = $attributeValue;

                if (!isset($rotation['value']) || !is_numeric($rotation['value']))
                    $rotation['value'] = 0;

                $rotation['interval'] = pow(60, $attributeValue - 1) * $rotation['value'];
            break;

            case 'shuffle' :
                $rotation['type'] = $attributeValue;
            break;
        }

        $this->block->setAttribute('rotation', $rotation);
    }

    /**
     *
     * Populates the object
     *
     * @param $zoneIndex
     * @param $blockId
     *
     */
    public function load($zoneIndex, $blockId)
    {
        $this->pageModel->load();
        $this->pageModel->loadLayout($zoneIndex, $blockId);
        $this->block = $this->pageModel->block;

        $this->blockName = BlockMethods::retrieveBlockDescription($this->block);
        $this->blockType = $this->block->attribute('type');

        $ini = \eZINI::instance('block.ini');
        $this->allowedClasses = IniUtilities::checkGetSetting($ini, $this->blockType, 'AllowedClasses', array());
    }

    /**
     *
     * Stores the contentObjectAttribute and the version-object.
     *
     */
    public function store()
    {
        \eZContentCacheManager::clearContentCache($this->pageModel->objectId);
        $this->pageModel->store();
    }

    /**
     *
     * Adds an item of it does not exist and is not marked for removal.
     *
     * @param string $nodeId
     * @param string $objectId
     *
     */
    public function checkAddItem($nodeId, $objectId)
    {
        $item = $this->fetchExistingItem('archieved', $objectId);
        if ($this->block->getItemCount() > 0)
            $item = $this->fetchExistingItem('items', $objectId, true);

        $exists = $this->fetchExistingItem('valid', $objectId);

        if ($item)
        {
            if (!$this->itemToBeRemoved)
                $this->block->addItem($item);

            $item->setXMLStorable(true);
            $item->setAttribute('node_id', $nodeId);
            $item->setAttribute('priority', $this->block->getItemCount());
            $item->setAttribute('ts_publication', time() - 10);
            $item->setAttribute('ts_visible', '0');
            $item->setAttribute('ts_hidden', '0');
            $item->setAttribute('action', 'modify');
        }
        elseif(!$exists)
        {
            $item = $this->block->addItem(new \eZPageBlockItem());
            $item->setAttribute('object_id', $objectId);
            $item->setAttribute('node_id', $nodeId);
            $item->setAttribute('priority', $this->block->getItemCount());
            $item->setAttribute('ts_publication', time() - 10);
            $item->setAttribute('action', 'add');
        }
    }

    /**
     *
     * Removes an item.
     *
     * @param $objectId
     *
     */
    public function removeItem($objectId)
    {
        if ($this->block->getItemCount() > 0)
        {
            /** @var $item \eZPageBlockItem */
            foreach ((array)$this->block->attribute('items') as $itemId => $item)
            {
                if ($item->attribute('object_id') == $objectId)
                {
                    if ($item->toBeAdded())
                        $this->block->removeItem($itemId);
                    elseif ($item->toBeModified())
                        $this->block->removeItem($itemId);
                    else
                    {
                        $item = $this->block->addItem(new \eZPageBlockItem());
                        $item->setAttribute('object_id', $objectId);
                        $item->setAttribute('action', 'remove');
                    }
                }
            }
        }

        $item = $this->block->addItem(new \eZPageBlockItem());
        $item->setAttribute('object_id', $objectId);
        $item->setAttribute('action', 'remove');
    }

    /**
     *
     * Updates the fetch-parameters.
     *
     * @param $attributeKey
     * @param $attributeValue
     *
     */
    public function updateFetchParameters($attributeKey, $attributeValue)
    {
        $fetchAttribute = unserialize($this->block->attribute('fetch_params'));
        $fetchAttribute[$attributeKey] = $attributeValue;
        $this->block->setAttribute('fetch_params', serialize($fetchAttribute));

        if ($persistedBlock = \eZFlowBlock::fetch($this->block->attribute('id')))
        {
            $persistedBlock->setAttribute('last_update', 0);
            $persistedBlock->store();
        }
    }

    /**
     *
     * Updates the fetch-parameters.
     *
     * @param $attributeKey
     * @param $attributeValue
     *
     */
    public function updateCustomAttribute($attributeKey, $attributeValue)
    {
        $customAttributes = $this->block->attribute('custom_attributes');
        $customAttributes[$attributeKey] = $attributeValue;
        $this->block->setAttribute('custom_attributes', $customAttributes);
    }

    /**
     *
     * Retrieves an existing item.
     *
     * @param string $attributeId
     * @param string $objectId
     * @param bool $checkStatus
     *
     * @return \eZPageBlockItem|bool
     */
    protected function fetchExistingItem($attributeId, $objectId, $checkStatus = false)
    {
        $items = (array) $this->block->attribute($attributeId) ?: array();

        /** @var $item \eZPageBlockItem    */
        foreach($items as $item)
        {
            if( $item->attribute('object_id') == $objectId)
            {
                if ($checkStatus && $item->toBeRemoved())
                    $this->itemToBeRemoved = true;

                return $item;
            }
        }

        return false;
    }

    /**
     *
     * Returns a new array indexed by the
     *
     * @param $elements
     * @param $key
     *
     * @return array
     *
     */
    protected function indexByKey($elements, $key)
    {
        $indexed = array();

        /** @var $value \eZPageBlockItem|array */
        foreach ($elements as $value)
        {
            if (is_array($value))
                $indexed[$value[$key]] = $value;
            else
                $indexed[$value->attribute($key)] = $value;
        }

        return $indexed;
    }

    /**
     *
     * Sorts the items.
     *
     * @param $itemType
     * @param $sortResults
     *
     */
    public function sortItems($itemType, $sortResults)
    {
        $indexedResults = $this->indexByKey($sortResults, 'id');

        /** @var $items \eZPageBlockItem[] */
        $items = $this->block->attribute($itemType);
        $count = count($items);

        $indexedItems = $this->indexByKey($items, 'object_id');

        /** @var $item \eZPageBlockItem */
        foreach ($indexedItems as $objectId => $item)
        {
            $newPriority = $count - $indexedResults[$objectId]['index'] - 1;
            if ($item->toBeModified())
            {
                $item->setAttribute('action', 'modify');
                $item->setAttribute('priority', $newPriority);
            }
            else
            {
                $tmpItem = $this->block->addItem(new \eZPageBlockItem());
                $tmpItem->setAttribute('priority', $newPriority);
                $tmpItem->setAttribute('ts_visible', $item->attribute('ts_visible'));
                $tmpItem->setAttribute('ts_hidden', $item->attribute('ts_hidden'));
                $tmpItem->setAttribute('object_id', $item->attribute('object_id'));
                $tmpItem->setAttribute('action', 'modify');
            }
        }
    }

    /**
     * Updates the publication timestamp.
     *
     * @param $itemType
     * @param $index
     * @param $date
     * @param $time
     * @return int Number of modified nodes
     */
    public function updatePublicationTimestamp($itemType, $index, $date, $time)
    {
        $timestamp = strtotime($date . ' ' . $time . ':00');
        $modified = 0;

        if ($timestamp !== false)
        {
            $items = $this->block->attribute($itemType);
            /** @var $item \eZPageBlockItem */
            $item = $items[$index];

            if ($item) {
                if ($item->toBeModified() || $item->toBeAdded())
                    $item->setAttribute('ts_publication', $timestamp);
                else {
                    $tmpItem = $this->block->addItem(new \eZPageBlockItem());
                    $tmpItem->setAttribute('object_id', $item->attribute('object_id'));
                    $tmpItem->setAttribute('action', 'modify');
                    $tmpItem->setAttribute('ts_publication', $timestamp);
                }
                $modified++;
            }
        }

        return $modified;
    }

    /**
     *
     * Retrieves all available nodes for the current block.
     *
     * @return NodeTree[]
     *
     */
    public function getAllNodes()
    {
        $valid = $this->block->attribute('valid');
        $waiting = $this->block->attribute('waiting');
        $archived = $this->block->attribute('archived');

        /** @var $allBlockItems \eZPageBlockItem[] */
        $allBlockItems = array_merge($valid, $waiting, $archived);

        $nodes = array();

        foreach($allBlockItems as $blockItem) {
            $node = new NodeTree();
            $node->init($blockItem->attribute('node_id'));
            $response = $node->buildTreeArray(false);
            $nodes[] = $response['node'];
        }

        return $nodes;
    }
}