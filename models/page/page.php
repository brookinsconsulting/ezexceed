<?php

/**
 * Model for managing the eZPage-attribute.
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\page;

use \ezexceed\models\content\Object;
use ezexceed\models\objectrenderer\ObjectRenderer;

/**
 *
 * Description
 *
 * Model for managing the eZPage-attribute.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 15.05.2012
 *
 */

class Page
{
    /** @var int */
    public $objectId;
    /** @var \eZContentObject */
    public $contentObject;
    /** @var \eZContentObjectVersion */
    public $version;
    /** @var \eZContentObjectAttribute */
    public $contentObjectAttribute;
    /** @var \eZPage */
    public $attributeContent;
    /** @var string */
    public $language;

    /** @var string */
    public $zoneIndex;
    /** @var \eZPageZone */
    public $zone;
    /** @var \eZPageZone[] */
    public $zones;
    /** @var string */
    public $blockIndex;
    /** @var string */
    public $blockId;
    /** @var \eZPageBlock */
    public $block;

    /**
     *
     * Initializes model.
     *
     * @param $objectId
     * @param $language
     *
     */
    public function __construct($objectId, $language)
    {
        $this->objectId = $objectId;
        $this->language = $language;
    }

    public function load()
    {
        $this->contentObject = Object::safeFetch($this->objectId);

        $objectRenderer = new ObjectRenderer($this->contentObject, $this->language);
        /** @var $version \eZContentObjectVersion */
        $this->version = $objectRenderer->checkCreateVersion($this->language);

        $pageAttribute = null;
        $this->contentObjectAttribute = self::fetchEzpageAttribute($this->version);
        $this->attributeContent = $this->contentObjectAttribute->content();

        if (!$this->contentObjectAttribute) {
            throw new \Exception("Requires an attribute of type 'ezpage' which object does not have");
        }
    }

    /**
     * Retrieves the eZPage-attribute for an entity.
     *
     * @param \eZContentObjectVersion $object
     *
     * @return \eZContentObjectAttribute|null
     */
    public static function fetchEzpageAttribute($object) {

        /** @var $attribute \eZContentObjectAttribute */
        foreach ($object->dataMap() as $attribute)
        {
            if ($attribute->attribute('data_type_string') === 'ezpage')
                return $attribute;
        }

        return null;
    }

    /**
     *
     * Loads zones, currentZone and currentBlock if any.
     *
     * @param $zoneIndex
     * @param $blockId
     *
     */
    public function loadLayout($zoneIndex, $blockId)
    {
        $this->zoneIndex = $zoneIndex;
        $this->blockId = $blockId;
        $this->zones = $this->attributeContent->attribute('zones');

        /** Passed by reference to allow propagation of changes. */
        if ($this->zone = $this->attributeContent->getZone($zoneIndex))
            $this->getBlock();
    }

    /**
     *
     * Gets the current block.
     *
     */
    public function getBlock()
    {
        $blockCount = $this->zone->getBlockCount();

        for($i=0; $i<$blockCount; $i++)
        {
            $block = $this->zone->getBlock($i);

            if ($block->attribute('id') === $this->blockId)
            {
                $this->blockIndex = $i;
                $this->block = $block;

                break;
            }
        }
    }

    /**
     *
     * Returns the value of the given attribute.
     *
     * @param $attribute
     *
     * @return mixed
     *
     */
    public function attribute($attribute)
    {
        return $this->$attribute;
    }

    /**
     *
     * Checks if the given property exists.
     *
     * @param $attributeName
     *
     * @return bool
     *
     */
    public function hasAttribute($attributeName)
    {
        if (property_exists(__CLASS__, $attributeName)) return true;
        else return false;
    }

    public function store()
    {
        $this->contentObjectAttribute->store();
        Object::updateObjectVersion($this->version, $this->contentObject, $this->language);
    }
}
