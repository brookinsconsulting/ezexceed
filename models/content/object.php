<?php
/**
 * Facade for working with the eZ content objects
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\content;

use \ezexceed\models\nodestructure\NodeOperations;
use \ezexceed\models\nodestructure\NodeTree;
use \ezexceed\models\content\object\Formatter;
use \ezexceed\classes\utilities\Objects;

use \eZDB;
use \eZUser;
use \eZContentClass;
use \eZContentObject;
use \eZPageBlockItem;
use \eZFunctionHandler;
use \eZPersistentObject;
use \eZContentObjectVersion;

use \Imagine\Image\Box;
use \Imagine\Image\Point;

/**
 * Facade for working with the eZ content objects
 *
 * @author Raymond Julin <raymond@keyteq.no>
 * @since 28.10.2011
 */
class Object
{
    const PUBLISHEDSTATUS_NEW = 'new';
    const PUBLISHEDSTATUS_CHANGED = 'changed';
    const PUBLISHEDSTATUS_PUBLISHED = 'published';
    const PUBLISHEDSTATUS_PUBLISHING = 'publishing';

    const DATE_FORMAT = '%d. %b, %Y';

    static $_caches = array(
        'names' => array(),
        'userDrafts' => array(),
        'images' => array(),
        'contentLanguages' => false
    );

    /**
     * Get all related objects
     * Also fetches related content in blocks if ezflow
     *
     * @static
     * @param \eZContentObject $object
     * @return array
     */
    public static function getRelatedObjects($object)
    {
        $user = \eZUser::currentUser();
        $userId = $user->id();
        $id = $object->attribute('id');

        $draft = Object::fetchLatestUserDraft($id, $userId, null, false, $object->attribute('modified'));

        if ($draft) {
            $objects = static::getAttributeRelatedObjects($draft);
        }
        else {
            $objects = static::getAttributeRelatedObjects($object);
        }

        /**
         * If ezflow, get content in blocks that affects the page
         */
        $dataMap = $object->attribute('data_map');

        $page = false;
        /** @var $attribute \eZContentObjectAttribute */
        foreach ($dataMap as $attribute)
        {
            if ($attribute->attribute('data_type_string') === 'ezpage')
            {
                /** @var $page \eZPage */
                $page = $attribute->content();
                break;
            }
        }

        if ($page)
        {
            $blockINI = \eZINI::instance('block.ini');
            /** @var $zone \eZPageZone */
            foreach ((array)$page->attribute('zones') as $zone)
            {
                if ($blocks = $zone->attribute('blocks'))
                {
                    /** @var $block \eZPageBlock */
                    foreach ($blocks as $block)
                    {
                        if ($blockINI->hasVariable($block->attribute('type'), 'ManualAddingOfItems')
                                && $blockINI->variable($block->attribute('type'), 'ManualAddingOfItems') === 'enabled'
                        )
                        {
                            /** Fetch valid and waiting items. */
                            $items = array_merge($block->attribute('valid'), $block->attribute('waiting'));

                            /** @var $item \eZPageBlockItem */
                            foreach ($items as $item)
                            {
                                $objects[] = \eZContentObject::fetch($item->attribute('object_id'));
                            }
                        }
                    }
                }
            }
        }

        return $objects;
    }

    /**
     * Creates a new internal draft.
     *
     * @param \eZContentObject $contentObject
     * @param int|bool|eZContentObjectVersion $copyFromVersion
     * @param bool $language
     *
     * @return \eZContentObjectVersion
     */
    public static function createInternalDraft($contentObject, $copyFromVersion = false, $language = false)
    {
        if (!$contentObject->attribute('versions')) {
            /** @var $newVersion \eZContentObjectVersion */
            $newVersion = $contentObject->createInitialVersion(\eZUser::currentUser()->id());
            $newVersion->store();
        }

        $language = $language ?: $contentObject->attribute('initial_language_code');

        if (!$copyFromVersion) {
            $copyFromLanguageCode = $contentObject->attribute('initial_language_code');
            $copyFromVersion = $contentObject->attribute('current_version');
        }
        else if (is_numeric($copyFromVersion)) {
            $copyFromVersion = $contentObject->version($copyFromVersion);
            $copyFromLanguageCode = $language;
        }
        else {
            $copyFromLanguageCode = $language;
        }

        if (!is_object($copyFromVersion)) {
            $copyFromVersion = $contentObject->version($copyFromVersion);
        }

        $db = eZDB::instance();
        $db->begin();
        $newVersion = $contentObject->createNewVersionIn($language, $copyFromLanguageCode, $copyFromVersion->attribute('version'), false, \eZContentObjectVersion::STATUS_INTERNAL_DRAFT);
        $db->commit();

        // To make this siteaccess independent we must manually copy the attributes
        $currentVersion = $copyFromVersion->attribute('version');

        /** @var $version \eZContentObjectVersion */
        $version = $contentObject->version($currentVersion);
        if ($version) {
            /** @var $contentObjectTranslations \eZContentObjectTranslation[] */
            $contentObjectTranslations = $version->translations();
            foreach ($contentObjectTranslations as $contentObjectTranslation) {
                if ($language != false && $contentObjectTranslation->attribute('language_code') != $language) {
                    continue;
                }

                $contentObjectAttributes = $contentObjectTranslation->objectAttributes();
                /**
                 * Remove the old attributes first
                 */
                $oldAttributes = \eZContentObjectVersion::fetchAttributes($newVersion->attribute('version'), $contentObject->ID, $language);

                /** @var $attribute \eZContentObjectAttribute */
                $db->begin();
                foreach ($oldAttributes as $attribute) {
                    $attribute->removeThis($attribute->attribute('id'), $newVersion->attribute('version'));
                }
                $db->commit();

                foreach ($contentObjectAttributes as $attribute) {
                    $clonedAttribute = $attribute->cloneContentObjectAttribute(
                        $newVersion->attribute('version'),
                        $version->attribute('version'),
                        $contentObject->ID,
                        $language
                    );
                    $clonedAttribute->sync();
                }
            }
        }

        $classID = $contentObject->attribute('contentclass_id');
        $class = eZContentClass::fetch($classID);

        $db->begin();
        $name = $class->contentObjectName($contentObject, $newVersion->attribute('version'), $language);
        $contentObject->setName($name, $newVersion->attribute('version'), $language);
        $db->commit();

        return $newVersion;
    }

    /**
     *
     * Saves a user-object.
     *
     * @static
     *
     * @param $objectId
     * @param $attributes
     * @param bool $publish
     *
     * @return bool
     *
     */
    public static function saveUser($objectId, $attributes, $publish = false)
    {
        $contentObject = \eZContentObject::fetch($objectId);

        $dataMap = $contentObject->dataMap();

        foreach ($attributes as $field => $value)
        {
            /** @var $attribute \eZContentObjectAttribute */
            $attribute = $dataMap[$field];
            $attribute->setAttribute('data_text', $value);
            $attribute->sync();
        }

        $initialLanguageID = $contentObject->attribute('initial_language_id');
        /** @var $language \eZContentLanguage */
        $language = \eZContentLanguage::fetch($initialLanguageID);
        $languageCode = $language->attribute('locale');

        $db = \eZDB::instance();
        $db->begin();

        /** @var $newVersion \eZContentObjectVersion */
        $newVersion = $contentObject->createNewVersion(false, true, $languageCode);
        $newVersion->setAttribute('modified', \eZDateTime::currentTimeStamp());

        if ($publish === false)
            $newVersion->setAttribute('status', \eZContentObjectVersion::STATUS_DRAFT);

        $newVersion->store();

        $db->commit();

        if ($publish === true)
        {
            $res = \eZOperationHandler::execute
            (
                'content',
                'publish',
                array
                (
                    'object_id' => $objectId,
                    'version' => $newVersion->attribute('version')
                )
            );

            return $res['status'] === \eZModuleOperationInfo::STATUS_CONTINUE;
        }
        else return true;
    }

    /**
     * Saves an object.
     *
     * @param $id
     *
     * @param array $attributes
     * @param array $saveOptions
     *
     * @return mixed
     */
    public static function save($id, array $attributes, array $saveOptions = array())
    {
        foreach ($attributes as $attribute) {
            if (is_array($attribute)) {
                foreach ($attribute as $attributeValues) {
                    if (isset($attributeValues['name']) && isset($attributeValues['value']))
                        $_POST[$attributeValues['name']] = $attributeValues['value'];

                    else if (is_array($attributeValues)) {
                        foreach ($attributeValues as $name => $attributeValue)
                            $_POST[$name][] = $attributeValue;
                    }
                }
            }
        }

        return Object::saveContentfromHTTP($id, (array)$attributes, $saveOptions);
    }

    /**
     *
     * Saves a content-object from HTTP
     *
     * @static
     *
     * @param $objectId
     * @param $attributes
     * @param array $options
     *
     * @return array
     *
     */
    public static function saveContentfromHTTP($objectId, $attributes, $options = array())
    {
        $response = array(
            'ok' => true,
            'errors' => array(),
            'attributes' => array()
        );
        $templatesToRender = array();
        $options += array(
            'version' => false,
            'languageId' => false,
            'returnTemplates' => false
        );
        $updateVersion = false;
        $fullLocationResponse = true;

        $contentObject = \eZContentObject::fetch($objectId);
        if (!$contentObject || !$contentObject->canEdit()) {
            $response['ok'] = false;
            $response['errors'] = array('Access denied');
            return $response;
        }

        /** @var $versionObject \eZContentObjectVersion */
        $versionObject = $contentObject->version($options['version']);
        if (!$versionObject) {
            $response['ok'] = false;
            $response['errors'] = array('Version not found');
            return $response;
        }

        // If we edit an object that is outdated / published we need to create a new version behind the scenes
        if ($versionObject->attribute('version') <= $contentObject->attribute('current_version')
                && $contentObject->attribute('status') == \eZContentObject::STATUS_PUBLISHED) {
            $versionObject = Object::createInternalDraft($contentObject, $versionObject, $versionObject->attribute('language'));
        }

        $options['version'] = $versionObject->attribute('version');
        $dataMap = $versionObject->dataMap();
        $attributesVersion = array();

        foreach ($dataMap as $attribute)
            $attributesVersion[$attribute->attribute('id')] = $attribute;


        $http = \eZHTTPTool::instance();
        $base = 'ContentObjectAttribute';

        /**
         * $attributes[id][0]['value']
         */
        foreach($attributes as $id => $value) {
            if (!is_array($value))
                $id = $value;

            if ($id === 'locations') {
                $response['location'] = static::updateLocations($contentObject, $value);
                $updateVersion = true;
                $fullLocationResponse = false;
                continue;
            }

            if ($id === 'states') {
                static::updateStates($contentObject, $value);
                continue;
            }

            if ($id === 'triggerVersionUpdate') {
                $updateVersion = true;
                continue;
            }

            if (!isset($attributesVersion[$id]))
                continue;

            /** @var $attribute \eZContentObjectAttribute */
            $attribute = $attributesVersion[$id];

            /**
             * If datatype is ezobjectrelationlist, we must reset content to remove relations that
             * is not posted
             */
            if ($attribute->attribute('data_type_string') == 'ezobjectrelationlist') {
                $dataType = $attribute->dataType();
                if ($dataType)
                    /** @var $dataType \eZObjectRelationListType */
                    $attribute->setContent($dataType->defaultObjectAttributeContent());
            }

            $invalidInput = $attribute->validateInput($http, $base, $inputParams) === \eZInputValidator::STATE_INVALID;
            if ($invalidInput) {
                $response['ok'] = false;
                $response['errors'][$id] = array(
                    'attributeId' => $id,
                    'error' => $attribute->validationError(),
                    'name' => $attribute->attribute('contentclass_attribute_name')
                );
            }
            else {
                $attribute->fixupInput($http, $base);
                $attribute->fetchInput($http, $base);
                $attribute->store();
                $response['attributes'][$id] = $value;
                $updateVersion = true;
            }

            if ($options['returnTemplates']) {
                $templatesToRender[$id] = array(
                    'template' => 'design:content/datatype/edit/' . $attribute->editTemplate() . '.tpl'
                );
            }
        }

        // Check for custom actions
        if ($http->hasPostVariable("CustomActionButton")) {
            $customActionArray = $http->postVariable("CustomActionButton");
            foreach ($customActionArray as $customActionKey => $customActionValue) {
                $customActionString = $customActionKey;

                if (preg_match("#^([0-9]+)_(.*)$#", $customActionString, $matchArray)) {
                    $customActionAttributeID = $matchArray[1];
                    $customAction = $matchArray[2];

                    /** @var $attribute \eZContentObjectAttribute */
                    if (isset($attributesVersion[$customActionAttributeID]))
                    {
                        $attribute = $attributesVersion[$customActionAttributeID];
                        if ($attribute) {
                            $attribute->customHTTPAction($http, $customAction, array());
                            $attribute->handleCustomHTTPActions($http, $base, array(), array());
                            $updateVersion = true;
                        }
                    }
                }
            }
        }

        if (!$options['languageId'])
            $options['languageId'] = $versionObject->initialLanguageCode();

        if ($updateVersion)
            self::updateObjectVersion($versionObject, $contentObject, $options['languageId']);

        /**
         * Must render templates at the end.
         * Images are moved if published, hence the url path could be wrong.
         * Must also fetch a fresh copy from db
         */
        if (count($templatesToRender)) {
            $response['templates'] = array();

            $dataMap = $versionObject->dataMap();
            $attributesVersion = array();

            foreach ($dataMap as $attribute) {
                $attributesVersion[$attribute->attribute('id')] = $attribute;
            }

            foreach ($templatesToRender as $id => $temp) {
                $tpl = \eZTemplate::factory();
                $tpl->setVariable('attribute_base', 'ContentObjectAttribute');
                $tpl->setVariable('attribute', $attributesVersion[$id]);

                $response['templates'][$id] = trim($tpl->fetch($temp['template']));
            }
        }

        if ($options['publish']) {
            $response['publish'] = self::publish($contentObject, array($options['version']));
        }

        if ($response['ok']) {
            $excludeFilter = array();
            if (!$fullLocationResponse) {
                $excludeFilter[] = 'locations';
            }

            $response['version'] = (int)$versionObject->attribute('version');
            $response['object'] = self::getContentObject($objectId, $options['languageId'], $excludeFilter);
            $response['status'] = self::changedSincePublished($contentObject);

            foreach ($response['status']['versions'] as $language => $versionId)
            {
                $language = \eZContentLanguage::fetchByLocale($language);
                $draft = self::fetchLatestUserDraft($objectId, \eZUser::currentUserID(), $language->attribute('id'));
                $response['object']['versions'][] = array(
                    'id' => (int)$draft->attribute('version'),
                    'language' => $language->attribute('locale'),
                    'created' => (int)$draft->attribute('created'),
                    'modified' => (int)$draft->attribute('modified'),
                    'status' => (int)$draft->attribute('status'),
                    'version' => (int)$draft->attribute('version')
                );
            }

            unset($response['errors']);
        }

        return $response;
    }

    /**
     * Retrieves and formats object for ajax-response.
     *
     * @param $id
     * @param int $languageId
     * @param array $excludeFilter
     *
     * @return array
     */
    public static function getContentObject($id, $languageId, $excludeFilter = array())
    {
        self::clearDraftCache($id, \eZUser::currentUserID(), $languageId);
        \eZContentObject::clearCache($id);
        $object = \eZContentObject::fetch($id);

        return self::formatter(false, $excludeFilter)->format($object);
    }

    /**
     *
     * Updates a versionObject.
     *
     * @param \eZContentObjectVersion $versionObject
     * @param \eZContentObject $contentObject
     * @param $editLanguage
     *
     */
    public static function updateObjectVersion($versionObject, $contentObject, $editLanguage)
    {
        $versionObject->setAttribute('modified', time());
        $versionObject->setAttribute('status', \eZContentObjectVersion::STATUS_DRAFT);
        $versionObject->store();

        $classID = $contentObject->attribute('contentclass_id');
        $class = eZContentClass::fetch($classID);

        $db = eZDB::instance();
        $db->begin();
        $contentObject->setName($class->contentObjectName($contentObject, $versionObject->attribute('version'), $editLanguage), $versionObject->attribute('version'), $editLanguage);
        $db->commit();

        \eZContentCacheManager::clearContentCacheIfNeeded($contentObject->ID, $versionObject->attribute('version'));
    }

    /**
     *
     * Fetch a list of objects globally for site. Excludes objects with only internal drafts
     *
     * @param string|int|bool|array $criteria A content class string or its numeric type (preferred)
     *                                        or take complete control by sending the raw array
     * @param array|null $limits An array containing `offset` and `length` or null for all
     * @param array|null $sort An array with $field => $direction, supports multiple sorts. `null` for natural sort
     * @param null $formatter A callback for a formatter to use to reformat each content object
     * @param null $preprocessor
     *
     * @return array Array of objects, either formatted or straight from eZ fetch
     *
     */
    public static function find($criteria = false, $limits = null, $sort = null, $formatter=null, $preprocessor=null)
    {
        $sql = array();
        $where = array();

        $mine = $criteria['mine'];
        $trash = $criteria['trash'];
        $db = eZDB::instance();

        if ($mine) {
            $id = "contentobject_id";
            $t = "v";
            $sql[] = "SELECT DISTINCT v.{$id} id FROM ezcontentobject_version v, ezcontentobject vo";

            $userId = eZUser::currentUserID();
            $where[] = "vo.id = v.{$id}";

            $where[] = "vo.status != " . \eZContentObject::STATUS_ARCHIVED;
            $where[] = "v.creator_id = {$userId}";

            $statuses = array(
                eZContentObjectVersion::STATUS_DRAFT,
                eZContentObjectVersion::STATUS_INTERNAL_DRAFT
            );
            $where[] = "v.`status` IN (" . implode(', ', $statuses) . ")";
        }
        elseif ($trash) {
            $id = "id";
            $t = "vo";
            $sql[] = "SELECT {$id} FROM ezcontentobject vo";
            $where[] = "`status` = " . \eZContentObject::STATUS_ARCHIVED;
        }
        else {
            $id = "vo.id";
            $t = "vo";
            $sql[] = "SELECT {$id} FROM ezcontentobject vo";
            $where[] = "`status` = 1";
        }

        if (isset($criteria['name'])) {
            $q = $criteria['name'];
            $queryArr = explode(':', $q);

            $prefix = false;
            // parse prefix with format "user:*keyteq.no"
            if (count($queryArr)>1&&strlen($queryArr[0])>0&&strlen($queryArr[1])>0) {
                $prefix = array_shift($queryArr);
                $q = implode(':', $queryArr);
            }

            switch ($prefix)
            {
                case 'user':
                    // parse * => %, db escape, make like-statements on fields
                    $subQueries = self::parseSearchQuery($q, array('u.email', 'u.login'), false);
                    if ($mine)
                        $where[] = "EXISTS (SELECT * FROM ezuser u WHERE u.contentobject_id = v.contentobject_id AND ({$subQueries}))";
                    else
                        $where[] = "EXISTS (SELECT * FROM ezuser u WHERE u.contentobject_id = vo.id AND ({$subQueries}))";
                    break;

                default:
                    // no or invalid prefix
                    if ($prefix)
                    {
                        $sql[count($sql)-1] = $sql[count($sql)-1].', ezcontentobject_attribute a';
    
                        $subQueries = self::parseSearchQuery($q, array('a.sort_key_string'), false);
                        
                        if ($mine)
                            $coid = 'v.contentobject_id';
                        else
                            $coid = 'vo.id';
                        
                        $where[] = "a.version = vo.current_version AND a.contentclassattribute_id IN (select id from ezcontentclass_attribute where identifier = '".$db->escapeString($prefix)."') AND ".$coid." = a.contentobject_id AND ({$subQueries})";
                    }
                    else
                    {
                        // parse * => %, db escape, make like-statements on fields
                        $subQueries = self::parseSearchQuery($q, array('n.name'));
    
                        if ($mine)
                            $where[] = "EXISTS (SELECT * FROM ezcontentobject_name n WHERE n.contentobject_id = v.contentobject_id AND n.content_version = v.version AND ({$subQueries}))";
                        else
                            $where[] = "EXISTS (SELECT * FROM ezcontentobject_name n WHERE n.contentobject_id = vo.id AND n.content_version = vo.current_version AND ({$subQueries}))";
                    }
                    break;
            }
        }

        if (isset($criteria['type'])) {
            $type = $criteria['type'];
            if (is_numeric($type)) {
                $type = (int) $type;
                $where[] = "vo.contentclass_id = {$type}";
            }
            elseif (is_array($type)) {
                $where[] = "vo.contentclass_id IN (" . implode(",", $type) . ")";
            }
        }

        if (!$mine && isset($criteria['owner_id'])) {
            $owner = (int) $criteria['owner_id'];
            $where[] = "vo.owner_id = {$owner}";
        }

        if (isset($criteria['modified'])) {
            $modified = (int) $criteria['modified'];
            $where[] = "{$t}.modified >= {$modified}";
        }
        
        if ($mine) {
            $where[] = "({$t}.modified > vo.modified)";
        }

        if (count($where)) {
            $sql[] = "WHERE";
            $sql[] = implode($where, " AND ");
        }

        $sql[] = "ORDER BY";
        foreach ($sort as $key => $dir) {
            $sql[] = "$key $dir";
        }


        $sql[] = "LIMIT {$limits['offset']}, {$limits['length']}";
        

        $ids = array();
        $sql = implode($sql, " ");
        $tmp = $db->arrayQuery($sql);
        $objects = array();
        foreach ($tmp as $arr) {
            $obj = eZContentObject::fetch($arr['id']);
            if ($obj)
                $objects[] = $obj;
        }

        if (is_callable($preprocessor)) call_user_func($preprocessor, $objects);
        if (is_callable($formatter)) $objects = array_map($formatter, $objects);

        return $objects;
    }

    protected static function parseSearchQuery($q, $searchFields, $defaultSearchMidWord = true)
    {
        $prefix = false;
        $db = eZDB::instance();

        $q = $db->escapeString($q);

        $subQueries = array();

        if (substr_count($q,'*')>0) {
            $q = str_replace('*', '%', $q);

            foreach ($searchFields as $searchField) {
                $subQueries[] = "{$searchField} LIKE '{$q}'";
            }
        }
        else {
            foreach ($searchFields as $searchField) {
                if ($defaultSearchMidWord)
                    $subQueries[] = "{$searchField} LIKE '{$q}%' or {$searchField} LIKE '% {$q}%'";
                else
                    $subQueries[] = "{$searchField} LIKE '{$q}%'";
            }
        }

        $query = implode(' or ', $subQueries);
        return $query;
    }

    /**
     * Get number of content objects of a given type
     *
     * @param string|int|bool|array $criteria A content class string or its numeric type (preferred)
     *                                        or take complete control by sending the raw array
     * @return int Number of content objects
     */
    public static function getCount($criteria = false)
    {
        $objectDef = \eZContentObject::definition();
        $versionDef = \eZContentObjectVersion::definition();

        $objectDef['fields'] = static::addTablenameToFields($objectDef['fields'], $objectDef['name']);
        $criteria = static::filterCriteria($criteria);
        $criteria = static::addTablenameToFields($criteria, $objectDef['name']);

        $customTables = array($versionDef['name']);

        $customConds = ' AND ' . $objectDef['name'] . '.id=' . $versionDef['name'] . '.contentobject_id' .
                ' AND ' . $versionDef['name'] . '.status!=' . \eZContentObjectVersion::STATUS_INTERNAL_DRAFT;

        $rows = eZPersistentObject::fetchObjectList($objectDef,
            array(),
            $criteria,
            false /* we don't want any sorting when counting. Sorting leads to error on postgresql 8.x */,
            null,
            false, false,
            array(array('operation' => 'count( DISTINCT '.$objectDef['name'].'.id )',
                'name' => 'count')),
            $customTables,
            $customConds
        );
        return $rows[0]['count'];
    }

    /**
     * Get owners of content objects (filtered by contentclass)
     *
     * @param bool|int|string $type Contentclass to filter on
     *
     * @return array
     */
    public static function owners(array $options = array())
    {
        $ini = \eZINI::instance('ezexceed.ini');

        $options += array(
            'limit' => false,
            'offset' => 0,
            'roleId' => $ini->variable('FindFacets', 'AuthorRoles')
        );

        $limit = $options['limit'];
        $offset = (int) $options['offset'];

        $query = "SELECT DISTINCT author.id id, author.name name
            FROM ezuser
            INNER JOIN ezcontentobject author ON (ezuser.contentobject_id = author.id AND ezuser.login != 'anonymous')\n";

        if ($options['roleId']) {
            $roleIds = is_array($options['roleId']) ? $options['roleId'] : array($options['roleId']);
            $ids = join($roleIds, ',');
            $query .= "LEFT JOIN ( ezuser_role role)
                ON (author.id = role.contentobject_id)
                WHERE author.id = role.contentobject_id AND
                    role.role_id = ($ids)";
        }

        if ($limit)
            $query .=  "\nLIMIT {$offset}, {$limit}";

        $db = eZDB::instance();
        return $db->arrayQuery($query);
    }

    /**
     * Get daterange objects exists in
     *
     * @param bool|int|string $type Contentclass to filter on
     * @param string $field
     *
     * @return array
     */
    public static function timespan($type = false, $field = 'published')
    {
        $db = eZDB::instance();
        $field = $db->escapeString($field);
        $query =
            "SELECT
                MAX({$field}) AS end, MIN({$field}) AS start
            FROM
                ezcontentobject orig";

        $where = array('modified > 0');
        if ($type)
        {
            if (is_string($type) && !is_numeric($type))
                $type = eZContentClass::classIDByIdentifier($type);
            if (is_numeric($type))
                $where[] = "orig.contentclass_id = " . (int) $type;
        }
        if ($where) $query .= " WHERE\n" . join("\nAND ", $where);

        $result = $db->arrayQuery($query);
        if ($result)
        {
            $result = current($result);
            array_walk($result, function(&$item, $key) {
                $item = (int) $item;
            });
        }
        return $result;
    }

    /**
     * Filter any passed criteria making sure its the sort of array
     * needed for eZContentObject::<fetchers>
     *
     * @param mixed $criteria Input to filter
     * @return array
     */
    protected static function filterCriteria($criteria)
    {
        if (!is_array($criteria)) {
            $input = $criteria;
            $criteria = array();
            if (is_string($input))
                $criteria['contentclass_id'] = eZContentClass::classIDByIdentifier($input);
            elseif (is_numeric($input))
                $criteria['contentclass_id'] = $input;
        }
        else {
            foreach ($criteria as $key => &$val)
            {
                switch ($key)
                {
                    case 'contentclass_id':
                        if (is_string($val))
                            $val = eZContentClass::classIDByIdentifier($val);
                        break;
                }
            }
        }
        return $criteria;
    }

    /**
     * Adds tablename to fields.
     *
     * @param array $fields
     * @param string $tableName
     * @param array $rules Specific `fieldname` => `talbeprefix` to apply
     *
     * @return array
     */
    public static function addTablenameToFields($fields, $defaultTableName, array $rules = array())
    {
        $response = array();
        if (is_array($fields)) {
            foreach ($fields as $fieldName => $values) {
                if (strpos($fieldName, '.') === false) {
                    $tableName = isset($rules[$fieldName]) ? $rules[$fieldName] : $defaultTableName;
                    if (strlen($tableName) > 0)
                        $fieldName = $tableName . '.' . $fieldName;
                }
                $response[$fieldName] = $values;
            }
        }
        return $response;
    }

    /**
     * Get a cache key for a draft version
     *
     * @param array $objectIds
     * @param array int $userId
     * @param array int $languageId
     * @return string
     */
    protected static function getDraftCacheKey(array $objectIds, $userId, $languageId, $extra = false)
    {
        $keys = array_filter(array_merge($objectIds, array($userId, $languageId, $extra)));
        return implode('::', $keys);
    }

    /**
     * Clear object draft cache for object
     *
     * @param array $objectIds
     * @param array int $userId
     * @param array int $languageId
     * @return string
     */
    public static function clearDraftCache($id, $userId, $languageId)
    {
        $key = self::getDraftCacheKey(array($id), $userId, $languageId);
        unset(self::$_caches['userDrafts'][$key]);
    }

    public static function getUsersDraftsInfo($userId)
    {
        static $cache;
        if (!isset($cache))
        {
            $cache = array();
        }
        if (isset($cache[$userId]))
        {
            return $cache[$userId];
        }

        $cache[$userId] = array();

        $drafts = \eZPersistentObject::fetchObjectList( \eZContentObjectVersion::definition(),
            array('contentobject_id', 'modified'), array( 'creator_id' => $userId,
                         'status' => \eZContentObjectVersion::STATUS_DRAFT
                         ),
            null, null,
            false
        );

        foreach ($drafts as $draft)
        {
            if (!isset($cache[$userId][$draft['contentobject_id']])||$draft['modified']>$cache[$userId][$draft['contentobject_id']]['modified'])
                $cache[$userId][$draft['contentobject_id']] = array('modified' => $draft['modified']);
        }

        return $cache[$userId];
    }

    /**
     * Returns the latest draft.
     *
     * @param $objectId
     * @param $userId
     * @param $languageId
     * @param bool $includeInternalDrafts
     * @param int $modified
     * @param bool $anyStatus
     *
     * @return \eZContentObjectVersion
     */
    public static function fetchLatestUserDraft($objectId, $userId = null, $languageId = null, $includeInternalDrafts = true, $modified = 0, $anyStatus = false)
    {
        $multiple = is_array($objectId);
        if (!$multiple) $objectId = array($objectId);

        $key = self::getDraftCacheKey($objectId, $userId, $languageId, $includeInternalDrafts);

        $cache = self::$_caches['userDrafts'];

        if (isset($cache[$key]))
            return $cache[$key];

        if ($includeInternalDrafts)
            $status = array(array(\eZContentObjectVersion::STATUS_DRAFT, \eZContentObjectVersion::STATUS_INTERNAL_DRAFT));
        else
            $status = \eZContentObjectVersion::STATUS_DRAFT;

        $criteria = array('contentobject_id' => array($objectId));

        if (!$anyStatus)
            $criteria['status'] = $status;

        if ($modified > 0)
            $criteria['modified'] = array('>', $modified);

        if ($languageId !== null)
            $criteria['initial_language_id'] = $languageId;

        if ($userId !== null)
            $criteria['creator_id'] = $userId;

        $versions = \eZPersistentObject::fetchObjectList(
            \eZContentObjectVersion::definition(),
            null,
            $criteria,
            array('modified' => 'desc'),
            (!$multiple ? array('length' => 1) : null) ,
            true
        );
        if ($multiple) {
            $cache[$key] = $versions;
        }
        elseif (count($versions) === 1) {
            $cache[$key] = $versions[0];
        }

        // Individual caches
        foreach ($versions as $version) {
            $languagedKey = self::getDraftCacheKey(
                array($version->attribute('contentobject_id')),
                $userId,
                $version->attribute('initial_language_id'),
                $includeInternalDrafts
            );
            $cache += array(
                $languagedKey => $version,
            );
        }

        return isset($cache[$key]) ? $cache[$key] : false;
    }

    /**
     * Uses the built in method of eZFlow to publish the ezPage-attribute.
     *
     * @param \eZContentObjectAttribute $contentAttribute
     * @param \eZContentObject $object
     */
    public static function publishPageAttribute($contentAttribute, $object)
    {
        $ezPageType = new \eZPageType();

        $ezPageType->onPublish($contentAttribute, $object, $object->assignedNodes());
    }

    /**
     * Update states on object. Takes array of stateId's to assign
     *
     * @param \eZContentObject $object
     * @param array $states
     *
     */
    public static function updateStates(eZContentObject $object, array $states)
    {
        $canAssignStateIDList = $object->attribute('allowed_assign_state_id_list');
        foreach ($states as $stateId)
        {
            if (in_array($stateId, $canAssignStateIDList))
            {
                $state = \eZContentObjectState::fetchById($stateId);
                $object->assignState($state);
            }
        }
        $newStates = $object->stateIDArray(true);
        //call appropriate method from search engine
        \eZSearch::updateObjectState($object->ID, $newStates);

        \eZContentCacheManager::clearContentCacheIfNeeded($object->ID);
    }

    /**
     *
     * Publishes an object.
     *
     * @param \eZContentObject $object
     * @param $versions
     *
     * @return array
     *
     */
    public static function publish(eZContentObject $object, $versions)
    {
        if (!is_array($versions) || count($versions) === 0)
            return array();

        $versions = (array) $versions;
        $response = array('ok' => true, 'versions' => array());
        $errors = array();
        $user = \eZUser::currentUser();

        foreach ($versions as $versionId) {
            /** @var $version \eZContentObjectVersion. Check if $user can edit the current version. */
            $version = $object->version($versionId);
            if ($version) {
                $isOwner = $version->attribute('creator_id') === $user->id();
                $language = $version->initialLanguageCode();

                if ($isOwner && $object->canEdit(false, false, false, $language)) {
                    if ($version->attribute('status') === \eZContentObjectVersion::STATUS_PUBLISHED) {
                        continue;
                    }

                    $behaviour = new \ezpContentPublishingBehaviour();
                    $behaviour->isTemporary = true;
                    $behaviour->disableAsynchronousPublishing = false;
                    \ezpContentPublishingBehaviour::setBehaviour($behaviour);

                    $object_id = $object->attribute('id');
                    $version = (int) $versionId;
                    $res = \eZOperationHandler::execute('content', 'publish', compact('object_id', 'version'));

                    if (!$res || !(array_key_exists('status', $res) || $res['status'] !== \eZModuleOperationInfo::STATUS_CONTINUE))
                        $errors[$versionId] = 'Publish failed';
                    else
                        $response['versions'][] = compact('version', 'language');
                }
                else
                    $errors[$versionId] = 'Access denied';
            }
        }
        if (count($errors)) {
            $response['ok'] = false;
            $response['errors'] = $errors;
        }

        return $response;
    }

    /**
     *
     * Updates the locations of an object.
     *
     * @static
     *
     * @param \eZContentObject $object
     * @param array $locations Containing all the current assigned locations.
     *
     * @return bool
     *
     */
    protected static function updateLocations(eZContentObject $object, $locations)
    {
        $user = \eZUser::currentUser();

        /** Checks user-permissions. */
        if (!$object->checkAccess('edit') || !$user->attribute('has_manage_locations'))
            return false;

        if (!empty($locations['mainNode'])) {
            $newMainNodeId = $locations['mainNode'];
            $newMainNode = \eZContentObjectTreeNode::fetch($newMainNodeId);
            if (!$newMainNode)
                return array('ok' => false);
            if (!$newMainNode->checkAccess('edit'))
                return array('ok' => false);

            $ok = NodeOperations::assignNewMainNode($object, $newMainNode);

            return compact('ok');
        }
        $nodeId = $locations[0]['location'];
        $action = $locations[0]['action'];

        $response = array();
        $response['ok'] = false;

        if ($action === 'add')
        {
            if (count($object->attribute('assigned_nodes')) == 0)
                $mainNode = true;
            else
                $mainNode = false;

            $newNode = NodeOperations::addNode($object, $nodeId, $mainNode, false);
            $treeNode = new NodeTree();
            $treeNode->init($newNode->attribute('node_id'), $newNode);

            $response['ok'] = true;
            $response = array_merge($response, array('node' => $treeNode->buildMinimalTree()));
        }
        elseif ($action === 'remove') {
            $response['ok'] = NodeOperations::removeNodeFromObject($nodeId);
            $response['removed'] = $nodeId;
        }

        return $response;
    }

    /**
     * Get content objects related to content object through attribute
     * relations (ezobjectrelation / ezobjectrelationlist)
     *
     * @param \eZContentObject $object
     *
     * @return array
     */
    protected static function getAttributeRelatedObjects($object)
    {
        $attributes = $object->attribute('data_map');
        $relationAttributeTypes = array('ezobjectrelation', 'ezobjectrelationlist');
        $attributeRelatedObjects = array();
        foreach ($attributes as $attr) {
            if (in_array($attr->attribute('data_type_string'), $relationAttributeTypes)) {
                $relations =  $attr->attribute('content');
                if (is_array($relations) && isset($relations['relation_list'])) {
                    foreach ($relations['relation_list'] as $rel) {
                        $attributeRelatedObjects[] = \eZContentObject::fetch($rel['contentobject_id']);
                    }
                }
                elseif ($relations instanceof \eZContentObject) {
                    $attributeRelatedObjects[] = $relations;
                }
            }
        }
        return $attributeRelatedObjects;
    }

    /**
     * Get correct object name for contentobject
     *
     * @param \eZContentObject $object
     * @param \eZContentObjectVersion|bool $version
     *
     * @return string
     */
    public static function name(\eZContentObject $object, $version = false)
    {
        $key = $object->attribute('id') . '::' . ($version ? $version->attribute('version') : 0);
        if (isset(self::$_caches['names'][$key]))
            return self::$_caches['names'][$key];

        $contentINI = \eZINI::instance('site.ini');
        $language = \eZContentLanguage::fetchByLocale($contentINI->variable('RegionalSettings', 'Locale'));

        if (!$language)
            $language = $object->currentLanguageObject();

        if (is_object($language))
            $langId = $language->attribute('id');
        else
            $langId = $object->initialLanguageCode();

        $latestDraft = self::fetchLatestUserDraft(
            $object->attribute('id'),
            \eZUser::currentUserID(), $langId, false,
            $object->attribute('modified')
        );

        if ($latestDraft instanceof \eZContentObjectVersion)
            $name = $latestDraft->attribute('version_name');
        else
            $name = $object->attribute('name');

        if (!$name && $version) {
            $versionName = $version->versionName();
            if ($versionName !== '')
                $name = $versionName;
        }
        // Cache for faster lookups later
        self::$_caches['names'][$key] = $name;
        return $name;
    }

    /**
     * Checks wether an object is currently being published or scheduled for publishing.
     *
     * @param $objectId
     *
     * @return bool
     */
    public static function publishInProgress($objectId)
    {
        if (\eZINI::instance('content.ini')->variable( 'PublishingSettings', 'AsynchronousPublishing' ) == 'enabled') {
            $statusArray = array(
                \ezpContentPublishingProcess::STATUS_WORKING,
                \ezpContentPublishingProcess::STATUS_PENDING,
                \ezpContentPublishingProcess::STATUS_DEFERRED
            );

            $rows = self::fetchVersionProgress($objectId, $statusArray);

            return isset($rows[0]);
        }
        else {
            return false;
        }
    }

    /**
     * Check if current user's view of object is changed since last publish.
     *
     * @param \eZContentObject $contentObject
     * @param array $languages
     *
     * @return array
     */
    public static function changedSincePublished(\eZContentObject $contentObject, array $languages = array())
    {
        $cachekey = md5($contentObject->ID.'|'.serialize($languages));

        if (isset(self::$_caches['publish'][$cachekey])) {
            return self::$_caches['publish'][$cachekey];
        }

        $response = array(
            'versions' => array(),
            'status' => self::PUBLISHEDSTATUS_PUBLISHED
        );

        if (self::publishInProgress($contentObject->ID)) {
            $response['status'] = self::PUBLISHEDSTATUS_PUBLISHING;

            return $response;
        }

        // Insidious hack to flush cached content languages
        // as these tend to get fubared on cluster file system
        if (self::$_caches['contentLanguages'] === false) {
            \eZContentLanguage::fetchList(true);
            self::$_caches['contentLanguages'] = true;
        }

        if ($contentObject->attribute('status') == \eZContentObject::STATUS_DRAFT)
            $response['status'] = self::PUBLISHEDSTATUS_NEW;

        /** Check if current user's draft is newer than published version. */
        $contentINI = \eZINI::instance('content.ini');
        $displayMode = $contentINI->variable('VersionManagement', 'DisplayMode');

        $objectId = $contentObject->attribute('id');
        $modified = $contentObject->attribute('modified');
        $currentVersion = $contentObject->attribute('current_version');

        if ($displayMode == 'latest') {
            $languages = $languages ?: $contentObject->translationStringList();

            $draft = false;
            foreach ($languages as $language) {
                $languageObject = false;

                if (is_numeric($language))
                    $languageObject = \eZContentLanguage::fetch($language);
                elseif ($language)
                    $languageObject = \eZContentLanguage::fetchByLocale($language);

                if ($languageObject) {
                    $tmpDraft = Object::fetchLatestUserDraft($objectId, \eZUser::currentUserID(), $languageObject->attribute('id'), false, $modified);
                    if ($tmpDraft) {
                        $locale = $tmpDraft->attribute('initial_language')->attribute('locale');
                        $response['versions'][$locale] = $tmpDraft->attribute('version');
                        if (!$draft) {
                            $draft = $tmpDraft;
                        }
                    }
                }
            }
        }

        if (isset($draft) && isset($languageObject) && is_object($draft)) {
            if ($draft->attribute('status') == \eZContentObjectVersion::STATUS_DRAFT && $draft->attribute('version') > $currentVersion)
                $response['status'] = self::PUBLISHEDSTATUS_CHANGED;
        }

        self::$_caches['publish'][$cachekey] = $response;

        return $response;
    }

    /**
     * Set up formatting rules and return a formatter to build these
     *
     * @param bool $smallLocationSummary
     *
     * @param array $excludeFilter
     * @return Formatter
     */
    public static function formatter($smallLocationSummary = false, $excludeFilter = array())
    {
        if ($smallLocationSummary) {
            $locations = 'locationsummary';
        }
        else {
            $locations = 'locations';
        }

        $fields = array(
            'id', 'initialLanguageId', 'name', 'class',
            'published', 'modified', 'status', 'ownerId', 'version', $locations,
            'canEdit' => 'can_edit',
            'canCreate' => 'can_create',
        );

        /** A little warning, closures cannot be excluded this way. */
        $fields = array_diff($fields, $excludeFilter);

        $fields['images'] = function ($object) {
            return Object::images($object);
        };

        if (\eZINI::instance('content.ini')->variable( 'PublishingSettings', 'AsynchronousPublishing' ) == 'enabled') {
            $fields['publishing'] = function($object)
            {
                return Object::publishInProgress($object->ID);
            };
        }

        return new Formatter($fields, array('self', 'finalizer'));
    }
    
    protected static function ensureCacheDirStructure()
    {
        $cacheDir = \eZSys::cacheDirectory();
        $dirs = array('public','ezexceed','findthumbs');
        foreach ($dirs as $dir)
        {
            $cacheDir .= '/'.$dir;
            if (!file_exists($cacheDir))
            {
                mkdir($cacheDir);
            }
        }
        
        return $cacheDir;
    }

    protected static function thumbSize($width, $height, $idealWidth, $idealHeight)
    {
        if ($width == 0 || $height == 0) return false;

        $idealRatio = $idealWidth/$idealHeight;
        $ratio = $width / $height;

        if ($idealRatio<$ratio)
        {
            $height = $idealHeight;
            $cropY = 0;
            $width = ceil($idealHeight * $ratio);
            $cropX = floor(($width-$idealWidth)/2);
        }
        else
        {
            $width = $idealWidth;
            $cropX = 0;
            $height = ceil($idealWidth / $ratio);
            $cropY = floor(($height-$idealHeight)/2);
        }
        
        return array('w' => $width, 'h' => $height, 'cx' => $cropX, 'cy' => $cropY);
    }
    
    protected static function handleKeyMediaThumb($attr, $idealWidth, $idealHeight)
    {
        $handler = $attr->attribute('content');

        if ($handler->hasMedia()) {
            $media = $handler->attribute('media');
            if ($media)
            {
                $size = $media->size();
                if (is_array($size)&&isset($size[0])&&isset($size[1])&&$size[0]>0&&$size[1]>0)
                {
                    $thumbSize = self::thumbSize($size[0], $size[1], $idealWidth, $idealHeight);

                    if ($thumbSize) {
                        $media = $handler->media(array($thumbSize['w'], $thumbSize['h']));
                        if ($media) {
                            return $media['url'];
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Fetch images from attributes on object
     *
     * @param object $object
     * @param array $options Specify `width` and `height` ideals
     * @return array
     */
    public static function images($object, array $options = array())
    {
        static $cacheDir;
        static $rootDir;

        if (!isset($cacheDir)) {
            $cacheDir = self::ensureCacheDirStructure();
            $rootDir = \eZSys::rootDir();
        }

        $options += array(
            'width' => 220,
            'height' => 180,
            'name' => 'thumb'
        );

        $idealWidth = $options['width'];
        $idealHeight = $options['height'];

        $userId = \eZUser::currentUserID();

        $cacheKey = join('_', array($object->attribute('id'), $userId, $idealWidth, $idealHeight, $options['name']));
        $cache = self::$_caches['images'];
        if (isset($cache[$cacheKey])) {
            return $cache[$cacheKey];
        }

        $objectModified = $object->Modified;
        $objectVersion = $object->CurrentVersion;

        $result = array();
        $id = $object->attribute('id');

        /** Object might not be valid. */
        if (!$id) {
            return array('thumb' => '');
        }

        $draftsList = self::getUsersDraftsInfo(\eZUser::currentUserID());

        if (isset($draftsList[$id])) {
            $draft = self::fetchLatestUserDraft($id, \eZUser::currentUserID(), $object->attribute('initial_language_id'), false, $object->attribute('modified'));
            if ($draft) {
                $objectVersion = $draft->Version;
                $objectModified = $draft->Modified;
            }
        }

        $hash = substr(md5($object->ID), 0, 2);

        $filepath = implode('/', array_filter(array($rootDir, $cacheDir, $hash)));
        if (!file_exists($filepath)) {
            mkdir($filepath, 0777, true);
        }

        $filename = implode('_', array($id, $objectVersion, $objectModified, $idealWidth, $idealHeight));
        if (file_exists($filepath . '/' . $filename.'.empty')) {
            $result += array($options['name'] => '');
            $cache[$cacheKey] = $result;
            return $result;
        }

        foreach (array('jpg','png','gif') as $ext) {
            if (file_exists("{$filepath}/{$filename}.{$ext}")) {
                $result += array($options['name'] => "/{$cacheDir}/{$hash}/{$filename}.{$ext}");
                $cache[$cacheKey] = $result;
                return $result;
            }
        }

        $chosen = false;
        $chosenThumbSize = false;
        $bestAreaMatch = 0;

        $db = \eZDB::instance();
        $langid = $object->attribute('initial_language_id');
        $query = "SELECT * FROM ezcontentobject_attribute WHERE contentobject_id = {$id} and version = {$object->CurrentVersion} and (language_id&{$langid})>0 and data_type_string IN ('keymedia','ezimage')";
        $attributesArray = $db->arrayQuery($query);
        $attributes = array();

        foreach ($attributesArray as $attribute) {
            $attr = new \eZContentObjectAttribute($attribute);

            $dataType = $attr->attribute('data_type_string');
            if ($dataType === 'keymedia') {
                if (!$chosen) $chosen = self::handleKeyMediaThumb($attr, $idealWidth, $idealHeight);
            }
            else {
                $attributes[] = $attr;
            }
        }

        $firstOriginal = false;

        if (!$chosen) {
            // All attributes left now are of type ezimage
            foreach ($attributes as $attr) {
                $versions = $attr->attribute('content')->aliasList();

                if (!$firstOriginal) {
                    $firstOriginal = $versions['original'];
                }

                foreach ($versions as $v) {
                    if ($v['width'] >= $idealWidth && $v['height'] >= $idealHeight)
                    {
                        $thumbSize = self::thumbSize($v['width'], $v['height'], $idealWidth, $idealHeight);
                        if (!$thumbSize) {
                            continue;
                        }

                        $extraArea = ($thumbSize['w']*$thumbSize['h']) - ($idealWidth*$idealHeight);

                        if ($bestAreaMatch == 0 || $extraArea < $bestAreaMatch) {
                            $bestAreaMatch = $extraArea;
                            $chosenThumbSize = $thumbSize;
                            $chosen = $v['url'];
                        }
                    }
                }
            }

            if (!$chosen && $firstOriginal) {
                $chosen = $firstOriginal['url'];
                $chosenThumbSize = self::thumbSize($firstOriginal['width'], $firstOriginal['height'], $idealWidth, $idealHeight);
            }

            if ($chosen && $chosenThumbSize && ($aliasFile = \eZClusterFileHandler::instance($chosen))) {
                $contents = $aliasFile->fetchContents();
                if ($contents) {
                    $namearr = explode('.', $chosen);
                    $fileExtension = strtolower($namearr[count($namearr)-1]);

                    $tempname = tempnam($rootDir.'/'.$cacheDir, 'tmp');

                    file_put_contents($tempname, $contents);

                    $destname = "{$filepath}/{$filename}.{$fileExtension}";
                    clearstatcache(dirname($destname));

                    $imagine = static::imagine();
                    try {
                        $imagine
                            ->open($tempname)
                            ->resize(new Box($chosenThumbSize['w'], $chosenThumbSize['h']))
                            ->crop(
                                new Point($chosenThumbSize['cx'], $chosenThumbSize['cy']),
                                new Box($idealWidth, $idealHeight)
                            )
                            ->save($destname, array('quality' => 85));
                        unlink($tempname);
                        $chosen = $cacheDir.'/'.$hash.'/'.$filename.'.'.$fileExtension;
                    }
                    catch (\Exception $e) {
                        $chosen = false;
                    }
                }
            }
        }

        if (!$chosen) {
            touch("{$filepath}/{$filename}.empty");
        }
        elseif ($chosen[0] !== '/') {
            $chosen = '/' . $chosen;
        }

        $result += array($options['name'] => $chosen);
        $cache[$cacheKey] = $result;
        return $result;
    }

    /**
     * Creates an object.
     *
     * @param $nodeId
     * @param $typeId
     * @param $hideNode
     * @param $name
     *
     * @return mixed
     */
    public static function create($nodeId, $typeId, $hideNode, $name)
    {
        $parent = \eZContentObjectTreeNode::fetch($nodeId);
        $object = \eZContentObject::createWithNodeAssignment($parent, $typeId, $parent->currentLanguage());
        $object->setAttribute('modified', time());

        $db = \eZDB::instance();
        $db->begin();

        $object->store();

        /** @var $version \eZContentObjectVersion */
        $version = $object->currentVersion();
        $attributes = $version->contentObjectAttributes();

        /** @var $attr \eZContentObjectAttribute */
        foreach ($attributes as $attr) {
            /** EZP-23218 made it impossible to save users. This fixes it. */
            if ($attr->DataTypeString == 'ezuser') {
                continue;
            }
            $attr->store();
        }

        /** If the attribute used to generate urls is found, set the necessary data. */
        if ($nameAttribute = Objects::getNameAttribute($object)) {
            $nameAttribute->fromString($name);
            $nameAttribute->store();
        }

        $version->setAttribute('status', $version::STATUS_INTERNAL_DRAFT);
        $version->store();

        NodeOperations::addNode($object, $nodeId, true, $hideNode);

        $db->commit();

        $response = self::formatter()->format($object);

        return $response;
    }

    /**
     * Deletes a set of objects.
     *
     * @param array $ids
     * @param bool $permanently If the deleted object should be completly deleted(not moved to trash)
     *
     * @return bool
     */
    public static function remove(array $ids, $permanently = false)
    {
        $moveToTrash = $permanently ? false : Objects::movetoTrash();
        foreach ($ids as $id) {
            $object = \eZContentObject::fetch($id);

            if (!$object->attribute('can_remove'))
                return false;

            $nodes = $object->assignedNodes();

            if (count($nodes) === 0) {
                NodeOperations::addNode($object, NodeOperations::getContentRootNodeId(), true, true);
                $nodes = $object->assignedNodes();
            }

            /** @var $node \eZContentObjectTreeNode */
            foreach ($nodes as $node)
            {
                $db = eZDB::instance();
                $db->begin();
                $node->removeNodeFromTree($moveToTrash);
                $db->commit();
            }
        }
        return true;
    }

    public static function removeUnused($id)
    {
        if ($object = \eZContentObject::fetch($id)) {
            if ($object->attribute('status') == \eZContentObject::STATUS_DRAFT) {
                /**
                 * Only objects that never have been edited are allowed to be deleted.
                 * If there is no other versions than internal drafts, then it's safe to delete
                 */
                $parameters = array(
                    'conditions' => array(
                        'status' => array('!=', \eZContentObjectVersion::STATUS_INTERNAL_DRAFT)
                ));
                $versions = $object->versions(true, $parameters);

                if (!count($versions)) {
                    return self::remove(array($id), true);
                }
            }
        }
        return false;
    }

    public static function removeDrafts($id, array $versions, $validStatus = false)
    {
        foreach ($versions as $versionId)
        {
            if ($version = \eZContentObjectVersion::fetchVersion($versionId, $id)) {
                if ($version->attribute('can_remove')) {
                    if (($version->attribute('status') == \eZContentObjectVersion::STATUS_PUBLISHED)
                        || ($validStatus && $version->attribute('status') != $validStatus))
                        continue;

                    $db = eZDB::instance();
                    $db->begin();
                    $version->removeThis();
                    $db->commit();
                }
            }
        }
        return true;
    }

    /**
     * Find keywords that starts with the term $q.
     * Case insensitive
     *
     * @param $q
     * @return array
     */
    public static function keywords($q)
    {
        $db = \eZDB::instance();

        $q = $db->escapeString($q);
        $wordArray = $db->arrayQuery("SELECT keyword FROM ezkeyword
                                           WHERE keyword LIKE '" . $q . "%' LIMIT 0, 100");

        $keywords = array();
        foreach ($wordArray as $word)
        {
            $keywords[] = $word['keyword'];
        }
        return $keywords;
    }

    /**
     * Retrieves any on-going publish-jobs for a contentobject.
     *
     * @param string $objectId
     * @param array $statusArray
     *
     * @return array
     */
    protected function fetchVersionProgress($objectId, $statusArray)
    {
        $userId = eZUser::currentUserID();
        $innerQuery = "SELECT id FROM ezcontentobject_version WHERE contentobject_id = $objectId AND creator_id = $userId";

        $statusSql = implode(',', $statusArray);
        $query = "SELECT * FROM ezpublishingqueueprocesses WHERE status IN ($statusSql) AND ezcontentobject_version_id in ($innerQuery)";

        $db = \eZDB::instance();
        $rows = $db->arrayQuery($query);

        return $rows;
    }

    /**
     * Safe-retrive for sites using async-publish. Callers should catch exception.
     * @see https://keyteq.atlassian.net/browse/NOMATPLAB-4
     *
     * @param $objectId
     * @throws \Exception
     * @return null|eZContentObject
     */
    public static function safeFetch($objectId)
    {
        if (\eZINI::instance('content.ini')->variable( 'PublishingSettings', 'AsynchronousPublishing' ) == 'enabled') {

            $rows = self::fetchVersionProgress($objectId, array(\ezpContentPublishingProcess::STATUS_WORKING));

            if (isset($rows[0])) {
                $contentVersionId = $rows[0]['ezcontentobject_version_id'];
                if ($process = \ezpContentPublishingProcess::fetchByContentVersionId($contentVersionId)) {

                    $tries = 1;
                    $numRetries = 100;

                    while ($process->attribute('status') == \ezpContentPublishingProcess::STATUS_WORKING) {
                        usleep(500000);

                        if ($tries >= $numRetries) {
                            throw new \Exception('Wait for publish timed out. Please try again in a little while.');
                        }

                        $tries++;
                        $process = \ezpContentPublishingProcess::fetchByContentVersionId($contentVersionId);
                    }
                }
            }
        }

        return eZContentObject::fetch($objectId);
    }

    /**
     * Load an Imagine instance
     * Detects what native bindings are supported and uses that
     *
     * @throws \Exception
     * @return Imagine\Image\ImagineInterface
     */
    protected static function imagine()
    {
        static $extensions;
        $extensions = $extensions ?: get_loaded_extensions();

        if (in_array('imagick', $extensions))
            return new \Imagine\Imagick\Imagine();
        if (in_array('gd', $extensions))
            return new \Imagine\Gd\Imagine();

        throw new \Exception("Imagick and GD library must be installed");
    }

    public static function statistics()
    {
        $db = \eZDB::instance();

        $query = 'SELECT contentclass_id as id, count(contentclass_id) AS count FROM ezcontentobject WHERE status = 1 GROUP BY contentclass_id';
        $rows = $db->arrayQuery($query);

        return $rows;
    }
}
