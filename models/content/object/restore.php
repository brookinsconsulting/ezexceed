<?php

namespace ezexceed\models\content\object;

use \ezexceed\models\nodestructure\NodeOperations;
use \ezexceed\models\nodestructure\NodeTree;

use \Exception;

use \eZDB;
use \eZINI;
use \eZNodeAssignment;
use \eZContentObject;
use \eZContentObjectVersion;
use \eZUser;
use \eZContentObjectTrashNode;

class Restore
{
    /**
     * Restores a content object
     *
     * @param int $id
     * @return bool
     */
    public static function contentObject($id)
    {
        $object = \eZContentObject::fetch($id);

        if (!is_object($object))
            throw new Exception("Object does not exist");
        if ($object->attribute('status') != \eZContentObject::STATUS_ARCHIVED)
            throw new Exception('Object is not archived, cannot restore.');

        $class = $object->contentClass();
        $version = $object->attribute('current');

        $location = static::getLocation($version, $class);

        if ($location) {
            $selectedNodeIDArray = array($location->attribute('parent_node'));
            $hideNode = false;
        }
        else {
            $selectedNodeIDArray = array(NodeOperations::getAssignableNodeId());
            $hideNode = true;
        }

        $db = eZDB::instance();
        $db->begin();
        $locationAdded = false;
        $isMain = true;

        $newLocationList = array();
        $failedLocationList = array();
        foreach ($selectedNodeIDArray as $selectedNodeID) {
            $parentNode = \eZContentObjectTreeNode::fetch($selectedNodeID);
            $parentNodeObject = $parentNode->attribute('object');

            $canCreate = $parentNode->checkAccess('create', $class->attribute('id'), $parentNodeObject->attribute('contentclass_id')) == 1;

            if ($canCreate) {
                $newLocationList[] = array(
                    'parent_node_id' => $selectedNodeID,
                    'is_main' => $isMain,
                    'hide' => $hideNode
                );

                $isMain = false;

                $locationAdded = true;
            }
            else {
                $failedLocationList[] = array('parent_node_id' => $selectedNodeID);
            }
        }

        // Check if we have failures
        if (count($failedLocationList) > 0) {
            throw new Exception("Object not added to any locations");
        }

        // Remove all existing assignments, only our new ones should be present.
        foreach ($version->attribute('node_assignments') as $assignment) {
            $assignment->purge();
        }

        // Add all new locations
        foreach ($newLocationList as $newLocation) {
            $newNode = NodeOperations::addNode($object, $newLocation['parent_node_id'], $newLocation['is_main'], $newLocation['hide']);
            $treeNode = new NodeTree();
            $treeNode->init($newNode->attribute('node_id'), $newNode);
        }

        $object->setAttribute('status', eZContentObject::STATUS_DRAFT);
        $object->store();

        $version->setAttribute('status', eZContentObjectVersion::STATUS_DRAFT);
        $version->setAttribute('modified', time());
        $version->store();

        $object->restoreObjectAttributes();

        $id = $object->attribute('id');
        $object = eZContentObject::fetch($id);

        eZContentObjectTrashNode::purgeForObject($id);

        $userClassID = eZINI::instance()->variable('UserSettings', 'UserClassID');
        if ($locationAdded && $object->attribute('contentclass_id') == $userClassID) {
            eZUser::purgeUserCacheByUserId($object->attribute('id'));
        }

        eZContentObject::fixReverseRelations($id, 'restore');

        $db->commit();

        return array(
            'ok' => true,
            'objectId' => $id,
            'className' => $class->attribute('identifier')
        );
    }

    protected static function getLocation($version, $class)
    {
        $assignments = $version->attribute('node_assignments');
        $location = false;
        foreach ($assignments as $assignment) {
            $opCode = $assignment->attribute('op_code');
            $opCode &= ~1;
            $opCodes = array(eZNodeAssignment::OP_CODE_CREATE_NOP, eZNodeAssignment::OP_CODE_NOP);
            // We only include assignments which create or nops.
            if (in_array($opCode, $opCodes)) {
                $node = $assignment->attribute('parent_node_obj');
                if (!$node) {
                    continue;
                }

                if ($assignment->attribute('is_main')) {
                    $parentNode = $assignment->attribute('parent_node_obj');
                    $parentNodeObject = $parentNode->attribute('object');
                    $canCreate = $parentNode->checkAccess('create', $class->attribute('id'), $parentNodeObject->attribute('contentclass_id')) == 1;

                    if ($canCreate) {
                        return $assignment;
                    }
                }
                else if (!$location) {
                    $parentNode = $assignment->attribute('parent_node_obj');
                    $parentNodeObject = $parentNode->attribute('object');
                    $canCreate = $parentNode->checkAccess('create', $class->attribute('id'), $parentNodeObject->attribute('contentclass_id')) == 1;

                    if ($canCreate) {
                        $location = $assignment;
                    }
                }
            }
        }
        return $Location;
    }
}
