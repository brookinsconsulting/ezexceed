<?php
/**
 * Transform a PHP object into an array based on field mapping rules
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\content\object;

use \ezexceed\models\content\Object;
use \eZContentObject;
use \ezexceed\models\nodestructure\NodeTree;

/**
 * Transform a PHP object into an array based on field mapping rules
 *
 * @author Raymond Julin <raymond@keyteq.no>
 * @since 28.10.2011
 */
class Formatter
{
    /**
     * Display mode
     * @var string
     */
    protected static $displayMode = 'old';

    /**
     * Cache who published what
     * @var array
     */
    protected static $publisherCache = array();

    /**
     * Cache userId => userName
     * @var array
     */
    protected static $userCache = array();

    /**
     * Cache of used objectIds
     * @var array
     */
    protected static $objectIds = array();

    /**
     * Cache of objects
     * @var array
     */
    protected static $objects = array();

    /**
     * Language cache
     * @var array
     */
    protected static $languageCache = array();

    /**
     * Cache of draft languages
     * @var array
     */
    protected static $objectsByLanguage = array();

    /**
     * Field definition for format mapping
     * @var array
     */
    protected $fields = array();

    /**
     * Performed after mapping rules
     * @var \Closure
     */
    protected $finalizer = false;

    /**
     * Pre-defined lookup rules
     * @var array
     */
    protected $_rules = array(
        'id' => 'id',
        'name' => 'name',
        'modified' => 'modified',
        'published' => 'published',
        'status' => 'status',
        'ownerId' => 'owner_id',
        'version' => 'current_version',
        'class' => 'contentclass_id',
        'contentClassId' => 'contentclass_id',
        'sectionId' => 'section_id',
        'initialLanguageId' => 'initial_language_id'
    );

    private function rules()
    {

        /**
         *
         * Formats locations.
         *
         * @param \eZContentObject $object
         *
         * @return array
         */
        $this->_rules['locations'] = function($object)
        {
            $nodes = array();

            /** @var $node \eZContentObjectTreeNode */
            foreach ($object->assignedNodes() as $node) {
                $nodeFormatter = new NodeTree();
                $nodeFormatter->init($node->attribute('node_id'), $node);

                $nodes[] = $nodeFormatter->buildMinimalTree();
            }

            return $nodes;
        };

        $this->_rules['locationsummary'] = function($object)
        {
            $nodes = array();

            $twoNodes = \eZPersistentObject::fetchObjectList(
                \eZContentObjectTreeNode::definition(),
                null, array('contentobject_id' => $object->ID), null, array('limit' => 2), true
            );

            if ($twoNodes && isset($twoNodes[0])) {
                $node = $twoNodes[0];
                $nodeFormatter = new NodeTree();
                $nodeFormatter->init($node->ParentNodeID, $node->fetchParent());

                $nodes[0] = $nodeFormatter->buildLocationSummaryTree($object);

                if (isset($twoNodes[1])) {
                    $rows = \eZPersistentObject::fetchObjectList(
                        \eZContentObjectTreeNode::definition(), array(),
                        array('contentobject_id' => $object->ID),
                        false/* we don't want any sorting when counting. Sorting leads to error on postgresql 8.x */,
                        null, false, false,
                        array(array(
                            'operation' => 'count( * )',
                            'name' => 'count'
                        ))
                    );

                    $nodes[0]['total'] = (int) $rows[0]['count'];
                }
                else {
                    $nodes[0]['total'] = 1;
                }
            }

            return $nodes;
        };
    }

    /**
     * Construct a formatter and take mapping rules
     *
     * @param array $fields The fields to map
     * @param bool|array $finalizer
     *
     */
    public function __construct(array $fields = array(), $finalizer = false)
    {
        $this->rules();
        if ($finalizer) $this->finalizer = $finalizer;

        foreach ($fields as $key => $field) {
            if (is_string($key))
                $this->fields[$key] = $field;
            elseif (isset($this->_rules[$field]))
                $this->fields[$field] = $this->_rules[$field];
        }
    }

    /**
     * Maps an object into an array structure based on
     * field rules provided in constructor
     *
     * @param object $object The object to peek into. Expects public properties for mapping
     * @return array
     *
     */
    public function format($object)
    {
        $result = array();

        /** @var $lookup \Closure  */
        foreach ($this->fields as $named => $lookup) {
            if (is_callable($lookup)) {
                $value = call_user_func($lookup, $object, $this, $result);
            }
            elseif (is_array($lookup)) {
                $value = $object;
                foreach ($lookup as $l) {
                    if (isset($value->$l))
                        $value = $value->$l;
                }
            }
            elseif (is_string($lookup) && method_exists($this, $lookup)) {
                $value = $this->$lookup($object, $this, $result);
            }
            else {
                $value = $object->attribute($lookup);
                if (is_numeric($value))
                    $value = ((int) $value == $value) ? (int) $value : (float) $value;
            }
            if (!is_string($named)) $named = $lookup;

            $result[$named] = $value;
        }

        $finalizer = $this->finalizer;
        if ($finalizer && is_callable($finalizer)) {
            $result = call_user_func($finalizer, $result, $object);
        }

        return $result;
    }

    /**
     * Find what user owns the latest published version
     *
     * @param $object \eZContentObject
     * @return array
     */
    public static function publishedBy($object)
    {
        $objectId = $object->ID;
        if (!isset(self::$publisherCache[$objectId])) {
            $version = $object->currentVersion(true);
            $userId = $version->CreatorID;
            self::$publisherCache[$objectId] = array(
                'id' => (int) $userId,
                'name' => self::getUserName($userId)
            );
        }
        return self::$publisherCache[$objectId];
    }

    /**
     * Creates a list of objectIds.
     *
     * @param $objects
     * @return $this
     */
    public static function preProcess($objects)
    {
        $contentINI = \eZINI::instance('content.ini');
        self::$displayMode = $contentINI->variable('VersionManagement', 'DisplayMode');

        $ids = array();
        foreach ($objects as $object) {
            $ids[] = $object->attribute('id');
        }

        if (count($ids) > 0) {
            $drafts = Object::fetchLatestUserDraft($ids, \eZUser::currentUserID(), null, true, 0);
        }
    }

    /**
     * Final formatting.
     *
     * @param $result
     * @return array
     */
    public static function finalizer($result, $object)
    {
        $id = $result['id'];
        $languageId = $result['initialLanguageId'];

        $result['changed'] = false;
        $result['versionsChanged'] = array();

        /**
         * Return which translations that are publishable
         */
        $translations = eZContentObject::translationList();
        $canEdit = false;
        $mainVersion = false;
        /** @var $language \eZLocale */
        foreach ($translations as $locale) {
            $localeCode = $locale->attribute('locale_code');

            if ($object->canEdit(false, false, false, $localeCode)) {
                $canEdit = true;
                $language = \eZContentLanguage::fetchByLocale($localeCode);
                /** @var $version \eZContentObjectVersion */
                $langId = $language->attribute('id');
                $version = self::usersVersion($id, $langId, false);

                if ($version) {
                    $modified = (int) $version->attribute('modified');
                    if ($langId === $languageId) {
                        $mainVersion = $version;
                    }

                    if ($modified > $result['modified']) {
                        if ($modified > $result['changed']) {
                            $result['changed'] = $modified;
                        }

                        $result['versionsChanged'][] = array(
                            'language' => $localeCode,
                            'modified' => (int) $version->attribute('modified'),
                            'version' => (int) $version->attribute('version')
                        );
                    }
                }
            }
        }

        if ($mainVersion && $mainVersion->attribute('modified') > $result['modified']) {
            $result['name'] = $mainVersion->name();
            $result['changedVersion'] = (int) $mainVersion->attribute('version');
        }

        $published = $result['status'] === eZContentObject::STATUS_PUBLISHED ? $result['published'] : false;
        $result['published'] = $published ?: false;
        $result['deleted'] = $result['status'] === eZContentObject::STATUS_ARCHIVED;
        $result['canEdit'] = $canEdit && !$result['deleted'];
        $result['createdBy'] = self::getUserName($object->attribute('owner_id'));
        $result['publishedBy'] = self::publishedBy($object);
        $result['initialLanguage'] = self::language($languageId);

        return $result;
    }

    /**
     * Retrieves the username for a given id.
     *
     * @param int $userId
     * @return string
     */
    public static function getUserName($userId)
    {
        if (!isset(self::$userCache[$userId])) {
            $user = eZContentObject::fetch($userId);
            self::$userCache[$userId] = $user ? $user->name() : '';
        }
        return self::$userCache[$userId];
    }

    /**
     * Get object version belonging to user if newer
     *
     * @param int $id
     * @param int $languageId
     * @return Object
     */
    protected static function usersVersion($id, $languageId, $includeInternalDrafts = true)
    {
        if (!$includeInternalDrafts)
        {
            static $drafts;
            if (!isset($drafts))
                $drafts = Object::getUsersDraftsInfo(\eZUser::currentUserID());

            // don't bother checking for drafts if the user doesn't have any drafts at all for given object 
            if (!isset($drafts[$id]))
                return false;
        }

        return Object::fetchLatestUserDraft($id, \eZUser::currentUserID(), $languageId, $includeInternalDrafts);
    }

    /**
     * Return languageId from language locale
     *
     * @param string $language
     * @return id
     */
    protected static function language($languageId)
    {
        /** @var $language \eZContentLanguage */
        if (!isset(self::$languageCache[$languageId]))
            self::$languageCache[$languageId] = \eZContentLanguage::fetch($languageId)->attribute('locale');

        return self::$languageCache[$languageId];
    }
}
