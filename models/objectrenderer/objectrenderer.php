<?php
/**
 * Renders the template used for object-editing
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\objectrenderer;

use \ezexceed\models\content\Object;

use \eZINI;
use \eZUser;
use \eZHTTPTool;
use \eZTemplate;
use \eZContentObject;
use \eZContentObjectVersion;
use \eZContentLanguage;

/**
 * Description
 *
 * Renders the template used for object-editing.
 *
 * @author Henning Kvinnesland / henning@keyteq.no
 * @since 28.02.2012
 */
class ObjectRenderer
{
    /** @var string */
    protected $editLanguage;
    /** @var int */
    protected $fromVersion;
    /** @var \eZContentObject */
    protected $object;
    /** @var \eZContentObjectVersion */
    protected $currentVersion;
    /** @var array */
    protected $categories;

    const CREATE_NEW_VERSION_THRESHOLD = 3600;
    const TEMPLATE = 'design:modules/editor/main.tpl';

    /**
     * Initializes necessary variables.
     *
     * @param \eZContentObject $object
     * @param bool $language
     */
    public function __construct($object, $language = false)
    {
        $http = eZHTTPTool::instance();
        $this->fromVersion = $http->hasVariable('version') ? $http->getVariable('version') : false;
        $this->object = $object;
        $this->currentVersion = $object->currentVersion(true);
        $this->editLanguage = $language ? $language : $this->object->attribute('current_language');

        $this->contentINI = eZINI::instance('content.ini');
        $this->contentEditorINI = eZINI::instance('contenteditor.ini');
        $this->defaultCategory = strtolower($this->contentINI->variable('ClassAttributeSettings', 'DefaultCategory'));
        $this->categories = $this->contentINI->variable('ClassAttributeSettings', 'CategoryList');

        $this->object->cleanupInternalDrafts();
    }

    /**
     * Renders gui for object-editing.
     *
     * @return string
     */
    public function render()
    {
        $tpl = eZTemplate::factory();

        $versions = $this->versions();
        $_versions = array_values($versions);
        $mainVersion = array_shift($_versions);

        $groupedAttributes =  $this->groupAttributes($versions);

        $tpl->setVariable('object', $this->object);
        $tpl->setVariable('grouped_attributes', $groupedAttributes);
        $tpl->setVariable('main_version', $mainVersion);
        $tpl->setVariable('versions', $versions);
        $tpl->setVariable('current_language', $this->editLanguage);

        $content = $tpl->fetch(self::TEMPLATE);

        return trim($content);
    }

    /**
     * Retrieves versions for editing.
     *
     * @param bool $filterLocale
     *
     * @return \eZContentObjectVersion[]
     */
    public function versions($filterLocale = false)
    {
        $versions = array();
        $translations = eZContentObject::translationList();

        /** @var $language \eZLocale */
        foreach ($translations as $language) {
            $localeCode = $language->attribute('locale_code');
            if ($filterLocale && $this->editLanguage !== $localeCode)
                continue;

            if ($this->object->canEdit(false, false, false, $localeCode)) {
                $version = $this->checkCreateVersion($localeCode);
                $versions[$version->attribute('version')] = $version;
            }
        }

        return $versions;
    }

    /**
     * Groups attributes by category.
     *
     * @param \eZContentObjectVersion[] $versions
     *
     * @return array
     */
    protected function groupAttributes($versions)
    {
        $groupedAttributes = array();

        foreach($this->categories as $category)
            $groupedAttributes[strtolower($category)] = array();

        $mainVersion = array_shift($versions);
        $attributes = $mainVersion->attribute('contentobject_attributes');
        $versionAttributes = array();

        foreach ($versions as $key => $v) {
            $versionAttributes[$key] = $v->attribute('contentobject_attributes');
        }

        /** @var $attribute \eZContentObjectAttribute */
        foreach ($attributes as $key => $attribute) {
            $type = $attribute->attribute('data_type_string');
            if ($type === 'ezpage') continue;
            $width = $this->attributeWidth($type);
            $category = $this->getAttributeCategory($attribute);
            $attrs = array($attribute);
            foreach ($versionAttributes as $v) {
                $attrs[] = $v[$key];
            }

            $groupedAttributes[$category][] = array(
                'width' => $width,
                'by_language' => $attrs
            );
        }

        return $groupedAttributes;
    }

    /**
     * Retrieves category for an attribute.
     *
     * @param \eZContentObjectAttribute $attribute
     *
     * @return string
     */
    protected function getAttributeCategory($attribute)
    {
        $category = $attribute->contentClassAttribute()->attribute('category');
        if (strlen($category) === 0)
            $category = $this->defaultCategory;

        return strtolower($category);
    }

    /**
     * Determines attribute-width.
     *
     * @param $type
     *
     * @return bool|string
     */
    protected function attributeWidth($type)
    {
        $ini = eZINI::instance('contenteditor.ini');
        $width = 'full-width';

        if ($ini->hasVariable('ByType', $type))
            $width =  $ini->variable('ByType', $type);

        if ($width === 'half' || $width === 'halfwidth')
            $width = 'half-width';

        return $width;
    }

    /**
     * Retrieves or creates a new version of an object.
     *
     * @param bool $language
     *
     * @return \eZContentObjectVersion|null
     */
    public function checkCreateVersion($language = false)
    {
        /** A specific version was requested */
        if (is_numeric($this->fromVersion))
            return Object::createInternalDraft($this->object, $this->fromVersion);

        /** Don't create a new version of an internal one. */
        $status = $this->currentVersion->attribute('status');
        $initialLanguageCode = $this->currentVersion->initialLanguageCode();

        $sameLanguage = $initialLanguageCode === $language;
        $isInternalDraft = $status == eZContentObjectVersion::STATUS_INTERNAL_DRAFT;
        $isDraft = $status == eZContentObjectVersion::STATUS_DRAFT;

        if ($sameLanguage && ($isInternalDraft || ($isDraft && $this->isOldDraft($this->currentVersion))))
            return $this->currentVersion;

        /** One or more drafts exist. */
        $language = $language ? $language : $this->editLanguage;
        $languageId = eZContentLanguage::idByLocale($language);

        /** @var $draft \eZContentObjectVersion */
        $draft = Object::fetchLatestUserDraft(
            $this->object->ID,
            eZUser::currentUserID(),
            $languageId,
            true
        );

        if ($draft) {
            $draftVersion = $draft->attribute('version');
            $currentVersion = $this->currentVersion->attribute('version');
            if ($draftVersion > $currentVersion
                || ($this->object->attribute('status') != eZContentObject::STATUS_PUBLISHED && $draftVersion == $currentVersion)) {
                /** The threshold for creating a new draft has been passed. */
                if ($this->isOldDraft($draft))
                    return Object::createInternalDraft($this->object, $draft, $language);

                /** The latest draft is returned. */
                return $draft;
            }
        }

        /** If all else fails a new draft based on the current object-version is returned. */
        return Object::createInternalDraft($this->object, false, $language);
    }

    /**
     * Check if a draft version is older than the threshold required before a new version should be created.
     *
     * @param Object $draft
     * @return bool
     */
    protected function isOldDraft($draft)
    {
        $threshold = time() - self::CREATE_NEW_VERSION_THRESHOLD;
        $modified = $draft->attribute('modified');
        return $modified < $threshold;
    }
}
