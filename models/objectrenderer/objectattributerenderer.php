<?php
/**
 * Renders the template used for object-editing
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models\objectrenderer;

use \ezexceed\models\content\Object;

/**
 * Description
 *
 * Renders the template used for object-editing.
 *
 * @author Raymond Julin (raymond@keyteq.no)
 * @since v1.1
 */

class ObjectAttributeRenderer extends ObjectRenderer
{
    /** @var string */
    protected $attribute;

    /**
     * Initializes necessary variables.
     *
     * @param \eZContentObject $object
     * @param string $attribute Name of attribute identifier
     * @param bool $language
     * @param $fromVersion
     */
    public function __construct($object, $attribute, $language = false, $fromVersion)
    {
        $this->fromVersion = $fromVersion;
        $this->attribute = $attribute;
        $this->object = $object;
        $this->currentVersion = $object->currentVersion();
        $this->editLanguage = $language ?: $this->object->attribute('current_language');

        $this->object->cleanupInternalDrafts();
    }

    /**
     * Renders gui for editing this attribute
     *
     * @return string
     */
    public function render()
    {
        $versions = $this->versions(true);
        $_versions = array_values($versions);
        /** @var $mainVersion \eZContentObjectVersion */
        $mainVersion = array_shift($_versions);
        $attributes = $mainVersion->dataMap();

        $tpl = \eZTemplate::factory();

        $tpl->setVariable('object', $this->object);
        $tpl->setVariable('versions', $versions);
        $tpl->setVariable('main_version', $mainVersion);
        $tpl->setVariable('attribute', $attributes[$this->attribute]);
        $tpl->setVariable('include', array('design:modules/editor/directedit.tpl'));
        $tpl->setVariable('current_language', $this->editLanguage);

        return $tpl->fetch(self::TEMPLATE);
    }
}