<?php
/**
 * Business logic for enabling direct manipulation
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 */

namespace ezexceed\models;

use \ezexceed\models\content\Object;
use \ezexceed\classes\utilities\BlockMethods;

/**
 * Description
 *
 * Model containing objects for the pencil-operator-view.
 *
 * @author Raymond Julin (raymond.julin@gmail.com)
 * @since v1.1
 */
class DirectManipulation
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var \eZTemplate
     */
    protected $tpl;

    /**
     * @var int
     */
    protected $objectId;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var int
     */
    protected $classId;

    /**
     * @var int
     */
    protected $attributeId;

    /**
     * @var int
     */
    protected $version;

    /**
     * @var string
     */
    protected $lang;

    /**
     * @var int
     */
    protected $langId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $canEdit;

    /**
     * Sets the default values.
     *
     * @param \eZTemplate $tpl
     */
    public function __construct($tpl)
    {
        $this->title = '';
        $this->tpl = $tpl;
    }

    /**
     * Populates the model based on the given parameters.
     *
     * @param $operatorValue
     */
    public function fill($entity)
    {
        if (is_array($entity))
            return false;
        $object = $entity->attribute('object');
        $this->objectId = $entity->ContentObjectID;
        $this->name = $entity->ContentClassAttributeIdentifier;
        $this->class = $entity->DataTypeString;
        $this->classId = $entity->ContentClassAttributeID;
        $this->attributeId = $entity->ID;
        $this->version = $entity->Version;
        $this->lang = $entity->LanguageCode;
        $this->langId = $entity->LanguageID;
        $this->canEdit = $object->attribute('can_edit');
    }

    /**
     *
     * Returns the value of the given attribute.
     *
     * @param $attribute
     *
     * @return mixed
     *
     */
    public function attribute($attribute)
    {
        return $this->$attribute;
    }

    /**
     *
     * Checks if the given property exists.
     *
     * @param $attributeName
     *
     * @return bool
     *
     */
    public function hasAttribute($attributeName)
    {
        if (property_exists(__CLASS__, $attributeName)) return true;
        else return false;
    }
}
