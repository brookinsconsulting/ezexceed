<?php
/**
 *
 * @copyright //autogen//
 * @license //autogen//
 * @version //autogen//
 *
 */

use \ezexceed\operators\Avatar;
use ezexceed\operators\TrashEnabled;
use \ezexceed\operators\eZPencil;
use \ezexceed\operators\Objectname;
use \ezexceed\operators\Device;
use \ezexceed\operators\SiteMapRootNode;
use \ezexceed\operators\GlobalClassList;
use \ezexceed\operators\Attributes;
use \ezexceed\operators\DirectManipulation;
use \ezexceed\operators\HTTPHeaders;

$eZTemplateOperatorArray = array(
    array(
        'script' => 'extension/ezexceed/operators/httpheaders.php',
        'class' => 'ezexceed\operators\HTTPHeaders',
        'operator_names' => HTTPHeaders::operatorList()
    ),
    array(
        'script' => 'extension/ezexceed/operators/ezpencil.php',
        'class' => 'ezexceed\operators\eZPencil',
        'operator_names' => eZPencil::operatorList()
    ),
    array(
        'script' => 'extension/ezexceed/operators/device.php',
        'class' => 'ezexceed\operators\Device',
        'operator_names' => Device::operatorList()
    ),
    array(
        'script' => 'extension/ezexceed/operators/objectname.php',
        'class' => 'ezexceed\operators\Objectname',
        'operator_names' => Objectname::operatorList()
    ),
    array(
        'script' => 'extension/ezexceed/operators/sitemaprootnode.php',
        'class' => 'ezexceed\operators\SiteMapRootNode',
        'operator_names' => array(SiteMapRootNode::OPERATOR_NAME)
    ),
    array(
        'script' => 'extension/ezexceed/operators/globalclasslist.php',
        'class' => 'ezexceed\operators\GlobalClassList',
        'operator_names' => array(GlobalClassList::OPERATOR_NAME)
    ),
    array(
        'script' => 'extension/ezexceed/operators/ezattributes.php',
        'class' => 'ezexceed\operators\Attributes',
        'operator_names' => ezexceed\operators\Attributes::operatorList()
    ),
    array(
        'script' => 'extension/ezexceed/operators/direct_manipulation.php',
        'class' => 'ezexceed\operators\DirectManipulation',
        'operator_names' => DirectManipulation::operatorList()
    ),
    array(
        'script' => 'extension/ezexceed/operators/avatar.php',
        'class' => 'ezexceed\operators\Avatar',
        'operator_names' => Avatar::operatorList()
    ),
    array(
        'script' => 'extension/ezexceed/operators/trashenabled.php',
        'class' => 'ezexceed\operators\TrashEnabled',
        'operator_names' => TrashEnabled::operatorList()
    )
);